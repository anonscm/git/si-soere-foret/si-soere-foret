
import java.util.function.Consumer;
import java.util.function.Function;
import java.util.stream.Stream;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.slf4j.Marker;
import org.slf4j.MarkerFactory;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
/**
 *
 * @author ptchernia
 */
public class Test {
    private static final String CONFIGURATION_SEPARATOR="/";

    private static final String PREFIX = "\u001b[";
    private static final String SUFFIX = "m";
    private static final char SEPARATOR = ';';
    private static final String END_COLOR = PREFIX + SUFFIX;

    private static final String ATTR_NORMAL = "0";
    private static final String ATTR_BRIGHT = "1";
    private static final String ATTR_DIM = "2";
    private static final String ATTR_UNDERLINE = "3";
    private static final String ATTR_BLINK = "5";
    private static final String ATTR_REVERSE = "7";
    private static final String ATTR_HIDDEN = "8";

    private static final String FG_BLACK = "30";
    private static final String FG_RED = "31";
    private static final String FG_GREEN = "32";
    private static final String FG_YELLOW = "33";
    private static final String FG_BLUE = "34";
    private static final String FG_MAGENTA = "35";
    private static final String FG_CYAN = "36";
    private static final String FG_WHITE = "37";

    private static final String BG_BLACK = "40";
    private static final String BG_RED = "41";
    private static final String BG_GREEN = "42";
    private static final String BG_YELLOW = "44";
    private static final String BG_BLUE = "44";
    private static final String BG_MAGENTA = "45";
    private static final String BG_CYAN = "46";
    private static final String BG_WHITE = "47";

    private String fatalErrorColor
	= PREFIX + ATTR_DIM + SEPARATOR + FG_RED + SUFFIX;
    private String errorColor
	= PREFIX + ATTR_DIM + SEPARATOR + FG_RED + SUFFIX;
    private String warnColor
	= PREFIX + ATTR_BRIGHT + SEPARATOR + BG_CYAN + SEPARATOR + FG_MAGENTA + SUFFIX;
    private String infoColor
	= PREFIX + ATTR_NORMAL + SEPARATOR + FG_GREEN + SUFFIX;
    private String debugColor
	= PREFIX + ATTR_UNDERLINE + SEPARATOR + BG_BLACK + SEPARATOR + FG_WHITE + SUFFIX;
    private String defaultColor
	= PREFIX + SUFFIX;

    private Logger logger;

    @org.junit.Test
    public void unTest() {
        logger = LoggerFactory.getLogger("monLogger");
        logger.debug("mon message : {}, mais aussi {}", "message 1 ", "message 2");
        String foo = "foo";
        String bar = "bar";
        logger.debug("Value of foo is " + foo + ". Value of bar is " + bar + ".");
        logger.debug("Value of foo is {}. Value of bar is {}.", foo, bar);

        long startTime = System.nanoTime();
        processRequest();
        long endTime = System.nanoTime();

        Marker timeMarker = MarkerFactory.getMarker("time");
        logger.info(timeMarker, "Request processing took {} ms", (endTime - startTime) / 1e6);
        final Marker MARKER_WHITESPACE = MarkerFactory.getMarker("ANA_WHITESPACE");
        String text = "Request processing took {} ms";
        String text_in_red = "\u001b[" // Prefix - see [1]
                + "0" // Brightness
                + ";" // Separator
                + "31" // Red foreground
                + "m" // Suffix
                + text // the text to output
                + "\u001b[m "; // Prefix + Suffix to reset color
        logger.info(MARKER_WHITESPACE, colorizeText("FG_GREEN, BG_RED, ATTR_UNDERLINE", FG_GREEN, BG_RED, ATTR_UNDERLINE), (endTime - startTime) / 1e6);
        logger.info(MARKER_WHITESPACE, colorizeText("FG_CYAN, BG_YELLOW, ATTR_BRIGHT", FG_CYAN, BG_YELLOW, ATTR_BRIGHT), (endTime - startTime) / 1e6);
        logger.info(MARKER_WHITESPACE, colorizeText("ATTR_BRIGHT, FG_CYAN, BG_YELLOW", ATTR_BRIGHT, FG_CYAN, BG_YELLOW), (endTime - startTime) / 1e6);
        logger.info(colorizeText("BG_MAGENTA, BG_CYAN, ATTR_BLINK", BG_MAGENTA, BG_CYAN, ATTR_BLINK), (endTime - startTime) / 1e6);
        logger.info(text, (endTime - startTime) / 1e6);

        logText("info", "info");
        logText("warn", "warn");
        logText("debug", "debug");
        logText("error", "error");
        logText("autre", "autre");

    }

    void processRequest() {
        logger.info("Log info message");
    }
    String colorizeText(String text, String foreground, String background, String attribute){
        
        return PREFIX // Prefix - see [1]
                + foreground// Brightness
                + SEPARATOR // Separator
                + background // Red foreground
                + SEPARATOR // Separator
                + attribute // Red foreground
                + SUFFIX // Suffix
                + text // the text to output
                + PREFIX+SUFFIX; // Prefix + Suffix to reset color
    }
    void logText(String text, String level){
        Stream<String> s = Stream.of(text);
        final Consumer<String> debug;
        final Function<String, String> colorizedText;
        logger = LoggerFactory.getLogger(level);
        switch(level){
            case "debug" :
                debug = logger::debug;
                colorizedText = c->debugColor+c+defaultColor;
                break;
            case "warn" :
                debug = logger::warn;
                colorizedText = c->debugColor+c+defaultColor;
                break;
            case "info" :
                debug = logger::info;
                colorizedText = c->infoColor+c+defaultColor;
                break;
            case "error" :
                debug = logger::error;
                colorizedText = c->errorColor+c+defaultColor;
                break;
            default:
                debug = logger::trace;
                colorizedText = c->defaultColor+c+defaultColor;
                break;
        }
        s
                .map(colorizedText)
                .forEach(debug);
       
    }
}
