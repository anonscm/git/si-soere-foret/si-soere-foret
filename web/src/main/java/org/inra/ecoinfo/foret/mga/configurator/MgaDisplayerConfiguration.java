package org.inra.ecoinfo.foret.mga.configurator;

import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.configurator.AbstractMgaDisplayerConfiguration;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.refdata.Refdata;
import org.inra.ecoinfo.refdata.theme.Theme;

/**
 * 
 * @author ryahiaoui
 */
public class MgaDisplayerConfiguration extends AbstractMgaDisplayerConfiguration {

    /**
     *
     */
    public MgaDisplayerConfiguration() {

        this.addColumnNamesForInstanceType(SiteForet.class, new String[] {
                "PROPERTY_COLUMN_LOCALISATION_NAME", "PROPERTY_COLUMN_LOCALISATION_NAME",
                "PROPERTY_COLUMN_LOCALISATION_NAME", "PROPERTY_COLUMN_LOCALISATION_NAME",
                "PROPERTY_COLUMN_PLATEFORM_NAME", "PROPERTY_COLUMN_PLATEFORM_NAME",
                "PROPERTY_COLUMN_SITE_DEFAULT" });

        this.addColumnNamesForInstanceType(Theme.class, new String[] {
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME",
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME",
                "PROPERTY_COLUMN_THEME_NAME", "PROPERTY_COLUMN_THEME_NAME" });

        this.addColumnNamesForInstanceType(DataType.class, new String[] {
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME",
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME",
                "PROPERTY_COLUMN_DATATYPE_NAME", "PROPERTY_COLUMN_DATATYPE_NAME" });

        this.addColumnNamesForInstanceType(VariableForet.class, new String[] {
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "Variableforet_3", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME" });

        this.addColumnNamesForInstanceType(DatatypeVariableUniteForet.class, new String[] {
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME",
                "PROPERTY_COLUMN_VARIABLE_NAME", "PROPERTY_COLUMN_VARIABLE_NAME" });

        this.addColumnNamesForInstanceType(Refdata.class, new String[] {
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME",
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME",
                "PROPERTY_COLUMN_REFDATA_NAME", "PROPERTY_COLUMN_REFDATA_NAME" });

    }

}
