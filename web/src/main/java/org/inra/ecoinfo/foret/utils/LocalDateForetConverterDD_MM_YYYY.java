package org.inra.ecoinfo.foret.utils;

import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.HashMap;
import java.util.Locale;
import java.util.Map;
import java.util.ResourceBundle;
import javax.faces.application.FacesMessage;
import javax.faces.component.UIComponent;
import javax.faces.context.FacesContext;
import javax.faces.convert.Converter;
import javax.faces.convert.ConverterException;
import javax.faces.convert.FacesConverter;
import org.inra.ecoinfo.utils.DateUtil;

/**
 * Faces converter for support of LocalDate
 *
 * @author Juneau
 */
@FacesConverter(value = "localDateForetConverterDD_MM_YYYY")
public class LocalDateForetConverterDD_MM_YYYY implements Converter {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.utils.dateConverter";
    /**
     * The Constant MSG_END_DATE_INVALID.
     */
    private static final String MSG_END_DATE_INVALID = "PROPERTY_MSG_END_DATE_INVALID";
    /**
     * The Constant MSG_START_DATE_INVALID.
     */
    private static final String MSG_START_DATE_INVALID = "PROPERTY_MSG_START_DATE_INVALID";
    private static final String COMPONENT_DATE_START_ID = "dateStart";
    private static final String COMPONENT_DATE_END_ID = "dateEnd";
    private static final Map<String, ResourceBundle> bundles = new HashMap<String, ResourceBundle>();

    @Override
    public Object getAsObject(FacesContext context, UIComponent component, String value) {
        if (value == null || "__/____".equals(value) || "__/__/____".equals(value)) {
            return null;
        }
        if (value.matches(DateUtil.MM_YYYY.replaceAll("[a-zA-Z]", "."))) {
            value = "01/".concat(value);
        }
        LocalDate date = null;
        try {
            date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, value);
        } catch (DateTimeParseException e) {
            Locale locale = context.getViewRoot().getLocale();
            ResourceBundle bundle = bundles
                    .computeIfAbsent(
                            locale.getLanguage(),
                            k -> ResourceBundle.getBundle(
                                    BUNDLE_SOURCE_PATH,
                                    locale
                            )
                    );
            String message = String.format(
                    bundle.getString(COMPONENT_DATE_START_ID.equals(component.getId()) ? MSG_START_DATE_INVALID : MSG_END_DATE_INVALID),
                    value,
                    DateUtil.DD_MM_YYYY
            );

            throw new ConverterException(new FacesMessage(FacesMessage.SEVERITY_ERROR, message, ""));
        }
        return date;
    }

    @Override
    public String getAsString(FacesContext context, UIComponent component, Object value) {
        String date = "";
        try {
            LocalDate dateValue = (LocalDate) value;
            date = DateUtil.getUTCDateTextFromLocalDateTime(dateValue, DateUtil.DD_MM_YYYY);
        } catch (DateTimeParseException  e) {
            throw new ConverterException(e);
        } catch (ClassCastException e) {
            return value.toString();
        }

        return date;
    }

}
