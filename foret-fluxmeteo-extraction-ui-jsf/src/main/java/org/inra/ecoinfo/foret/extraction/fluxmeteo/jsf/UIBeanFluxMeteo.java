package org.inra.ecoinfo.foret.extraction.fluxmeteo.jsf;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.foret.extraction.IForetDatasetManager;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxMeteoDatasetManager;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.impl.FluxMeteoParameterVO;
import org.inra.ecoinfo.foret.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.foret.extraction.jsf.Collector;
import org.inra.ecoinfo.foret.extraction.jsf.associate.UIAssociate;
import org.inra.ecoinfo.foret.extraction.jsf.date.DatesFormParamVO;
import org.inra.ecoinfo.foret.extraction.jsf.date.DatesRequestParamVO;
import org.inra.ecoinfo.foret.extraction.jsf.date.UIDate;
import org.inra.ecoinfo.foret.extraction.jsf.nodeable.AvailablesSynthesisDepositSiteDAO;
import org.inra.ecoinfo.foret.extraction.jsf.nodeable.UISite;
import org.inra.ecoinfo.foret.extraction.jsf.variable.AvailablesSynthesisVariablesDAO;
import org.inra.ecoinfo.foret.extraction.jsf.variable.UIVariable;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiFluxMeteo2")
@ViewScoped
public class UIBeanFluxMeteo extends AbstractUIBeanForSteps implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanFluxMeteo.class);

    /**
     *
     */
    public static final String MPS = "variableMPS";

    private static final String RULE_NAVIGATION_JSF = Constantes.RULE_NAVIGATION_JSF;

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final String STEP_SITE = "nodeable";

    /**
     *
     */
    protected static final String STEP_DATES = "dates";

    /**
     *
     */
    protected static final String STEP_VARIABLES = "variables";

    private Properties propertiesVariablesNames;
    //private Map<Long, VOVariableJSF> variablesAvailables;

    /**
     *
     */
    @ManagedProperty(value = "#{fluxMeteoDatasetManager}")
    protected IFluxMeteoDatasetManager fluxMeteoDatasetManager;
    @ManagedProperty(value = "#{availablesSynthesisDepositSiteDAO}")
    AvailablesSynthesisDepositSiteDAO availablesSynthesisDepositPlacesDAO;
    @ManagedProperty(value = "#{availablesSynthesisVariablesDAO}")
    AvailablesSynthesisVariablesDAO availablesSynthesisVariablesDAO;
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;
    private DatesFormParamVO datesFormParamVO;

    /**
     *
     */
    public UIBeanFluxMeteo() {
        super();
    }

    @PostConstruct
    void initBean() {
        collector = new Collector(stepBuilders);
        final FluxMeteoParameterVO parameters = new FluxMeteoParameterVO();
        collector.setParameters(parameters);
        availablesSynthesisDepositPlacesDAO.getGenericSynthesisDatatypesClasses().addAll(getSynthesisDatatypeClasses());

        availablesSynthesisVariablesDAO.getGenericSynthesisValuesClasses().putAll(getSynthesisValueClasses());
        final UISite uiSite = new org.inra.ecoinfo.foret.extraction.jsf.nodeable.UISite(availablesSynthesisDepositPlacesDAO);
        stepBuilders.add(uiSite);
        uiSite.init(localizationManager, policyManager, collector);
        datesFormParamVO = new DatesFormParamVO(localizationManager);
        datesFormParamVO.setRythme(Constantes.SEMI_HORAIRE);
        final DatesRequestParamVO datesRequestParamVO = new DatesRequestParamVO(datesFormParamVO);
        stepBuilders.add(new UIDate(datesRequestParamVO));
        stepBuilders.add(new UIVariable(availablesSynthesisVariablesDAO));
        stepBuilders.add(new UIAssociate(fileCompManager));
    }

    /**
     *
     * @param <T>
     * @return
     */
    protected <T> Map<String, List<Class<? extends T>>> getSynthesisValueClasses() {
        Map<String, List<Class<? extends T>>> genericSynthesisValues = new HashMap();
        genericSynthesisValues.computeIfAbsent(Constantes.MENSUEL, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.fluxm.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.MENSUEL, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.meteom.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.fluxj.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.meteoj.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.SEMI_HORAIRE, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.fluxsh.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.SEMI_HORAIRE, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.meteosh.SynthesisValue.class);
        return genericSynthesisValues;
    }

    /**
     *
     * @return
     */
    protected List<Class<? extends GenericSynthesisDatatype>> getSynthesisDatatypeClasses() {
        List<Class<? extends GenericSynthesisDatatype>> genericSynthesisDatatypes = new LinkedList();
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.fluxm.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.meteom.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.fluxj.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.meteoj.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.fluxsh.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.meteosh.SynthesisDatatype.class);
        return genericSynthesisDatatypes;
    }

    /**
     *
     * @return
     */
    public boolean getFormIsValid() {
        return stepBuilders.get(2).isStepValid();

    }

    /**
     * @return @throws BusinessException
     * @throws BadExpectedValueException
     * @throws ParseException***********
     */
    public String extract() throws BusinessException, BadExpectedValueException, ParseException {
        FluxMeteoParameterVO parameters = (FluxMeteoParameterVO) collector.getParameters();
        this.extractionManager.extract(parameters, parameters.getAffichage());

        return null;
    }

    /**
     * @param localizationManager the localizationManager to set
     */
    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager);
    }

    /**
     * @return the propertiesVariablesNames
     */
    public Properties getPropertiesVariablesNames() {
        return this.propertiesVariablesNames;
    }

    /**
     * @param propertiesVariablesNames the propertiesVariablesNames to set
     */
    public void setPropertiesVariablesNames(Properties propertiesVariablesNames) {
        this.propertiesVariablesNames = propertiesVariablesNames;
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return UIBeanFluxMeteo.RULE_NAVIGATION_JSF;
    }

    /**
     *
     * @param fluxMeteoDatasetManager
     */
    public void setFluxMeteoDatasetManager(IFluxMeteoDatasetManager fluxMeteoDatasetManager) {
        this.fluxMeteoDatasetManager = fluxMeteoDatasetManager;
    }

    /**
     *
     * @return
     */
//    @Override
    protected IForetDatasetManager getDatasetManager() {
        return fluxMeteoDatasetManager;
    }

    /**
     *
     * @return
     */
    @Override
    public boolean isFormValid() {
        return true;
    }

    @Override
    public String getTitleForStep() {
        return super.getTitleForStep(); //To change body of generated methods, choose Tools | Templates.
    }

    /**
     *
     * @param availablesSynthesisDepositPlacesDAO
     */
    public void setAvailablesSynthesisDepositPlacesDAO(AvailablesSynthesisDepositSiteDAO availablesSynthesisDepositPlacesDAO) {
        this.availablesSynthesisDepositPlacesDAO = availablesSynthesisDepositPlacesDAO;
    }

    /**
     *
     * @param availablesSynthesisVariablesDAO
     */
    public void setAvailablesSynthesisVariablesDAO(AvailablesSynthesisVariablesDAO availablesSynthesisVariablesDAO) {
        this.availablesSynthesisVariablesDAO = availablesSynthesisVariablesDAO;
    }

    /**
     *
     * @param extractionManager
     */
    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    /**
     *
     * @param fileCompManager
     */
    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    @Override
    public String nextStep() {
        return super.nextStep(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String prevStep() {
        return super.prevStep(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
