/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.List;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.extraction.entity.IProspecteurUnite;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.slf4j.Logger;

/**
 *
 * @author tcherniatinsky
 */
public class FluxMeteoBuildOutputDisplayTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    FluxMeteoBuildOutputDisplay instance;
    MockUtils m = MockUtils.getInstance();

    /**
     *
     */
    public FluxMeteoBuildOutputDisplayTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        instance = new FluxMeteoBuildOutputDisplayImpl();
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of dateComplete method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    public void testDateComplete() {
        String dateToString = "24/04/2016";
        String timeToString = "12:34";
        String expResult = "24/04/2016 12:34";
        String result = FluxMeteoBuildOutputDisplay.dateComplete(dateToString, timeToString);
        assertEquals(expResult, result);
    }

    /**
     * Test of buildOutput method, of class FluxMeteoBuildOutputDisplay.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testBuildOutput() throws Exception {
        IParameter parameters = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        RObuildZipOutputStream expResult = null;
        RObuildZipOutputStream result = instance.buildOutput(parameters);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLocalizationManager method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testGetLocalizationManager() {
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        ILocalizationManager expResult = null;
        ILocalizationManager result = instance.getLocalizationManager();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setLocalizationManager method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testSetLocalizationManager() {
        ILocalizationManager localizationManager = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.setLocalizationManager(localizationManager);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of removeFile method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testRemoveFile() {
        String fileName = "";
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.removeFile(fileName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setConfiguration method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testSetConfiguration() {
        ICoreConfiguration configuration = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.setConfiguration(configuration);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setProspecteurUnite method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testSetProspecteurUnite() {
        IProspecteurUnite prospecteurUnite = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.setProspecteurUnite(prospecteurUnite);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setFileCompConfiguration method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testSetFileCompConfiguration() {
        IFileCompConfiguration fileCompConfiguration = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.setFileCompConfiguration(fileCompConfiguration);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildOutputData method, of class FluxMeteoBuildOutputDisplay.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testBuildOutputData() throws Exception {
        ParameterHelper ph = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.buildOutputData(ph);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFile method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testGetFile() {
        ParameterHelper ph = null;
        String fileName = "";
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        File expResult = null;
        File result = instance.getFile(ph, fileName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getSeparator method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testGetSeparator() {
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        String expResult = "";
        String result = instance.getSeparator();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getFs method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testGetFs() {
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        String expResult = "";
        String result = instance.getFs();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildExtractionPath method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testBuildExtractionPath() {
        String fs = "";
        String fileName = "";
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        String expResult = "";
        String result = instance.buildExtractionPath(fs, fileName);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getStream method, of class FluxMeteoBuildOutputDisplay.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testGetStream() throws Exception {
        ParameterHelper ph = null;
        File fluxFile = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        PrintStream expResult = null;
        PrintStream result = instance.getStream(ph, fluxFile);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildRequestRemainders method, of class FluxMeteoBuildOutputDisplay.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testBuildRequestRemainders() throws Exception {
        ParameterHelper ph = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        String expResult = "";
        String result = instance.buildRequestRemainders(ph);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of writeComment method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testWriteComment() {
        PrintStream fluxPrintStream = null;
        ParameterHelper ph = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.writeComment(fluxPrintStream, ph);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of writeTitle method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testWriteTitle() {
        PrintStream fluxPrintStream = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.writeTitle(fluxPrintStream);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of AddMessageNoResultExtract method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testAddMessageNoResultExtract() {
        ParameterHelper ph = null;
        PrintStream fluxPrintStream = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.addMessageNoResultExtract(ph, fluxPrintStream);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of writeFrequecyAndDates method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testWriteFrequecyAndDates() {
        PrintStream fluxPrintStream = null;
        ParameterHelper ph = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.writeFrequecyAndDates(fluxPrintStream, ph);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of writeVariables method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testWriteVariables() {
        List<DatatypeVariableUniteForet> selectedVariables1 = null;
        PrintStream fluxPrintStream = null;
        String typeVariable = "";
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.writeVariables(selectedVariables1, fluxPrintStream, typeVariable);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of writeSites method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testWriteSites() {
        List<SiteForet> selectedSites1 = null;
        PrintStream fluxPrintStream = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        instance.writeSites(selectedSites1, fluxPrintStream);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of format method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testFormat() {
        float nombre = 0.0F;
        int nombreDeChiffresSignificatifs = 0;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        String expResult = "";
        String result = instance.format(nombre, nombreDeChiffresSignificatifs);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getLogger method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testGetLogger() {
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        Logger expResult = null;
        Logger result = instance.getLogger();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExtractionFile method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testGetExtractionFile() {
        String extractionPath = "";
        ParameterHelper ph = null;
        String fs = "";
        String separator = "";
        String fileRandomSuffix = "";
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        File expResult = null;
        File result = instance.getExtractionFile(extractionPath, ph, fs, separator, fileRandomSuffix);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of getExtention method, of class FluxMeteoBuildOutputDisplay.
     */
    @Test
    @Ignore
    public void testGetExtention() {
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        String expResult = "";
        String result = instance.getExtention();
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of recupererParametres method, of class FluxMeteoBuildOutputDisplay.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testRecupererParametres() throws Exception {
        IParameter parameters = null;
        FluxMeteoBuildOutputDisplay instance = new FluxMeteoBuildOutputDisplayImpl();
        ParameterHelper expResult = null;
        ParameterHelper result = instance.recupererParameters(parameters);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildDateComplete method, of class FluxMeteoBuildOutputDisplay.
     * @throws java.text.ParseException
     */
    @Test
    public void testBuildDateComplete() throws ParseException {
        //no time
        boolean isSemHoraire = false;
        LocalDate date = m.dateDebutComplete.toLocalDate();
        LocalTime time = m.dateDebutComplete.toLocalTime();
        LocalDateTime result = instance.buildDateComplete(isSemHoraire, date, time);
        assertEquals(m.dateDebut, result);
        //with time
        isSemHoraire = true;
        result = instance.buildDateComplete(isSemHoraire, date, time);
        assertEquals(m.dateDebutComplete, result);
    }
    @Test
    public void testBitOperation() throws ParseException {
        int un=1;
        int deux=2;
        int quatre = 4;
        int trois = un|deux;
        assertTrue((deux&trois) !=0);
        assertTrue((un&trois) !=0);
        assertTrue((quatre&trois) ==0);
        
       
    }

    /**
     *
     */
    public class FluxMeteoBuildOutputDisplayImpl extends FluxMeteoBuildOutputDisplay {

        /**
         *
         * @param ph
         * @throws FileNotFoundException
         * @throws BusinessException
         * @throws IOException
         */
        @Override
        public void buildOutputData(ParameterHelper ph) throws FileNotFoundException, BusinessException, IOException {
        }

        /**
         *
         * @return
         */
        @Override
        public Logger getLogger() {
            return null;
        }
    }
    
}
