package org.inra.ecoinfo.foret.extraction.fluxtours;

import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.foret.ForetTransactionalTestFixtureExecutionListener;
import org.junit.runner.RunWith;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = {ForetTransactionalTestFixtureExecutionListener.class})
public class ExtractionFluxToursFastFixture extends org.inra.ecoinfo.foret.extraction.ExtractionFastFixture {

    /**
     *
     */
    public ExtractionFluxToursFastFixture() {
        super();
    }
}
