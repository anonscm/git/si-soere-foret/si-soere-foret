package org.inra.ecoinfo.foret.extraction;

import java.text.ParseException;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.foret.ForetTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.impl.FluxMeteoParameterVO;
import org.inra.ecoinfo.foret.extraction.jsf.date.DatesFormParamVO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.ISiteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = {ForetTransactionalTestFixtureExecutionListener.class})
public class ExtractionFastFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    private static final Logger LOGGER = LoggerFactory.getLogger(ExtractionFastFixture.class);

    ISiteForetDAO siteForetDAO;
    ILocalizationManager localizationManager;
    IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO;

    /**
     *
     */
    public ExtractionFastFixture() {
        super();
        MockitoAnnotations.initMocks(this);
        this.siteForetDAO = ForetTransactionalTestFixtureExecutionListener.siteForetDAO;
        this.localizationManager = ForetTransactionalTestFixtureExecutionListener.localizationManager;
        this.datatypeVariableUniteForetDAO = ForetTransactionalTestFixtureExecutionListener.datatypeVariableUniteDAO;
    }

    /**
     *
     * @param listeSitesCodes
     * @param variablesListNames
     * @param dateDedebut
     * @param datedeFin
     * @param rythme
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws BadExpectedValueException
     * @throws ParseException
     */
    public String extract(String listeSitesCodes, String variablesListNames, String dateDedebut, String datedeFin, String rythme, String filecomps, String commentaire, String affichage) throws BusinessException, PersistenceException, BadExpectedValueException, ParseException {
        List<SiteForet> sites = Stream.of(listeSitesCodes.split(Constantes.CST_COMMA))
                .map(siteCode -> this.siteForetDAO.getByPath(siteCode))
                .filter(s -> s.isPresent())
                .map(s -> (SiteForet) s.get())
                .collect(Collectors.toList());
        final Map<String, Object> metadatasMap = new HashMap<>();
        metadatasMap.put(SiteForet.class.getSimpleName(), sites);
        List<String> variableNames = Stream.of(variablesListNames.split(":")[1].split(Constantes.CST_COMMA))
                .collect(Collectors.toList());
        List<DatatypeVariableUniteForet> selectedVariables = datatypeVariableUniteForetDAO.getAll(DatatypeVariableUniteForet.class).stream()
                .filter(
                        dvu -> variableNames.contains(dvu.getVariable().getCode())
                )
                .distinct()
                .collect(Collectors.toList());
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);
        FluxMeteoParameterVO parameters = new FluxMeteoParameterVO();
        parameters.getParameters().put(FluxMeteoParameterVO.SITE_PARAMETER, sites);
        parameters.getParameters().put(FluxMeteoParameterVO.VARIABLE_PARAMETER, selectedVariables);
        parameters.setCommentaire(commentaire);
        parameters.setAffichage(Integer.parseInt(affichage));
        DatesFormParamVO datesFormParamVO = new DatesFormParamVO(localizationManager);
        datesFormParamVO.setRythme(rythme);
        Map<String, String> period = new HashMap();
        period.put(datesFormParamVO.START_INDEX, Constantes.MENSUEL.equals(rythme) ? "01/".concat(dateDedebut) : dateDedebut);
        period.put(datesFormParamVO.END_INDEX, Constantes.MENSUEL.equals(rythme) ? "01/".concat(datedeFin) : datedeFin);
        datesFormParamVO.getPeriods().clear();
        datesFormParamVO.getPeriods().add(0, period);
        parameters.getParameters().put(FluxMeteoParameterVO.DATES_PARAMETER, datesFormParamVO);
        try {
            ForetTransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final NumberFormatException e) {
            return "false : " + e.getMessage();
        }
        return "true";

    }
}
