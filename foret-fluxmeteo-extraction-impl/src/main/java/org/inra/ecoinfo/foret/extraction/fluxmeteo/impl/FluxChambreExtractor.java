package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;


import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.impl.DefaultExtractionManager;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxChambreDAO;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxChambreParameter;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.ISiteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author philippe Extraction de flux
 *
 */
public class FluxChambreExtractor extends DefaultExtractionManager implements IExtractor {

    /**
     *
     */
    public static final String FLUX_CHAMBRE_EXTRACTOR_NAME = "flux_chambre";
    /**
     *
     */
    public static final String FLUX_CHAMBRE_M = "chambrefluxsol_m";

    /**
     *
     */
    public static final String FLUX_CHAMBRE_SH = "chambrefluxsol_infraj";

    /**
     *
     */
    public static final String FLUX_CHAMBRE_J = "chambrefluxsol_j";

    /**
     *
     */
    public static final String RYTHME_INVALID = "Le rythme \"%s\" n'est pas un rythme valide.";
    private static final String RESOURCE_PATH = "%s/%s/%s";
    private static final String RESOURCE_PATH_VARIABLE = "%s/%s/%s/%s";
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     */
    protected IFluxChambreDAO fluxChambreDAO;

    /**
     *
     */
    protected ISiteForetDAO siteDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO;

    /**
     *
     */
    public FluxChambreExtractor() {
        super();
    }

    /**
     *
     * @param parameters
     * @throws BusinessException
     */
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        try {
            String rythme = ((IFluxChambreParameter) parameters).getRythme();
            IntervalDate intervalDate = ((IFluxChambreParameter) parameters).getIntervalDate();
            DataType dataType = this.getDatatype(rythme)
                    .orElseThrow(()->new BusinessException(String.format(FluxChambreExtractor.RYTHME_INVALID, rythme)));
            List<SiteForet> selectedSites = ((IFluxChambreParameter) parameters).getSelectedSites();
            List<DatatypeVariableUniteForet> selectedVariables = ((IFluxChambreParameter) parameters).getSelectedFluxChambresVariables();

            List<SiteForet> sites = addSites(selectedSites);
            double debut = System.currentTimeMillis();
            List<ValeurChambreFluxSol> valeurFluxChambre = this.fluxChambreDAO.extractFluxChambre(sites, selectedVariables, intervalDate.getBeginDate().toLocalDate(), intervalDate.getEndDate().toLocalDate(), rythme, policyManager.getCurrentUser());


            if (!valeurFluxChambre.isEmpty()) {

                ((IFluxChambreParameter) parameters).setValeurFluxChambres(valeurFluxChambre);
            }
        } catch (BusinessException e) {
            String EXTRACTION_ABORTED = "Abandon de l'extraction...";
            this.LOGGER.error(EXTRACTION_ABORTED, e);
            this.sendNotification(EXTRACTION_ABORTED, Notification.ERROR, PersistenceException.getLastCauseExceptionMessage(e), (Utilisateur) policyManager.getCurrentUser());
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param extraction
     */
    @Override
    public void setExtraction(Extraction extraction) {
        

    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        return 0;
    }

    /**
     *
     * @param parameter
     * @return
     */
    public String getExtractName(IParameter parameter) {
        return FLUX_CHAMBRE_EXTRACTOR_NAME;
    }

    /**
     *
     * @return
     */
    public IFluxChambreDAO getFluxChambreDAO() {
        return this.fluxChambreDAO;
    }

    /**
     *
     * @param fluxChambreDAO
     */
    public void setFluxChambreDAO(IFluxChambreDAO fluxChambreDAO) {
        this.fluxChambreDAO = fluxChambreDAO;
    }

    /**
     *
     * @param datatypeVariableUniteForetDAO
     */
    public void setDatatypeVariableUniteForetDAO(IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO) {
        this.datatypeVariableUniteForetDAO = datatypeVariableUniteForetDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteForetDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    private Optional<DataType> getDatatype(String rythme) throws  BusinessException {
        Optional<DataType> dataType = null;
        if (null != rythme) {
            switch (rythme) {
                case Constantes.SEMI_HORAIRE:
                    dataType = this.datatypeDAO.getByCode(FLUX_CHAMBRE_SH);
                    break;
                case Constantes.JOURNALIER:
                    dataType = this.datatypeDAO.getByCode(FLUX_CHAMBRE_J);
                    break;
                case Constantes.MENSUEL:
                    dataType = this.datatypeDAO.getByCode(FLUX_CHAMBRE_M);
                    break;
                default:
                    throw new BusinessException(String.format(FluxChambreExtractor.RYTHME_INVALID, rythme));
            }
        }
        return dataType;
    }

    /**
     *
     * @param selectedSites
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    protected List<SiteForet> addSites(List<SiteForet> selectedSites) throws BusinessException {
        List<SiteForet> sites = new LinkedList<>();
        for (SiteForet site : selectedSites) {
            sites.add(this.siteDAO.getByPath(site.getPath())
                    .map(s->(SiteForet)s)
                    .orElseThrow(()->new BusinessException("bad site")));
        }
        return sites;
    }
}
