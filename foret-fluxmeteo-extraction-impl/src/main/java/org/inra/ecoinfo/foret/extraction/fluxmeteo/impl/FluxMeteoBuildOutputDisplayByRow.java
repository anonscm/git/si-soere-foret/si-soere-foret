package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.time.temporal.ChronoUnit;
import java.time.temporal.TemporalAccessor;
import java.time.temporal.TemporalUnit;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Properties;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.inra.ecoinfo.foret.dataset.entity.QualityClass;
import org.inra.ecoinfo.foret.dataset.entity.Valeur;
import org.inra.ecoinfo.foret.dataset.flux.entity.MesureFlux;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_infraj;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationComplementaire;
import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import static org.inra.ecoinfo.foret.extraction.fluxmeteo.impl.FluxMeteoBuildOutputDisplay.*;
import org.inra.ecoinfo.foret.refdata.RefDataConstantes;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import static org.inra.ecoinfo.foret.utils.Constantes.*;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author philippe
 *
 */
public class FluxMeteoBuildOutputDisplayByRow extends FluxMeteoBuildOutputDisplay {

    /**
     *
     */
    protected static final String PROPERTY_MSG_FLUX_CHAMBER_GENERIC_HEADER = "PROPERTY_MSG_FLUX_CHAMBER_GENERIC_HEADER";

    /**
     *
     */
    protected static final String PROPERTY_MSG_FLUX_CHAMBER_SH_GENERIC_HEADER = "PROPERTY_MSG_FLUX_CHAMBER_SH_GENERIC_HEADER";

    /**
     *
     */
    protected static final String PROPERTY_MSG_FLUX_METEO_GENERIC_HEADER = "PROPERTY_MSG_FLUX_METEO_GENERIC_HEADER";

    /**
     *
     */
    protected static final String PROPERTY_MSG_FLUX_METEO_SH_GENERIC_HEADER = "PROPERTY_MSG_FLUX_METEO_SH_GENERIC_HEADER";

    /**
     *
     */
    protected final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     * @param ph
     * @throws FileNotFoundException
     * @throws BusinessException
     * @throws IOException
     */
    @Override
    public void buildOutputData(ParameterHelper ph) throws FileNotFoundException, BusinessException, IOException {
        try {
            ph.remplirLesTreeMap();
            if (ph.selectedFluxVariables.size() + ph.selectedMeteoVariables.size() > 0) {
                File fluxMeteoExtractionFile = getFile(ph, DATA_FLUX_METEO);
                PrintStream fluxMeteoPrintStream = getStream(ph, fluxMeteoExtractionFile);
                buidDataForStream(ph, fluxMeteoPrintStream, FLUX_TOWER_CONSTRUCTION | METEO_CONSTRUCTION);
                ph.getFilesMap().put(FluxMeteoBuildOutputDisplay.DATA_FLUX_METEO.concat(getExtention()), fluxMeteoExtractionFile);
            }
            if (!ph.selectedFluxChambresVariables.isEmpty()) {
                File fluxChambersFileExtractionFile = getFile(ph, DATA_FLUX_CHAMBERS);
                PrintStream fluxChamberStreamPrintStream = getStream(ph, fluxChambersFileExtractionFile);
                buidDataForStream(ph, fluxChamberStreamPrintStream, FLUX_CHAMBERS_CONSTRUCTION);
                ph.getFilesMap().put(FluxMeteoBuildOutputDisplay.DATA_FLUX_CHAMBERS.concat(getExtention()), fluxChambersFileExtractionFile);
            }
        } catch (IOException ex) {
            LOGGER.error(ex.getMessage());
        }
        //ph.getFilesMap().put(FluxMeteoBuildOutputDisplay.DATA.concat(getExtention()), extractioFile);
    }

    /**
     *
     * @param ph
     * @param stream
     * @param code
     */
    protected void buidDataForStream(ParameterHelper ph, PrintStream stream, int code) {
        String entete = this.faireEntete(ph, code);
        buildMaps(ph, code);
        stream.print(entete);
        StringBuffer ligne = null;
        for (Iterator<Entry<String, SiteForet>> iteratorSite = ph.sitesVO.entrySet().iterator(); iteratorSite.hasNext();) {
            insertSiteEntry(stream, iteratorSite.next().getValue(), ph, code);
        }
    }

    /**
     *
     * @param ph
     * @param code
     */
    protected void buildMaps(ParameterHelper ph, int code) {
        creerMapPourFlux(ph, code);
        creerMapPourMeteo(ph, code);
        creerMapPourFluxChambres(ph, code);
    }

    /**
     *
     * @param stream
     * @param site
     * @param ph
     * @param code
     */
    protected void insertSiteEntry(PrintStream stream, SiteForet site, ParameterHelper ph, int code) {
        if (ph.hasValues.get(site) != null && (ph.hasValues.get(site) & code) != 0) {
            for (Iterator<LocalDateTime> iteratorLocalDateTime = getDateIterator(site, ph, code); iteratorLocalDateTime.hasNext();) {
                insertLocalDateTimeEntry(stream, iteratorLocalDateTime, site, ph, code);
            }
            stream.print(Constantes.CST_NEW_LINE);
        }
    }

    /**
     *
     * @param site
     * @param ph
     * @param code
     * @return
     */
    protected Iterator<LocalDateTime> getDateIterator(SiteForet site, ParameterHelper ph, int code) {
        if (((FLUX_TOWER_CONSTRUCTION & code) | (METEO_CONSTRUCTION & code)) != 0) {
            LocalDateTime minFluxDate = ph.valeursFluxByDateVariable2.get(site) == null ? LocalDateTime.MAX : ph.valeursFluxByDateVariable2.get(site).firstKey();
            LocalDateTime minMeteoDate = ph.valeursMeteoByDateVariable2.get(site) == null ? LocalDateTime.MAX : ph.valeursMeteoByDateVariable2.get(site).firstKey();
            LocalDateTime mawFluxdate = ph.valeursFluxByDateVariable2.get(site) == null ? LocalDateTime.MIN : ph.valeursFluxByDateVariable2.get(site).lastKey();
            LocalDateTime mawMeteodate = ph.valeursMeteoByDateVariable2.get(site) == null ? LocalDateTime.MIN : ph.valeursMeteoByDateVariable2.get(site).lastKey();
            LocalDateTime minDate = minFluxDate.isBefore(minMeteoDate) ? minFluxDate : minMeteoDate;
            LocalDateTime maxDate = mawFluxdate.isAfter(mawMeteodate) ? mawFluxdate : mawMeteodate;
            if (maxDate.equals(LocalDateTime.MIN) && minDate.equals(LocalDateTime.MAX)) {
                return new LinkedList<LocalDateTime>().iterator();
            }
            return new LocalDateTimeIterator(ph, minDate, maxDate);
        } else if ((FLUX_CHAMBERS_CONSTRUCTION & code) != 0) {
            if (ph.valeursFluxChambresByDateVariable2.get(site) == null) {
                return new LinkedList<LocalDateTime>().iterator();
            }
            if (SEMI_HORAIRE.equals(ph.rythme)) {
                return ph.valeursFluxChambresByDateVariable2.get(site).keySet().iterator();
            }
            LocalDateTime minDate = ph.valeursFluxChambresByDateVariable2.get(site).firstKey();
            LocalDateTime maxDate = ph.valeursFluxChambresByDateVariable2.get(site).lastKey();
            return new LocalDateTimeIterator(ph, minDate, maxDate);
        }
        return new LinkedList<LocalDateTime>().iterator();
    }

    /**
     *
     * @param stream
     * @param iteratorLocalDateTime
     * @param site
     * @param ph
     * @param code
     */
    protected void insertLocalDateTimeEntry(PrintStream stream, Iterator<LocalDateTime> iteratorLocalDateTime, SiteForet site, ParameterHelper ph, int code) {
        Map<DatatypeVariableUniteForet, ValeurFluxTour> entryFlux = null;
        Map<DatatypeVariableUniteForet, ValeurMeteo> entryMeteo = null;
        SortedMap<Integer, Map<DatatypeVariableUniteForet, ValeurChambreFluxSol>> entryChambre = null;
        boolean isFluxTower = (FLUX_TOWER_CONSTRUCTION & code) != 0;
        boolean isMeteo = (METEO_CONSTRUCTION & code) != 0;
        boolean isFluxChambers = (FLUX_CHAMBERS_CONSTRUCTION & code) != 0;
        StringBuffer ligne;
        LocalDateTime date = iteratorLocalDateTime.next();
        entryFlux = ph.valeursFluxByDateVariable2
                .computeIfAbsent(site, k -> new TreeMap<LocalDateTime, Map<DatatypeVariableUniteForet, ValeurFluxTour>>())
                .computeIfAbsent(date, k -> new HashMap<DatatypeVariableUniteForet, ValeurFluxTour>());
        entryMeteo = ph.valeursMeteoByDateVariable2
                .computeIfAbsent(site, k -> new TreeMap<LocalDateTime, Map<DatatypeVariableUniteForet, ValeurMeteo>>())
                .computeIfAbsent(date, k -> new HashMap<DatatypeVariableUniteForet, ValeurMeteo>());
        entryChambre = ph.valeursFluxChambresByDateVariable2
                .computeIfAbsent(site, k -> new TreeMap<LocalDateTime, SortedMap<Integer, Map<DatatypeVariableUniteForet, ValeurChambreFluxSol>>>())
                .computeIfAbsent(date, k -> new TreeMap<Integer, Map<DatatypeVariableUniteForet, ValeurChambreFluxSol>>());
        ligne = new StringBuffer(site.getCode()).append(CSY_CSV_SEPARATOR)
                .append((MENSUEL.equals(ph.rythme) ? DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.MM_YYYY) : DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY)))
                .append(SEMI_HORAIRE.equals(ph.rythme) ? CSY_CSV_SEPARATOR : "")
                .append(SEMI_HORAIRE.equals(ph.rythme) ? DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.HH_MM) : "");
        int r = readVariablesColumnsForFluxMeteo(ph, ph.selectedFluxVariables, ph.variableFluxWithQualityCode, entryFlux, ligne, isFluxTower);
        r += readVariablesColumnsForFluxMeteo(ph, ph.selectedMeteoVariables, ph.variableMeteoWithQualityCode, entryMeteo, ligne, isMeteo);
        if (r > 0) {
            ligne.append("\n");
            stream.print(ligne);
        }
        readVariablesColumnsForChambers(stream, ph, ph.selectedFluxChambresVariables, ph.variableFluxChambresWithInformationComplementaire, entryChambre, isFluxChambers, date, site);
    }

    private <T extends Valeur> int readVariablesColumnsForFluxMeteo(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variablesWithQualityCode, Map<DatatypeVariableUniteForet, T> entry, StringBuffer ligne, boolean canDo) {
        if (!canDo || entry == null) {
            return 0;
        }
        for (DatatypeVariableUniteForet dvu : selectedVariables) {
            if (entry == null || !entry.containsKey(dvu)) {
                ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                if (variablesWithQualityCode.contains(dvu.getCode())) {
                    ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                }
            } else {
                T valeur = entry.get(dvu);
                ligne.append(CSY_CSV_SEPARATOR).append(this.format(valeur.getValue(), 4));
                if (variablesWithQualityCode.contains(dvu.getCode())) {
                    QualityClass qualite = valeur.getQualityClass();
                    if (qualite != null) {
                        ligne.append(CSY_CSV_SEPARATOR).append(valeur.getQualityClass().getNumero());
                    } else {
                        ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                    }
                }
            }
        }
        return 1;
    }

    private <T extends Valeur> void readVariablesColumnsForChambers(PrintStream stream, ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variablesWithQualityCode, SortedMap<Integer, Map<DatatypeVariableUniteForet, T>> dateEntry, boolean canDo, TemporalAccessor date, SiteForet site) {
        if (!canDo || dateEntry == null) {
            return;
        }
        for (Entry<Integer, Map<DatatypeVariableUniteForet, T>> noChamberEntry : dateEntry.entrySet()) {

            StringBuffer ligne = new StringBuffer(site.getCode()).append(CSY_CSV_SEPARATOR)
                    .append((MENSUEL.equals(ph.rythme) ? DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.MM_YYYY) : DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY)))
                    .append(SEMI_HORAIRE.equals(ph.rythme) ? CSY_CSV_SEPARATOR : "")
                    .append(SEMI_HORAIRE.equals(ph.rythme) ? DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.HH_MM) : "");
            Map<DatatypeVariableUniteForet, T> entry = noChamberEntry.getValue();
            addGenericsInfo(entry, ph, ligne);
            selectedVariables.forEach((variableVO) -> {
                T valeur = entry.get(variableVO);
                if (entry == null || !entry.containsKey(variableVO)) {
                    ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                } else {
                    ligne.append(CSY_CSV_SEPARATOR).append(this.format(valeur.getValue(), 4));
                }
                addComplementaryInformationsValues(ligne, variablesWithQualityCode, variableVO, valeur);
            });
            ligne.append("\n");
            stream.print(ligne);
        }
    }

    private <T extends Valeur> void addGenericsInfo(Map<DatatypeVariableUniteForet, T> entry, ParameterHelper ph, StringBuffer ligne) {
        ValeurChambreFluxSol valeur = null;
        Properties traitementProperties = localizationManager.newProperties(Traitement.TABLE_NAME, RefDataConstantes.COLUMN_LIBELLE_TRT);
        String treatment = PROPERTY_CST_NOT_AVALAIBALE;
        String noChamber = PROPERTY_CST_NOT_AVALAIBALE;
        String dureeMesure = PROPERTY_CST_NOT_AVALAIBALE;
        for (T valeurEntry : entry.values()) {
            if (valeurEntry != null) {
                valeur = (ValeurChambreFluxSol) valeurEntry;
                treatment = traitementProperties.getProperty(valeur.getMesure().getTraitement().getLibelle(), valeur.getMesure().getTraitement().getLibelle());
                noChamber = valeur.getMesure().getNoChambre();
                if (valeur.getClass().equals(ValeurChambreFluxSol_infraj.class)) {
                    ValeurChambreFluxSol_infraj vInfra = (ValeurChambreFluxSol_infraj) valeur;
                    dureeMesure = Integer.toString(vInfra.getMesure().getDureeMesure());
                }
                break;
            }
        }
        if (Constantes.SEMI_HORAIRE.equals(ph.rythme)) {
            ligne.append(String.format(";%s", dureeMesure));
        }
        ligne.append(String.format(";%s;%s", treatment, noChamber));
    }

    private <T extends Valeur> void addComplementaryInformationsValues(StringBuffer ligne, List<String> variablesWithInformationsComplementaires, DatatypeVariableUniteForet variableVO, T valeur) {
        if (valeur == null || !valeur.getClass().getSuperclass().equals(ValeurChambreFluxSol.class)) {
            return;
        }
        ValeurChambreFluxSol v = (ValeurChambreFluxSol) valeur;
        for (String variablesWithInformationsComplementaire : variablesWithInformationsComplementaires) {
            if (variablesWithInformationsComplementaire.matches(String.format("%s_[^_]*", variableVO.getVariable().getCode()))) {
                boolean isNull = true;
                for (Iterator<ValeurInformationComplementaire> iterator = v.getComplements().iterator(); iterator.hasNext();) {
                    ValeurInformationComplementaire vic = iterator.next();
                    if (vic.getIdentite().getNom().equals(variablesWithInformationsComplementaire)) {
                        ligne.append(CSY_CSV_SEPARATOR).append(vic.valeurToString());
                        isNull = false;
                        break;
                    }
                }
                if (isNull) {
                    ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                }
            }
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Logger getLogger() {
        return this.LOGGER;
    }

    private StringBuilder doHeaderVariableNameAndReturnUnitLine(StringBuilder enTete, ParameterHelper ph, int code) {
        StringBuilder ligneUnite;
        if (FLUX_CHAMBERS_CONSTRUCTION == code) {
            final String genericsColumns = SEMI_HORAIRE.equals(ph.rythme)
                    ? FORETRecorder.getForetMessageWithBundle(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_FLUX_CHAMBER_SH_GENERIC_HEADER)
                    : FORETRecorder.getForetMessageWithBundle(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_FLUX_CHAMBER_GENERIC_HEADER);
            enTete = enTete.append("\n").append(genericsColumns);
            ligneUnite = new StringBuilder(genericsColumns.replaceAll("[^;]", ""));
        } else {
            final String genericsColumns = SEMI_HORAIRE.equals(ph.rythme)
                    ? FORETRecorder.getForetMessageWithBundle(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_FLUX_METEO_SH_GENERIC_HEADER)
                    : FORETRecorder.getForetMessageWithBundle(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_FLUX_METEO_GENERIC_HEADER);
            enTete = enTete.append("\n").append(genericsColumns);
            ligneUnite = new StringBuilder(genericsColumns.replaceAll("[^;]", ""));
        }
        writeHeader(enTete, ligneUnite, ph.fluxDatatype, ph, ph.selectedFluxVariables, ph.variableFluxWithQualityCode, (code & FLUX_TOWER_CONSTRUCTION) == 0, code);
        writeHeader(enTete, ligneUnite, ph.meteoDatatype, ph, ph.selectedMeteoVariables, ph.variableMeteoWithQualityCode, (code & METEO_CONSTRUCTION) == 0, code);
        writeHeader(enTete, ligneUnite, ph.fluxChambresDatatype, ph, ph.selectedFluxChambresVariables, ph.variableFluxChambresWithInformationComplementaire, (code & FLUX_CHAMBERS_CONSTRUCTION) == 0, code);
        return ligneUnite;
    }

    private void writeHeader(StringBuilder enTete, StringBuilder ligneUnite, String datatype, ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variablesWithQualityCode, boolean cantBeDone, int code) {
        if (cantBeDone) {
            return;
        }
        Properties variablesProperties = localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableForet.class), Nodeable.ENTITE_COLUMN_NAME);
        for (DatatypeVariableUniteForet variableVO : selectedVariables) {
            String codeVariable = variableVO.getVariable().getCode();
            codeVariable = variablesProperties.getProperty(codeVariable, codeVariable);
            enTete = enTete.append(CSY_CSV_SEPARATOR).append(codeVariable);
            ligneUnite = ligneUnite.append(CSY_CSV_SEPARATOR);
            ligneUnite = ligneUnite.append(variableVO.getUnite().getCode());
            if (((code & FLUX_TOWER_CONSTRUCTION) | (code & METEO_CONSTRUCTION)) != 0 && variablesWithQualityCode.contains(variableVO.getCode())) {
                enTete = enTete.append(QUALITY_CLASS_FIELD);
                ligneUnite = ligneUnite.append(QUALITY_CLASS_FIELD);
            }
            if ((code & FLUX_CHAMBERS_CONSTRUCTION) != 0) {
                addComplementariesInformationsColumns(enTete, ligneUnite, variablesWithQualityCode, variableVO);
            }
        }
    }

    private void addComplementariesInformationsColumns(StringBuilder enTete, StringBuilder ligneUnite, List<String> variablesWithInformationsComplementaires, DatatypeVariableUniteForet variableVO) {
        for (String variablesWithInformationsComplementaire : variablesWithInformationsComplementaires) {
            if (variablesWithInformationsComplementaire.matches(String.format("%s_[^_]*", variableVO.getVariable().getCode()))) {
                enTete.append(String.format(";%s", variablesWithInformationsComplementaire));
                ligneUnite.append(";");
            }
        }
    }

    /**
     *
     * @param ph
     * @param code
     * @return
     */
    protected void creerMapPourFlux(ParameterHelper ph, int code) {
        if ((code & FLUX_TOWER_CONSTRUCTION) == 0) {
            ph.valeursFluxByDateVariable2 = new TreeMap();
            return;
        }
        for (Iterator<ValeurFluxTour> iterator = ph.valeursFluxTours.iterator(); iterator.hasNext();) {
            ValeurFluxTour valeurFluxTour = iterator.next();
            if (valeurFluxTour == null) {
                continue;
            }
            MesureFlux mesureFlux = valeurFluxTour.getMesure();
            final SiteForet site = ph.sitesVO.get(mesureFlux.getSite().getPath());
            Integer hasValue = ph.hasValues.get(site);
            hasValue = hasValue == null ? 0 : hasValue;
            hasValue = hasValue | FLUX_TOWER_CONSTRUCTION;
            ph.hasValues.put(site, hasValue);
            LocalDateTime dateComplete = buildDateComplete(SEMI_HORAIRE.equals(ph.rythme), mesureFlux.getDate(), mesureFlux.getTime());
            ph.valeursFluxByDateVariable2
                    .computeIfAbsent(site, k -> new TreeMap<>())
                    .computeIfAbsent(dateComplete, k -> new TreeMap<>())
                    .put(ph.variablesFluxVO.get(valeurFluxTour.getRealNode().getNodeable().getId()), valeurFluxTour);
            iterator.remove();
        }
    }

    /**
     *
     * @param ph
     * @param code
     * @return
     */
    protected void creerMapPourMeteo(ParameterHelper ph, int code) {
        if ((code & METEO_CONSTRUCTION) == 0) {
            ph.valeursMeteoByDateVariable2 = new TreeMap();
            return;
        }
        for (Iterator<ValeurMeteo> iterator = ph.valeursMeteoTours.iterator(); iterator.hasNext();) {
            ValeurMeteo valeurMeteo = iterator.next();
            if (valeurMeteo == null) {
                continue;
            }
            MesureMeteo mesureMeteo = valeurMeteo.getMesure();
            final SiteForet site = ph.sitesVO.get(mesureMeteo.getSite().getPath());
            Integer hasValue = ph.hasValues.get(site);
            hasValue = hasValue == null ? 0 : hasValue;
            hasValue = hasValue | METEO_CONSTRUCTION;
            ph.hasValues.put(site, hasValue);
            LocalDateTime date = buildDateComplete(SEMI_HORAIRE.equals(ph.rythme), mesureMeteo.getDate(), mesureMeteo.getTime());
            ph.valeursMeteoByDateVariable2
                    .computeIfAbsent(site, k -> new TreeMap<>())
                    .computeIfAbsent(date, k -> new TreeMap<>())
                    .put(ph.variablesMeteoVO.get(valeurMeteo.getRealNode().getNodeable().getId()), valeurMeteo);
            iterator.remove();
        }
    }

    /**
     *
     * @param ph
     * @param code
     * @return
     */
    protected void creerMapPourFluxChambres(ParameterHelper ph, int code) {
        if ((code & FLUX_CHAMBERS_CONSTRUCTION) == 0) {
            ph.valeursFluxChambresByDateVariable2 = new TreeMap();
            return;
        }
        for (Iterator<ValeurChambreFluxSol> iterator = ph.valeursFluxChambres.iterator(); iterator.hasNext();) {
            ValeurChambreFluxSol valeurFluxChambres = iterator.next();
            if (valeurFluxChambres == null) {
                continue;
            }
            MesureChambreFluxSol mesureChambres = valeurFluxChambres.getMesureFluxChambre();
            final SiteForet site = ph.sitesVO.get(mesureChambres.getSite().getPath());
            Integer hasValue = ph.hasValues.get(site);
            hasValue = hasValue == null ? 0 : hasValue;
            hasValue = hasValue | FLUX_CHAMBERS_CONSTRUCTION;
            ph.hasValues.put(site, hasValue);
            LocalDateTime date = buildDateComplete(SEMI_HORAIRE.equals(ph.rythme), mesureChambres.getDate(), mesureChambres.getTime());
            final String noChambreString = valeurFluxChambres.getMesureFluxChambre().getNoChambre();
            final Integer noChambre = Strings.isNullOrEmpty(noChambreString)?-1:Integer.parseInt(valeurFluxChambres.getMesureFluxChambre().getNoChambre());     
            final Long variableId = valeurFluxChambres.getRealNode().getNodeable().getId();
            ph.valeursFluxChambresByDateVariable2
                    .computeIfAbsent(site, k -> new TreeMap<LocalDateTime, SortedMap<Integer, Map<DatatypeVariableUniteForet, ValeurChambreFluxSol>>>())
                    .computeIfAbsent(date, k -> new TreeMap<Integer, Map<DatatypeVariableUniteForet, ValeurChambreFluxSol>>())
                    .computeIfAbsent(noChambre, k -> new HashMap<DatatypeVariableUniteForet, ValeurChambreFluxSol>())
                    .put(ph.variablesFluxChambresVO.get(variableId), valeurFluxChambres);
            iterator.remove();
        }
    }

    /**
     *
     * @param enTete
     * @param ph
     * @param code
     * @return
     */
    protected StringBuilder doHeaderDatatypeLine(StringBuilder enTete, ParameterHelper ph, int code) {
        enTete = doHeaderDatatypeLine(ph, ph.selectedFluxVariables, enTete, PROPERTY_MSG_FLOW, (code & FLUX_TOWER_CONSTRUCTION) == 0);
        enTete = doHeaderDatatypeLine(ph, ph.selectedMeteoVariables, enTete, PROPERTY_MSG_METEO, (code & METEO_CONSTRUCTION) == 0);
        enTete = doHeaderDatatypeLine(ph, ph.selectedFluxChambresVariables, enTete, PROPERTY_MSG_CHAMBER, (code & FLUX_CHAMBERS_CONSTRUCTION) == 0);
        return enTete;
    }

    /**
     *
     * @param ph
     * @param selectedVariables
     * @param enTete
     * @param datatypeName
     * @param cantBeDone
     * @return
     */
    public StringBuilder doHeaderDatatypeLine(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, StringBuilder enTete, String datatypeName, boolean cantBeDone) {
        if (cantBeDone) {
            return enTete;
        }
        if (!selectedVariables.isEmpty()) {
            enTete = enTete.append(this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, datatypeName));
        }
        for (DatatypeVariableUniteForet variableVO : selectedVariables) {
            enTete = enTete.append(CSY_CSV_SEPARATOR);
            if (ph.variableFluxWithQualityCode.contains(variableVO.getCode())) {
                enTete = enTete.append(CSY_CSV_SEPARATOR);
            }
            if (PROPERTY_MSG_CHAMBER.equalsIgnoreCase(datatypeName)) {
                List<String> variableFluxChambresWithInformationComplementaire = ph.variableFluxChambresWithInformationComplementaire.stream()
                        .filter(vwic -> vwic.startsWith(variableVO.getVariable().getCode().concat("_")))
                        .collect(Collectors.toList());
                for (int i = 0; i < variableFluxChambresWithInformationComplementaire.size(); i++) {
                    enTete = enTete.append(CSY_CSV_SEPARATOR);
                }
            }
        }
        return enTete;
    }

    String faireEntete(ParameterHelper ph, int code) {
        StringBuilder enTete = new StringBuilder("");
        enTete = enTete.append(SEMI_HORAIRE.equals(ph.rythme) ? ";;;" : ";;");
        enTete = doHeaderDatatypeLine(enTete, ph, code);
        StringBuilder ligneUnite = doHeaderVariableNameAndReturnUnitLine(enTete, ph, code);
        enTete = enTete.append("\n");
        enTete = enTete.append(ligneUnite);
        enTete = enTete.append("\n");
        return enTete.toString();

    }

    /**
     *
     */
    protected class LocalDateTimeIterator implements Iterator<LocalDateTime> {

        private final LocalDateTime maxDate;
        private final String rythme = null;
        private LocalDateTime currentDate;
        private TemporalUnit field = ChronoUnit.DAYS;
        private long step = 0;

        /**
         *
         * @param ph
         * @param minDate
         * @param maxDate
         */
        protected LocalDateTimeIterator(ParameterHelper ph, LocalDateTime minDate, LocalDateTime maxDate) {
            this.currentDate = minDate;
            this.maxDate = maxDate;
            setIntervale(ph.rythme);
        }

        @Override
        public boolean hasNext() {
            return !maxDate.isBefore(currentDate);
        }

        @Override
        public LocalDateTime next() {
            final LocalDateTime date = currentDate;
            currentDate = currentDate.plus(step, field);
            return date;
        }

        @Override
        public void remove() {
            //nothing to remove
        }

        private void setIntervale(String rythme) {
            switch (rythme) {
                case SEMI_HORAIRE:
                    field = ChronoUnit.MINUTES;
                    step = 30;
                    break;
                case JOURNALIER:
                    field = ChronoUnit.DAYS;
                    step = 1;
                    break;
                case MENSUEL:
                    field = ChronoUnit.MONTHS;
                    step = 1;
                    break;
                default:
                    field = ChronoUnit.YEARS;
                    step = 1000;

            }
        }

    }
}
