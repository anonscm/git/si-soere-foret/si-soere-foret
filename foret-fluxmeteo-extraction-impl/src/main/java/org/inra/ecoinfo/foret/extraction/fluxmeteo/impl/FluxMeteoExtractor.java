package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import java.util.List;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.MO;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxMeteoParameter;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author philippe Extraction de flux et de météo
 *
 */
public class FluxMeteoExtractor extends MO implements IExtractor {

    /**
     *
     */
    public static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";
    private static final String MSG_EXTRACTION_ABORTED = "PROPERTY_MSG_FAILED_EXTRACT";

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.extraction.impl.messages";
    private static final String BUNDLE_SOURCE_PATH_EXTRACT = "org.inra.ecoinfo.extraction.impl.messages";

    /**
     *
     */
    protected IExtractor fluxExtractor;

    /**
     *
     */
    protected IExtractor meteoExtractor;

    /**
     *
     */
    protected IExtractor fluxChambresExtractor;

    @Override
    public void extract(IParameter parameters) throws BusinessException {
        try {
            IFluxMeteoParameter parameterFluxMeteo = (IFluxMeteoParameter) parameters;
            if (!parameterFluxMeteo.getSelectedFluxVariables().isEmpty()) {
                this.fluxExtractor.extract(parameters);
            }
            if (!parameterFluxMeteo.getSelectedMeteoVariables().isEmpty()) {
                this.meteoExtractor.extract(parameters);
            }
            if (!parameterFluxMeteo.getSelectedFluxChambresVariables().isEmpty()) {
                this.fluxChambresExtractor.extract(parameters);
            }
            List<ValeurFluxTour> valeursFluxTours = parameterFluxMeteo.getValeurFluxTours();
            List<ValeurMeteo> valeursMeteoTours = parameterFluxMeteo.getValeurMeteoTours();
            List<ValeurChambreFluxSol> valeursFluxChambres = parameterFluxMeteo.getValeurFluxChambres();
            List<DatatypeVariableUniteForet> selectedFluxVariables = parameterFluxMeteo.getSelectedFluxVariables();
            List<DatatypeVariableUniteForet> selectedMeteoVariables = parameterFluxMeteo.getSelectedMeteoVariables();
            List<DatatypeVariableUniteForet> selectedFluxChambresVariables = parameterFluxMeteo.getSelectedFluxChambresVariables();
            if ((CollectionUtils.isEmpty(valeursFluxTours) || CollectionUtils.isEmpty(selectedFluxVariables)) 
                    && (CollectionUtils.isEmpty(valeursMeteoTours) || CollectionUtils.isEmpty(selectedMeteoVariables))
                    && (CollectionUtils.isEmpty(valeursFluxChambres) || CollectionUtils.isEmpty(selectedFluxChambresVariables))) {
                String noResult = this.localizationManager.getMessage(FluxMeteoExtractor.BUNDLE_SOURCE_PATH, FluxMeteoExtractor.PROPERTY_MSG_NO_RESULT_EXTRACT);
                String message = String.format(noResult, parameterFluxMeteo.getExtractionTypeCode());
                NoExtractionResultException erreur = new NoExtractionResultException(message);
                throw erreur;
            }

        } catch (NoExtractionResultException e) {
            String message = String.format(FORETRecorder.getForetMessageWithBundle(BUNDLE_SOURCE_PATH_EXTRACT, FluxMeteoExtractor.MSG_EXTRACTION_ABORTED));
            if (CollectionUtils.isEmpty(((FluxMeteoParameterVO) parameters).getAsssociatesFiles())) {
                throw new BusinessException(message, e);
            }
        } catch (BusinessException e) {
            throw e;
        }
    }

    @Override
    public void setExtraction(Extraction extraction) {

    }

    @Override
    public long getExtractionSize(IParameter parameters) {
        
        return 0;
    }

    /**
     *
     * @param fluxExtractor
     */
    public void setFluxExtractor(IExtractor fluxExtractor) {
        this.fluxExtractor = fluxExtractor;
    }

    /**
     *
     * @param meteoExtractor
     */
    public void setMeteoExtractor(IExtractor meteoExtractor) {
        this.meteoExtractor = meteoExtractor;
    }

    /**
     *
     * @param fluxChambresExtractor
     */
    public void setFluxChambres(IExtractor fluxChambresExtractor) {
        this.fluxChambresExtractor = fluxChambresExtractor;
    }

}
