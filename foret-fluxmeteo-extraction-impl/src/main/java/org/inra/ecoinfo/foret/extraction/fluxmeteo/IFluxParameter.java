package org.inra.ecoinfo.foret.extraction.fluxmeteo;

import java.util.List;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.utils.IntervalDate;


/**
 * @author philippe
 * 
 */
public interface IFluxParameter extends IParameter {

    /**
     *
     * @return
     */
    String getRythme();
    
    /**
     *
     * @return
     */
    IntervalDate getIntervalDate();

    /**
     *
     * @return
     */
    List<DatatypeVariableUniteForet> getSelectedFluxVariables();

    /**
     *
     * @return
     */
    List<SiteForet> getSelectedSites();

    /**
     *
     * @return
     */
    List<ValeurFluxTour> getValeurFluxTours();

    /**
     *
     * @param valeurFluxTours
     */
    void setValeurFluxTours(List<ValeurFluxTour> valeurFluxTours);
}