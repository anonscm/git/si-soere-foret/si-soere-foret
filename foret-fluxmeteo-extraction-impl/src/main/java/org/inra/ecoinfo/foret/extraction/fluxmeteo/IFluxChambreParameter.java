package org.inra.ecoinfo.foret.extraction.fluxmeteo;

import java.util.List;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.utils.IntervalDate;


/**
 * @author philippe
 * 
 */
public interface IFluxChambreParameter extends IParameter {

    /**
     *
     * @return
     */
    String getRythme();
    
    /**
     *
     * @return
     */
    IntervalDate getIntervalDate();

    /**
     *
     * @return
     */
    List<DatatypeVariableUniteForet> getSelectedFluxChambresVariables();

    /**
     *
     * @return
     */
    List<SiteForet> getSelectedSites();

    /**
     *
     * @return
     */
    List<ValeurChambreFluxSol> getValeurFluxChambres();

    /**
     *
     * @param valeurFluxChambre
     */
    void setValeurFluxChambres(List<ValeurChambreFluxSol> valeurFluxChambre);
}