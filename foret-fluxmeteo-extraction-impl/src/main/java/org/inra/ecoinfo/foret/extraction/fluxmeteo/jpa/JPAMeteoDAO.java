package org.inra.ecoinfo.foret.extraction.fluxmeteo.jpa;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationComplementaire;
import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo_;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_j;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_j_;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_m;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_m_;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_sh;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_sh_;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IMeteoDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class JPAMeteoDAO extends AbstractJPADAO<ValeurMeteo> implements IMeteoDAO {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     * @param selectedSites
     * @param selectedVariables
     * @param dateDebut
     * @param dateFin
     * @param rythme
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    protected <T extends ValeurMeteo> List<T> extractMeteo(Class<T> valueClass, String mesureMeteoAttribute, List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, IUser user) {
        if (selectedSites == null || selectedSites.isEmpty() || selectedVariables == null || selectedVariables.isEmpty() || dateDebut == null || dateFin == null) {
            return new LinkedList<>();
        }
        CriteriaQuery<T> query = builder.createQuery(valueClass);
        Root<T> v = query.from(valueClass);
        Fetch<T, ValeurInformationComplementaire> complements = v.fetch(ValeurMeteo_.complements, JoinType.LEFT);
        Join<T, MesureMeteo> m = v.join(mesureMeteoAttribute);
        Join<T, RealNode> dvuRealNode = v.join(ValeurMeteo_.realNode);
        Join<RealNode, DatatypeVariableUniteForet> dvu = builder.treat(dvuRealNode.join(RealNode_.nodeable), DatatypeVariableUniteForet.class);
        Join<MesureMeteo, VersionFile> version = m.join(MesureMeteo_.version);
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Join<Dataset, RealNode> datasetRealNode = dataset.join(Dataset_.realNode);
        Join<RealNode, RealNode> themeRealNode = datasetRealNode.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRealNode = themeRealNode.join(RealNode_.parent);
        Join<RealNode, SiteForet> site = builder.treat(siteRealNode.join(RealNode_.nodeable), SiteForet.class);
        List<Predicate> and = new LinkedList();
        and.add(site.in(selectedSites));
        and.add(dvu.in(selectedVariables));
        if (!user.getIsRoot()) {
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            and.add(builder.equal(nds.get(NodeDataSet_.realNode), dvuRealNode));
            Outils.addRestrictiveRequestOnRoles(user, query, and, builder, nds, m.<LocalDate>get(MesureMeteo_.date));
        }

        and.add(builder.between(m.<LocalDate>get(MesureMeteo_.date), dateDebut, dateFin));
        query.
                select(v)
                .where(and.toArray(new Predicate[0]));
        return getResultList(query);
    }

    private Optional<Class<?extends ValeurMeteo>> getValueClassFromRythme(String rythme) {
        switch (rythme) {
            case Constantes.SEMI_HORAIRE:
                return Optional.of(ValeurMeteo_sh.class);
            case Constantes.JOURNALIER:
                return Optional.of(ValeurMeteo_j.class);
            case Constantes.MENSUEL:
                return Optional.of(ValeurMeteo_m.class);
            default:
                return Optional.empty();
        }
    }

    private String getMesureAttribute(String rythme) {
        switch (rythme) {
            case Constantes.SEMI_HORAIRE:
                return ValeurMeteo_sh_.mesure.getName();
            case Constantes.JOURNALIER:
                return ValeurMeteo_j_.mesure.getName();
            case Constantes.MENSUEL:
                return ValeurMeteo_m_.mesure.getName();
            default:
                return "";
        }
    }

    @Override
    public List<ValeurMeteo> extractMeteo(List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, String rythme, IUser user) {
        Optional<Class<? extends ValeurMeteo>> classOpt = getValueClassFromRythme(rythme);
        if(!classOpt.isPresent()){
            return new LinkedList();
        }
        return extractMeteo(classOpt.get(), getMesureAttribute(rythme), selectedSites, selectedVariables, dateDebut, dateFin, user)
                .stream()
                .map(v->(ValeurMeteo)v)
                .collect(Collectors.toList());
    }
}
