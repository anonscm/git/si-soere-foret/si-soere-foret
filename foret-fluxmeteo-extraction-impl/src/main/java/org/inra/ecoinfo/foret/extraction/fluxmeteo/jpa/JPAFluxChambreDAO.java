package org.inra.ecoinfo.foret.extraction.fluxmeteo.jpa;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_infraj;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_j;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_m;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol_;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol_;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationComplementaire;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxChambreDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author philippe DAO d'extraction des données de flux
 *
 */
public class JPAFluxChambreDAO extends AbstractJPADAO<ValeurChambreFluxSol> implements IFluxChambreDAO {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     * @param selectedSites
     * @param selectedVariables
     * @param dateDebut
     * @param dateFin
     * @param rythme
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    protected <T extends ValeurChambreFluxSol> List<T> extractFluxChambre(Class<T> valueClass, List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, IUser user) {
        if (selectedSites == null || selectedSites.isEmpty() || selectedVariables == null || selectedVariables.isEmpty() || dateDebut == null || dateFin == null) {
            return new LinkedList<>();
        }
        CriteriaQuery<T> query = builder.createQuery(valueClass);
        Root<T> v = query.from(valueClass);
        Fetch<T, ValeurInformationComplementaire> complements = v.fetch(ValeurChambreFluxSol_.complements, JoinType.LEFT);
        Join<T, MesureChambreFluxSol> m = v.join(ValeurChambreFluxSol_.mesure);
        Join<T, RealNode> dvuRealNode = v.join(ValeurChambreFluxSol_.realNode);
        Join<RealNode, DatatypeVariableUniteForet> dvu = builder.treat(dvuRealNode.join(RealNode_.nodeable), DatatypeVariableUniteForet.class);
        Join<MesureChambreFluxSol, VersionFile> version = m.join(MesureChambreFluxSol_.version);
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Join<Dataset, RealNode> datasetRealNode = dataset.join(Dataset_.realNode);
        Join<RealNode, RealNode> themeRealNode = datasetRealNode.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRealNode = themeRealNode.join(RealNode_.parent);
        Join<RealNode, SiteForet> site = builder.treat(siteRealNode.join(RealNode_.nodeable), SiteForet.class);
        List<Predicate> and = new LinkedList();
        and.add(site.in(selectedSites));
        and.add(dvu.in(selectedVariables));
        if (!user.getIsRoot()) {
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            and.add(builder.equal(nds.get(NodeDataSet_.realNode), dvuRealNode));
            Outils.addRestrictiveRequestOnRoles(user, query, and, builder, nds, m.<LocalDate>get(MesureChambreFluxSol_.date));
        }

        and.add(builder.between(m.<LocalDate>get(MesureChambreFluxSol_.date), dateDebut, dateFin));
        query.
                select(v)
                .where(and.toArray(new Predicate[0]));
        return getResultList(query);
    }

    private Optional<Class<?extends ValeurChambreFluxSol>> getValueClassFromRythme(String rythme) {
        switch (rythme) {
            case Constantes.SEMI_HORAIRE:
                return Optional.of(ValeurChambreFluxSol_infraj.class);
            case Constantes.JOURNALIER:
                return Optional.of(ValeurChambreFluxSol_j.class);
            case Constantes.MENSUEL:
                return Optional.of(ValeurChambreFluxSol_m.class);
            default:
                return Optional.empty();
        }
    }

    @Override
    public List<ValeurChambreFluxSol> extractFluxChambre(List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, String rythme, IUser user) {
        Optional<Class<? extends ValeurChambreFluxSol>> classOpt = getValueClassFromRythme(rythme);
        if(!classOpt.isPresent()){
            return new LinkedList();
        }
        return extractFluxChambre(classOpt.get(), selectedSites, selectedVariables, dateDebut, dateFin, user)
                .stream()
                .map(v->(ValeurChambreFluxSol)v)
                .collect(Collectors.toList());
    }

}
