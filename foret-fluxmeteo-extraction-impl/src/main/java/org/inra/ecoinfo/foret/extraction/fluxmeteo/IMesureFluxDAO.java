package org.inra.ecoinfo.foret.extraction.fluxmeteo;


import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.PersistenceException;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.flux.entity.MesureFlux;


/**
 * @author philippe
 * 
 * @param <T>
 */
public interface IMesureFluxDAO<T> extends IDAO<MesureFlux> {

    /**
     *
     * @param date
     * @param time
     * @return
     * @throws PersistenceException
     */
    Object[] getLinePublicationNameDoublon(LocalDate date, LocalTime time) throws PersistenceException;
}
