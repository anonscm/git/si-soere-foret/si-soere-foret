package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import java.io.File;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.TreeSet;
import java.util.function.Function;
import java.util.stream.Collectors;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxMeteoParameter;
import org.inra.ecoinfo.foret.extraction.jsf.date.AbstractDatesFormParam;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 * @author philippe
 *
 */
public class FluxMeteoParameterVO extends DefaultParameter implements IFluxMeteoParameter {

    /**
     *
     */
    public final static String SITE_PARAMETER = Nodeable.class.getSimpleName();

    /**
     *
     */
    public final static String VARIABLE_PARAMETER = DatatypeVariableUniteForet.class.getSimpleName();

    /**
     *
     */
    public final static String DATES_PARAMETER = AbstractDatesFormParam.class.getSimpleName();

    /**
     *
     */
    protected static final String FLUX_M = "flux_m";

    /**
     *
     */
    protected static final String FLUX_SH = "flux_sh";

    /**
     *
     */
    protected static final String FLUX_J = "flux_j";

    /**
     *
     */
    protected static final String FLUX_CHAMBRE_M = "chambrefluxsol_m";

    /**
     *
     */
    protected static final String FLUX_CHAMBRE_SH = "chambrefluxsol_infraj";

    /**
     *
     */
    protected static final String FLUX_CHAMBRE_J = "chambrefluxsol_j";

    /**
     *
     */
    protected static final String MENSUEL = "m";

    /**
     *
     */
    protected static final String SEMI_HORAIRE = "sh";

    /**
     *
     */
    protected static final String JOURNALIER = "j";

    /**
     *
     */
    protected static final String METEO_M = "meteo_m";

    /**
     *
     */
    protected static final String METEO_SH = "meteo_sh";

    /**
     *
     */
    protected static final String METEO_J = "meteo_j";

    /**
     *
     */
    protected static final String FLUX_METEO_EXTRACTION_TYPE_CODE = "flux_et_meteo";

    /**
     *
     */
    protected final Map<String, File> filesMap = new HashMap<>();

    /**
     *
     */
    protected final String RYTHME_INVALID = "Le rythme \"%s\" n'est pas un rythme valide.";

    /**
     *
     */
    protected int affichage=1;

    /**
     *
     */
    protected List<ValeurFluxTour> valeurFluxTours = new LinkedList<>();

    /**
     *
     */
    protected List<ValeurMeteo> valeurMeteoTours = new LinkedList<>();

    /**
     *
     */
    protected List<ValeurChambreFluxSol> valeurChambreFluxSols = new LinkedList<>();

    /**
     *
     */
    public FluxMeteoParameterVO() {

    }

    /*
     * (non-Javadoc)
     *
    * @see org.inra.ore.forets.plugins.forets1.ui.flex.vo.IFluxParameter# getSelectedSites()
     */
    @Override
    public List<SiteForet> getSelectedSites() {
        return (List<SiteForet>) Optional.ofNullable(getParameters())
                .map(p -> p.get(Nodeable.class.getSimpleName()))
                .orElseGet(ArrayList::new);
    }

    @Override
    public IntervalDate getIntervalDate() {
        return Optional.ofNullable(getParameters())
                .map(p -> (AbstractDatesFormParam) p.get(AbstractDatesFormParam.class.getSimpleName()))
                .map(dfp -> dfp.intervalsDate())
                .map(l -> l.get(0))
                .orElse(null);
    }

    /**
     *
     * @return
     */
    public List<ValeurChambreFluxSol> getValeurChambreFluxSols() {
        return valeurChambreFluxSols;
    }

    /**
     *
     * @param valeurChambreFluxSols
     */
    public void setValeurChambreFluxSols(List<ValeurChambreFluxSol> valeurChambreFluxSols) {
        this.valeurChambreFluxSols = valeurChambreFluxSols;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ore.forets.plugins.forets1.ui.flex.vo.IFluxParameter#getAffichage ()
     */
    @Override
    public int getAffichage() {
        return this.affichage;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ore.forets.plugins.forets1.ui.flex.vo.IFluxParameter#setAffichage (int)
     */
    /**
     *
     * @param affichage
     */
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }

    @Override
    public String getDatatypeFlux(String datatypeFluxTours) {
        String dataType = "";
        if (null != datatypeFluxTours) {
            switch (datatypeFluxTours) {
                case FluxMeteoParameterVO.SEMI_HORAIRE:
                    dataType = FluxMeteoParameterVO.FLUX_SH;
                    break;
                case FluxMeteoParameterVO.JOURNALIER:
                    dataType = FluxMeteoParameterVO.FLUX_J;
                    break;
                case FluxMeteoParameterVO.MENSUEL:
                    dataType = FluxMeteoParameterVO.FLUX_M;
                    break;
                default:
            }
        }
        return dataType;
    }

    @Override
    public String getDatatypeMeteo(String datatypeMeteo) {
        String dataType = "";
        if (null != datatypeMeteo) {
            switch (datatypeMeteo) {
                case FluxMeteoParameterVO.SEMI_HORAIRE:
                    dataType = FluxMeteoParameterVO.METEO_SH;
                    break;
                case FluxMeteoParameterVO.JOURNALIER:
                    dataType = FluxMeteoParameterVO.METEO_J;
                    break;
                case FluxMeteoParameterVO.MENSUEL:
                    dataType = FluxMeteoParameterVO.METEO_M;
                    break;
                default:
            }
        }
        return dataType;
    }

    @Override
    public String getDatatypeFluxChambres(String datatypeFluxChambre) {
        String dataType = "";
        if (null != datatypeFluxChambre) {
            switch (datatypeFluxChambre) {
                case FluxMeteoParameterVO.SEMI_HORAIRE:
                    dataType = FluxMeteoParameterVO.FLUX_CHAMBRE_SH;
                    break;
                case FluxMeteoParameterVO.JOURNALIER:
                    dataType = FluxMeteoParameterVO.FLUX_CHAMBRE_J;
                    break;
                case FluxMeteoParameterVO.MENSUEL:
                    dataType = FluxMeteoParameterVO.FLUX_CHAMBRE_M;
                    break;
                default:
            }
        }
        return dataType;
    }

    /**
     *
     * @return
     */
    @Override
    public String getExtractionTypeCode() {
        return FluxMeteoParameterVO.FLUX_METEO_EXTRACTION_TYPE_CODE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ore.forets.plugins.forets1.ui.flex.vo.IFluxParameter#getRythme()
     */
    @Override
    public String getRythme() {
        return Optional.ofNullable(getParameters())
                .map(p -> (AbstractDatesFormParam) p.get(AbstractDatesFormParam.class.getSimpleName()))
                .map(dfp -> dfp.getRythme())
                .orElseGet(String::new);
    }

    @Override
    public List<ValeurFluxTour> getValeurFluxTours() {
        return this.valeurFluxTours;
    }

    /*
     * (non-Javadoc)
     *
    * @see org.inra.ore.forets.plugins.forets1.ui.flex.vo.IFluxParameter# setValeurFluxTours(java.util.List)
     */
    @Override
    public void setValeurFluxTours(List<ValeurFluxTour> valeurFluxTours) {
        this.valeurFluxTours = valeurFluxTours;
    }

    @Override
    public List<ValeurMeteo> getValeurMeteoTours() {
        return this.valeurMeteoTours;
    }

    @Override
    public void setValeurFluxChambres(List<ValeurChambreFluxSol> valeurFluxChambre) {
        this.valeurChambreFluxSols = valeurFluxChambre;
    }

    @Override
    public List<ValeurChambreFluxSol> getValeurFluxChambres() {
        return this.valeurChambreFluxSols;
    }

    @Override
    public void setValeurMeteoTours(List<ValeurMeteo> valeurMetoTours) {
        this.valeurMeteoTours = valeurMetoTours;
    }

    /**
     *
     * @return
     */
    @Override
    public Set<FileComp> getAsssociatesFiles() {
        return (Set<FileComp>) Optional.ofNullable(getParameters())
                .map(p -> p.get(FileComp.class.getSimpleName()))
                .orElseGet(TreeSet::new);
    }

    /**
     *
     * @param datatypeForRythme
     * @return
     */
    protected List<DatatypeVariableUniteForet> getListVariable(Function<String, String> datatypeForRythme) {
        return Optional.ofNullable(getParameters())
                .map(p -> (List<DatatypeVariableUniteForet>) p.get(DatatypeVariableUniteForet.class.getSimpleName()))
                .orElseGet(ArrayList::new)
                .stream()
                .filter(dvu -> dvu.getDatatype().getCode().equals(datatypeForRythme.apply(getRythme())))
                .collect(Collectors.toList());
    }

    @Override
    public List<DatatypeVariableUniteForet> getSelectedFluxVariables() {
        return getListVariable(this::getDatatypeFlux);
    }

    @Override
    public List<DatatypeVariableUniteForet> getSelectedMeteoVariables() {
        return getListVariable(this::getDatatypeMeteo);
    }

    @Override
    public List<DatatypeVariableUniteForet> getSelectedFluxChambresVariables() {
        return getListVariable(this::getDatatypeFluxChambres);
    }

    @Override
    public Map<String, File> getFilesMap() {
        return filesMap;
    }
}
