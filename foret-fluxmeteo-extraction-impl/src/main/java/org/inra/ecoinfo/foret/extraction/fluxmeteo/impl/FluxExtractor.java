package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;


import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.impl.DefaultExtractionManager;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxDAO;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxParameter;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.ISiteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author philippe Extraction de flux
 *
 */
public class FluxExtractor extends DefaultExtractionManager implements IExtractor {

    /**
     *
     */
    public static final String RYTHME_INVALID = "Le rythme \"%s\" n'est pas un rythme valide.";
    private static final String RESOURCE_PATH = "%s/%s/%s";
    private static final String RESOURCE_PATH_VARIABLE = "%s/%s/%s/%s";
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     */
    protected IFluxDAO fluxDAO;

    /**
     *
     */
    protected ISiteForetDAO siteDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO;

    /**
     *
     */
    public FluxExtractor() {
        super();
    }

    /**
     *
     * @param parameters
     * @throws BusinessException
     */
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        try {
            String rythme = ((IFluxParameter) parameters).getRythme();
            IntervalDate intervalDate = ((IFluxParameter) parameters).getIntervalDate();
            DataType dataType = this.getDatatype(rythme)
                    .orElseThrow(() -> new BusinessException(String.format(FluxExtractor.RYTHME_INVALID, rythme)));
            List<SiteForet> selectedSites = ((IFluxParameter) parameters).getSelectedSites();
            List<DatatypeVariableUniteForet> selectedVariables = ((IFluxParameter) parameters).getSelectedFluxVariables();
            List<SiteForet> sites = addSites(selectedSites);
            List<ValeurFluxTour> valeursFluxTour = this.fluxDAO.extractFluxTours(sites, selectedVariables, intervalDate.getBeginDate().toLocalDate(), intervalDate.getEndDate().toLocalDate(), rythme, policyManager.getCurrentUser());

            if (!valeursFluxTour.isEmpty()) {

                ((IFluxParameter) parameters).setValeurFluxTours(valeursFluxTour);
            }
        } catch (BusinessException e) {
            String EXTRACTION_ABORTED = "Abandon de l'extraction...";
            this.LOGGER.error(EXTRACTION_ABORTED, e);
            this.sendNotification(EXTRACTION_ABORTED, Notification.ERROR, PersistenceException.getLastCauseExceptionMessage(e), (Utilisateur) policyManager.getCurrentUser());
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param extraction
     */
    @Override
    public void setExtraction(Extraction extraction) {

    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {
        return 0;
    }

    /**
     *
     * @param parameter
     * @return
     */
    public String getExtractName(IParameter parameter) {
        return Constantes.THEME_FLUX;
    }

    /**
     *
     * @return
     */
    public IFluxDAO getFluxDAO() {
        return this.fluxDAO;
    }

    /**
     *
     * @param fluxDAO
     */
    public void setFluxDAO(IFluxDAO fluxDAO) {
        this.fluxDAO = fluxDAO;
    }

    /**
     *
     * @param datatypeVariableUniteForetDAO
     */
    public void setDatatypeVariableUniteForetDAO(IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO) {
        this.datatypeVariableUniteForetDAO = datatypeVariableUniteForetDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteForetDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    private Optional<DataType> getDatatype(String rythme) throws BusinessException {
        Optional<DataType> dataType = null;
        if (null != rythme) {
            switch (rythme) {
                case Constantes.SEMI_HORAIRE:
                    dataType = this.datatypeDAO.getByCode(Constantes.DATATYPE_FREQUENCE_FLUX[0]);
                    break;
                case Constantes.JOURNALIER:
                    dataType = this.datatypeDAO.getByCode(Constantes.DATATYPE_FREQUENCE_FLUX[1]);
                    break;
                case Constantes.MENSUEL:
                    dataType = this.datatypeDAO.getByCode(Constantes.DATATYPE_FREQUENCE_FLUX[2]);
                    break;
                default:
                    throw new BusinessException(String.format(FluxExtractor.RYTHME_INVALID, rythme));
            }
        }
        return dataType;
    }

    /**
     *
     * @param selectedVariables
     * @param dataType
     * @return
     */
    protected List<Variable> addVariables(List<Variable> selectedVariables, DataType dataType) {
        List<Variable> variables = new LinkedList<>();
        for (Variable variableVO : selectedVariables) {
            final DatatypeVariableUniteForet dvu = this.datatypeVariableUniteForetDAO.getAllVariableTypeDonneesByDataType(dataType.getCode()).get(variableVO.getCode());
            Variable var = dvu == null ? null : dvu.getVariable();
            if (var != null) {
                variables.add(var);
            }
        }
        return variables;
    }

    /**
     *
     * @param selectedSites
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    protected List<SiteForet> addSites(List<SiteForet> selectedSites) throws BusinessException {
        List<SiteForet> sites = new LinkedList<>();
        for (SiteForet site : selectedSites) {
            sites.add(this.siteDAO.getByPath(site.getPath())
                    .map(s -> (SiteForet) s)
                    .orElseThrow(() -> new BusinessException("bad site"))
            );
        }
        return sites;
    }
}
