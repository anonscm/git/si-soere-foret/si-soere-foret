package org.inra.ecoinfo.foret.extraction.fluxmeteo;

import java.util.Collection;
import java.util.List;
import java.util.Set;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.utils.IntervalDate;


/**
 * @author philippe
 * 
 */
public interface IMeteoParameter extends IParameter {

    /**
     *
     * @return
     */
    String getRythme();
    
    /**
     *
     * @return
     */
    IntervalDate getIntervalDate();

    /**
     *
     * @return
     */
    List<DatatypeVariableUniteForet> getSelectedMeteoVariables();

    /**
     *
     * @return
     */
    List<SiteForet> getSelectedSites();

    /**
     *
     * @return
     */
    List<ValeurMeteo> getValeurMeteoTours();

    /**
     *
     * @param valeurMeteoTours
     */
    void setValeurMeteoTours(List<ValeurMeteo> valeurMeteoTours);

    /**
     *
     * @return
     */
    Set<FileComp> getAsssociatesFiles();
}
