package org.inra.ecoinfo.foret.extraction.fluxmeteo;

import java.io.File;
import java.util.Map;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author philippe
 *
 */
public interface IFluxMeteoParameter extends IFluxParameter, IMeteoParameter, IFluxChambreParameter {

    /**
     *
     * @return
     */
    int getAffichage();

    /**
     *
     * @param datatypeFlux
     * @return
     * @throws BusinessException
     */
    String getDatatypeFlux(String datatypeFlux) throws BusinessException;

    /**
     *
     * @param datatypeMeteo
     * @return
     * @throws BusinessException
     */
    String getDatatypeMeteo(String datatypeMeteo) throws BusinessException;

    /**
     *
     * @param dataypeFluxChambre
     * @return
     * @throws BusinessException
     */
    String getDatatypeFluxChambres(String dataypeFluxChambre) throws BusinessException;

    /**
     *
     * @return
     */
    Map<String, File> getFilesMap();

}
