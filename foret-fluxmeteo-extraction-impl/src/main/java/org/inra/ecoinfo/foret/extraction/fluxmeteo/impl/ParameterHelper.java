/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.foret.dataset.entity.Valeur;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxMeteoParameter;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.IInformationComplementaireDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class ParameterHelper {

    /**
     *
     */
    protected SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<String, ValeurFluxTour>>> valeursFluxByDateVariable = new TreeMap<>();
    protected SortedMap<SiteForet, SortedMap<LocalDateTime, Map<DatatypeVariableUniteForet, ValeurFluxTour>>> valeursFluxByDateVariable2 = new TreeMap<>();

    /**
     *
     */
    protected SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<String, ValeurMeteo>>> valeursMeteoByDateVariable = new TreeMap<>();
    protected SortedMap<SiteForet, SortedMap<LocalDateTime, Map<DatatypeVariableUniteForet, ValeurMeteo>>> valeursMeteoByDateVariable2 = new TreeMap<>();

    /**
     *
     */
    protected SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<Integer, SortedMap<String, ValeurChambreFluxSol>>>> valeursFluxChambresByDateVariable = new TreeMap<>();
    protected SortedMap<SiteForet, SortedMap<LocalDateTime, SortedMap<Integer, Map<DatatypeVariableUniteForet, ValeurChambreFluxSol>>>> valeursFluxChambresByDateVariable2 = new TreeMap<>();
    protected Map<SiteForet, Integer> hasValues = new HashMap();
    private final ILocalizationManager localizationManager;

    /**
     *
     */
    protected final String extractionCode;

    /**
     *
     */
    protected IntervalDate intervalDate;

    /**
     *
     */
    protected String fluxDatatype = "";

    /**
     *
     */
    protected String meteoDatatype = "";

    /**
     *
     */
    protected String fluxChambresDatatype = "";

    /**
     *
     */
    protected TreeMap<String, SiteForet> sitesVO = new TreeMap<>();
    IFluxMeteoParameter parameterVO;

    /**
     *
     */
    protected List<SiteForet> selectedSites = null;

    /**
     *
     */
    protected List<DatatypeVariableUniteForet> selectedFluxVariables = null;

    /**
     *
     */
    protected List<DatatypeVariableUniteForet> selectedMeteoVariables = null;

    /**
     *
     */
    protected List<DatatypeVariableUniteForet> selectedFluxChambresVariables = null;

    /**
     *
     */
    protected List<String> variableMeteoWithQualityCode = new ArrayList<>();

    /**
     *
     */
    protected List<String> variableFluxWithQualityCode = new ArrayList<>();

    /**
     *
     */
    protected List<String> variableFluxChambresWithInformationComplementaire = new ArrayList<>();

    /**
     *
     */
    protected String rythme = null;

    /**
     *
     */
    protected List<ValeurFluxTour> valeursFluxTours;

    /**
     *
     */
    protected List<ValeurMeteo> valeursMeteoTours;

    /**
     *
     */
    protected List<ValeurChambreFluxSol> valeursFluxChambres;

    /**
     *
     */
    protected TreeMap<Long, DatatypeVariableUniteForet> variablesFluxVO = new TreeMap<>();

    /**
     *
     */
    protected TreeMap<Long, DatatypeVariableUniteForet> variablesMeteoVO = new TreeMap<>();

    /**
     *
     */
    protected TreeMap<Long, DatatypeVariableUniteForet> variablesFluxChambresVO = new TreeMap<>();
    private final IInformationComplementaireDAO informationComplementaireDAO;
    

    /**
     *
     * @param parameters
     * @param localizationManager
     * @throws BusinessException
     * @throws IOException
     */
    public ParameterHelper(IParameter parameters, ILocalizationManager localizationManager, IInformationComplementaireDAO informationComplementaireDAO) throws BusinessException, IOException {
        this.parameterVO = (IFluxMeteoParameter) parameters;
        this.localizationManager = localizationManager;
        this.selectedSites = parameterVO.getSelectedSites();
        this.selectedFluxVariables = parameterVO.getSelectedFluxVariables();
        this.selectedMeteoVariables = parameterVO.getSelectedMeteoVariables();
        this.selectedFluxChambresVariables = parameterVO.getSelectedFluxChambresVariables();
        this.valeursFluxTours = parameterVO.getValeurFluxTours();
        this.valeursMeteoTours = parameterVO.getValeurMeteoTours();
        this.valeursFluxChambres = parameterVO.getValeurFluxChambres();
        this.rythme = parameterVO.getRythme();
        this.fluxDatatype = parameterVO.getDatatypeFlux(this.rythme);
        this.meteoDatatype = parameterVO.getDatatypeMeteo(this.rythme);
        this.fluxChambresDatatype = parameterVO.getDatatypeFluxChambres(this.rythme);
        this.variableFluxWithQualityCode = this.faireVariableWithQualityCode(this.valeursFluxTours);
        this.variableMeteoWithQualityCode = this.faireVariableWithQualityCode(this.valeursMeteoTours);
        this.variableFluxChambresWithInformationComplementaire = this.faireVariableWithInformationsComplementaires(this.valeursFluxChambres);
        this.intervalDate = parameterVO.getIntervalDate();
        this.extractionCode = parameterVO.getExtractionTypeCode();
        this.informationComplementaireDAO = informationComplementaireDAO;
        if ((CollectionUtils.isEmpty(this.valeursFluxTours)
                || CollectionUtils.isEmpty(this.selectedFluxVariables))
                && (CollectionUtils.isEmpty(this.valeursMeteoTours)
                || CollectionUtils.isEmpty(this.selectedMeteoVariables))
                && (CollectionUtils.isEmpty(this.valeursFluxChambres)
                || CollectionUtils.isEmpty(this.selectedFluxChambresVariables))) {
            String noResult = String.format(localizationManager.getMessage(FluxMeteoBuildOutputDisplay.BUNDLE_SOURCE_PATH, FluxMeteoBuildOutputDisplay.PROPERTY_MSG_NO_RESULT_EXTRACT));
            String message = String.format(noResult, parameters.getExtractionTypeCode());
            throw new IOException(message);
        }
    }

    private <T extends Valeur> List<String> faireVariableWithQualityCode(List<T> valeurs) {
        List<String> liste = new ArrayList<>();
        if (valeurs != null) {
            for (T valeur : valeurs) {
                if (valeur.getQualityClass() != null) {
                    RealNode realNode = valeur.getRealNode();
                    if (!liste.contains(((DatatypeVariableUniteForet)realNode.getNodeable()).getCode())) {
                        liste.add(((DatatypeVariableUniteForet)realNode.getNodeable()).getCode());
                    }
                }
            }
        }
        return liste;
    }

    private List<String> faireVariableWithInformationsComplementaires(List<ValeurChambreFluxSol> valeurs) {
        return valeurs.stream()
                .map(v->v.getComplements())
                .flatMap(List::stream)
                .map(complement->complement.getIdentite().getNom())
                .distinct()
                .sorted()
                .collect(Collectors.toList());
    }

    /**
     *
     */
    protected void remplirLesTreeMap() {
        selectedFluxVariables.stream()
                .forEachOrdered((variableFluxVO) -> variablesFluxVO.put(variableFluxVO.getId(), variableFluxVO));
        selectedMeteoVariables.stream()
                .forEachOrdered((variableMeteoVO) -> variablesMeteoVO.put(variableMeteoVO.getId(), variableMeteoVO));
        selectedFluxChambresVariables.stream()
                .forEachOrdered((variableFluxChambres) -> variablesFluxChambresVO.put(variableFluxChambres.getId(), variableFluxChambres));
        selectedSites.forEach((siteVO) -> sitesVO.put(siteVO.getPath(), siteVO));
    }

    Map<String, File> getFilesMap() {
        return parameterVO.getFilesMap();
    }

}
