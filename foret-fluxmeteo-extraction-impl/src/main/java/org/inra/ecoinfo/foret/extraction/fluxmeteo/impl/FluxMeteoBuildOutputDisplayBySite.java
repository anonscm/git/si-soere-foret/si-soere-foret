package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.time.LocalDateTime;
import java.util.Iterator;
import java.util.List;
import java.util.Map.Entry;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.foret.dataset.entity.Valeur;
import org.inra.ecoinfo.foret.dataset.flux.entity.MesureFlux;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import static org.inra.ecoinfo.foret.extraction.fluxmeteo.impl.FluxMeteoBuildOutputDisplay.*;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import static org.inra.ecoinfo.foret.utils.Constantes.*;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.util.CollectionUtils;

/**
 * @author philippe
 *
 */
public class FluxMeteoBuildOutputDisplayBySite extends FluxMeteoBuildOutputDisplay {

    /**
     *
     */
    private static final String PROPERTY_MSG_DATE = "PROPERTY_MSG_DATE";

    /**
     *
     */
    private static final String PROPERTY_MSG_DATE_TIME = "PROPERTY_MSG_DATE_TIME";
    /**
     *
     */
    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     */
    /**
     *
     */
    private PrintStream fluxPrintStream = null;

    /**
     *
     * @param ph
     * @throws FileNotFoundException
     * @throws BusinessException
     * @throws IOException
     */
    @Override
    public void buildOutputData(ParameterHelper ph) throws FileNotFoundException, BusinessException, IOException {
        File extractioFile = null;
        try {
            extractioFile = getFile(ph, DATA);
            this.fluxPrintStream = getStream(ph, extractioFile);
            ph.remplirLesTreeMap();
            ph.valeursFluxByDateVariable = this.remplirPourFlux(ph.sitesVO, ph.variablesFluxVO, ph);
            ph.valeursMeteoByDateVariable = this.remplirPourMeteo(ph.sitesVO, ph.variablesMeteoVO, ph);
            ph.valeursFluxChambresByDateVariable = this.remplirPourChambres(ph.sitesVO, ph.variablesFluxChambresVO, ph);
            this.construireEntete(ph);
            StringBuffer ligne = null;
            SortedSet<LocalDateTime> dates = new TreeSet<>();
            dates.addAll(ph.valeursFluxByDateVariable.keySet());
            dates.addAll(ph.valeursMeteoByDateVariable.keySet());
            dates.addAll(ph.valeursFluxChambresByDateVariable.keySet());
            for (Iterator<LocalDateTime> iteratorDate = dates.iterator(); iteratorDate.hasNext();) {
                LocalDateTime date = iteratorDate.next();
                ligne = new StringBuffer().append((MENSUEL.equals(ph.rythme) ? DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.MM_YYYY) : DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY)))
                        .append(SEMI_HORAIRE.equals(ph.rythme) ? CSY_CSV_SEPARATOR : CST_STRING_EMPTY)
                        .append(SEMI_HORAIRE.equals(ph.rythme) ? DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.HH_MM) : CST_STRING_EMPTY);
                for (Iterator<Entry<String, SiteForet>> iteratorSite = ph.sitesVO.entrySet().iterator(); iteratorSite.hasNext();) {
                    Entry<String, SiteForet> siteEntry = iteratorSite.next();
                    final SiteForet site = siteEntry.getValue();
                    SortedMap<String, ValeurFluxTour> entryFlux = ph.valeursFluxByDateVariable
                            .computeIfAbsent(date, k -> new TreeMap<SiteForet, SortedMap<String, ValeurFluxTour>>())
                            .computeIfAbsent(site, k -> new TreeMap<String, ValeurFluxTour>());
                    SortedMap<String, ValeurMeteo> entryMeteo = ph.valeursMeteoByDateVariable
                            .computeIfAbsent(date, k -> new TreeMap<SiteForet, SortedMap<String, ValeurMeteo>>())
                            .computeIfAbsent(site, k -> new TreeMap<String, ValeurMeteo>());
                    SortedMap<Integer, SortedMap<String, ValeurChambreFluxSol>> entryChambres = ph.valeursFluxChambresByDateVariable
                            .computeIfAbsent(date, k -> new TreeMap<SiteForet, SortedMap<Integer, SortedMap<String, ValeurChambreFluxSol>>>())
                            .computeIfAbsent(site, k -> new TreeMap<Integer, SortedMap<String, ValeurChambreFluxSol>>());

                    writeVariables(ph, ph.selectedFluxVariables, ph.variableFluxWithQualityCode, entryFlux, ligne);
                    writeVariables(ph, ph.selectedMeteoVariables, ph.variableMeteoWithQualityCode, entryMeteo, ligne);
                    writeVariablesChambers(ph, ph.selectedFluxChambresVariables, ph.variableFluxChambresWithInformationComplementaire, entryChambres, ligne);
                    iteratorSite.remove();
                }
                ligne.append(CST_NEW_LINE);
                this.fluxPrintStream.print(ligne);
                iteratorDate.remove();
            }
        } catch (IOException e) {
            throw new IOException(e);
        }
        this.fluxPrintStream.close();
        ph.getFilesMap().put(FluxMeteoBuildOutputDisplay.DATA_FLUX_METEO.concat(getExtention()), extractioFile);
    }

    /**
     *
     * @param <T>
     * @param ph
     * @param selectedVariables
     * @param variableWithQualityCode
     * @param entry
     * @param ligne
     */
    private <T extends Valeur> void writeVariables(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variableWithQualityCode, SortedMap<String, T> entry, StringBuffer ligne) {
        selectedVariables.forEach((variableVO) -> {
            if (entry == null || !entry.containsKey(variableVO.getCode())) {
                ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                if (variableWithQualityCode.contains(variableVO.getCode())) {
                    ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                }
            } else {

                ligne.append(CSY_CSV_SEPARATOR).append(this.format(entry.get(variableVO.getCode()).getValue(), 4));
                if (variableWithQualityCode.contains(variableVO.getCode())) {
                    T valeur = entry.get(variableVO.getCode());
                    if (valeur.getQualityClass() != null) {
                        ligne.append(CSY_CSV_SEPARATOR).append(entry.get(variableVO.getCode()).getQualityClass().getNumero());
                    } else {
                        ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                    }
                }
            }
        });
    }

    private <T extends Valeur> void writeVariablesChambers(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variableWithQualityCode, SortedMap<Integer, SortedMap<String, T>> entrySite, StringBuffer ligne) {
        for (Entry<Integer, SortedMap<String, T>> noChamberEntry : entrySite.entrySet()) {
            SortedMap<String, T> entry = noChamberEntry.getValue();
            selectedVariables.forEach((variableVO) -> {
                if (entry == null || !entry.containsKey(variableVO.getCode())) {
                    ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                    if (variableWithQualityCode.contains(variableVO.getCode())) {
                        ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                    }
                } else {

                    ligne.append(CSY_CSV_SEPARATOR).append(this.format(entry.get(variableVO.getCode()).getValue(), 4));
                    if (variableWithQualityCode.contains(variableVO.getCode())) {
                        T valeur = entry.get(variableVO.getCode());
                        if (valeur.getQualityClass() != null) {
                            ligne.append(CSY_CSV_SEPARATOR).append(entry.get(variableVO.getCode()).getQualityClass().getNumero());
                        } else {
                            ligne.append(CSY_CSV_SEPARATOR).append(PROPERTY_CST_NOT_AVALAIBALE);
                        }
                    }
                }
            });
        }
    }

    /**
     *
     * @return
     */
    @Override
    public Logger getLogger() {
        return this.LOGGER;
    }

    /**
     *
     * @param sitesVO
     * @param variablesFluxVO
     * @param ph
     * @return
     */
    private SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<String, ValeurFluxTour>>> remplirPourFlux(TreeMap<String, SiteForet> sitesVO, TreeMap<Long, DatatypeVariableUniteForet> variablesFluxVO, ParameterHelper ph) {
        SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<String, ValeurFluxTour>>> valeursFluxByDateVariable = new TreeMap<>();
        for (Iterator<ValeurFluxTour> iterator = ph.valeursFluxTours.iterator(); iterator.hasNext();) {
            ValeurFluxTour valeurFluxTour = iterator.next();
            if (valeurFluxTour == null) {
                continue;
            }
            MesureFlux mesureFlux = valeurFluxTour.getMesure();
            LocalDateTime date = buildDateComplete(SEMI_HORAIRE.equals(ph.rythme), mesureFlux.getDate(), mesureFlux.getTime());
            if (valeursFluxByDateVariable.get(date) == null) {
                valeursFluxByDateVariable.put(date, new TreeMap<>());
            }
            SortedMap<SiteForet, SortedMap<String, ValeurFluxTour>> etageSite = valeursFluxByDateVariable.get(date);
            SiteForet site = sitesVO.get(mesureFlux.getSite().getPath());
            if (etageSite.get(site) == null) {
                etageSite.put(site, new TreeMap<>());
            }
            SortedMap<String, ValeurFluxTour> etageVariable = etageSite.get(site);
            DatatypeVariableUniteForet variableEtudiee = variablesFluxVO.get(valeurFluxTour.getVariable().getId());
            etageVariable.put(variableEtudiee.getCode(), valeurFluxTour);
            iterator.remove();
        }
        return valeursFluxByDateVariable;
    }

    private SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<String, ValeurMeteo>>> remplirPourMeteo(TreeMap<String, SiteForet> sitesVO, TreeMap<Long, DatatypeVariableUniteForet> variablesMeteoVO, ParameterHelper ph) {
        SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<String, ValeurMeteo>>> valeursFluxByDateVariable = new TreeMap<>();
        for (Iterator<ValeurMeteo> iterator = ph.valeursMeteoTours.iterator(); iterator.hasNext();) {
            ValeurMeteo valeurMeteo = iterator.next();
            if (valeurMeteo == null) {
                continue;
            }
            MesureMeteo mesureMeteo = valeurMeteo.getMesure();
            LocalDateTime date = buildDateComplete(SEMI_HORAIRE.equals(ph.rythme), mesureMeteo.getDate(), mesureMeteo.getTime());

            if (valeursFluxByDateVariable.get(date) == null) {
                valeursFluxByDateVariable.put(date, new TreeMap<>());
            }
            SortedMap<SiteForet, SortedMap<String, ValeurMeteo>> etageSite = valeursFluxByDateVariable.get(date);
            SiteForet site = sitesVO.get(mesureMeteo.getSite().getPath());
            if (etageSite.get(site) == null) {
                etageSite.put(site, new TreeMap<>());
            }
            SortedMap<String, ValeurMeteo> etageVariable = etageSite.get(site);
            DatatypeVariableUniteForet variableEtudiee = variablesMeteoVO.get(valeurMeteo.getVariable().getId());
            etageVariable.put(variableEtudiee.getCode(), valeurMeteo);
            iterator.remove();
        }
        return valeursFluxByDateVariable;
    }

    private SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<Integer, SortedMap<String, ValeurChambreFluxSol>>>> remplirPourChambres(TreeMap<String, SiteForet> sitesVO, TreeMap<Long, DatatypeVariableUniteForet> variablesChambres, ParameterHelper ph) {
        SortedMap<LocalDateTime, SortedMap<SiteForet, SortedMap<Integer, SortedMap<String, ValeurChambreFluxSol>>>> valeursFluxChambresByDateVariable = new TreeMap<>();
        for (Iterator<ValeurChambreFluxSol> iterator = ph.valeursFluxChambres.iterator(); iterator.hasNext();) {
            ValeurChambreFluxSol valeurChambreFluxSol = iterator.next();
            if (valeurChambreFluxSol == null) {
                continue;
            }
            MesureChambreFluxSol mesureFluxChambres = valeurChambreFluxSol.getMesureFluxChambre();
            LocalDateTime date = buildDateComplete(SEMI_HORAIRE.equals(ph.rythme), mesureFluxChambres.getDate(), mesureFluxChambres.getTime());
            SiteForet site = sitesVO.get(mesureFluxChambres.getSite().getPath());
            final String noChambreString = valeurChambreFluxSol.getMesureFluxChambre().getNoChambre();
            final Integer noChambre = Strings.isNullOrEmpty(noChambreString)?-1:Integer.parseInt(noChambreString);
            DatatypeVariableUniteForet variableEtudiee = variablesChambres.get(valeurChambreFluxSol.getVariable().getId());
            valeursFluxChambresByDateVariable
                    .computeIfAbsent(date, k -> new TreeMap<SiteForet, SortedMap<Integer, SortedMap<String, ValeurChambreFluxSol>>>())
                    .computeIfAbsent(site, k -> new TreeMap<Integer, SortedMap<String, ValeurChambreFluxSol>>())
                    .computeIfAbsent(noChambre, k -> new TreeMap<String, ValeurChambreFluxSol>())
                    .computeIfAbsent(variableEtudiee.getCode(), k -> valeurChambreFluxSol);
            iterator.remove();
        }
        return valeursFluxChambresByDateVariable;
    }

    /**
     *
     * @param ph
     * @param ph
     */
    private void construireEntete(ParameterHelper ph) {
        StringBuilder enTete1 = new StringBuilder(CST_STRING_EMPTY);
        StringBuilder enTete2 = new StringBuilder(CST_STRING_EMPTY);
        enTete1 = this.faireLigneNomsDesSites(enTete1, ph);
        enTete2 = this.faireLigneNomsDesThemes(enTete2, ph);
        enTete1 = enTete1.append(CST_NEW_LINE).append(enTete2).append(CST_NEW_LINE)
                .append(SEMI_HORAIRE.equals(ph.rythme)
                        ? this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_DATE_TIME)
                        : this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_DATE));
        enTete1 = this.faireLigneNomDesVariables(enTete1, ph);
        enTete1 = enTete1.append(CST_NEW_LINE);
        enTete1 = this.faireLigneUnites(enTete1, ph);
        enTete1 = enTete1.append(CST_NEW_LINE);
        this.fluxPrintStream.print(enTete1);
    }

    /**
     *
     * @param ligne
     * @param ph
     * @return
     */
    private StringBuilder faireLigneNomDesVariables(StringBuilder ligne, ParameterHelper ph) {
        for (int i = 0; i < ph.sitesVO.size(); i++) {
            ligne = writeVariablesHeader(ph, ph.selectedFluxVariables, ph.variableFluxWithQualityCode, ligne);
            ligne = writeVariablesHeader(ph, ph.selectedMeteoVariables, ph.variableMeteoWithQualityCode, ligne);
            ligne = writeVariablesHeader(ph, ph.selectedFluxChambresVariables, ph.variableFluxChambresWithInformationComplementaire, ligne);
        }
        return ligne;
    }

    private StringBuilder writeVariablesHeader(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variableWithQualityCode, StringBuilder ligne) {
        for (DatatypeVariableUniteForet variableVO : selectedVariables) {
            ligne = ligne.append(CSY_CSV_SEPARATOR).append(variableVO.getCode());
            if (variableWithQualityCode.contains(variableVO.getCode())) {
                ligne = ligne.append(FluxMeteoBuildOutputDisplay.QUALITY_CLASS_FIELD);
            }
        }
        return ligne;
    }

    /**
     *
     * @param ligne
     * @param ph
     * @return
     */
    private StringBuilder faireLigneNomsDesSites(StringBuilder ligne, ParameterHelper ph) {
        ligne = ligne.append(SEMI_HORAIRE.equals(ph.rythme) ? ";;" : ";");
        for (Entry<String, SiteForet> siteEntry : ph.sitesVO.entrySet()) {
            ligne = ligne.append(siteEntry.getValue().getName());
            ligne = writeLigneSiteName(ph, ph.selectedFluxVariables, ph.variableFluxWithQualityCode, ligne);
            ligne = writeLigneSiteName(ph, ph.selectedMeteoVariables, ph.variableMeteoWithQualityCode, ligne);
            ligne = writeLigneSiteName(ph, ph.selectedFluxChambresVariables, ph.variableFluxChambresWithInformationComplementaire, ligne);
        }
        return ligne;
    }

    private StringBuilder writeLigneSiteName(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variableWithQualityCode, StringBuilder ligne) {
        for (DatatypeVariableUniteForet variableVO : selectedVariables) {
            ligne = ligne.append(CSY_CSV_SEPARATOR);
            if (variableWithQualityCode.contains(variableVO.getCode())) {
                ligne = ligne.append(CSY_CSV_SEPARATOR);
            }
        }
        return ligne;
    }

    /**
     *
     * @param ligne
     * @param ph
     * @return
     */
    private StringBuilder faireLigneNomsDesThemes(StringBuilder ligne, ParameterHelper ph) {
        ligne = ligne.append(SEMI_HORAIRE.equals(ph.rythme) ? ";;" : ";");
        for (int i = 0; i < ph.sitesVO.size(); i++) {
            ligne = writeLineThematic(ph, ph.selectedFluxVariables, ph.variableFluxWithQualityCode, PROPERTY_MSG_FLOW, ligne);
            ligne = writeLineThematic(ph, ph.selectedMeteoVariables, ph.variableMeteoWithQualityCode, PROPERTY_MSG_METEO, ligne);
            ligne = writeLineThematic(ph, ph.selectedFluxChambresVariables, ph.variableFluxChambresWithInformationComplementaire, PROPERTY_MSG_CHAMBER, ligne);

        }
        return ligne;
    }

    private StringBuilder writeLineThematic(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variableWithQualityCode, String thematicName, StringBuilder ligne) {
        ligne = ligne.append(this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, thematicName));
        for (DatatypeVariableUniteForet variableVO : selectedVariables) {

            ligne = ligne.append(CSY_CSV_SEPARATOR);
            if (variableWithQualityCode.contains(variableVO.getCode())) {
                ligne = ligne.append(CSY_CSV_SEPARATOR);
            }
        }
        return ligne;
    }

    /**
     *
     * @param ligne
     * @param ph
     * @return
     */
    private StringBuilder faireLigneUnites(StringBuilder ligne, ParameterHelper ph) {
        ligne = ligne.append(SEMI_HORAIRE.equals(ph.rythme) ? ";;" : ";");
        for (int i = 0; i < ph.sitesVO.size(); i++) {
            ligne = writeUniteLine(ph, ph.selectedFluxVariables, ph.variableFluxWithQualityCode, ligne, ph.fluxDatatype);
            ligne = writeUniteLine(ph, ph.selectedMeteoVariables, ph.variableMeteoWithQualityCode, ligne, ph.meteoDatatype);
            ligne = writeUniteLine(ph, ph.selectedFluxChambresVariables, ph.variableFluxChambresWithInformationComplementaire, ligne, ph.fluxChambresDatatype);

        }
        return ligne;
    }

    private StringBuilder writeUniteLine(ParameterHelper ph, List<DatatypeVariableUniteForet> selectedVariables, List<String> variableWithQualityCode, StringBuilder ligne, String datatype) {
        for (DatatypeVariableUniteForet variableVO : selectedVariables) {
            String unite = variableVO.getUnite().getCode();
            if (unite != null) {
                ligne = ligne.append(unite).append(CSY_CSV_SEPARATOR);
            } else {
                ligne = ligne.append(CSY_CSV_SEPARATOR).append(CSY_CSV_SEPARATOR);
            }
            if (variableWithQualityCode.contains(variableVO.getCode())) {
                ligne = ligne.append(CST_STAR).append(CSY_CSV_SEPARATOR);
            }
        }
        return ligne;
    }
}
