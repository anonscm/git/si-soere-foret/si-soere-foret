package org.inra.ecoinfo.foret.extraction.fluxmeteo;

import org.inra.ecoinfo.foret.extraction.IForetDatasetManager;

/**
 *
 * @author ptcherniati
 */
public interface IFluxMeteoDatasetManager extends IForetDatasetManager {

}