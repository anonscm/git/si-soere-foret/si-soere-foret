package org.inra.ecoinfo.foret.extraction.fluxmeteo.jpa;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Fetch;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.foret.dataset.flux.entity.MesureFlux;
import org.inra.ecoinfo.foret.dataset.flux.entity.MesureFlux_;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour_;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFlux_jTour;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFlux_jTour_;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFlux_mTour;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFlux_mTour_;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFlux_shTour;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationComplementaire;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author philippe DAO d'extraction des données de flux
 *
 */
public class JPAFluxDAO extends AbstractJPADAO<ValeurFluxTour> implements IFluxDAO {

    private final Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     * @param selectedSites
     * @param selectedVariables
     * @param dateDebut
     * @param dateFin
     * @param rythme
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    protected <T extends ValeurFluxTour> List<T> extractFluxTours(Class<T> valueClass, String mesureFluxAttribute, List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, IUser user) {
        if (selectedSites == null || selectedSites.isEmpty() || selectedVariables == null || selectedVariables.isEmpty() || dateDebut == null || dateFin == null) {
            return new LinkedList<>();
        }
        CriteriaQuery<T> query = builder.createQuery(valueClass);
        Root<T> v = query.from(valueClass);
        Fetch<T, ValeurInformationComplementaire> complements = v.fetch(ValeurFluxTour_.complements, JoinType.LEFT);
        Join<T, MesureFlux> m = v.join(mesureFluxAttribute);
        Join<T, RealNode> dvuRealNode = v.join(ValeurFluxTour_.realNode);
        Join<RealNode, DatatypeVariableUniteForet> dvu = builder.treat(dvuRealNode.join(RealNode_.nodeable), DatatypeVariableUniteForet.class);
        Join<MesureFlux, VersionFile> version = m.join(MesureFlux_.version);
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Join<Dataset, RealNode> datasetRealNode = dataset.join(Dataset_.realNode);
        Join<RealNode, RealNode> themeRealNode = datasetRealNode.join(RealNode_.parent);
        Join<RealNode, RealNode> siteRealNode = themeRealNode.join(RealNode_.parent);
        Join<RealNode, SiteForet> site = builder.treat(siteRealNode.join(RealNode_.nodeable), SiteForet.class);
        List<Predicate> and = new LinkedList();
        and.add(site.in(selectedSites));
        and.add(dvu.in(selectedVariables));
        if (!user.getIsRoot()) {
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            and.add(builder.equal(nds.get(NodeDataSet_.realNode), dvuRealNode));
            Outils.addRestrictiveRequestOnRoles(user, query, and, builder, nds, m.<LocalDate>get(MesureFlux_.date));
        }

        and.add(builder.between(m.<LocalDate>get(MesureFlux_.date), dateDebut, dateFin));
        query.
                select(v)
                .where(and.toArray(new Predicate[0]));
        return getResultList(query);
    }

    private Optional<Class<?extends ValeurFluxTour>> getValueClassFromRythme(String rythme) {
        switch (rythme) {
            case Constantes.SEMI_HORAIRE:
                return Optional.of(ValeurFlux_shTour.class);
            case Constantes.JOURNALIER:
                return Optional.of(ValeurFlux_jTour.class);
            case Constantes.MENSUEL:
                return Optional.of(ValeurFlux_mTour.class);
            default:
                return Optional.empty();
        }
    }

    private String getMesureAttribute(String rythme) {
        switch (rythme) {
            case Constantes.SEMI_HORAIRE:
                return ValeurFlux_jTour_.mesure.getName();
            case Constantes.JOURNALIER:
                return ValeurFlux_jTour_.mesure.getName();
            case Constantes.MENSUEL:
                return ValeurFlux_mTour_.mesure.getName();
            default:
                return "";
        }
    }

    @Override
    public List<ValeurFluxTour> extractFluxTours(List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, String rythme, IUser user) {
        Optional<Class<? extends ValeurFluxTour>> classOpt = getValueClassFromRythme(rythme);
        if(!classOpt.isPresent()){
            return new LinkedList();
        }
        return extractFluxTours(classOpt.get(), getMesureAttribute(rythme), selectedSites, selectedVariables, dateDebut, dateFin, user)
                .stream()
                .map(v->(ValeurFluxTour)v)
                .collect(Collectors.toList());
    }

}
