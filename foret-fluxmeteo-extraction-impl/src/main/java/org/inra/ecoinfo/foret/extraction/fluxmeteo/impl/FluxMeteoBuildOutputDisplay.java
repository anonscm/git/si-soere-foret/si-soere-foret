package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.text.ParseException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.zip.ZipOutputStream;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.extraction.entity.IProspecteurUnite;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.IInformationComplementaireDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.foret.utils.LoggerForExtraction;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.FileWithFolderCreator;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

abstract class FluxMeteoBuildOutputDisplay extends AbstractOutputBuilder implements IOutputBuilder {


    /*
     * protected class ComparatorVariable implements Comparator<Variable> {
     * 
     * @Override public int compare(Variable arg0, Variable arg1) { if (arg0.getId() == arg1.getId()) return 0; return arg0.getId() > arg1.getId() ? 1 : 0; } }
     * 
     * protected final ComparatorVariable comparator = new ComparatorVariable();
     */
 /**/
 /* internationalization */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.extraction.impl.messages";
    protected static final String BUNDLE_LOCAL_SOURCE_PATH = "org.inra.ecoinfo.foret.extraction.fluxmeteo.messages";

    protected static final String PROPERTY_CST_EXTRACTION = "PROPERTY_CST_EXTRACTION";
    protected static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";
    protected static final String PROPERTY_CST_UNDERSCORE = "PROPERTY_CST_UNDERSCORE";
    protected static final String PROPERTY_CST_ZIP_SUFFIX = "PROPERTY_CST_ZIP_SUFFIX";
    protected static final String PROPERTY_CST_FILE_SUFFIX = "PROPERTY_CST_FILE_SUFFIX";
    protected static final String PROPERTY_CST_HYPHEN = "PROPERTY_CST_HYPHEN";
    protected static final String PROPERTY_CST_SEPARATOR_FILE = "PROPERTY_CST_SEPARATOR_FILE";
    protected static final String EXTENSION_TXT = ".txt";
    protected static final String CHARACTER_ENCODING_ISO_8859_1 = "ISO-8859-1";
    protected static final String CHARACTER_ENCODING_UTF8 = "UTF-8";
    protected static final String QUALITY_CLASS_FIELD = ";*";
    protected static final String DATA_FLUX_METEO = "flux_meteo_dataResult";
    protected static final String DATA_FLUX_CHAMBERS = "fluxchambers_dataResult";
    protected static final String REQUEST_REMINDER = "request_reminder";

    protected static final String PROPERTY_MSG_BAD_PARAMETERS = "PROPERTY_MSG_BAD_PARAMETERS";
    protected static final String PROPERTY_MSG_EXTRACTION_TIME = "PROPERTY_MSG_EXTRACTION_TIME";
    protected static final String PROPERTY_CST_TAB2 = ".*\t";
    protected static final String PROPERTY_CST_TAB = "\t -";
    protected static final String PROPERTY_CST_TAB_LINE = "\t-%s";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_FROM_TO = "PROPERTY_MSG_REQUEST_REMINDER_FROM_TO";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_COMMENT = "PROPERTY_MSG_REQUEST_REMINDER_COMMENT";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_DATE = "PROPERTY_MSG_REQUEST_REMINDER_DATE";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES_METEO = "PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES_METEO";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES = "PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES_CHAMBERS = "PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES_CHAMBERS";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_PLACES = "PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_PLACES";
    protected static final String PROPERTY_CST_MSG_FREQUENCY = "PROPERTY_CST_MSG_FREQUENCY";
    protected static final String PROPERTY_MSG_REQUEST_REMINDER_TITLE = "PROPERTY_MSG_REQUEST_REMINDER_TITLE";

    protected static final String PROPERTY_MSG_METEO = "PROPERTY_MSG_METEO";
    protected static final String PROPERTY_MSG_FLOW = "PROPERTY_MSG_FLOW";
    protected static final String PROPERTY_MSG_CHAMBER = "PROPERTY_MSG_CHAMBER";
    protected static final Logger LOGGER = LoggerFactory.getLogger(FluxMeteoBuildOutputDisplay.class.getName());
    protected static final int FLUX_TOWER_CONSTRUCTION = 1;
    protected static final int METEO_CONSTRUCTION = 2;
    protected static final int FLUX_CHAMBERS_CONSTRUCTION = 4;

    protected static String dateComplete(final String dateToString, String timeToString) {
        return new StringBuilder(dateToString).append(Constantes.CST_SPACE).append(timeToString).toString();
    }

    private IFileCompConfiguration fileCompConfiguration;
    protected IProspecteurUnite prospecteurUnite;
    protected ILocalizationManager localizationManager;
    protected IInformationComplementaireDAO informationComplementaireDAO;

    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException {
        RObuildZipOutputStream rObuildZipOutputStream = null;
        try {
             rObuildZipOutputStream = super.buildOutput(parameters);
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                ParameterHelper ph = this.recupererParameters(parameters);
                buildRequestRemainders(ph);
                if (!CollectionUtils.isEmpty(ph.valeursFluxTours)
                        || !CollectionUtils.isEmpty(ph.valeursMeteoTours)
                        || !CollectionUtils.isEmpty(ph.valeursFluxChambres)) {
                    this.buildOutputData(ph);
                }
                AbstractIntegrator.embedInZip(zipOutputStream, ph.getFilesMap());
                zipOutputStream.flush();
            } catch (ParseException ex) {
                LOGGER.error(ex.getMessage());
            }
        } catch (IOException e) {
            throw new BusinessException(e);
        }
        return rObuildZipOutputStream;
    }

    @Override
    public ILocalizationManager getLocalizationManager() {
        return this.localizationManager;
    }

    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    public void removeFile(String fileName) {
        File fluxFile = null;
        fluxFile = new File(fileName);
        fluxFile.delete();
    }

    public void setProspecteurUnite(IProspecteurUnite prospecteurUnite) {
        this.prospecteurUnite = prospecteurUnite;
    }

    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }
    
    protected abstract void buildOutputData(ParameterHelper ph) throws FileNotFoundException, BusinessException, IOException;

    /**
     * <p>return a new tmp file <p>
     * <p>return file in extractionPath/extractiontypeCode(ph)/extractiontypeCode(ph)_fileRandomSuffix.extention</p>
     * 
     * @param ph
     * @param fileName
     * @return 
     */
    protected File getFile(ParameterHelper ph, String fileName) {
        String fs = getFs();
        String separator = getSeparator();
        String extractionPath = buildExtractionPath(fs, fileName);
        String hyphen = String.format(this.localizationManager.getMessage(FluxMeteoBuildOutputDisplay.BUNDLE_SOURCE_PATH, FluxMeteoBuildOutputDisplay.PROPERTY_CST_HYPHEN));
        String fileRandomSuffix = Long.toString(Instant.now().toEpochMilli()).concat(hyphen).concat(Double.toString(Math.random() * 1_000_000_000).substring(0, 6));
        return getExtractionFile(extractionPath, ph, fs, separator, fileRandomSuffix);
    }

    protected String getSeparator() {
        return String.format(this.localizationManager.getMessage(FluxMeteoBuildOutputDisplay.BUNDLE_SOURCE_PATH, FluxMeteoBuildOutputDisplay.PROPERTY_CST_UNDERSCORE));
    }

    protected String getFs() {
        return System.getProperty(String.format(this.localizationManager.getMessage(FluxMeteoBuildOutputDisplay.BUNDLE_SOURCE_PATH, FluxMeteoBuildOutputDisplay.PROPERTY_CST_SEPARATOR_FILE)));
    }

    /**
     * <p>build path of a repository filename for file filename in repositoryURI</p>
     * <p> ex : repository/URI/filename/ </p>
     * 
     * @param fs separator
     * @param fileName
     * @return 
     */
    protected String buildExtractionPath(String fs, String fileName) {
        return this.configuration.getRepositoryURI().concat(fs).concat(fileName).concat(fs);
    }

    protected PrintStream getStream(ParameterHelper ph, File fluxFile) throws IOException {
        String extension = getExtention();
        PrintStream fluxPrintStream = null;
        try {
            fluxPrintStream = new PrintStream(fluxFile, FluxMeteoBuildOutputDisplay.CHARACTER_ENCODING_ISO_8859_1);
        } catch (FileNotFoundException e) {
            throw new IOException(e);
        }
        return fluxPrintStream;
    }

    protected String buildRequestRemainders(ParameterHelper ph) throws IOException, ParseException {
        File remiderFile = getFile(ph, FluxMeteoBuildOutputDisplay.REQUEST_REMINDER);
        String requestreminder;
        try (PrintStream fluxPrintStream = getStream(ph, remiderFile)) {
            writeTitle(fluxPrintStream);
            writeSites(ph.selectedSites, fluxPrintStream);
            writeVariables(ph.selectedFluxVariables, fluxPrintStream, PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES);
            writeVariables(ph.selectedMeteoVariables, fluxPrintStream, PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES_METEO);
            writeVariables(ph.selectedFluxChambresVariables, fluxPrintStream, PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_VARIABLES_CHAMBERS);
            writeFrequecyAndDates(fluxPrintStream, ph);
            writeComment(fluxPrintStream, ph);
            addMessageNoResultExtract(ph, fluxPrintStream);
            requestreminder = fluxPrintStream.toString();
        }
        ph.getFilesMap().put(FluxMeteoBuildOutputDisplay.REQUEST_REMINDER.concat(FluxMeteoBuildOutputDisplay.EXTENSION_TXT), remiderFile);
        requestreminder.replaceFirst(PROPERTY_CST_TAB2, Constantes.CST_STRING_EMPTY);
        LoggerForExtraction.logRequest((Utilisateur) policyManager.getCurrentUser(), remiderFile);
        return requestreminder;
    }

    protected void writeComment(PrintStream fluxPrintStream, ParameterHelper ph) {
        String commentaires = null;
        commentaires = ph.parameterVO.getCommentaire();
        fluxPrintStream.append(String.format(this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_REQUEST_REMINDER_COMMENT)));
        fluxPrintStream.append(commentaires);
        fluxPrintStream.println();
        fluxPrintStream.println();
    }

    protected void writeTitle(PrintStream fluxPrintStream) {
        fluxPrintStream.println(String.format(FORETRecorder.getForetMessageWithBundle(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_REQUEST_REMINDER_TITLE)));
        fluxPrintStream.println();
        fluxPrintStream.println();
    }

    protected void addMessageNoResultExtract(ParameterHelper ph, PrintStream fluxPrintStream) {
        if (CollectionUtils.isEmpty(ph.valeursFluxTours) && CollectionUtils.isEmpty(ph.valeursMeteoTours) && CollectionUtils.isEmpty(ph.valeursFluxChambres)) {
            fluxPrintStream.println();
            String noResult = this.localizationManager.getMessage(FluxMeteoBuildOutputDisplay.BUNDLE_SOURCE_PATH, FluxMeteoBuildOutputDisplay.PROPERTY_MSG_NO_RESULT_EXTRACT);
            String message = String.format(noResult, ph.extractionCode);
            fluxPrintStream.println(message);
        }
    }

    protected void writeFrequecyAndDates(PrintStream fluxPrintStream, ParameterHelper ph) {
        fluxPrintStream.println();
        fluxPrintStream.append(String.format(this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_CST_MSG_FREQUENCY)));
        fluxPrintStream.println();
        fluxPrintStream.append(String.format(PROPERTY_CST_TAB_LINE, ph.rythme));
        fluxPrintStream.println();
        fluxPrintStream.append(String.format(this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_REQUEST_REMINDER_DATE)));
        fluxPrintStream.println();
        fluxPrintStream.append(String.format(
                this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_REQUEST_REMINDER_FROM_TO), 
                DateUtil.getUTCDateTextFromLocalDateTime(ph.intervalDate.getBeginDate(), Constantes.MENSUEL.equals(ph.rythme)?DateUtil.MM_YYYY:DateUtil.DD_MM_YYYY), 
                DateUtil.getUTCDateTextFromLocalDateTime(ph.intervalDate.getEndDate(), Constantes.MENSUEL.equals(ph.rythme)?DateUtil.MM_YYYY:DateUtil.DD_MM_YYYY)
        ));
        fluxPrintStream.println();
    }

    protected void writeVariables(List<DatatypeVariableUniteForet> selectedVariables1, PrintStream fluxPrintStream, String typeVariable) {
        if (selectedVariables1 != null && selectedVariables1.size() > 0) {
            fluxPrintStream.println(String.format(this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, typeVariable)));
            fluxPrintStream.println();
            for (DatatypeVariableUniteForet dvu : selectedVariables1) {
                final Variable variable = dvu.getVariable();
                fluxPrintStream.append(PROPERTY_CST_TAB).append(variable.getCode()).append(" (").append(variable.getName()).append(")");
                fluxPrintStream.println();
            }
        }
    }

    protected void writeSites(List<SiteForet> selectedSites1, PrintStream fluxPrintStream) {
        if (selectedSites1 != null && selectedSites1.size() > 0) {
            fluxPrintStream.println(String.format(this.localizationManager.getMessage(BUNDLE_LOCAL_SOURCE_PATH, PROPERTY_MSG_REQUEST_REMINDER_CHOOSEN_PLACES)));
            fluxPrintStream.println();
            for (SiteForet site : selectedSites1) {
                fluxPrintStream.append(PROPERTY_CST_TAB).append(localizationManager.getLocalName(site));
                fluxPrintStream.println();
            }
        }
    }

    protected String format(float nombre, int nombreDeChiffresSignificatifs) {
        if (Float.floatToIntBits(nombre) == 0) {
            return "0";
        }
        int exposant = 0;
        float arrondi = (float) Math.pow(10, nombreDeChiffresSignificatifs - 1);
        while (Math.abs(nombre) <= arrondi) {
            nombre *= 10;
            exposant++;
        }
        return new StringBuffer().append(Math.round(nombre) / Math.pow(10, exposant)).toString();
    }

    protected abstract Logger getLogger();

    /**
     * <p>return file in extractionPath/extractiontypeCode(ph)/extractiontypeCode(ph)_fileRandomSuffix.extention</p>
     * 
     * @param extractionPath
     * @param ph
     * @param fs
     * @param separator
     * @param fileRandomSuffix
     * @return 
     */
    protected File getExtractionFile(String extractionPath, ParameterHelper ph, String fs, String separator, String fileRandomSuffix) {
        String extension = getExtention();
        return FileWithFolderCreator.createFile(extractionPath.concat(ph.parameterVO.getExtractionTypeCode()).concat(fs).concat(ph.parameterVO.getExtractionTypeCode()).concat(separator).concat(fileRandomSuffix).concat(getExtention()));
    }

    protected String getExtention() {
        return String.format(this.localizationManager.getMessage(FluxMeteoBuildOutputDisplay.BUNDLE_SOURCE_PATH, FluxMeteoBuildOutputDisplay.PROPERTY_CST_FILE_SUFFIX));
    }

    protected ParameterHelper recupererParameters(IParameter parameters) throws IOException, BusinessException {
        return new ParameterHelper(parameters, localizationManager, informationComplementaireDAO);
    }

    public void setInformationComplementaireDAO(IInformationComplementaireDAO informationComplementaireDAO) {
        this.informationComplementaireDAO = informationComplementaireDAO;
    }

    protected LocalDateTime buildDateComplete(boolean isSemHoraire, LocalDate date, LocalTime time) {
        return isSemHoraire?date.atTime(time):date.atStartOfDay();
    }

    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException{
        return "";
    }

    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException{
        return new HashMap();        
    }
}
