package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import org.inra.ecoinfo.extraction.IExtractor;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.impl.DefaultExtractionManager;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IMeteoDAO;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IMeteoParameter;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.ISiteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.notifications.entity.Notification;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author philippe Extraction de météo
 *
 */
public class MeteoExtractor extends DefaultExtractionManager implements IExtractor {

    /**
     *
     */
    public static final String RYTHME_INVALID = "Le rythme \"%s\" n'est pas un rythme valide.";
    private static final Logger LOGGER = LoggerFactory.getLogger(MeteoExtractor.class);
    private static final String RESOURCE_PATH = "%s/%s/%s";
    private static final String RESOURCE_PATH_VARIABLE = "%s/%s/%s/%s";

    /**
     *
     */
    protected IMeteoDAO meteoDAO;

    /**
     *
     */
    protected ISiteForetDAO siteDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO;

    /**
     *
     * @param parameters
     * @throws BusinessException
     */
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        try {
            String rythme = ((IMeteoParameter) parameters).getRythme();
            IntervalDate intervalDate = ((IMeteoParameter) parameters).getIntervalDate();
            DataType dataType = this.getDatatype(rythme)
                    .orElseThrow(()->new BusinessException(String.format(MeteoExtractor.RYTHME_INVALID, rythme)));
            List<SiteForet> selectedSites = ((IMeteoParameter) parameters).getSelectedSites();
            List<DatatypeVariableUniteForet> selectedVariables = ((IMeteoParameter) parameters).getSelectedMeteoVariables();

            List<SiteForet> sites = new LinkedList<>();
            for (SiteForet site : selectedSites) {
                sites.add(
                        this.siteDAO.getByPath(site.getPath())
                                .map(s -> (SiteForet) s)
                                .orElseThrow(() -> new BusinessException("bad site"))
                );
            }

            List<ValeurMeteo> valeurMeteos = this.meteoDAO.extractMeteo(selectedSites, selectedVariables, intervalDate.getBeginDate().toLocalDate(), intervalDate.getEndDate().toLocalDate(), rythme, policyManager.getCurrentUser());


            if (!valeurMeteos.isEmpty()) {
                ((IMeteoParameter) parameters).setValeurMeteoTours(valeurMeteos);
            }

        } catch (BusinessException e) {
            String EXTRACTION_ABORTED = "Abandon de l'extraction...";
            LOGGER.error(EXTRACTION_ABORTED, e);
            this.sendNotification(EXTRACTION_ABORTED, Notification.ERROR, PersistenceException.getLastCauseExceptionMessage(e), (Utilisateur) policyManager.getCurrentUser());
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param extraction
     */
    @Override
    public void setExtraction(Extraction extraction) {

    }

    /**
     *
     * @param parameters
     * @return
     */
    @Override
    public long getExtractionSize(IParameter parameters) {

        return 0;
    }

    /**
     *
     * @param parameter
     * @return
     */
    public String getExtractName(IParameter parameter) {
        return Constantes.THEME_METEO;
    }

    /**
     *
     * @return
     */
    public IMeteoDAO getMeteoDAO() {
        return this.meteoDAO;
    }

    /**
     *
     * @param meteoDAO
     */
    public void setMeteoDAO(IMeteoDAO meteoDAO) {
        this.meteoDAO = meteoDAO;
    }

    /**
     *
     * @param datatypeVariableUniteForetDAO
     */
    public void setDatatypeVariableUniteForetDAO(IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO) {
        this.datatypeVariableUniteForetDAO = datatypeVariableUniteForetDAO;
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteDAO(ISiteForetDAO siteDAO) {
        this.siteDAO = siteDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    private Optional<DataType> getDatatype(String rythme) throws BusinessException {
        Optional<DataType> dataType = null;
        if (null != rythme) {
            switch (rythme) {
                case Constantes.SEMI_HORAIRE:
                    dataType = this.datatypeDAO.getByCode(Constantes.DATATYPE_FREQUENCE_METEO[0]);
                    break;
                case Constantes.JOURNALIER:
                    dataType = this.datatypeDAO.getByCode(Constantes.DATATYPE_FREQUENCE_METEO[1]);
                    break;
                case Constantes.MENSUEL:
                    dataType = this.datatypeDAO.getByCode(Constantes.DATATYPE_FREQUENCE_METEO[2]);
                    break;
                default:
                    throw new BusinessException(String.format(MeteoExtractor.RYTHME_INVALID, rythme));
            }
        }
        return dataType;
    }
}
