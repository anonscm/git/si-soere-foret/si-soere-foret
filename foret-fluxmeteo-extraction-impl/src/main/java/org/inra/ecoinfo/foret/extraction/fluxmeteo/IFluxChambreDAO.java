package org.inra.ecoinfo.foret.extraction.fluxmeteo;


import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.mga.business.IUser;


/**
 * @author philippe Interface d'extraction des données de flux
 * 
 */
public interface IFluxChambreDAO {

    /**
     *
     * @param selectedSites
     * @param selectedVariables
     * @param dateDebut
     * @param dateFin
     * @param rythme
     * @param user
     * @return
     */
    List<ValeurChambreFluxSol> extractFluxChambre(List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, String rythme, IUser user);
}
