package org.inra.ecoinfo.foret.extraction.fluxmeteo.impl;

import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxChambreDAO;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxDAO;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IFluxMeteoDatasetManager;
import org.inra.ecoinfo.foret.extraction.fluxmeteo.IMeteoDAO;
import org.inra.ecoinfo.refdata.sitethemedatatype.ISiteThemeDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class FluxMeteoDatasetManager extends DefaultDatasetManager implements IFluxMeteoDatasetManager {

    private static final Logger LOGGER = LoggerFactory.getLogger(FluxMeteoDatasetManager.class);
    private static final String SITE_PRIVILEGE_PATTERN = "%s/(tour_a_flux|meteo)/*";
    private static final String VARIABLE_FLUX_PRIVILEGE_PATTERN = "%s/tour_a_flux/*/%s";
    private static final String VARIABLE_METEO_PRIVILEGE_PATTERN = "%s/meteo/*/%s";
    private static final String PATH_PATTERN = "%s/%s/%s/%s";
    private IFluxDAO fluxDAO;
    private IMeteoDAO meteoDAO;
    private IFluxChambreDAO fluxChambreDAO;
    private IVariableDAO variableDAO;


    /**
     *
     */
    protected ISiteThemeDatatypeDAO siteThemeDatatypeDAO;

    /**
     *
     */
    public FluxMeteoDatasetManager() {
        super();
    }

    /**
     *
     * @param fluxChambreDAO
     */
    public void setFluxChambreDAO(IFluxChambreDAO fluxChambreDAO) {
        this.fluxChambreDAO = fluxChambreDAO;
    }

    /**
     *
     * @param fluxDAO
     */
    public void setFluxDAO(IFluxDAO fluxDAO) {
        this.fluxDAO = fluxDAO;
    }

    /**
     *
     * @param meteoDAO
     */
    public void setMeteoDAO(IMeteoDAO meteoDAO) {
        this.meteoDAO = meteoDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param siteThemeDatatypeDAO
     */
    public void setSiteThemeDatatypeDAO(ISiteThemeDatatypeDAO siteThemeDatatypeDAO) {
        this.siteThemeDatatypeDAO = siteThemeDatatypeDAO;
    }
}
