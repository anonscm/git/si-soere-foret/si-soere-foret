package org.inra.ecoinfo.foret.extraction.fluxmeteo;


import java.time.LocalDate;
import java.util.List;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.mga.business.IUser;


/**
 * @author philippe Interface d'extraction des données de météo
 * 
 */
public interface IMeteoDAO {

    /**
     *
     * @param selectedSites
     * @param selectedVariables
     * @param dateDebut
     * @param dateFin
     * @param rythme
     * @param user
     * @return
     */
    List<ValeurMeteo> extractMeteo(List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, LocalDate dateDebut, LocalDate dateFin, String rythme, IUser user);
}