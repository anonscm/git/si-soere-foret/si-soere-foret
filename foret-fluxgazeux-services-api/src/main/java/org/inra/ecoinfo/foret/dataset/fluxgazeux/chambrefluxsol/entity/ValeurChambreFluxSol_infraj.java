/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity;

import java.util.Collection;
import java.util.LinkedList;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.foret.dataset.entity.QualityClass;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.FluxGazeuxConstantes;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationComplementaire;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * @author sophie
 *
 */
@Entity
@Table(name = ValeurChambreFluxSol_infraj.TABLE_NAME,
        indexes = {
            @Index(name = "Valeur_mesureChambreFluxSol_infraj_idx", columnList = FluxGazeuxConstantes.COLUMN_ID_MESURE),
            @Index(name = "vcinfra_var_idx", columnList = VariableForet.ID_JPA)
        })
@AttributeOverride(name = FluxGazeuxConstantes.NAME_PK_ID_VAL, column = @Column(name = ValeurChambreFluxSol_infraj.ID_JPA))
public class ValeurChambreFluxSol_infraj extends ValeurChambreFluxSol<MesureChambreFluxSol_infraj> {

    /**
     *
     */
    public static final String ID_JPA = FluxGazeuxConstantes.VALEUR_CHAMBRE_FLUX_SOL_IJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = FluxGazeuxConstantes.VALEUR_CHAMBRE_FLUX_SOL_IJ_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     * empty constructor
     */
    public ValeurChambreFluxSol_infraj() {
        super();
    }

    /**
     * @param realNode
     * @param value
     */
    public ValeurChambreFluxSol_infraj(RealNode realNode, Float value) {
        super(realNode, value);
    }

    /**
     * @param variable
     * @param mesure
     * @param realNode
     * @param value
     * @param qc
     * @param complements
     */
    public ValeurChambreFluxSol_infraj(float value, MesureChambreFluxSol_infraj mesure, RealNode realNode, QualityClass qc, Collection complements) {
        super(realNode, value);
        setMesure(mesure);
        setQualityClass(qc);
        this.complements = new LinkedList();
        for (Object complement : complements) {
            this.complements.add((ValeurInformationComplementaire) complement);
        }
    }
}
