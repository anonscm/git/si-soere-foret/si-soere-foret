/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.entity;

import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.foret.dataset.entity.Valeur;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IValeurFluxChambre;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * @param <M1>
 * 
 */
@MappedSuperclass
public abstract class ValeurChambreFluxSol<M1 extends MesureChambreFluxSol> extends Valeur implements IValeurFluxChambre {

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = FluxGazeuxConstantes.COLUMN_ID_MESURE)
    @LazyToOne(LazyToOneOption.PROXY)
    private M1 mesure;

    /**
     * empty constructor
     */
    public ValeurChambreFluxSol() {
        super();
    }

    /**
     * @param realNode
     * @param value
     */
    public ValeurChambreFluxSol(RealNode realNode, Float value) {
        super(value, realNode, null);
    }

    /**
     * @return the id
     */
    /*
     * public Long getId() { return id; }
     */

    /**
     * @param id
     *            the id to set
     */
    /*
     * public void setId(Long id) { this.id = id; }
     */

    /**
     * @return the mesure
     */
    public M1 getMesure() {
        return this.mesure;
    }

    /**
     * @param mesure
     *            the mesure to set
     */
    public void setMesure(M1 mesure) {
        this.mesure = mesure;
    }

    @Override
    public MesureChambreFluxSol getMesureFluxChambre() {
        return mesure;
    }

    @Override
    public Long getMesureFluxChambreId() {
        return this.getMesureFluxChambre()== null ? null : this.getMesureFluxChambre().getId();
    }

}
