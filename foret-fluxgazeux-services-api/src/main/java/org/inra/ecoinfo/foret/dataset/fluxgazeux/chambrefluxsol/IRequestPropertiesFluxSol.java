/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol;

import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;


/**
 * @author sophie
 * 
 */
public interface IRequestPropertiesFluxSol extends IRequestProperties, IRequestPropertiesIntervalValue {

    /**
     *
     * @return
     */
    String getDatatype();

    /**
     *
     * @param datatype
     */
    void setDatatype(String datatype);

}
