/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity;


import java.time.LocalDate;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.FluxGazeuxConstantes;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;

/**
 * @author sophie
 *
 */
@Entity
@Table(name = MesureChambreFluxSol_j.TABLE_NAME,
        indexes = {
            @Index(name = "MesureChambreFluxSol_j_date_idx", columnList = FluxGazeuxConstantes.COLUMN_DATE),
            @Index(name = "MesureChambreFluxSol_j_version_idx", columnList = VersionFile.ID_JPA),
            @Index(name = "MesureChambreFluxSol_j_site_idx", columnList =  SiteForet.RECURENT_NAME_ID),
        },
        uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    FluxGazeuxConstantes.COLUMN_DATE,
    FluxGazeuxConstantes.COLUMN_NO_CHAMBRE}))
@AttributeOverride(name = FluxGazeuxConstantes.NAME_PK_ID, column = @Column(name = MesureChambreFluxSol_j.ID_JPA))
public class MesureChambreFluxSol_j extends MesureChambreFluxSol<ValeurChambreFluxSol_j> {

    /**
     *
     */
    public static final String ID_JPA = FluxGazeuxConstantes.MESURE_CHAMBRE_FLUX_SOL_J_ID;

    /**
     *
     */
    public static final String TABLE_NAME = FluxGazeuxConstantes.MESURE_CHAMBRE_FLUX_SOL_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     * empty constructor
     */
    public MesureChambreFluxSol_j() {
        super();
    }

    /**
     * @param versionFile
     * @param traitement
     * @param date
     * @param noLigne
     * @param noChambre
     */
    public MesureChambreFluxSol_j(VersionFile versionFile, Traitement traitement, LocalDate date, long noLigne, String noChambre) {
        super(versionFile, traitement, date, noLigne, noChambre);
    }
}
