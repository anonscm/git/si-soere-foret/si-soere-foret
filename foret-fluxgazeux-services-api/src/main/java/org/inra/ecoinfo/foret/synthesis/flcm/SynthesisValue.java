/**
 * OREILacs project - see LICENCE.txt for use created: 31 mars 2009 13:30:55
 */
package org.inra.ecoinfo.foret.synthesis.flcm;


import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.foret.synthesis.Facteur;
import org.inra.ecoinfo.foret.synthesis.IGraphPresenceAbsence;
import org.inra.ecoinfo.foret.synthesis.ITypeGraphiqueSynthese;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author ptcherniati
 */
@Entity(name = "FlcmSynthesisValue")
@Table(name = "FlcmSynthesisValue", 
        indexes = {
        @Index(name = "FlcmSynthesisValue_site_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends GenericSynthesisValue implements IGraphPresenceAbsence, ITypeGraphiqueSynthese {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final boolean SEMIHORAIRE = false;

    /**
     *
     */
    protected Long traitement;

    /**
     *
     */
    protected String chambre;

    /**
     *
     */
    public SynthesisValue() {
        super();
    }

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param value
     * @param chambre
     */
    public SynthesisValue(LocalDate date, String site, String variable, Double value, Long traitement, String chambre) {
        super();
        this.date = date.atStartOfDay();
        this.site = site;
        this.variable = variable;
        this.valueFloat = value.floatValue();
        this.valueString = value.toString();
        this.traitement = traitement;
        this.chambre = chambre;
        this.isMean=false;
    }

    @Override
    public boolean isSemihoraire() {
        return SEMIHORAIRE;
    }

    @Override
    public boolean isPresenceAbsence() {
        return false;
    }

    @Override
    public Facteur getOrdonnee() {
        Facteur facteur = new Facteur(this.traitement, false);
        return facteur;
    }

    /**
     *
     * @return
     */
    public Long getTraitement() {
        return this.traitement;
    }

    /**
     *
     * @param traitement
     */
    public void setTraitement(Long traitement) {
        this.traitement = traitement;
    }

    /**
     *
     * @return
     */
    public String getChambre() {
        return this.chambre;
    }

    /**
     *
     * @param chambre
     */
    public void setChambre(String chambre) {
        this.chambre = chambre;
    }

    @Override
    public Facteur getRepetition() {
        Facteur facteur = new Facteur(this.chambre, false);
        return facteur;
    }

}
