package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;


/**
 * @author philippe
 * @param <T>
 * 
 */
public interface IValeurFluxChambre<T extends MesureChambreFluxSol> {

    /**
     *
     * @return
     */
    MesureChambreFluxSol getMesureFluxChambre();

    /**
     *
     * @return
     */
    Long getMesureFluxChambreId();
}
