/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.foret.dataset.entity.QualityClass;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.FluxGazeuxConstantes;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * @author sophie
 *
 */
@Entity
@Table(name = ValeurChambreFluxSol_j.TABLE_NAME,
        indexes = {
            @Index(name = "Valeur_mesureChambreFluxSol_j_idx", columnList = FluxGazeuxConstantes.COLUMN_ID_MESURE),
            @Index(name = "vcj_var_idx", columnList = VariableForet.ID_JPA)
        })
@AttributeOverride(name = FluxGazeuxConstantes.NAME_PK_ID_VAL, column = @Column(name = ValeurChambreFluxSol_j.ID_JPA))
public class ValeurChambreFluxSol_j extends ValeurChambreFluxSol<MesureChambreFluxSol_j> {

    /**
     *
     */
    public static final String ID_JPA = FluxGazeuxConstantes.VALEUR_CHAMBRE_FLUX_SOL_J_ID;

    /**
     *
     */
    public static final String TABLE_NAME = FluxGazeuxConstantes.VALEUR_CHAMBRE_FLUX_SOL_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     * empty constructor
     */
    public ValeurChambreFluxSol_j() {
        super();
    }

    /**
     * @param realNode
     * @param value
     */
    public ValeurChambreFluxSol_j(RealNode realNode, Float value) {
        super(realNode, value);
    }

    /**
     * @param realNode
     * @param mesure
     * @param value
     * @param qc
     */
    public ValeurChambreFluxSol_j(Float value, MesureChambreFluxSol_j mesure, RealNode realNode, QualityClass qc) {
        super(realNode, value);
        setMesure(mesure);
        setQualityClass(qc);
    }
}
