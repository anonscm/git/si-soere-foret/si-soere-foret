/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol;

import org.inra.ecoinfo.foret.dataset.ITestValues;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

/**
 * @author sophie
 * 
 */
public interface IVerificateurDonneesFluxSol extends ITestValues {

    /**
     *
     * @param requestProperties
     * @param datasetDescriptor
     * @param values
     * @param lineNumber
     * @param badsFormatsReport
     */
    void verifieVariableAndVariableGfCoherente(IRequestPropertiesFluxSol requestProperties,
            DatasetDescriptor datasetDescriptor, String[] values, long lineNumber,
            BadsFormatsReport badsFormatsReport);
}
