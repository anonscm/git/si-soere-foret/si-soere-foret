/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.ALL;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.entity.Mesure;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.foret.utils.Outils;

/**
 * @author sophie
 * @param <V0>
 *
 */
@MappedSuperclass
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public class MesureChambreFluxSol<V0 extends ValeurChambreFluxSol> extends Mesure implements Serializable {

    private static final long serialVersionUID = 1L;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = true, targetEntity = Traitement.class)
    @JoinColumn(name = Traitement.ID_JPA, referencedColumnName = Traitement.ID_JPA, nullable = true)
    @LazyToOne(LazyToOneOption.PROXY)
    private Traitement traitement;

    @Column(name = FluxGazeuxConstantes.COLUMN_DATE, nullable = false)
    private LocalDate date;

    @Column(nullable = false, name = FluxGazeuxConstantes.COLUMN_NO_LIGNE, length = 20)
    private long noLigne;

    @Column(nullable = false, name = FluxGazeuxConstantes.COLUMN_NO_CHAMBRE, length = 30)
    private String noChambre;

    @OneToMany(mappedBy = FluxGazeuxConstantes.NAME_ATTRIBUT_MESURE, cascade = ALL)

    private List<V0> lstValeurs = new LinkedList<>();

    /**
     * empty constructor
     */
    public MesureChambreFluxSol() {
        super();
    }

    /**
     * @param versionFile
     * @param traitement
     * @param date
     * @param noLigne
     * @param noChambre
     */
    public MesureChambreFluxSol(VersionFile versionFile, Traitement traitement, LocalDate date, long noLigne, String noChambre) {
        super();
        setVersion(versionFile);
        Outils.chercherNodeable(versionFile, SiteForet.class).ifPresent(s->setSite(s));
        this.traitement = traitement;
        this.date = date;
        this.noLigne = noLigne;
        this.noChambre = noChambre;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the lstValeurs
     */
    public List<V0> getLstValeurs() {
        return this.lstValeurs;
    }

    /**
     * @param lstValeurs the lstValeurs to set
     */
    public void setLstValeurs(List<V0> lstValeurs) {
        this.lstValeurs = lstValeurs;
    }

    /**
     * @return the noChambre
     */
    public String getNoChambre() {
        return this.noChambre;
    }

    /**
     * @param noChambre the noChambre to set
     */
    public void setNoChambre(String noChambre) {
        this.noChambre = noChambre;
    }

    /**
     * @return the noLigne
     */
    public long getNoLigne() {
        return this.noLigne;
    }

    /**
     * @param noLigne the noLigne to set
     */
    public void setNoLigne(long noLigne) {
        this.noLigne = noLigne;
    }

    /**
     * @return the traitement
     */
    public Traitement getTraitement() {
        return this.traitement;
    }

    /**
     * @param traitement the traitement to set
     */
    public void setTraitement(Traitement traitement) {
        this.traitement = traitement;
    }

    /**
     *
     * @return
     */
    public LocalTime getTime() {
        return null;
    }

}
