/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import org.inra.ecoinfo.foret.dataset.ExpectedColumn;

/**
 * @author sophie
 * 
 */
public class VariableValue {

    private String         value;
    private ExpectedColumn expectedColumn;

    /**
     * empty constructor
     */
    public VariableValue() {
        super();
    }

    /**
     * @param value
     * @param expectedColumn
     */
    public VariableValue(String value, ExpectedColumn expectedColumn) {
        super();
        this.value = value;
        this.expectedColumn = expectedColumn;
    }

    /**
     * @return the expectedColumn
     */
    public ExpectedColumn getExpectedColumn() {
        return this.expectedColumn;
    }

    /**
     * @param expectedColumn
     *            the expectedColumn to set
     */
    public void setExpectedColumn(ExpectedColumn expectedColumn) {
        this.expectedColumn = expectedColumn;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }
}
