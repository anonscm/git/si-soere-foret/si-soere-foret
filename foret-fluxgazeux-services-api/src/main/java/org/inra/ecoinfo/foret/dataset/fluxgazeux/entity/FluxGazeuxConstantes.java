/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.entity;

/**
 * @author sophie
 * 
 */
public class FluxGazeuxConstantes {

    /*
     * nom de l'attribut correpondant à la primary key dans les classes MesureChambreFlux,
     * ValeurChambreFlux
     */

    /**
     *
     */
    
    public static final String NAME_PK_ID                            = "id";

    /**
     *
     */
    public static final String NAME_PK_ID_VAL                        = "id_valeur";

    /* Noms des colonnes pour les tables de MesureChambreFlux */

    /**
     *
     */
    
    public static final String COLUMN_DATE                           = "date";

    /**
     *
     */
    public static final String COLUMN_HEURE_MOYENNE                  = "heure_moyenne";

    /**
     *
     */
    public static final String COLUMN_NO_LIGNE                       = "no_ligne";

    /**
     *
     */
    public static final String COLUMN_NO_CHAMBRE                     = "no_chambre";

    /**
     *
     */
    public static final String COLUMN_ZET_ID                         = "zet_id";

    /**
     *
     */
    public static final String COLUMN_DUREE_MESURE                   = "duree_mesure";

    /* Noms des colonnes pour les tables de ValeurChambreFluxSol */

    /**
     *
     */
    
    public static final String COLUMN_VALUE                          = "value";

    /**
     *
     */
    public static final String COLUMN_ID_VAR                         = "var_id";

    /**
     *
     */
    public static final String COLUMN_ID_MESURE                      = "mesure_id";

    /* Noms des tables */

    /**
     *
    /* Noms des tables */

    /**
     *
     */
    

    public static final String MESURE_CHAMBRE_FLUX_SOL_IJ_TABLE_NAME = "mesure_chambre_flux_sol_infraj";

    /**
     *
     */
    public static final String MESURE_CHAMBRE_FLUX_SOL_J_TABLE_NAME  = "mesure_chambre_flux_sol_j";

    /**
     *
     */
    public static final String MESURE_CHAMBRE_FLUX_SOL_M_TABLE_NAME  = "mesure_chambre_flux_sol_m";

    /**
     *
     */
    public static final String VALEUR_CHAMBRE_FLUX_SOL_IJ_TABLE_NAME = "valeur_chambre_flux_sol_infraj";

    /**
     *
     */
    public static final String VALEUR_CHAMBRE_FLUX_SOL_J_TABLE_NAME  = "valeur_chambre_flux_sol_j";

    /**
     *
     */
    public static final String VALEUR_CHAMBRE_FLUX_SOL_M_TABLE_NAME  = "valeur_chambre_flux_sol_m";

    /* Id des tables */

    /**
     *
     */
    
    public static final String MESURE_CHAMBRE_FLUX_SOL_IJ_ID         = "mchfij_id";

    /**
     *
     */
    public static final String MESURE_CHAMBRE_FLUX_SOL_J_ID          = "mchfj_id";

    /**
     *
     */
    public static final String MESURE_CHAMBRE_FLUX_SOL_M_ID          = "mchfm_id";

    /**
     *
     */
    public static final String VALEUR_CHAMBRE_FLUX_SOL_IJ_ID         = "vchfij_id";

    /**
     *
     */
    public static final String VALEUR_CHAMBRE_FLUX_SOL_J_ID          = "vchfj_id";

    /**
     *
     */
    public static final String VALEUR_CHAMBRE_FLUX_SOL_M_ID          = "vchfm_id";

    /**
     *
     */
    public static final String NAME_ATTRIBUT_MESURE                  = "mesure";

}
