/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.foret.dataset.entity.QualityClass;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.FluxGazeuxConstantes;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * @author sophie
 *
 */
@Entity
        @Table(name = ValeurChambreFluxSol_m.TABLE_NAME,
                indexes = {
                    @Index(name = "Valeur_mesureChambreFluxSol_m_idx", columnList = FluxGazeuxConstantes.COLUMN_ID_MESURE),
                    @Index(name = "vcm_var_idx", columnList = VariableForet.ID_JPA)
})
@AttributeOverride(name = FluxGazeuxConstantes.NAME_PK_ID_VAL, column = @Column(name = ValeurChambreFluxSol_m.ID_JPA))
public class ValeurChambreFluxSol_m extends ValeurChambreFluxSol<MesureChambreFluxSol_m> {

    /**
     *
     */
    public static final String ID_JPA = FluxGazeuxConstantes.VALEUR_CHAMBRE_FLUX_SOL_M_ID;

    /**
     *
     */
    public static final String TABLE_NAME = FluxGazeuxConstantes.VALEUR_CHAMBRE_FLUX_SOL_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     * empty constructor
     */
    public ValeurChambreFluxSol_m() {
        super();
    }

    /**
     * @param realNode
     * @param value
     */
    public ValeurChambreFluxSol_m(RealNode realNode, Float value) {
        super(realNode, value);
    }

    /**
     * @param realNode
     * @param mesure
     * @param value
     * @param qc
     */
    public ValeurChambreFluxSol_m(Float value, MesureChambreFluxSol_m mesure, RealNode realNode, QualityClass qc) {
        super(realNode, value);
        setMesure(mesure);
        setQualityClass(qc);
    }
}
