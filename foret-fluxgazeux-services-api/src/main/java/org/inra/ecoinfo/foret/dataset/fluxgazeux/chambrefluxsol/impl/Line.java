/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;


/**
 * @author sophie
 * 
 */

public class Line implements Comparable<Line> {

    private LocalDate date;
    private LocalTime time;
    private int dureeMesure;
    private String codeTraitemement;
    private String noChambre;
    private long lineNo;
    private List<VariableValue> lstVariablesValues = new LinkedList<>();

    /**
     * empty constructor
     */
    public Line() {
        super();
    }

    /**
     * @param date
     * @param time
     * @param dureeMesure
     * @param codeTraitemement
     * @param noChambre
     * @param lineNo
     * @param lstVariablesValues
     */
    public Line(LocalDate date, LocalTime time, int dureeMesure, String codeTraitemement, String noChambre, long lineNo, List<VariableValue> lstVariablesValues) {
        super();
        this.date = date;
        this.time = time;
        this.dureeMesure = dureeMesure;
        this.codeTraitemement = codeTraitemement;
        this.noChambre = noChambre;
        this.lineNo = lineNo;
        this.lstVariablesValues = lstVariablesValues;
    }

    /**
     * @param date
     * @param codeTraitemement
     * @param noChambre
     * @param lineNo
     * @param lstVariablesValues
     */
    public Line(LocalDate date, String codeTraitemement, String noChambre, long lineNo, List<VariableValue> lstVariablesValues) {
        super();
        this.date = date;
        this.codeTraitemement = codeTraitemement;
        this.noChambre = noChambre;
        this.lineNo = lineNo;
        this.lstVariablesValues = lstVariablesValues;
    }

    /**
     * @return the codeTraitemement
     */
    public String getCodeTraitemement() {
        return this.codeTraitemement;
    }

    /**
     * @param codeTraitemement
     *            the codeTraitemement to set
     */
    public void setCodeTraitemement(String codeTraitemement) {
        this.codeTraitemement = codeTraitemement;
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the dureeMesure
     */
    public int getDureeMesure() {
        return this.dureeMesure;
    }

    /**
     * @param dureeMesure
     *            the dureeMesure to set
     */
    public void setDureeMesure(int dureeMesure) {
        this.dureeMesure = dureeMesure;
    }

    /**
     * @return the lineNo
     */
    public long getLineNo() {
        return this.lineNo;
    }

    /**
     * @param lineNo
     *            the lineNo to set
     */
    public void setLineNo(long lineNo) {
        this.lineNo = lineNo;
    }

    /**
     * @return the lstVariablesValues
     */
    public List<VariableValue> getLstVariablesValues() {
        return this.lstVariablesValues;
    }

    /**
     * @param lstVariablesValues
     *            the lstVariablesValues to set
     */
    public void setLstVariablesValues(List<VariableValue> lstVariablesValues) {
        this.lstVariablesValues = lstVariablesValues;
    }

    /**
     * @return the noChambre
     */
    public String getNoChambre() {
        return this.noChambre;
    }

    /**
     * @param noChambre
     *            the noChambre to set
     */
    public void setNoChambre(String noChambre) {
        this.noChambre = noChambre;
    }

    /**
     * @return the time
     */
    public LocalTime getTime() {
        return this.time;
    }

    /**
     * @param time
     *            the time to set
     */
    public void setTime(LocalTime time) {
        this.time = time;
    }

    @Override
    public int compareTo(Line o) {
        return (int) (this.lineNo - o.lineNo);
    }

    @Override
    public int hashCode() {
        int hash = 7;
        hash = 31 * hash + (int) (this.lineNo ^ (this.lineNo >>> 32));
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (this == obj) {
            return true;
        }
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Line other = (Line) obj;
        return this.lineNo == other.lineNo;
    }

}
