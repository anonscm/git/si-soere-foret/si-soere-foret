/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity;


import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.FluxGazeuxConstantes;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;

/**
 * @author sophie
 *
 */
@Entity
@Table(name = MesureChambreFluxSol_infraj.TABLE_NAME,
        indexes = {
            @Index(name = "MesureChambreFluxSol_infraj_date_idx", columnList = FluxGazeuxConstantes.COLUMN_DATE),
            @Index(name = "MesureChambreFluxSol_infraj_version_idx", columnList = VersionFile.ID_JPA),
            @Index(name = "MesureChambreFluxSol_infraj_site_idx", columnList = SiteForet.RECURENT_NAME_ID),},
        uniqueConstraints = @UniqueConstraint(columnNames = {
    VersionFile.ID_JPA,
    FluxGazeuxConstantes.COLUMN_DATE,
    FluxGazeuxConstantes.COLUMN_HEURE_MOYENNE,
    FluxGazeuxConstantes.COLUMN_NO_CHAMBRE}))
@AttributeOverride(name = FluxGazeuxConstantes.NAME_PK_ID, column = @Column(name = MesureChambreFluxSol_infraj.ID_JPA))
public class MesureChambreFluxSol_infraj extends MesureChambreFluxSol<ValeurChambreFluxSol_infraj> {

    /**
     *
     */
    public static final String ID_JPA = FluxGazeuxConstantes.MESURE_CHAMBRE_FLUX_SOL_IJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = FluxGazeuxConstantes.MESURE_CHAMBRE_FLUX_SOL_IJ_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    @Column(name = FluxGazeuxConstantes.COLUMN_HEURE_MOYENNE, nullable = false)
    private LocalTime heureMoyenne;

    @Column(name = FluxGazeuxConstantes.COLUMN_DUREE_MESURE, nullable = false)
    private int dureeMesure;

    /**
     * empty constructor
     */
    public MesureChambreFluxSol_infraj() {
        super();
    }

    /**
     * @param versionFile
     * @param heureMoyenne
     * @param traitement
     * @param dureeMesure
     * @param date
     * @param noChambre
     * @param noLigne
     */
    public MesureChambreFluxSol_infraj(VersionFile versionFile, Traitement traitement, LocalDate date, long noLigne, String noChambre, LocalTime heureMoyenne, int dureeMesure) {
        super(versionFile, traitement, date, noLigne, noChambre);
        this.heureMoyenne = heureMoyenne;
        this.dureeMesure = dureeMesure;
    }

    /**
     * @return the dureeMesure
     */
    public int getDureeMesure() {
        return this.dureeMesure;
    }

    /**
     * @param dureeMesure the dureeMesure to set
     */
    public void setDureeMesure(int dureeMesure) {
        this.dureeMesure = dureeMesure;
    }

    /**
     * @return the heureMoyenne
     */
    public LocalTime getHeureMoyenne() {
        return this.heureMoyenne;
    }

    /**
     * @param heureMoyenne the heureMoyenne to set
     */
    public void setHeureMoyenne(LocalTime heureMoyenne) {
        this.heureMoyenne = heureMoyenne;
    }

    @Override
    public LocalTime getTime() {
        return this.heureMoyenne;
    }

}
