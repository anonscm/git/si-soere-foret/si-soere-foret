/**
 *
 */
package org.inra.ecoinfo.foret.refdata.notequalite;

/**
 * @author sophie
 * 
 */
/*
 * @Entity
 * 
 * @Table(name=NoteQualite.TABLE_NAME ,uniqueConstraints = @UniqueConstraint(columnNames = { RefDataConstantes.ECHELLEQUALITE_ID, RefDataConstantes.COLUMN_LIBELLE_NQ, RefDataConstantes.COLUMN_NOTE_NQ }))
 * 
 * public class NoteQualite {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * public static final String ID_JPA = RefDataConstantes.NOTEQUALITE_ID
 * 
 * @Id
 * 
 * @Column(name = ID_JPA)
 * 
 * @GeneratedValue(strategy = GenerationType.SEQUENCE) private Long id;
 * 
 * @ManyToOne(cascade = { PERSIST, MERGE, REFRESH }, optional = false, targetEntity = EchelleQualite.class)
 * 
 * @JoinColumn(name = RefDataConstantes.ECHELLEQUALITE_ID, referencedColumnName = EchelleQualite.ID_JPA, nullable = false)
 * 
 * @LazyToOne(LazyToOneOption.PROXY) private EchelleQualite echelleQualite;
 * 
 * @Column(nullable = false, name = RefDataConstantes.COLUMN_LIBELLE_NQ, length = 100) private String libelle;
 * 
 * @Column(nullable = false, name = RefDataConstantes.COLUMN_NOTE_NQ, length = 100) private int note;
 * 
 * public NoteQualite() { super(); }
 */

/**
 * @param id
 * @param echelleQualite
 * @param libelle
 * @param note
 */
/*
 * public NoteQualite(Long id, EchelleQualite echelleQualite, String libelle, int note) { super(); this.id = id; this.echelleQualite = echelleQualite; this.libelle = libelle; this.note = note; }
 */

/**
 * @return the id
 */
/*
 * public Long getId() { return id; }
 */

/**
 * @param id
 *            the id to set
 */
/*
 * public void setId(Long id) { this.id = id; }
 */

/**
 * @return the echelleQualite
 */
/*
 * public EchelleQualite getEchelleQualite() { return echelleQualite; }
 */

/**
 * @param echelleQualite
 *            the echelleQualite to set
 */
/*
 * public void setEchelleQualite(EchelleQualite echelleQualite) { this.echelleQualite = echelleQualite; }
 */

/**
 * @return the libelle
 */
/*
 * public String getLibelle() { return libelle; }
 */

/**
 * @param libelle
 *            the libelle to set
 */
/*
 * public void setLibelle(String libelle) { this.libelle = libelle; }
 */

/**
 * @return the note
 */
/*
 * public int getNote() { return note; }
 */

/**
 * @param note
 *            the note to set
 */
/*
 * public void setNote(int note) { this.note = note; }
 * 
 * 
 * }
 */
