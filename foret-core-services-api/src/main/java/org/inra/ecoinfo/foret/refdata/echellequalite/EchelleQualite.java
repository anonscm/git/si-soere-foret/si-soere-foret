/**
 *
 */
package org.inra.ecoinfo.foret.refdata.echellequalite;

/**
 * @author sophie
 * 
 */
/*
 * @Entity
 * 
 * @Table(name=EchelleQualite.TABLE_NAME ,uniqueConstraints = @UniqueConstraint(columnNames = {RefDataConstantes.COLUMN_NOM_ECHQ})) public class EchelleQualite {
 * 
 * private static final long serialVersionUID = 1L;
 * 
 * public static final String ID_JPA = RefDataConstantes.ECHELLEQUALITE_ID
 * 
 * @Id
 * 
 * @Column(name = ID_JPA)
 * 
 * @GeneratedValue(strategy = GenerationType.SEQUENCE) private Long id;
 * 
 * @Column(nullable = false, unique = true, name = RefDataConstantes.COLUMN_NOM_ECHQ, length = 50) private String nom;
 */

/**
 * empty constructor
 */
/*
 * public EchelleQualite() { super(); }
 */

/**
 * @param nom
 */
/*
 * public EchelleQualite(String nom) { super(); this.nom = nom; }
 */

/**
 * @return the id
 */
/*
 * public Long getId() { return id; }
 */

/**
 * @param id
 *            the id to set
 */
/*
 * public void setId(Long id) { this.id = id; }
 */

/**
 * @return the nom
 */
/*
 * public String getNom() { return nom; }
 */

/**
 * @param nom
 *            the nom to set
 */
/*
 * public void setNom(String nom) { this.nom = nom; }
 * 
 * 
 * }
 */
