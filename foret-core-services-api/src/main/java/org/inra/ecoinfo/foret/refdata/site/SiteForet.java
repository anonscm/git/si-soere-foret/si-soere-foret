package org.inra.ecoinfo.foret.refdata.site;


import java.time.LocalDate;
import java.util.Optional;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.DiscriminatorValue;
import javax.persistence.Entity;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.PrimaryKeyJoinColumn;
import javax.persistence.Table;
import org.inra.ecoinfo.foret.refdata.typezoneetude.TypeSite;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.site.Site;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = SiteForet.NAME_ENTITY_JPA)
@DiscriminatorValue("site_foret")
@PrimaryKeyJoinColumn(name = SiteForet.RECURENT_NAME_ID)

public class SiteForet extends Site {

    /**
     *
     */
    static public final String NAME_ENTITY_JPA = "zones_etude_zet";
    /**
     * The Constant RECURENT_NAME_PARENT_ID @link(String).
     */
    public static final String NAME_PARENT_ID = "parent_id";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    
    @ManyToOne(cascade = {MERGE, PERSIST, REFRESH}, optional = false)
    @JoinColumn(name = TypeSite.ID_JPA, referencedColumnName = TypeSite.ID_JPA, nullable = false)
    private TypeSite typeSite;

    @Column(nullable = true, unique = false, name = "zet_latitude", length = 50)
    private String latitude;

    @Column(nullable = true, unique = false, name = "zet_longitude", length = 50)
    private String longitude;

    @Column(nullable = true, unique = false, name = "zet_surface", length = 50)
    private String surface;

    @Column(nullable = true, unique = false, name = "zet_altitude", length = 50)
    private String altitude;

    @Column(nullable = true, unique = false, name = "zet_pente", length = 50)
    private String pente;

    @Column(nullable = true, unique = false, name = "zet_dir_pente", length = 50)
    private String dir_pente;

    @Column(nullable = true, unique = false, name = "zet_pays", length = 50)
    private String pays;

    @Column(nullable = true, unique = false, name = "zet_region", length = 50)
    private String region;

    @Column(nullable = true, unique = false, name = "zet_temp_moy_an", length = 50)
    private String temperature_moyenne_annuelle;

    @Column(nullable = true, unique = false, name = "zet_precip_moy_an", length = 50)
    private String precipitation_moyenne_annuelle;

    @Column(nullable = true, unique = false, name = "zet_dir_vent", length = 50)
    private String direction_vent;

    @Column(nullable = true, unique = false, name = "zet_type_foret", length = 500)
    private String type_foret;

    @Column(nullable = false, unique = false, name = "zet_date_debut")
    private LocalDate date_debut;

    @Column(nullable = true, unique = false, name = "zet_date_fin")
    private LocalDate date_fin;

    /**
     *
     */
    public SiteForet() {
        super();
    }

    /**
     *
     * @param nom
     */
    public SiteForet(String nom) {
        super(nom, null, null);
        this.setDate_debut(LocalDate.now());
        this.setDate_fin(LocalDate.now());
    }

    /**
     *
     * @param typeSite
     * @param parent
     * @param nom
     * @param description
     * @param latitude
     * @param longitude
     * @param surface
     * @param altitude
     * @param pente
     * @param dirPente
     * @param pays
     * @param region
     * @param temperatureMoyenneAnnuelle
     * @param precipitationMoyenneAnnuelle
     * @param directionVent
     * @param typeForet
     * @param dateDebut
     * @param dateFin
     */
    public SiteForet(TypeSite typeSite, SiteForet parent, String nom, String description, String latitude, String longitude, String surface, String altitude, String pente, String dirPente, String pays, String region,
        String temperatureMoyenneAnnuelle, String precipitationMoyenneAnnuelle, String directionVent, String typeForet, LocalDate dateDebut, LocalDate dateFin)
    {
        super(nom, description, parent);
        Optional.ofNullable(typeSite).ifPresent(t->this.typeSite = t);
        this.latitude = latitude;
        this.longitude = longitude;
        this.surface = surface;
        this.altitude = altitude;
        this.pente = pente;
        this.dir_pente = dirPente;
        this.pays = pays;
        this.region = region;
        this.temperature_moyenne_annuelle = temperatureMoyenneAnnuelle;
        this.precipitation_moyenne_annuelle = precipitationMoyenneAnnuelle;
        this.direction_vent = directionVent;
        this.type_foret = typeForet;
        this.date_debut = dateDebut;
        this.date_fin = dateFin;
    }

    /**
     *
     * @return
     */
    public String getAltitude() {
        return this.altitude;
    }

    /**
     *
     * @param altitude
     */
    public void setAltitude(String altitude) {
        this.altitude = altitude;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_debut() {
        return this.date_debut;
    }

    /**
     *
     * @param dateDebut
     */
    public void setDate_debut(LocalDate dateDebut) {
        this.date_debut = dateDebut;
    }

    /**
     *
     * @return
     */
    public LocalDate getDate_fin() {
        return this.date_fin;
    }

    /**
     *
     * @param dateFin
     */
    public void setDate_fin(LocalDate dateFin) {
        this.date_fin = dateFin;
    }

    /**
     *
     * @return
     */
    public String getDir_pente() {
        return this.dir_pente;
    }

    /**
     *
     * @param dirPente
     */
    public void setDir_pente(String dirPente) {
        this.dir_pente = dirPente;
    }

    /**
     *
     * @return
     */
    public String getDirection_vent() {
        return this.direction_vent;
    }

    /**
     *
     * @param directionVent
     */
    public void setDirection_vent(String directionVent) {
        this.direction_vent = directionVent;
    }

    /**
     *
     * @return
     */
    public String getLatitude() {
        return this.latitude;
    }

    /**
     *
     * @param latitude
     */
    public void setLatitude(String latitude) {
        this.latitude = latitude;
    }

    /**
     *
     * @return
     */
    public String getLongitude() {
        return this.longitude;
    }

    /**
     *
     * @param longitude
     */
    public void setLongitude(String longitude) {
        this.longitude = longitude;
    }

    /**
     *
     * @return
     */
    public String getPays() {
        return this.pays;
    }

    /**
     *
     * @param pays
     */
    public void setPays(String pays) {
        this.pays = pays;
    }

    /**
     *
     * @return
     */
    public String getPente() {
        return this.pente;
    }

    /**
     *
     * @param pente
     */
    public void setPente(String pente) {
        this.pente = pente;
    }

    /**
     *
     * @return
     */
    public String getPrecipitation_moyenne_annuelle() {
        return this.precipitation_moyenne_annuelle;
    }

    /**
     *
     * @param precipitationMoyenneAnnuelle
     */
    public void setPrecipitation_moyenne_annuelle(String precipitationMoyenneAnnuelle) {
        this.precipitation_moyenne_annuelle = precipitationMoyenneAnnuelle;
    }

    /**
     *
     * @return
     */
    public String getRegion() {
        return this.region;
    }

    /**
     *
     * @param region
     */
    public void setRegion(String region) {
        this.region = region;
    }

    /**
     *
     * @return
     */
    public SiteForet getRoot() {
        if (this.getParent() != null) {
            return ((SiteForet) this.getParent()).getRoot();
        } else {
            return this;
        }
    }

    /**
     *
     * @return
     */
    public SiteForet getRootSite() {
        SiteForet root = this;
        while (root.getParent() != null) {
            root = (SiteForet) root.getParent();
        }
        return root;
    }

    /**
     *
     * @return
     */
    public String getSurface() {
        return this.surface;
    }

    /**
     *
     * @param surface
     */
    public void setSurface(String surface) {
        this.surface = surface;
    }

    /**
     *
     * @return
     */
    public String getTemperature_moyenne_annuelle() {
        return this.temperature_moyenne_annuelle;
    }

    /**
     *
     * @param temperatureMoyenneAnnuelle
     */
    public void setTemperature_moyenne_annuelle(String temperatureMoyenneAnnuelle) {
        this.temperature_moyenne_annuelle = temperatureMoyenneAnnuelle;
    }

    /**
     *
     * @return
     */
    public String getType_foret() {
        return this.type_foret;
    }

    /**
     *
     * @param typeForet
     */
    public void setType_foret(String typeForet) {
        this.type_foret = typeForet;
    }

    /**
     *
     * @return
     */
    public TypeSite getTypeSite() {
        return this.typeSite;
    }

    /**
     *
     * @param typeSite
     */
    public void setTypeSite(TypeSite typeSite) {
        this.typeSite = typeSite;
    }

    @Override
    public String toString() {
        return "Site : " + this.getPath();
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) SiteForet.class;
    }
}