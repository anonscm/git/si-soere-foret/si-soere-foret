package org.inra.ecoinfo.foret.identification;

import org.inra.ecoinfo.identification.IUtilisateurDAO;

/**
 *
 * @author ptcherniati
 */
public interface IUtilisateurForetDAO extends IUtilisateurDAO {
}
