package org.inra.ecoinfo.foret.refdata.variable;

import javax.persistence.Entity;
import javax.persistence.Table;
import org.inra.ecoinfo.refdata.variable.Variable;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = VariableForet.NAME_ENTITY_JPA)
public class VariableForet extends Variable {

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "variable_foret";

    /**
     *
     */
    public static final String ID_JPA = "id";
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public VariableForet() {
        super();
    }

    /**
     * @param nom
     * @param definition
     * @param code
     */
    public VariableForet(String nom, String code, String definition) {
        super();
        this.setCode(code);
        this.setDefinition(definition);
    }

    /**
     *
     * @param id
     * @param nom
     * @param code
     * @param definition
     */
    public VariableForet(long id, String nom, String code,String definition) {
        this(nom, code, definition);
        setId(id);
    }
}
