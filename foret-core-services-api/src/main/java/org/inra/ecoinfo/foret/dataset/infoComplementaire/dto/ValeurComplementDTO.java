package org.inra.ecoinfo.foret.dataset.infoComplementaire.dto;

/**
 *
 * @author ptcherniati
 */
public class ValeurComplementDTO {

    /**
     *
     */
    protected String nom;

    /**
     *
     */
    public ValeurComplementDTO() {
        super();
    }

    /**
     *
     * @return
     */
    public String getNom() {
        return this.nom;
    }

    /**
     *
     * @param nom
     */
    public void setNom(String nom) {
        this.nom = nom;
    }

}
