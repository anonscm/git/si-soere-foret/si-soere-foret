package org.inra.ecoinfo.foret.refdata.datatypevariableunite;

import javax.persistence.Column;
import javax.persistence.DiscriminatorColumn;
import javax.persistence.DiscriminatorType;
import javax.persistence.Entity;
import javax.persistence.Table;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.Variable;


/**
 * @author philippe
 * 
 */
@Entity
@Table(name = DatatypeVariableUniteForet.NAME_ENTITY_JPA_FORET)
@DiscriminatorColumn(discriminatorType = DiscriminatorType.STRING, name = "datatypeVariableUnite_foret")
public class DatatypeVariableUniteForet extends DatatypeVariableUnite {

    /**
     *
     */
    static public final String NAME_ENTITY_JPA_FORET = "datatype_unite_variable_foret_vdt";

    /**
     *
     */
    public static final String NAME_ENTITY_JPA = "datatype_unite_variable_vdt";
    /**
     *
     */
    private static final long serialVersionUID = 1L;

    @Column(name = Constantes.CST_MIN)
    private Float min=null;

    @Column(name = Constantes.CST_MAX)
    private Float max=null;

    /**
     *
     */
    public DatatypeVariableUniteForet() {
        super();
    }

    /**
     *
     * @param datatype
     * @param unite
     * @param variable
     * @param min
     * @param max
     */
    public DatatypeVariableUniteForet(DataType datatype, Unite unite, Variable variable, Float min, Float max) {
        super();
        this.setDatatype(datatype);
        this.setUnite(unite);
        this.setVariable(variable);
        this.min = min;
        this.max = max;
        setCode(String.format("%s_%s_%s", datatype.getCode(), variable.getCode(), unite.getCode()));
    }

    /**
     *
     * @return
     */
    public Float getMax() {
        return this.max;
    }

    /**
     *
     * @param max
     */
    public void setMax(Float max) {
        this.max = max;
    }

    /**
     *
     * @return
     */
    public Float getMin() {
        return this.min;
    }

    /**
     *
     * @param min
     */
    public void setMin(Float min) {
        this.min = min;
    }

    /**
     *
     * @param <T>
     * @return
     */
    @Override
    public <T extends INodeable> Class<T> getNodeableType() {
        return (Class<T>) DatatypeVariableUniteForet.class;
    }

    @Override
    public String toString() {
        return getCode();
    }
}
