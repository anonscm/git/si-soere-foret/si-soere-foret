package org.inra.ecoinfo.foret.synthesis;

/**
 *
 * @author ptcherniati
 */
public interface ITypeGraphiqueSynthese {

    /**
     *
     * @return
     */
    boolean isPresenceAbsence();

    /**
     *
     * @return
     */
    boolean isSemihoraire();
}
