package org.inra.ecoinfo.foret.dataset;

/**
 *
 * @author ptcherniati
 * @param <T>
 */
public interface IFabrique<T> {

    /**
     *
     * @return
     */
    T fabrique();
}
