package org.inra.ecoinfo.foret.utils;

import org.inra.ecoinfo.localization.ILocalizationManager;

/**
 *
 * @author ptcherniati
 */
public class ForetMessages {


    /** The Constant ACBB_DATASET_BUNDLE_NAME. */
    public static final String          ACBB_DATASET_BUNDLE_NAME = "org.inra.ecoinfo.foret.dataset.messages";

    private static ILocalizationManager localizationManager;

    /**
     * Gets the aCBB message.
     * 
     * @param key
     * @link(String) the key
     * @return the ACBB message from key {@link String} the key
     */
    public static final String getForetMessage(final String key) {
        return ForetMessages.getForetMessageWithBundle(ForetMessages.ACBB_DATASET_BUNDLE_NAME, key);
    }

    /**
     * Gets the aCBB message.
     * 
     * @param bundlePath
     * @link(String) the bundle path
     * @param key
     * @link(String) the key
     * @return the ACBB message from key
     * @link(String) the bundle path {@link String} the key
     */
    public static final String getForetMessageWithBundle(final String bundlePath, final String key) {
        return ForetMessages.localizationManager == null ? key : ForetMessages.localizationManager
                .getMessage(bundlePath, key);
    }

    /**
     *
     * @param localizationManager
     */
    public static void setLocalizationManager(ILocalizationManager localizationManager) {
        ForetMessages.localizationManager = localizationManager;
    }

}
