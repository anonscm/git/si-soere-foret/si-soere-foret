/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.embeddedpostgresql;

import java.io.IOException;
import java.sql.Connection;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import javax.sql.DataSource;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import static org.junit.Assert.*;

/**
 *
 * @author ptcherniati
 */
public class EmbeddedDatasourceFactoryTest {

    EmbeddedDatasourceFactory instance;

    public EmbeddedDatasourceFactoryTest() {
    }

    @BeforeClass
    public static void setUpClass() {
    }

    @AfterClass
    public static void tearDownClass() {
    }

    @Before
    public void setUp() throws IOException, SQLException {
        instance = new EmbeddedDatasourceFactory();
    }

    @After
    public void tearDown() {
    }

    /**
     * Test of getDataSource method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testGetDataSource() throws Exception {
        System.out.println("getDataSource");
        DataSource dataSource = instance.getDataSource();
        assertNotNull(dataSource);
        testDatasource(dataSource, instance.userName, instance.password);
    }

    /**
     * Test of setDatabase method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testSetDatabase() {
        System.out.println("setDatabase");
        String database = "database";
        instance.setDatabase(database);
        assertEquals(database, instance.database);
    }

    /**
     * Test of setUserName method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testSetUserName() {
        System.out.println("setUserName");
        String userName = "userName";
        instance.setUserName(userName);
        assertEquals(userName, instance.userName);
    }

    /**
     * Test of setPassword method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testSetPassword() {
        System.out.println("setPassword");
        String password = "password";
        instance.setPassword(password);
        assertEquals(password, instance.password);
    }

    /**
     * Test of setPort method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testSetPort() {
        System.out.println("setPort");
        String port = "5432";
        instance.setPort(port);
        assertEquals(5432, instance.port);
    }

    /**
     * Test of setDriver method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testSetDriver() {
        System.out.println("setDriver");
        String driver = "driver";
        instance.setDriver(driver);
        assertEquals(driver, instance.driver);
    }

    /**
     * Test of setUrl method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testSetUrl() {
        System.out.println("setUrl");
        String url = "jdbc:h2://localhost:5432/mabase";
        instance.setUrl(url);
        assertEquals("h2", instance.driver);
        assertEquals(5432, instance.port);
        assertEquals("mabase", instance.database);
    }
        

    /**
     * Test of setUrl method, of class EmbeddedDatasourceFactory.
     */
    @Test
    public void testSetUrl2() {
        System.out.println("setUrl2");
        String url = "jdbc:${postgresql}://localhost/$[mabase}";
        instance.setUrl(url);
        assertEquals("postgresql", instance.driver);
        assertEquals(15432, instance.port);
        assertEquals("monsoeretest", instance.database);
    }

    private void testDatasource(DataSource dataSource, String userName, String password) throws SQLException {
        Connection connection = dataSource.getConnection(userName, password);
        Statement s = connection.createStatement();
        ResultSet rs = s.executeQuery("SELECT 1");
        assertTrue(rs.next());
        assertEquals(1, rs.getInt(1));
        assertFalse(rs.next());
    }

}
