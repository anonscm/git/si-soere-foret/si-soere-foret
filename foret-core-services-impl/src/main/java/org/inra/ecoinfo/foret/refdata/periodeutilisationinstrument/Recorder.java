/**
 *
 */
package org.inra.ecoinfo.foret.refdata.periodeutilisationinstrument;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.SRDV;
import org.inra.ecoinfo.foret.refdata.instrument.IInstrumentDAO;
import org.inra.ecoinfo.foret.refdata.instrument.Instrument;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<PeriodeUtilisationInstrument> {

    private static final Logger LOGGER = Logger.getLogger(Recorder.class);

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.refdata.messages";

    /**
     *
     */
    protected static final String INSTR_NONDEFINI = "INSTR_NONDEFINI";

    /**
     *
     */
    protected static final String PERIODE_INSTRUMENT_NOT_FOUND = "PERIODE_INSTRUMENT_NOT_FOUND";
    private static final String DATEFORMAT_ERROR = "DATEFORMAT_ERROR";
    IInstrumentDAO instrumentDAO;
    IPeriodeUtilisationInstrumentDAO periodeUtilisationInstrumentDAO;
    IDatatypeVariableUniteForetDAO variableUniteForetDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#deleteRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */

    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String stdtVarDisplay = tokenizerValues.nextToken();
                String codeInstr = tokenizerValues.nextToken();
                String dateDebut = tokenizerValues.nextToken();
                String dateFin = tokenizerValues.nextToken();
                SRDV srdv = new SRDV(stdtVarDisplay, localizationManager, policyManager.getMgaServiceBuilder().getRecorder(), variableUniteForetDAO);
                if (srdv.getRealNode() == null) {
                    throw new BusinessException(srdv.getErrorMessage());
                }
                Optional<Instrument> instrument = this.instrumentDAO.getByCode(codeInstr);
                LocalDate debutPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDebut).toLocalDate();
                LocalDate finPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateFin).toLocalDate();
                Optional<PeriodeUtilisationInstrument> periodeUtilisationInstrument = this.periodeUtilisationInstrumentDAO.getByStdtVarInstrAndDates(srdv.getRealNode(), instrument.orElseThrow(() -> new BusinessException(getMessageInstrument(codeInstr))), debutPeriode, finPeriode);
                this.periodeUtilisationInstrumentDAO.remove(periodeUtilisationInstrument.orElseThrow(() -> new BusinessException(getMessagePeriode(stdtVarDisplay, codeInstr, dateDebut, dateFin))));
            }
        } catch (IOException | PersistenceException | DateTimeParseException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     * @return the instrumentDAO
     */
    public IInstrumentDAO getInstrumentDAO() {
        return this.instrumentDAO;
    }

    /**
     * @param instrumentDAO the instrumentDAO to set
     */
    public void setInstrumentDAO(IInstrumentDAO instrumentDAO) {
        this.instrumentDAO = instrumentDAO;
    }

    /**
     *
     * @param periodeUtilisationInstrument
     * @return
     * @throws BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PeriodeUtilisationInstrument periodeUtilisationInstrument) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        String dateDebut = "";
        String dateFin = "";
        SRDV srdv = null;
        if (periodeUtilisationInstrument != null) {
            RealNode rn = periodeUtilisationInstrument.getRealNode();
            srdv = new SRDV(rn, localizationManager, policyManager.getMgaServiceBuilder().getRecorder(), variableUniteForetDAO);

            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(periodeUtilisationInstrument.getDateDebut().atStartOfDay(), DateUtil.DD_MM_YYYY);
            dateFin = DateUtil.getUTCDateTextFromLocalDateTime(periodeUtilisationInstrument.getDateFin().atStartOfDay(), DateUtil.DD_MM_YYYY);
        }
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeUtilisationInstrument == null ? Constantes.CST_STRING_EMPTY : srdv.getCode(), this._getStdtVarPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeUtilisationInstrument == null ? Constantes.CST_STRING_EMPTY : periodeUtilisationInstrument.getInstrument().getCode(), this._getInstrPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeUtilisationInstrument == null ? Constantes.CST_STRING_EMPTY : dateDebut, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeUtilisationInstrument == null ? Constantes.CST_STRING_EMPTY : dateFin, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        return lineModelGridMetadata;
    }

    /**
     * @return the periodeUtilisationInstrumentDAO
     */
    public IPeriodeUtilisationInstrumentDAO getPeriodeUtilisationInstrumentDAO() {
        return this.periodeUtilisationInstrumentDAO;
    }

    /**
     * @param periodeUtilisationInstrumentDAO the
     * periodeUtilisationInstrumentDAO to set
     */
    public void setPeriodeUtilisationInstrumentDAO(IPeriodeUtilisationInstrumentDAO periodeUtilisationInstrumentDAO) {
        this.periodeUtilisationInstrumentDAO = periodeUtilisationInstrumentDAO;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, PeriodeUtilisationInstrument.TABLE_NAME);
                String stdtVarDisplay = tokenizerValues.nextToken();
                SRDV srdv = new SRDV(stdtVarDisplay, localizationManager, policyManager.getMgaServiceBuilder().getRecorder(), variableUniteForetDAO);
                if (srdv.getRealNode() == null) {
                    errorsReport.addErrorMessage(srdv.getErrorMessage());
                    continue;
                }
                String codeInstr = tokenizerValues.nextToken();
                String dateDebut = tokenizerValues.nextToken();
                String dateFin = tokenizerValues.nextToken();
                Optional<Instrument> instrument = this.instrumentDAO.getByCode(codeInstr);
                RealNode realNode = srdv.getRealNode();
                if (!instrument.isPresent()) {
                    errorsReport.addErrorMessage(getMessageInstrument(codeInstr));
                    continue;
                }
                LocalDate debutPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDebut).toLocalDate();
                LocalDate finPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateFin).toLocalDate();
                PeriodeUtilisationInstrument dbPeriodeUtilisationInstrument = this.periodeUtilisationInstrumentDAO.getByStdtVarInstrAndDates(realNode, instrument.get(), debutPeriode, finPeriode)
                        .orElseGet(() -> new PeriodeUtilisationInstrument(instrument.get(), realNode, debutPeriode, finPeriode));
                if (dbPeriodeUtilisationInstrument.getId() == null) {
                    this.periodeUtilisationInstrumentDAO.saveOrUpdate(dbPeriodeUtilisationInstrument);
                    this.periodeUtilisationInstrumentDAO.flush();
                }
            }
        } catch (IOException | PersistenceException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (DateTimeParseException e) {
            LOGGER.debug(e.getMessage());
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, "DATEFORMAT_ERROR")));
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] _getInstrPossibles() {
        List<Instrument> lstInstruments = this.instrumentDAO.getAll(Instrument.class);
        String[] codeInstrPossibles = new String[lstInstruments.size()];
        int index = 0;
        for (Instrument instr : lstInstruments) {
            codeInstrPossibles[index++] = instr.getCode();
        }
        return codeInstrPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] _getStdtVarPossibles() {
        return policyManager.getMgaServiceBuilder().getRecorder().getNodesByTypeResource(WhichTree.TREEDATASET, VariableForet.class)
                .map(n -> n.getRealNode())
                .map(rn -> rn.getPath())
                .collect(Collectors.toSet())
                .toArray(new String[0]);
    }

    /*
     * (non-Javadoc)
     *
    * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */

    /**
     *
     * @return
     * @throws BusinessException
     */

    @Override
    protected List<PeriodeUtilisationInstrument> getAllElements() throws BusinessException {
        return this.periodeUtilisationInstrumentDAO.getAll(PeriodeUtilisationInstrument.class);
    }

    private String getMessageInstrument(String code) {
        return String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, INSTR_NONDEFINI), code);
    }

    private String getMessagePeriode(String stdtVarDisplay, String codeMCalc, String dateDebut, String dateFin) {
        return String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PERIODE_INSTRUMENT_NOT_FOUND), stdtVarDisplay, codeMCalc, dateDebut, dateFin);
    }

    /**
     *
     * @param variableUniteForetDAO
     */
    public void setDatatypeVariableUniteForetDAO(IDatatypeVariableUniteForetDAO variableUniteForetDAO) {
        this.variableUniteForetDAO = variableUniteForetDAO;
    }

}
