/**
 *
 */
package org.inra.ecoinfo.foret.refdata.reference;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author sophie
 * 
 */
public interface IReferenceDAO extends IDAO<Reference> {

    /**
     *
     * @param doi
     * @return
     */
    Optional<Reference> getByDOI(String doi);

}
