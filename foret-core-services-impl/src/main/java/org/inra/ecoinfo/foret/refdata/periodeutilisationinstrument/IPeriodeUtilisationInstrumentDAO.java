/**
 *
 */
package org.inra.ecoinfo.foret.refdata.periodeutilisationinstrument;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.refdata.instrument.Instrument;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public interface IPeriodeUtilisationInstrumentDAO extends IDAO<PeriodeUtilisationInstrument> {

    /**
     *
     * @param realNode
     * @param siteThemeDatatypeVariable
     * @param instrument
     * @param debutPeriode
     * @param finPeriode
     * @return
     */
    Optional<PeriodeUtilisationInstrument> getByStdtVarInstrAndDates(RealNode realNode, Instrument instrument, LocalDate debutPeriode, LocalDate finPeriode);

}