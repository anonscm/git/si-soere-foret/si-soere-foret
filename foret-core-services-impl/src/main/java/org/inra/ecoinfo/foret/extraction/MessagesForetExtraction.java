/*
 *
 */
package org.inra.ecoinfo.foret.extraction;

import java.util.MissingResourceException;
import java.util.ResourceBundle;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * The Class MessagesForetExtraction.
 */
public final class MessagesForetExtraction {

    private static final Logger LOGGER          = LoggerFactory
                                                        .getLogger(MessagesForetExtraction.class
                                                                .getName());

    /**
     * The Constant BUNDLE_NAME @link(String).
     */
    static final String         BUNDLE_NAME     = "org.inra.ecoinfo.acbb.extraction.messages";

    /**
     * The Constant RESOURCE_BUNDLE @link(ResourceBundle).
     */
    static final ResourceBundle RESOURCE_BUNDLE = ResourceBundle
                                                        .getBundle(MessagesForetExtraction.BUNDLE_NAME);

    /**
     * Gets the string.
     * 
     * @param key
     *            the key
     * @return the string
     */
    public static String getString(final String key) {
        try {
            return MessagesForetExtraction.RESOURCE_BUNDLE.getString(key);
        } catch (final MissingResourceException e) {
            return '!' + key + '!';
        }
    }

    /**
     * Instantiates a new messages acbb extraction.
     */
    MessagesForetExtraction() {
    }
}
