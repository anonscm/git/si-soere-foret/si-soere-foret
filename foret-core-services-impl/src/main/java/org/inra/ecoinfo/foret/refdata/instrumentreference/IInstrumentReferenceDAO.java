/**
 *
 */
package org.inra.ecoinfo.foret.refdata.instrumentreference;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author sophie
 * 
 */
public interface IInstrumentReferenceDAO extends IDAO<InstrumentReference> {

    /**
     *
     * @param codeInstrument
     * @param doiReference
     * @return
     */
    Optional<InstrumentReference> getByCodeInstrDoiRef(String codeInstrument, String doiReference);

}