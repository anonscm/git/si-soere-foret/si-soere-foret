package org.inra.ecoinfo.foret.dataset.impl.fileNameCheckers;

import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
public class FileNameCheckerddmmyyyy extends AbstractForetFileNameChecker {

    /**
     *
     * @return
     */
    @Override
    protected String getDatePattern() {
        return DateUtil.DD_MM_YYYY_FILE;
    }
}
