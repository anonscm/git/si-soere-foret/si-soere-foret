package org.inra.ecoinfo.foret.identification.jpa;


import org.inra.ecoinfo.foret.identification.IUtilisateurForetDAO;
import org.inra.ecoinfo.identification.jpa.JPAUtilisateurDAO;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class JPAUtilisateurForetDAO extends JPAUtilisateurDAO implements IUtilisateurForetDAO {
    private static final Logger LOGGER = LoggerFactory.getLogger(JPAUtilisateurForetDAO.class);

    /**
     *
     */
    private static final long serialVersionUID = 1L;

}
