package org.inra.ecoinfo.foret.refdata.datatypevariableunite;

import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.datatypevariableunite.IDatatypeVariableUniteDAO;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface IDatatypeVariableUniteForetDAO extends IDatatypeVariableUniteDAO {

    /**
     *
     */
    public static final String VARIABLE_CODE_ALIAS="variable_code";

    /**
     *
     */
    public static final String DATATYPE_ALIAS="datatype";

    /**
     *
     * @param name
     * @return
     */
    Map<String, DatatypeVariableUniteForet> getAllVariableTypeDonneesByDataType(String name);

    /**
     *
     * @param lstCodeDatatype
     * @param user
     * @return
     */
    List<VariableForet> getLstVariableByLstDatatypeAndUser(List<String> lstCodeDatatype, IUser user);

    /**
     *
     * @param dvu
     * @throws PersistenceException
     */
    void delete(DatatypeVariableUniteForet dvu) throws PersistenceException;

}
