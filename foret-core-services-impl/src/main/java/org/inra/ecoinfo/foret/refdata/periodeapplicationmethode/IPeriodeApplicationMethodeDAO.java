/**
 *
 */
package org.inra.ecoinfo.foret.refdata.periodeapplicationmethode;


import java.time.LocalDate;
import java.util.Optional;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.refdata.methodecalcul.MethodeCalcul;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public interface IPeriodeApplicationMethodeDAO extends IDAO<PeriodeApplicationMethode> {

    /**
     *
     * @param realNode
     * @param siteThemeDatatypeVariable
     * @param methodeCalcul
     * @param debutPeriode
     * @param finPeriode
     * @return
     */
    Optional<PeriodeApplicationMethode> getByStdtVarMCalcAndDates(RealNode realNode, MethodeCalcul methodeCalcul, LocalDate debutPeriode, LocalDate finPeriode);

}