/**
 *
 */
package org.inra.ecoinfo.foret.refdata.periodeapplicationmethode;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.SRDV;
import org.inra.ecoinfo.foret.refdata.methodecalcul.IMethodeCalculDAO;
import org.inra.ecoinfo.foret.refdata.methodecalcul.MethodeCalcul;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<PeriodeApplicationMethode> {

    private static final Logger LOGGER = Logger.getLogger(Recorder.class);

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.refdata.messages";

    /**
     *
     */
    protected static final String MCALC_NONDEFINI = "MCALC_NONDEFINI";

    /**
     *
     */
    protected static final String PERIODE_METHOD_NOT_FOUND = "PERIODE_METHOD_NOT_FOUND";
    private static final String DATEFORMAT_ERROR = "DATEFORMAT_ERROR";
    IMethodeCalculDAO methodeCalculDAO;
    IPeriodeApplicationMethodeDAO periodeApplicationMethodeDAO;
    IDatatypeVariableUniteForetDAO variableUniteForetDAO;

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String stdtVarDisplay = tokenizerValues.nextToken();
                String codeMCalc = tokenizerValues.nextToken();
                String dateDebut = tokenizerValues.nextToken();
                String dateFin = tokenizerValues.nextToken();
                SRDV srdv = new SRDV(stdtVarDisplay, localizationManager, policyManager.getMgaServiceBuilder().getRecorder(), variableUniteForetDAO);
                if (srdv.getRealNode() == null) {
                    throw new BusinessException(srdv.getErrorMessage());
                }
                Optional<MethodeCalcul> methodeCalcul = this.methodeCalculDAO.getByCode(codeMCalc);
                LocalDate debutPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDebut).toLocalDate();
                LocalDate finPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateFin).toLocalDate();
                Optional<PeriodeApplicationMethode> periodeApplicationMethode = this.periodeApplicationMethodeDAO.getByStdtVarMCalcAndDates(srdv.getRealNode(), methodeCalcul.orElseThrow(() -> new BusinessException(getMessageMethode(codeMCalc))), debutPeriode, finPeriode);
                this.periodeApplicationMethodeDAO.remove(periodeApplicationMethode.orElseThrow(() -> new BusinessException(getMessagePeriode(stdtVarDisplay, codeMCalc, dateDebut, dateFin))));
            }
        } catch (IOException | PersistenceException | DateTimeParseException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.refdata.IMetadataRecorder#getNewLineModelGridMetadata(java.lang.Object)
     */

    /**
     *
     * @param periodeApplicationMethode
     * @return
     * @throws BusinessException
     */

    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(PeriodeApplicationMethode periodeApplicationMethode) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        String dateDebut = "";
        String dateFin = "";
        SRDV srdv = null;
        if (periodeApplicationMethode != null) {
            RealNode rn = periodeApplicationMethode.getRealNode();
            srdv = new SRDV(rn, localizationManager, policyManager.getMgaServiceBuilder().getRecorder(), variableUniteForetDAO);

            dateDebut = DateUtil.getUTCDateTextFromLocalDateTime(periodeApplicationMethode.getDateDebut(), DateUtil.DD_MM_YYYY);
            dateFin = DateUtil.getUTCDateTextFromLocalDateTime(periodeApplicationMethode.getDateFin(), DateUtil.DD_MM_YYYY);
        }
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeApplicationMethode == null ? Constantes.CST_STRING_EMPTY :srdv.getCode(), this._getStdtVarPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeApplicationMethode == null ? Constantes.CST_STRING_EMPTY : periodeApplicationMethode.getMethodeCalcul().getCode(), this._getMCalcPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeApplicationMethode == null ? Constantes.CST_STRING_EMPTY : dateDebut, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(periodeApplicationMethode == null ? Constantes.CST_STRING_EMPTY : dateFin, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        return lineModelGridMetadata;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#processRecord(com.Ostermiller.util.CSVParser, java.io.File, java.lang.String)
     */

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */

    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            while ((values = parser.getLine()) != null) {

                TokenizerValues tokenizerValues = new TokenizerValues(values, PeriodeApplicationMethode.TABLE_NAME);
                String stdtVarDisplay = tokenizerValues.nextToken();
                SRDV srdv = new SRDV(stdtVarDisplay, localizationManager, policyManager.getMgaServiceBuilder().getRecorder(), variableUniteForetDAO);
                if (srdv.getRealNode() == null) {
                    errorsReport.addErrorMessage(srdv.getErrorMessage());
                    continue;
                }
                String codeMCalc = tokenizerValues.nextToken();
                String dateDebut = tokenizerValues.nextToken();
                String dateFin = tokenizerValues.nextToken();
                RealNode realNode = srdv.getRealNode();
                Optional<MethodeCalcul> methodeCalcul = this.methodeCalculDAO.getByCode(codeMCalc);
                if (!methodeCalcul.isPresent()) {
                    errorsReport.addErrorMessage(getMessageMethode(codeMCalc));
                    continue;
                }
                LocalDate debutPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDebut).toLocalDate();
                LocalDate finPeriode = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateFin).toLocalDate();
                PeriodeApplicationMethode dbPeriodeApplicationMethode = this.periodeApplicationMethodeDAO.getByStdtVarMCalcAndDates(realNode, methodeCalcul.get(), debutPeriode, finPeriode)
                        .orElseGet(() -> new PeriodeApplicationMethode(methodeCalcul.get(), realNode, debutPeriode, finPeriode));

                if (dbPeriodeApplicationMethode.getId() == null) {
                    this.periodeApplicationMethodeDAO.saveOrUpdate(dbPeriodeApplicationMethode);
                    this.periodeApplicationMethodeDAO.flush();
                }
            }
        } catch (IOException | PersistenceException e1) {
            throw new BusinessException(e1.getMessage(), e1);
        } catch (DateTimeParseException e) {
            LOGGER.debug(e.getMessage());
            errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(Recorder.BUNDLE_SOURCE_PATH, DATEFORMAT_ERROR)));
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    /**
     * @param methodeCalculDAO the methodeCalculDAO to set
     */
    public void setMethodeCalculDAO(IMethodeCalculDAO methodeCalculDAO) {
        this.methodeCalculDAO = methodeCalculDAO;
    }

    /**
     * @param periodeApplicationMethodeDAO the periodeApplicationMethodeDAO to
     * set
     */
    public void setPeriodeApplicationMethodeDAO(IPeriodeApplicationMethodeDAO periodeApplicationMethodeDAO) {
        this.periodeApplicationMethodeDAO = periodeApplicationMethodeDAO;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] _getMCalcPossibles() {
        List<MethodeCalcul> lstMethodeCalculs = this.methodeCalculDAO.getAll(MethodeCalcul.class);
        String[] codeMCalcPossibles = new String[lstMethodeCalculs.size()];
        int index = 0;
        for (MethodeCalcul mCalc : lstMethodeCalculs) {
            codeMCalcPossibles[index++] = mCalc.getCode();
        }
        return codeMCalcPossibles;
    }

    /**
     * @return @throws PersistenceException
     */
    private String[] _getStdtVarPossibles() {
        return policyManager.getMgaServiceBuilder().getRecorder().getNodesByTypeResource(WhichTree.TREEDATASET, VariableForet.class)
                .map(n -> n.getRealNode())
                .map(rn -> rn.getPath())
                .collect(Collectors.toSet())
                .toArray(new String[0]);
    }

    /*
     * (non-Javadoc)
     *
    * @see org.inra.ecoinfo.refdata.impl.AbstractCSVMetadataRecorder#getAllElements()
     */

    /**
     *
     * @return
     * @throws BusinessException
     */

    @Override
    protected List<PeriodeApplicationMethode> getAllElements() throws BusinessException {
        return this.periodeApplicationMethodeDAO.getAll(PeriodeApplicationMethode.class);
    }

    /**
     *
     * @return
     */
    @Override
    protected ModelGridMetadata<PeriodeApplicationMethode> initModelGridMetadata() {
        this._getStdtVarPossibles();
        this._getMCalcPossibles();
        return super.initModelGridMetadata();
    }

    private String getMessageMethode(String code) {
        return String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, MCALC_NONDEFINI), code);
    }

    private String getMessagePeriode(String stdtVarDisplay, String codeMCalc, String dateDebut, String dateFin) {
        return String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PERIODE_METHOD_NOT_FOUND), stdtVarDisplay, codeMCalc, dateDebut, dateFin);
    }

    /**
     *
     * @param variableUniteForetDAO
     */
    public void setDatatypeVariableUniteForetDAO(IDatatypeVariableUniteForetDAO variableUniteForetDAO) {
        this.variableUniteForetDAO = variableUniteForetDAO;
    }

}
