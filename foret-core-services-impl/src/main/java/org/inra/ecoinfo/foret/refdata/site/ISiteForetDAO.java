/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:53:17
 */
package org.inra.ecoinfo.foret.refdata.site;

import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.refdata.site.ISiteDAO;


/**
 * @author philippe
 * 
 */
public interface ISiteForetDAO extends ISiteDAO {

    /**
     *
     * @param code
     * @return
     */
    Optional<SiteForet> getByCode(String code);

    /**
     *
     * @param code
     * @param parent
     * @return
     */
    Optional<SiteForet> getByNameAndParent(String code, SiteForet parent);

    /**
     *
     * @return
     */
    Map<String, SiteForet> getListSite();
}