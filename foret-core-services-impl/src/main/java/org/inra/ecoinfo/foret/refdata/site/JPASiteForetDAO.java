/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.refdata.site;

import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.foret.refdata.typezoneetude.ITypeSiteDAO;
import org.inra.ecoinfo.refdata.site.JPASiteDAO;
import org.inra.ecoinfo.refdata.site.Site_;

/**
 * @author philippe
 *
 */
public class JPASiteForetDAO extends JPASiteDAO implements  ISiteForetDAO{

    private static final Logger LOGGER = Logger.getLogger(JPASiteForetDAO.class);

    /**
     *
     */
    protected ITypeSiteDAO typeSiteDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ore.forets.plugins.forets1.data.dao.ISiteDAO#getSiteByName(java .lang.String)
     */

    /**
     *
     * @param code
     * @return
     */

    @Override
    public Optional<SiteForet> getByCode(String code) {
        CriteriaQuery<SiteForet> query = builder.createQuery(SiteForet.class);
        Root<SiteForet> site = query.from(SiteForet.class);
        query
                .select(site)
                .where(builder.equal(site.get(SiteForet_.code), code));
        return getOptional(query);
    }

    /**
     *
     * @param name
     * @param parent
     * @return
     */
    @Override
    public Optional<SiteForet> getByNameAndParent(String name, SiteForet parent) {
        CriteriaQuery<SiteForet> query = builder.createQuery(SiteForet.class);
        Root<SiteForet> site = query.from(SiteForet.class);
        query
                .select(site)
                .where(builder.and(
                        builder.equal(site.get(SiteForet_.name), name),
                        builder.equal(site.get(Site_.parent), parent)
                ));
        return getOptional(query);
    }

    /**
     *
     * @return
     */
    @Override
    public Map<String, SiteForet> getListSite() {
        return getAll()
                .stream()
                .map(s->(SiteForet) s)
                .collect(Collectors.toMap(SiteForet::getPath, s->s));
    }

    /**
     *
     * @param typeSiteDAO
     */
    public void setTypeSiteDAO(ITypeSiteDAO typeSiteDAO) {
        this.typeSiteDAO = typeSiteDAO;
    }
}