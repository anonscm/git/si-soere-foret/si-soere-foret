/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.extraction.jsf.variable;

import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.foret.extraction.jsf.date.AbstractDatesFormParam;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;

/**
 *
 * @author ptchernia
 */
public interface IAvailablesSynthesisVariablesDAO {

    /**
     *
     * @param user
     * @param datesFormParam
     * @param nodeables
     * @return
     */
    Map<DatatypeVariableUnite, List<Long>> getVariables(IUser user, AbstractDatesFormParam datesFormParam, List<? extends Nodeable> nodeables);
    
}
