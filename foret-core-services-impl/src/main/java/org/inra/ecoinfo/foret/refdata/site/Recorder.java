/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.foret.refdata.site;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.File;
import java.io.IOException;
import java.time.LocalDate;
import java.time.format.DateTimeParseException;
import java.util.Collections;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.List;
import java.util.Locale;
import java.util.Optional;
import java.util.Properties;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.foret.refdata.RefDataConstantes;
import org.inra.ecoinfo.foret.refdata.typezoneetude.ITypeSiteDAO;
import org.inra.ecoinfo.foret.refdata.typezoneetude.TypeSite;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.entity.Localization;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.site.Site;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Antoine Schellenberger"
 *
 */
public class Recorder extends AbstractCSVMetadataRecorder<SiteForet> {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.refdata.messages";
    private static final String SITE_NONDEFINI = "STRING_EMPTY";
    private static final String PARENT_SITE_NONDEFINI = "PARENT_SITE_NONDEFINI";

    private static final String STRING_EMPTY = "";
    private static final String DATE_NON_VALIDE = "la date \"%s\" est non valide à la ligne %d";
    private static final String TYPESITE_NON_REFERENCE = "Le type de site \"%s\" est requis et n'est pas référencé en base de données à la ligne %d";
    private static final String PROPERTY_CST_TYPE_ZONE = "Type de zone";
    private static final String PROPERTY_CST_NAME = "nom";
    private static final String PROPERTY_CST_DESCRIPTION = "description";
    private static final String PROPERTY_CST_START_DATE = "date début";
    private static final String PROPERTY_CST_END_DATE = "date fin";
    private static final String PROPERTY_CST_LATITUDE = "latitude";
    private static final String PROPERTY_CST_LONGITUDE = "longitude";
    private static final String PROPERTY_CST_SURFACE = "surface";
    private static final String PROPERTY_CST_ALTITUDE = "altitude";
    private static final String PROPERTY_CST_PENTE = "pente";
    private static final String PROPERTY_CST_DIR_PENTE = "dir pente";
    private static final String PROPERTY_CST_COUNTRY = "pays";
    private static final String PROPERTY_CST_REGION = "region";
    private static final String PROPERTY_CST_TEMPERATURE = "température";
    private static final String PROPERTY_CST_PRECIPITATION = "précipitation";
    private static final String PROPERTY_CST_DIR_WIND = "dir vent";
    private static final String PROPERTY_CST_TYPE_FOREST = "type foret";
    private static final String PROPERTY_CST_PARENT_PLACE = "zone parente";
    private static final SiteForet EMPTY_SITE = new SiteForet(Recorder.STRING_EMPTY);

    /**
     *
     */
    protected ISiteForetDAO siteForetDAO;

    /**
     *
     */
    protected ITypeSiteDAO typeSiteDAO;
    Properties description_en;
    Properties directionPente_en;
    Properties pays_en;
    Properties region_en;
    Properties directionVent_en;
    Properties typeForet_en;
    String[] typeSitesPossibles;
    String[] sitesPossibles;
    
    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    /*
     * @Override public void checkUpdatesOperations(CSVParser parser) throws BusinessException, UpdateNotificationException { throw new NotYetImplementedException();
     * 
     * }
     */
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                String nom = values[1];
                String parent_path = values[values.length - 1];
                Optional<SiteForet> parent = siteForetDAO.getByCode(parent_path);
                Optional<SiteForet> site = Strings.isNullOrEmpty(parent_path)
                        ? Optional.empty()
                        : siteForetDAO.getByNameAndParent(nom, parent.orElseThrow(() -> new BusinessException(getMessage(parent_path))));
                this.siteForetDAO.remove(site.orElseThrow(() -> new BusinessException(getMessage(nom, parent_path))));
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException("Cause = " + e.getCause() + " Message = " + e.getMessage());
        }
    }

    /**
     *
     * @param site
     * @return
     * @throws BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(SiteForet site) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getTypeSite().getNom(), typeSitesPossibles, Recorder.PROPERTY_CST_TYPE_ZONE, false, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : localizationManager.getLocalName(site), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_NAME, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getDescription(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_DESCRIPTION, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Constantes.CST_STRING_EMPTY : description_en.getProperty(site.getDescription(), site.getDescription()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : DateUtil.getUTCDateTextFromLocalDateTime(site.getDate_debut(), DateUtil.DD_MM_YYYY), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_START_DATE, false,
                        false, true));
        String dateFin = this.dateFin(site);
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(dateFin, ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_END_DATE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getLatitude(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_LATITUDE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getLongitude(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_LONGITUDE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getSurface(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_SURFACE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getAltitude(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_ALTITUDE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getPente(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_PENTE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getDir_pente(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_DIR_PENTE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Constantes.CST_STRING_EMPTY : directionPente_en.getProperty(site.getDir_pente(), site.getDir_pente()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getPays(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_COUNTRY, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Constantes.CST_STRING_EMPTY : pays_en.getProperty(site.getPays(), site.getPays()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getRegion(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_REGION, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Constantes.CST_STRING_EMPTY : region_en.getProperty(site.getRegion(), site.getRegion()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getTemperature_moyenne_annuelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_TEMPERATURE, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getPrecipitation_moyenne_annuelle(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_PRECIPITATION, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getDirection_vent(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_DIR_WIND, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Constantes.CST_STRING_EMPTY : directionVent_en.getProperty(site.getDirection_vent(), site.getDirection_vent()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Recorder.STRING_EMPTY : site.getType_foret(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, Recorder.PROPERTY_CST_TYPE_FOREST, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null ? Constantes.CST_STRING_EMPTY : typeForet_en.getProperty(site.getType_foret(), site.getType_foret()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(site == null || site.getParent() == null ? Recorder.STRING_EMPTY : site.getParent().getPath(), sitesPossibles, Recorder.PROPERTY_CST_PARENT_PLACE, false, false, true));
        return lineModelGridMetadata;
    }

    /**
     * @param parser
     * @param encoding
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(SiteForet.class));
                lineNumber++;
                String typeSiteCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                    //public Localization createorUpdateLocalization(final String localizationString, final String entity, final String colonne, final String defaultString, final String localeString) throws BusinessException {

                String nom = tokenizerValues.nextToken();
                String code = Utils.createCodeFromString(nom);
                Localization localizationNameFr = localizationManager.createorUpdateLocalization("fr", Nodeable.getLocalisationEntite(SiteForet.class), Nodeable.ENTITE_COLUMN_NAME, code, nom);
                localizationManager.saveLocalization(localizationNameFr);
                Localization localizationNameEn = localizationManager.createorUpdateLocalization("en", Nodeable.getLocalisationEntite(SiteForet.class), Nodeable.ENTITE_COLUMN_NAME, code, nom);
                localizationManager.saveLocalization(localizationNameEn);
                String description = tokenizerValues.nextToken();
                String dateDebutString = tokenizerValues.nextToken();
                LocalDate dateDebut;
                try {
                    dateDebut = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateDebutString).toLocalDate();
                } catch (DateTimeParseException e) {
                    throw new BusinessException(String.format(Recorder.DATE_NON_VALIDE, dateDebutString, lineNumber));
                }
                String dateFinString = tokenizerValues.nextToken();
                LocalDate dateFin;
                if (StringUtils.isEmpty(dateFinString)) {
                    dateFin = null;
                } else {
                    try {
                        dateFin = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, dateFinString).toLocalDate();
                    } catch (DateTimeParseException e) {
                        throw new BusinessException(String.format(Recorder.DATE_NON_VALIDE, dateFinString, lineNumber));
                    }
                }
                String latitude = tokenizerValues.nextToken();
                String longitude = tokenizerValues.nextToken();
                String surface = tokenizerValues.nextToken();
                String altitude = tokenizerValues.nextToken();
                String pente = tokenizerValues.nextToken();
                String direction_pente = tokenizerValues.nextToken();
                String pays = tokenizerValues.nextToken();
                String region = tokenizerValues.nextToken();
                String temperatureMoyenneAnnuelle = tokenizerValues.nextToken();
                String precipitationMoyenneAnnuelle = tokenizerValues.nextToken();
                String directionVent = tokenizerValues.nextToken();
                String typeForet = tokenizerValues.nextToken();
                String nom_path_parent = tokenizerValues.nextToken();
                this.persistSite(errorsReport, typeSiteCode, nom_path_parent, code, description, latitude, longitude, surface, altitude, pente, direction_pente, pays, region, temperatureMoyenneAnnuelle, precipitationMoyenneAnnuelle, directionVent, typeForet, dateDebut, dateFin, lineNumber);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param siteDAO
     */
    public void setSiteForetDAO(ISiteForetDAO siteDAO) {
        this.siteForetDAO = siteDAO;
    }

    /**
     *
     * @param typeSiteDAO
     */
    public void setTypeSiteDAO(ITypeSiteDAO typeSiteDAO) {
        this.typeSiteDAO = typeSiteDAO;
    }

    /**
     * @param site
     * @param dbSite
     */
    /*
     * @Override public void checkUpdatesOperations(CSVParser parser) throws BusinessException, UpdateNotificationException { throw new NotYetImplementedException();
     *
     * }
     */
    @SuppressWarnings("unused")
    private void createOrUpdateSite(SiteForet parent, String code, String description, String latitude, String longitude, String surface, String altitude, String pente, String dirPente, String pays, String region, String temperatureMoyenneAnnuelle, String precipitationMoyenneAnnuelle, String directionVent, String typeForet, LocalDate dateDebut, LocalDate dateFin, SiteForet dbSite, TypeSite dbTypeSite) throws PersistenceException {
        if (dbSite == null) {
            this.createSite(parent, code, description, latitude, longitude, surface, altitude, pente, dirPente, pays, region, temperatureMoyenneAnnuelle, precipitationMoyenneAnnuelle, directionVent, typeForet, dateDebut, dateFin, dbTypeSite);
        } else {
            this.updateSite(parent, code, description, latitude, longitude, surface, altitude, pente, dirPente, pays, region, temperatureMoyenneAnnuelle, precipitationMoyenneAnnuelle, directionVent, typeForet, dateDebut, dateFin, dbSite, dbTypeSite);
        }
    }

    private void createSite(SiteForet parent, String code, String description, String latitude, String longitude, String surface, String altitude, String pente, String dirPente, String pays, String region, String temperatureMoyenneAnnuelle, String precipitationMoyenneAnnuelle, String directionVent, String typeForet, LocalDate dateDebut, LocalDate dateFin, TypeSite dbTypeSite) throws PersistenceException {
        SiteForet site = new SiteForet(dbTypeSite, parent, code, description, latitude, longitude, surface, altitude, pente, dirPente, pays, region, temperatureMoyenneAnnuelle, precipitationMoyenneAnnuelle, directionVent, typeForet, dateDebut,
                dateFin);
        site.setTypeSite(dbTypeSite);
        dbTypeSite.addSite(site);
        this.siteForetDAO.flush();
        this.typeSiteDAO.flush();
    }

    private String dateFin(SiteForet site) {
        return Optional.ofNullable(site)
                .map(s -> s.getDate_fin())
                .map(d -> DateUtil.getUTCDateTextFromLocalDateTime(d, DateUtil.DD_MM_YYYY))
                .orElseGet(String::new);
    }

    private String[] getNamesSitesPossibles() {
        List<Site> sites = this.siteForetDAO.getAll();
        String[] namesSitesPossibles = new String[sites.size() + 1];
        namesSitesPossibles[0] = Recorder.STRING_EMPTY;
        int index = 1;
        for (Site site : sites) {
            namesSitesPossibles[index++] = site.getPath();
        }
        return namesSitesPossibles;
    }

    private String[] getNamesTypesSitesPossibles() {
        List<TypeSite> typesSites = this.typeSiteDAO.getAll(TypeSite.class);
        String[] namesTypesSitesPossibles = new String[typesSites.size()];
        int index = 0;
        for (TypeSite typeSite : typesSites) {
            namesTypesSitesPossibles[index++] = typeSite.getNom();
        }
        return namesTypesSitesPossibles;
    }

    private void persistSite(ErrorsReport errorsReport, String typeSiteCode, String nom_path_parent, String code, String description, String latitude, String longitude, String surface, String altitude, String pente, String dirPente, String pays, String region, String temperatureMoyenneAnnuelle, String precipitationMoyenneAnnuelle, String directionVent, String typeForet, LocalDate dateDebut, LocalDate dateFin, int lineNumber) throws PersistenceException {
        Optional<TypeSite> dbTypeSite = this.typeSiteDAO.getTypeSiteByCode(typeSiteCode);
        if (!dbTypeSite.isPresent()) {
            errorsReport.addErrorMessage(String.format(Recorder.TYPESITE_NON_REFERENCE, typeSiteCode, lineNumber));
            return;
        }
        String codeSite = Utils.createCodeFromString(code);
        Optional<SiteForet> parent = this.siteForetDAO.getByCode(nom_path_parent);
        if (!Strings.isNullOrEmpty(nom_path_parent) && !parent.isPresent()) {
            errorsReport.addErrorMessage(getMessage(code, nom_path_parent));
            return;
        }
        Optional<SiteForet> parentOpt = siteForetDAO.getByCode(nom_path_parent);
        if (!(Strings.isNullOrEmpty(nom_path_parent) || parentOpt.isPresent())) {
            errorsReport.addErrorMessage(getMessage(nom_path_parent));
            return;
        }
        Optional<SiteForet> dbSite = Strings.isNullOrEmpty(nom_path_parent)
                ? siteForetDAO.getByCode(codeSite)
                : siteForetDAO.getByNameAndParent(code, parentOpt.orElse(null));

        this.createOrUpdateSite(parent.orElse(null), code, description, latitude, longitude, surface, altitude, pente, dirPente, pays, region, temperatureMoyenneAnnuelle, precipitationMoyenneAnnuelle, directionVent, typeForet, dateDebut, dateFin, dbSite.orElse(null), dbTypeSite.get());
    }

    private void updateSite(SiteForet parent, String nom, String description, String latitude, String longitude, String surface, String altitude, String pente, String dirPente, String pays, String region, String temperatureMoyenneAnnuelle, String precipitationMoyenneAnnuelle, String directionVent, String typeForet, LocalDate dateDebut, LocalDate dateFin, SiteForet dbSite, TypeSite dbTypeSite) throws PersistenceException {
        dbSite.setTypeSite(dbTypeSite);
        dbSite.setParent(parent);
        dbSite.setName(nom);
        dbSite.setDescription(description);
        dbSite.setLatitude(latitude);
        dbSite.setLongitude(longitude);
        dbSite.setSurface(surface);
        dbSite.setAltitude(altitude);
        dbSite.setPente(pente);
        dbSite.setDir_pente(dirPente);
        dbSite.setPays(pays);
        dbSite.setRegion(region);
        dbSite.setTemperature_moyenne_annuelle(temperatureMoyenneAnnuelle);
        dbSite.setPrecipitation_moyenne_annuelle(precipitationMoyenneAnnuelle);
        dbSite.setDirection_vent(directionVent);
        dbSite.setDate_debut(dateDebut);
        dbSite.setDate_fin(dateFin);
        dbTypeSite.addSite(dbSite);
        this.siteForetDAO.flush();
        this.typeSiteDAO.flush();
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Override
    protected List<SiteForet> getAllElements() throws BusinessException {
        LinkedList<SiteForet> sites = new LinkedList<>(this.siteForetDAO.getListSite().values());
        Collections.sort(sites, new Comparator<SiteForet>() {

            @Override
            public int compare(SiteForet o1, SiteForet o2) {
                return o1.getPath().compareTo(o2.getPath());
            }
        });
        return sites;
    }

    private String getMessage(String nom) {
        return String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, SITE_NONDEFINI), nom);
    }

    private String getMessage(String nom, String parent_path) {
        return String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, PARENT_SITE_NONDEFINI), parent_path, nom);
    }

    /**
     *
     * @return
     */
    @Override
    protected ModelGridMetadata<SiteForet> initModelGridMetadata() {
        String entity = Nodeable.getLocalisationEntite(SiteForet.class);
        description_en = this.localizationManager.newProperties(entity, RefDataConstantes.COLUMN_SITE_DESCRIPTION, Locale.ENGLISH);
        directionPente_en = this.localizationManager.newProperties(entity, RefDataConstantes.COLUMN_SITE_DIRECTION_PENTE, Locale.ENGLISH);
        pays_en = this.localizationManager.newProperties(entity, RefDataConstantes.COLUMN_SITE_PAYS, Locale.ENGLISH);
        region_en = this.localizationManager.newProperties(entity, RefDataConstantes.COLUMN_SITE_REGION, Locale.ENGLISH);
        directionVent_en = this.localizationManager.newProperties(entity, RefDataConstantes.COLUMN_SITE_DIRECTION_VENT, Locale.ENGLISH);
        typeForet_en = this.localizationManager.newProperties(entity, RefDataConstantes.COLUMN_SITE_TYPE_FORET, Locale.ENGLISH);
        typeSitesPossibles = getNamesSitesPossibles();
        sitesPossibles = getNamesSitesPossibles();
        
        return super.initModelGridMetadata();
    }

}
