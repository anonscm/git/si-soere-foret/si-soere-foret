/**
 *
 */
package org.inra.ecoinfo.foret.refdata.periodeutilisationinstrument;


import java.time.LocalDate;
import java.util.Optional;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.foret.refdata.instrument.Instrument;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public class JPAPeriodeUtilisationInstrumentDAO extends AbstractJPADAO<PeriodeUtilisationInstrument> implements IPeriodeUtilisationInstrumentDAO {
    private static final Logger LOGGER = Logger.getLogger(JPAPeriodeUtilisationInstrumentDAO.class);

    /**
     *
     * @param realNode
     * @param siteThemeDatatypeVariable
     * @param instrument
     * @param debutPeriode
     * @param finPeriode
     * @return
     */
    @Override
    public Optional<PeriodeUtilisationInstrument> getByStdtVarInstrAndDates(RealNode realNode, Instrument instrument, LocalDate debutPeriode, LocalDate finPeriode) {
        CriteriaQuery<PeriodeUtilisationInstrument> query = builder.createQuery(PeriodeUtilisationInstrument.class);
        Root<PeriodeUtilisationInstrument> periode = query.from(PeriodeUtilisationInstrument.class);
        query
                .select(periode)
                .where(builder.and(
                        builder.equal(periode.get(PeriodeUtilisationInstrument_.realNode), realNode),
                        builder.equal(periode.get(PeriodeUtilisationInstrument_.instrument), instrument),
                        builder.equal(periode.get(PeriodeUtilisationInstrument_.dateDebut), debutPeriode),
                        builder.equal(periode.get(PeriodeUtilisationInstrument_.dateFin), finPeriode)
                ));
        return getOptional(query);
    }

}