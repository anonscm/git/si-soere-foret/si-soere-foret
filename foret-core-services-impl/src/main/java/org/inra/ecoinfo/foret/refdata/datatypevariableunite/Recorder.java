/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.foret.refdata.datatypevariableunite;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.stream.Collectors;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.configuration.PatternConfigurator;
import org.inra.ecoinfo.mga.enums.WhichTree;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.IDatatypeDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.unite.IUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * @author "Antoine Schellenberger"
 * 
 */
public class Recorder extends AbstractCSVMetadataRecorder<DatatypeVariableUniteForet> {
    private static final String BUNDLE_PATH = "org.inra.ecoinfo.foret.refdata.messages";
    private static final Logger LOGGER = LoggerFactory.getLogger(Recorder.class);

    private static final String MSG_VARIABLE_MISSING_IN_DATABASE = "MSG_VARIABLE_MISSING_IN_DATABASE";
    private static final String MSG_DATATYPE_MISSING_IN_DATABASE = "MSG_DATATYPE_MISSING_IN_DATABASE";
    private static final String MSG_UNITE_MISSING_IN_DATABASE = "MSG_UNITE_MISSING_IN_DATABASE";

    /**
     *
     */
    protected IDatatypeDAO datatypeDAO;

    /**
     *
     */
    protected IVariableDAO variableDAO;

    /**
     *
     */
    protected IUniteDAO uniteDAO;

    /**
     *
     */
    protected IDatatypeVariableUniteForetDAO datatypeVariableUniteDAO;

    /*
     * @Override public void checkUpdatesOperations(CSVParser parser) throws BusinessException, UpdateNotificationException { throw new NotYetImplementedException();
     * 
     * }
     */

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */


    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                final TokenizerValues tokenizerValues = new TokenizerValues(values);
                final String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String variableCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                final String uniteCode = tokenizerValues.nextToken();
                final DatatypeVariableUniteForet dbDVU = this.datatypeVariableUniteDAO
                        .getByNKey(datatypeCode, variableCode, uniteCode)
                        .map(d->(DatatypeVariableUniteForet)d)
                        .orElseThrow(PersistenceException::new);
                datatypeVariableUniteDAO.delete(dbDVU);
            }
        } catch (final IOException | PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param datatypeVariableUnite
     * @return
     * @throws BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(DatatypeVariableUniteForet datatypeVariableUnite) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getDatatype().getCode(), this.getNamesDatatypesPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getVariable().getCode(), this.getNamesVariablesPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getUnite().getCode(), this.getNamesUnitesPossibles(), null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getMin(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(
                new ColumnModelGridMetadata(datatypeVariableUnite == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : datatypeVariableUnite.getMax(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, false, false));
        return lineModelGridMetadata;
    }

    /**
     * @param parser
     * @param encoding
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
    ErrorsReport errorsReport = new ErrorsReport();
    Map<DataType, List<DatatypeVariableUniteForet>> nodeablesDVU = new HashMap<>();
            try {
            this.skipHeader(parser);
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(DatatypeVariableUniteForet.class));
                String datatypeCode = Utils.createCodeFromString(tokenizerValues.nextToken());
                String variableCode = tokenizerValues.nextToken();
                String uniteCode = tokenizerValues.nextToken();
                Float minValue = tokenizerValues.nextTokenFloat();
                Float maxValue = tokenizerValues.nextTokenFloat();
                this.persistDatatypeVariable(errorsReport, datatypeCode, variableCode, uniteCode, minValue, maxValue, nodeablesDVU);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
            try {

                Map<DataType, List<INode>> datatypeNodesMap = mgaServiceBuilder.loadNodesByTypeResource(WhichTree.TREEDATASET, DataType.class)
                        .filter(n -> nodeablesDVU.containsKey(n.getNodeable()))
                        .collect(Collectors.groupingBy(
                                p -> ((DataType) p.getNodeable()))
                        );
                int[] i = new int[]{0};
                for (Iterator<Map.Entry<DataType, List<DatatypeVariableUniteForet>>> iteratorOnDVU = nodeablesDVU.entrySet().iterator(); iteratorOnDVU.hasNext();) {
                    Map.Entry<DataType, List<DatatypeVariableUniteForet>> dvusEntry = iteratorOnDVU.next();
                    DataType datatype = dvusEntry.getKey();
                    List<DatatypeVariableUniteForet> dvus = dvusEntry.getValue();
                    List<INode> datatypeNodeList = datatypeNodesMap.get(datatype);
                    for (Iterator<INode> iteratorOnDatatypeNodes = datatypeNodeList.iterator(); iteratorOnDatatypeNodes.hasNext();) {
                        INode datatypeNode = iteratorOnDatatypeNodes.next();
                        for (Iterator<DatatypeVariableUniteForet> dvuIterator = dvus.iterator(); dvuIterator.hasNext();) {
                            DatatypeVariableUniteForet dvu = dvuIterator.next();
                            RealNode parentRn = datatypeNode.getRealNode();
                            String path = String.format("%s%s%s", parentRn.getPath(), PatternConfigurator.PATH_SEPARATOR, dvu.getCode());
                            RealNode rn = new RealNode(parentRn, null, dvu, path);
                            mgaServiceBuilder.getRecorder().saveOrUpdate(rn);
                            NodeDataSet nds = new NodeDataSet((NodeDataSet) datatypeNode, null);
                            nds.setRealNode(rn);
                            mgaServiceBuilder.getRecorder().merge(nds);
                        }
                        iteratorOnDatatypeNodes.remove();
                    }
                    mgaServiceBuilder.getRecorder().getEntityManager().flush();
                    iteratorOnDVU.remove();
                }
                policyManager.clearTreeFromSession();

                if (errorsReport.hasErrors()) {
                    throw new BusinessException(errorsReport.getErrorsMessages());
                }
            } catch (final BusinessException e) {
                throw new BusinessException(e.getMessage(), e);
            }
        } catch (final IOException e) {
            LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        } catch (final PersistenceException e) {
            LOGGER.debug(e.getMessage(), e);
            throw new BusinessException(e);
        }
}

    /**
     *
     * @param datatypeDAO
     */
    public void setDatatypeDAO(IDatatypeDAO datatypeDAO) {
        this.datatypeDAO = datatypeDAO;
    }
    
    /**
     *
     * @param datatypeVariableUniteDAO
     */
    public void setDatatypeVariableUniteForetDAO(IDatatypeVariableUniteForetDAO datatypeVariableUniteDAO) {
        this.datatypeVariableUniteDAO = datatypeVariableUniteDAO;
    }
    
    /**
     *
     * @param uniteDAO
     */
    public void setUniteDAO(IUniteDAO uniteDAO) {
        this.uniteDAO = uniteDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    private String[] getNamesDatatypesPossibles(){
        List<DataType> datatypes = this.datatypeDAO.getAll(DataType.class);
        String[] namesDatatypesPossibles = new String[datatypes.size()];
        int index = 0;
        for (DataType datatype : datatypes) {
            namesDatatypesPossibles[index++] = datatype.getName();
        }
        return namesDatatypesPossibles;
    }

    private String[] getNamesUnitesPossibles() {
        List<Unite> unites = this.uniteDAO.getAll(Unite.class);
        String[] namesUnitesPossibles = new String[unites.size()];
        int index = 0;
        for (Unite unite : unites) {
            namesUnitesPossibles[index++] = unite.getCode();
        }
        return namesUnitesPossibles;
    }

    private String[] getNamesVariablesPossibles() {
        List<Variable> variables = this.variableDAO.getAll(Variable.class);
        String[] namesVariablesPossibles = new String[variables.size()];
        int index = 0;
        for (Variable variable : variables) {
            namesVariablesPossibles[index++] = variable.getName();
        }
        return namesVariablesPossibles;
    }

    private void persistDatatypeVariable(ErrorsReport errorsReport, String datatypeCode, String variableCode, String uniteCode, Float min, Float max, Map<DataType, List<DatatypeVariableUniteForet>> nodeablesDVU) throws PersistenceException {
        DataType dbDatatype = this.retrieveDBDatatype(errorsReport, datatypeCode);
        Variable dbVariable = this.retrieveDBVariable(errorsReport, variableCode);
        Unite dbUnite = this.retrieveDBUnite(errorsReport, uniteCode);
        if (dbDatatype != null && dbVariable != null && dbUnite != null) {
            DatatypeVariableUniteForet datatypeVariableUniteForetDB = (DatatypeVariableUniteForet) this.datatypeVariableUniteDAO.getByNKey(datatypeCode, variableCode, uniteCode)
                    .orElse(null);
            if (datatypeVariableUniteForetDB == null) {
                DatatypeVariableUniteForet datatypeVariableUnite = new DatatypeVariableUniteForet(dbDatatype, dbUnite, dbVariable, min, max);
                this.datatypeVariableUniteDAO.saveOrUpdate(datatypeVariableUnite);
                this.datatypeVariableUniteDAO.flush();
                nodeablesDVU.computeIfAbsent(dbDatatype, k->new LinkedList())
                        .add(datatypeVariableUnite);
            } else {
                datatypeVariableUniteForetDB.setMin(min);
                datatypeVariableUniteForetDB.setMax(max);
                this.datatypeVariableUniteDAO.saveOrUpdate(datatypeVariableUniteForetDB);
                this.datatypeVariableUniteDAO.flush();
            }
        }
    }

    private DataType retrieveDBDatatype(ErrorsReport errorsReport, String datatypeCode) throws PersistenceException {
        DataType datatype = this.datatypeDAO.getByCode(datatypeCode).orElse(null);
        if (datatype == null) {
            errorsReport.addErrorMessage(localizationManager.getMessage(BUNDLE_PATH, String.format(Recorder.MSG_DATATYPE_MISSING_IN_DATABASE, datatypeCode)));
        }
        return datatype;
    }

    private Unite retrieveDBUnite(ErrorsReport errorsReport, String uniteCode) throws PersistenceException {
        Unite unite = this.uniteDAO.getByCode(uniteCode).orElse(null);
        if (unite == null) {
            errorsReport.addErrorMessage(localizationManager.getMessage(BUNDLE_PATH, String.format(Recorder.MSG_UNITE_MISSING_IN_DATABASE, uniteCode)));
        }
        return unite;
    }

    private Variable retrieveDBVariable(ErrorsReport errorsReport, String variableCode) throws PersistenceException {
        Variable variable = this.variableDAO.getByCode(variableCode).orElse(null);
        if (variable == null) {
            errorsReport.addErrorMessage(localizationManager.getMessage(BUNDLE_PATH, String.format(Recorder.MSG_VARIABLE_MISSING_IN_DATABASE, variableCode)));
        }
        return variable;
    }

    /**
     *
     * @return
     */
    @Override
    protected List<DatatypeVariableUniteForet> getAllElements() {
        List<DatatypeVariableUniteForet> datatypeVariableUniteForets = new LinkedList<>();
        for (DatatypeVariableUnite datatypeVariableUniteForet : this.datatypeVariableUniteDAO.getAll(DatatypeVariableUniteForet.class)) {
            datatypeVariableUniteForets.add((DatatypeVariableUniteForet) datatypeVariableUniteForet);
        }
        return datatypeVariableUniteForets;
    }

}