package org.inra.ecoinfo.foret.refdata.variable;

import java.util.List;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;

/**
 *
 * @author ptcherniati
 */
public interface IVariableForetDAO extends IVariableDAO {

    /**
     *
     * @return
     */
    List<VariableForet> getAllVariablesForet();

    /**
     *
     * @param themeCode
     * @return
     */
    List<VariableForet> getDatatypeVariableUniteForTheme(String themeCode);

    /**
     *
     * @param variableCode
     * @return
     */
    List<DatatypeVariableUniteForet> getDatatypeVariableUniteForVariable(String variableCode);
}