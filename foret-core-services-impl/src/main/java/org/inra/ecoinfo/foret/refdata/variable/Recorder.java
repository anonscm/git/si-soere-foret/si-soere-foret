/**
 * OREILacs project - see LICENCE.txt for use created: 7 avr. 2009 16:17:33
 */
package org.inra.ecoinfo.foret.refdata.variable;

import com.Ostermiller.util.CSVParser;
import java.io.File;
import java.io.IOException;
import java.util.List;
import java.util.Locale;
import java.util.Properties;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.AbstractCSVMetadataRecorder;
import org.inra.ecoinfo.refdata.ColumnModelGridMetadata;
import org.inra.ecoinfo.refdata.LineModelGridMetadata;
import org.inra.ecoinfo.refdata.ModelGridMetadata;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author "Philippe"
 */
public class Recorder extends AbstractCSVMetadataRecorder<VariableForet> {

    /**
     *
     */
    protected IVariableForetDAO variableDAO;
    Properties propertiesName;

    /**
     *
     * @param parser
     * @param file
     * @param encoding
     * @throws BusinessException
     */
    @Override
    public void deleteRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        try {
            String[] values = null;
            while ((values = parser.getLine()) != null) {
                TokenizerValues tokenizerValues = new TokenizerValues(values);
                String nom = tokenizerValues.nextToken();
                this.variableDAO.remove(this.variableDAO.getByCode(nom)
                .orElseThrow(()->new BusinessException("bad varaible")));
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param variable
     * @return
     * @throws BusinessException
     */
    @Override
    public LineModelGridMetadata getNewLineModelGridMetadata(VariableForet variable) throws BusinessException {
        LineModelGridMetadata lineModelGridMetadata = new LineModelGridMetadata();
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getName(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, true, false, true));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : variable.getDefinition(), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        lineModelGridMetadata.getColumnsModelGridMetadatas().add(new ColumnModelGridMetadata(variable == null ? AbstractCSVMetadataRecorder.EMPTY_STRING : propertiesName.getProperty(variable.getDefinition(), variable.getDefinition()), ColumnModelGridMetadata.NULL_MAP_POSSIBLES, null, false, true, false));
        return lineModelGridMetadata;
    }

    /**
     *
     * @return
     */
    @Override
    protected ModelGridMetadata<VariableForet> initModelGridMetadata() {
        propertiesName = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableForet.class), Nodeable.ENTITE_COLUMN_NAME, Locale.ENGLISH);
        return super.initModelGridMetadata();
    }

    /**
     * @param parser
     * @param encoding
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, File file, String encoding) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            this.skipHeader(parser);
            String[] values = null;
            int lineNumber = 0;
            while ((values = parser.getLine()) != null) {
                lineNumber++;
                TokenizerValues tokenizerValues = new TokenizerValues(values, Nodeable.getLocalisationEntite(VariableForet.class));
                String nom = tokenizerValues.nextToken();
                String code = nom;
                String definition = tokenizerValues.nextToken();
                this.persistVariable(errorsReport, nom, code, definition, lineNumber);
            }
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            }
        } catch (IOException | PersistenceException e) {
            throw new BusinessException(e.getMessage(), e);
        }
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableForetDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /*
     * @Override public void checkUpdatesOperations(CSVParser parser) throws BusinessException, UpdateNotificationException {
     *
     * throw new NotYetImplementedException(); }
     */
    private void createOrUpdateVariable(ErrorsReport errorsReport, String nom, String code, String definition, int lineNumber, VariableForet dbVariable) throws PersistenceException {

        if (dbVariable == null) {
            dbVariable = new VariableForet(nom, code, definition);
            this.variableDAO.saveOrUpdate(dbVariable);
        } else {
            this.updateDBVariable(dbVariable, dbVariable);
        }
    }

    private void persistVariable(ErrorsReport errorsReport, String nom, String code, String definition, int lineNumber) throws PersistenceException {

        VariableForet dbVariable = (VariableForet) this.variableDAO.getByCode(code).orElse(null);
        this.createOrUpdateVariable(errorsReport, nom, code, definition, lineNumber, dbVariable);
    }

    private void updateDBVariable(Variable variable, Variable dbVariable) {
        dbVariable.setCode(variable.getCode());
        dbVariable.setDefinition(variable.getDefinition());
    }

    /**
     *
     * @return
     * @throws BusinessException
     */
    @Override
    protected List<VariableForet> getAllElements() throws BusinessException {
        return  this.variableDAO.getAllVariablesForet();
    }

}