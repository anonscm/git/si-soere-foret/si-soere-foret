/**
 *
 */
package org.inra.ecoinfo.foret.refdata.listevaleurinfo;

import java.util.Optional;
import org.inra.ecoinfo.IDAO;


/**
 * @author sophie
 * 
 */
public interface IListeValeurInfoDAO extends IDAO<ListeValeurInfo> {

    /**
     *
     * @param nom
     * @return
     */
    Optional<ListeValeurInfo> getByNom(String nom);

}
