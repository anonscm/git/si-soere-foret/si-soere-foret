package org.inra.ecoinfo.foret.refdata.variable;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.Query;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Root;
import org.apache.log4j.Logger;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.refdata.variable.JPAVariableDAO;

/**
 *
 * @author ptcherniati
 */
public class JPAVariableForetDAO extends JPAVariableDAO implements IVariableForetDAO {
    private static final Logger LOGGER = Logger.getLogger(JPAVariableForetDAO.class);
    private static final String QUUERY_GET_VARIABLES_FOR_THEME ="select dvu.variable from DatatypeVariableUniteForet dvu where dvu.theme.code= :themeCode";
    private static final String QUUERY_GET_DVU_FOR_VARIABLE ="from DatatypeVariableUniteForet dvu where dvu.variable.code= :variableCode";

    /**
     *
     * @param themeCode
     * @return
     */
    @Override
    public List<VariableForet> getDatatypeVariableUniteForTheme(String themeCode) {
        Query query = entityManager.createQuery(QUUERY_GET_VARIABLES_FOR_THEME);
        query.setParameter("themeCode", themeCode);
        try {
            return query.getResultList();
        } catch (IllegalStateException e) {
            LOGGER.error("no variable for theme "+themeCode, e);
            return new LinkedList();
        }
    }

    /**
     *
     * @param variableCode
     * @return
     */
    @Override
    public List<DatatypeVariableUniteForet> getDatatypeVariableUniteForVariable(String variableCode) {
        Query query = entityManager.createQuery(QUUERY_GET_DVU_FOR_VARIABLE);
        query.setParameter("variableCode", variableCode);
        try {
            return query.getResultList();
        } catch (IllegalStateException e) {
            LOGGER.error("no DatatypeVariableUniteForet for variable "+variableCode, e);
            return new LinkedList();
        }        
    }

    /**
     *
     * @return
     */
    @Override
    public List<VariableForet> getAllVariablesForet() {
        CriteriaQuery<VariableForet> query = builder.createQuery(VariableForet.class);
        Root<VariableForet> variable = query.from(VariableForet.class);
        query
                .select(variable)
                .orderBy(builder.asc(variable.get(VariableForet_.code)));
        return getResultList(query);
    }
}