package org.inra.ecoinfo.foret.refdata.datatypevariableunite;

import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.INode;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.datatype.DataType_;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.refdata.datatypevariableunite.JPADatatypeVariableUniteDAO;
import org.inra.ecoinfo.refdata.unite.Unite;
import org.inra.ecoinfo.refdata.unite.Unite_;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.refdata.variable.Variable_;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class JPADatatypeVariableUniteForetDAO extends JPADatatypeVariableUniteDAO implements IDatatypeVariableUniteForetDAO {

    private static final Logger LOGGER = LoggerFactory.getLogger(JPADatatypeVariableUniteForetDAO.class);

    @Override
    public Map<String, DatatypeVariableUniteForet> getAllVariableTypeDonneesByDataType(String datatypeCode) {
        CriteriaQuery<DatatypeVariableUniteForet> query = builder.createQuery(DatatypeVariableUniteForet.class);
        Root<DatatypeVariableUniteForet> dvu = query.from(DatatypeVariableUniteForet.class);
        Join<DatatypeVariableUniteForet, DataType> datatype = dvu.join(DatatypeVariableUniteForet_.datatype);
        query
                .select(dvu)
                .where(builder.equal(datatype.get(DataType_.code), Utils.createCodeFromString(datatypeCode)));
        return getResultAsStream(query)
                .collect(Collectors.toMap(
                        d -> d.getVariable().getCode(),
                        d -> d
                ));
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO#getLstVariableByLstDatatype(java.util.List)
     */
    @Override
    public List<VariableForet> getLstVariableByLstDatatypeAndUser(List<String> lstCodeDatatype, IUser user) {
        CriteriaQuery<VariableForet> query = builder.createQuery(VariableForet.class);
        Root<RealNode> realNodeDatatype = query.from(RealNode.class);
        Join<RealNode, DatatypeVariableUniteForet> dvu = builder.treat(realNodeDatatype.join(RealNode_.nodeable), DatatypeVariableUniteForet.class);
        Join<DatatypeVariableUniteForet, DataType> datatype = dvu.join(DatatypeVariableUniteForet_.datatype);
        Join<DatatypeVariableUniteForet, VariableForet> variable = builder.treat(dvu.join(DatatypeVariableUniteForet_.variable), VariableForet.class);
        List<Predicate> predicatesAnd = new LinkedList();
        if (!user.getIsRoot()) {
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            predicatesAnd.add(builder.equal(nds.get(NodeDataSet_.realNode), realNodeDatatype));
            Outils.addRestrictiveRequestOnRoles(user, query, predicatesAnd, builder, nds, null);
        }
        predicatesAnd.add(datatype.get(DataType_.code).in(lstCodeDatatype));
        query
                .select(variable)
                .where(datatype.get(DataType_.code).in(lstCodeDatatype));
        return getResultList(query);
    }

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @return
     */
    @Override
    public Optional<Unite> getUnite(final String datatypeCode, final String variableCode) {
        CriteriaQuery<Unite> query = builder.createQuery(Unite.class);
        Root<DatatypeVariableUniteForet> dvu = query.from(DatatypeVariableUniteForet.class);
        Join<DatatypeVariableUniteForet, Unite> unite = dvu.join(DatatypeVariableUniteForet_.unite);
        Join<DatatypeVariableUniteForet, DataType> datatype = dvu.join(DatatypeVariableUniteForet_.datatype);
        Join<DatatypeVariableUniteForet, Variable> variable = dvu.join(DatatypeVariableUniteForet_.variable);
        query
                .select(unite)
                .where(
                        builder.equal(datatype.get(DataType_.code), datatypeCode),
                        builder.equal(variable.get(Variable_.code), variableCode)
                );
        return getOptional(query);
    }

    /**
     *
     * @param datatypeCode
     * @param variableCode
     * @param uniteCode
     * @return
     */
    @Override
    public Optional<DatatypeVariableUnite> getByNKey(String datatypeCode, String variableCode, String uniteCode) {
        CriteriaQuery<DatatypeVariableUnite> query = builder.createQuery(DatatypeVariableUnite.class);
        Root<DatatypeVariableUniteForet> dvu = query.from(DatatypeVariableUniteForet.class);
        Join<DatatypeVariableUniteForet, Unite> unite = dvu.join(DatatypeVariableUniteForet_.unite);
        Join<DatatypeVariableUniteForet, DataType> datatype = dvu.join(DatatypeVariableUniteForet_.datatype);
        Join<DatatypeVariableUniteForet, Variable> variable = dvu.join(DatatypeVariableUniteForet_.variable);
        query
                .select(dvu)
                .where(
                        builder.equal(datatype.get(DataType_.code), datatypeCode),
                        builder.equal(variable.get(Variable_.code), variableCode),
                        builder.equal(unite.get(Unite_.code), uniteCode)
                );
        return getOptional(query);
    }

    /**
     *
     * @param dvu
     * @throws PersistenceException
     */
    @Override
    public void delete(DatatypeVariableUniteForet dvu) throws PersistenceException {
        try {
            CriteriaQuery<INode> query = builder.createQuery(INode.class);
            Root<NodeDataSet> node = query.from(NodeDataSet.class);
            query.select(node);
            List<INode> nodes = getResultAsStream(query)
                    .filter(
                            n -> n.getRealNode().getNodeable().getCode().equals(dvu.getCode())
                    )
                    .collect(Collectors.toList());
            CriteriaDelete<NodeDataSet> nodedatasetCriteriaDelete = builder.createCriteriaDelete(NodeDataSet.class);
            Root<NodeDataSet> nds = nodedatasetCriteriaDelete.from(NodeDataSet.class);
            nodedatasetCriteriaDelete.where(nds.in(nodes));
            if (delete(nodedatasetCriteriaDelete) > 0) {
                List<RealNode> realNodes = nodes.stream().map(n -> n.getRealNode()).collect(Collectors.toList());
                CriteriaDelete<RealNode> realnodesCriteriaDelete = builder.createCriteriaDelete(RealNode.class);
                Root<RealNode> rn = realnodesCriteriaDelete.from(RealNode.class);
                realnodesCriteriaDelete.where(rn.in(realNodes));
                if (delete(realnodesCriteriaDelete) > 0) {
                    remove(dvu);
                }
            }
        } catch (final Exception e) {
            LOGGER.error(PersistenceException.getLastCauseExceptionMessage(e));
            throw new PersistenceException(e.getMessage(), e);
        }
    }

}
