package org.inra.ecoinfo.foret.dataset.impl.fileNameCheckers;

import org.inra.ecoinfo.utils.DateUtil;

/**
 *
 * @author ptcherniati
 */
public class FileNameCheckermmyyyy extends AbstractForetFileNameChecker {

    /**
     *
     * @return
     */
    @Override
    protected String getDatePattern() {
        return DateUtil.MM_YYYY_FILE;
    }
}
