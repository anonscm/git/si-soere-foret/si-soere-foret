package org.inra.ecoinfo.foret;

import java.io.IOException;
import java.sql.SQLException;
import org.dbunit.DatabaseUnitException;
import org.dbunit.dataset.DataSetException;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.dataset.IDatasetManager;
import org.inra.ecoinfo.dataset.versioning.IDatasetDAO;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.ISiteForetDAO;
import org.inra.ecoinfo.foret.refdata.variable.IVariableForetDAO;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.notifications.INotificationsManager;
import org.inra.ecoinfo.refdata.IMetadataManager;
import org.inra.ecoinfo.refdata.sitethemedatatype.ISiteThemeDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.IVariableDAO;
import org.springframework.test.context.TestContext;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
public class ForetTransactionalTestFixtureExecutionListener extends TransactionalTestFixtureExecutionListener {

    /**
     *
     */
    public static ISiteForetDAO siteForetDAO = null;

    /**
     *
     */
    public static IVariableForetDAO variableForetDAO = null;
    public static IDatatypeVariableUniteForetDAO datatypeVariableUniteDAO = null;

    /**
     *
     */
    public static IMetadataManager metadataManager;

    /**
     *
     */
    public static IDatasetManager datasetManager;

    /**
     *
     */
    public static IDatasetDAO datasetDAO;

    /**
     *
     */
    public static IVersionFileDAO versionFileDAO;

    /**
     *
     */
    public static IVariableDAO variableDAO;

    /**
     *
     */
    public static ILocalizationManager localizationManager;

    /**
     *
     */
    public static IExtractionManager extractionManager;

    /**
     *
     */
    public static INotificationsManager notificationsManager;

    /**
     *
     */
    public static IFileCompManager fileCompManager;

    /**
     *
     */
    public static IFileCompConfiguration fileCompConfiguration;

    /**
     *
     */
    public static ISiteThemeDatatypeDAO siteThemeDatatypeDAO;
    private static String testName = "";

    /**
     * Before test class.
     *
     * @param testContext
     * @link(TestContext) the test context
     * @throws Exception the exception @see
     * org.springframework.test.context.support.AbstractTestExecutionListener
     * #beforeTestClass(org.springframework.test.context.TestContext)
     */
    @Override
    public void beforeTestClass(final TestContext testContext) throws Exception {
        super.beforeTestClass(testContext);
        ForetTransactionalTestFixtureExecutionListener.metadataManager = (IMetadataManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("metadataManager");
        ForetTransactionalTestFixtureExecutionListener.datasetManager = (IDatasetManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetManager");
        ForetTransactionalTestFixtureExecutionListener.datasetDAO = (IDatasetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datasetDAO");
        ForetTransactionalTestFixtureExecutionListener.versionFileDAO = (IVersionFileDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("versionFileDAO");
        ForetTransactionalTestFixtureExecutionListener.variableDAO = (IVariableDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        ForetTransactionalTestFixtureExecutionListener.localizationManager = (ILocalizationManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("localizationManager");
        ForetTransactionalTestFixtureExecutionListener.extractionManager = (IExtractionManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("extractionManager");
        ForetTransactionalTestFixtureExecutionListener.notificationsManager = (INotificationsManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("notificationsManager");
        ForetTransactionalTestFixtureExecutionListener.fileCompManager = (IFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompManager");
        //ForetTransactionalTestFixtureExecutionListener.associateFileCompManager = (IAssociateFileCompManager) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateFileCompManager");
        ForetTransactionalTestFixtureExecutionListener.fileCompConfiguration = (IFileCompConfiguration) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompConfiguration");
        //ForetTransactionalTestFixtureExecutionListener.associateDAO = (IAssociateDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("associateDAO");
        ForetTransactionalTestFixtureExecutionListener.siteThemeDatatypeDAO = (ISiteThemeDatatypeDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("siteThemeDatatypeDAO");
        ForetTransactionalTestFixtureExecutionListener.siteForetDAO = (ISiteForetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("siteForetDAO");
        ForetTransactionalTestFixtureExecutionListener.variableForetDAO = (IVariableForetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("variableDAO");
        ForetTransactionalTestFixtureExecutionListener.datatypeVariableUniteDAO = (IDatatypeVariableUniteForetDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("datatypeVariableUniteForetDAO");

        if (testName.isEmpty()) {
            testName = "../target/concordion/" + testContext.getTestClass().getName().replaceAll(".*\\.(.*?)Fixture", "$1");
        }
    }

    /**
     * Clean tables.
     *
     * @throws SQLException the sQL exception
     */
    @Override
    protected void cleanTables() throws SQLException {
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from group_utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from utilisateur cascade;")
                .execute();
        getDatasourceConnexion()
                .getConnection()
                .prepareStatement(
                        "delete from groupe where group_name!='public';")
                .execute();
    }

    /**
     * After super class.
     *
     * @param testContext
     * @link(TestContext) the test context
     * @throws IOException Signals that an I/O exception has occurred.
     * @throws DataSetException the data set exception
     * @throws DatabaseUnitException the database unit exception
     * @throws SQLException the sQL exception
     * @throws Exception the exception
     * @link(TestContext) the test context
     */
    @Transactional(rollbackFor = Exception.class)
    void afterSuperClass(final TestContext testContext) throws IOException, DataSetException, DatabaseUnitException, SQLException, Exception {
        super.afterTestClass(testContext);
    }
}
