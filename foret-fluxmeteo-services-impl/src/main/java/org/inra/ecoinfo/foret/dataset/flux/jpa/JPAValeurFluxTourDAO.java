/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.flux.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;


/**
 * @author philippe
 * @param <T>
 * 
 */
public class JPAValeurFluxTourDAO<T> extends AbstractJPADAO<ValeurFluxTour> {

}
