/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.flux.jpa;


import org.inra.ecoinfo.foret.dataset.flux.IMesureFluxDAO;
import org.inra.ecoinfo.foret.dataset.flux.entity.MesureFlux_m;


/**
 * @author philippe
 * 
 */
public class JPAMesureFlux_mDAO extends JPAMesureFluxDAO<MesureFlux_m> implements IMesureFluxDAO<MesureFlux_m> {

    /**
     *
     * @return
     */
    @Override
    public Class<MesureFlux_m> getMeasureClass() {
        return MesureFlux_m.class;
    }
}
