/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.meteo.jpa;

import org.inra.ecoinfo.foret.dataset.meteo.IValeurMeteoTourDAO;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_sh;


/**
 * @author philippe
 * 
 */
public class JPAValeurMeteo_shTourDAO extends JPAValeurMeteoDAO<ValeurMeteo_sh> implements IValeurMeteoTourDAO<ValeurMeteo_sh> {

}
