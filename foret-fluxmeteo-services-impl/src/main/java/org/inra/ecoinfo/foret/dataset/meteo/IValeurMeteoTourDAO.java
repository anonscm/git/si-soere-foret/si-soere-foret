package org.inra.ecoinfo.foret.dataset.meteo;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;


/**
 * @author philippe
 * 
 * @param <T>
 */
public interface IValeurMeteoTourDAO<T> extends IDAO<ValeurMeteo> {

}
