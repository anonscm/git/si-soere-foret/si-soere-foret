/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.meteo.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo;


/**
 * @author philippe
 * @param <T>
 * 
 */
public class JPAValeurMeteoDAO<T> extends AbstractJPADAO<ValeurMeteo> {

}
