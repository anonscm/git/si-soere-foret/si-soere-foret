package org.inra.ecoinfo.foret.dataset.meteo.jpa;

import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo_j;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_j;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author philippe
 * 
 */
public class JPAPublicationMeteo_jDAO extends JPAPublicationMeteoDAO {

    
    /**
     *
     * @param versionId
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(Long versionId) throws PersistenceException {
        super.removeVersion(versionId, MesureMeteo_j.class, ValeurMeteo_j.class);
    }
}