package org.inra.ecoinfo.foret.dataset.meteo.jpa;

import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo_sh;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_sh;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author philippe
 * 
 */
public class JPAPublicationMeteo_shDAO extends JPAPublicationMeteoDAO {

   
    /**
     *
     * @param versionId
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(Long versionId) throws PersistenceException {
        super.removeVersion(versionId, MesureMeteo_sh.class, ValeurMeteo_sh.class);
    }
}