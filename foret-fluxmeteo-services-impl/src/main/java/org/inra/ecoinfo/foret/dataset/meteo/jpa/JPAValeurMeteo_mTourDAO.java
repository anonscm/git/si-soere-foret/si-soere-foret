/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.meteo.jpa;

import org.inra.ecoinfo.foret.dataset.meteo.IValeurMeteoTourDAO;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_m;


/**
 * @author philippe
 * 
 */
public class JPAValeurMeteo_mTourDAO extends JPAValeurMeteoDAO<ValeurMeteo_m> implements IValeurMeteoTourDAO<ValeurMeteo_m> {

}
