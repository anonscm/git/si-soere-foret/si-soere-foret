/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.flux.jpa;

import org.inra.ecoinfo.foret.dataset.flux.IValeurFluxTourDAO;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFlux_mTour;


/**
 * @author philippe
 * 
 */
public class JPAValeurFlux_mTourDAO extends JPAValeurFluxTourDAO<ValeurFlux_mTour> implements IValeurFluxTourDAO<ValeurFlux_mTour> {

}
