package org.inra.ecoinfo.foret.dataset.meteo;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Optional;
import javax.persistence.Tuple;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo;


/**
 * @author philippe
 * 
 * @param <T>
 */
public interface IMesureMeteoDAO<T extends MesureMeteo> extends IDAO<T> {

    /**
     *
     * @param date
     * @param time
     * @return
     */
    Optional<Tuple> getLinePublicationNameDoublon(LocalDate date, LocalTime time);

    /**
     *
     * @return
     */
    Class<T> getMeasureClass();
}
