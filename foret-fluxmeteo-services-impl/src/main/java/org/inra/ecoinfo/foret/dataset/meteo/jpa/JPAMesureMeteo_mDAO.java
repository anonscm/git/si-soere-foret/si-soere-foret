/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.meteo.jpa;

import org.inra.ecoinfo.foret.dataset.meteo.IMesureMeteoDAO;
import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo_m;

/**
 * @author philippe
 *
 */
public class JPAMesureMeteo_mDAO extends JPAMesureMeteoDAO<MesureMeteo_m> implements IMesureMeteoDAO<MesureMeteo_m> {

    @Override
    public Class<MesureMeteo_m> getMeasureClass() {
        return MesureMeteo_m.class;
    }
}
