package org.inra.ecoinfo.foret.dataset.fluxmeteo.entete;

import org.inra.ecoinfo.foret.utils.Constantes;

/**
 *
 * @author ptcherniati
 */
@SuppressWarnings("rawtypes")
public class SpecialisteEnteteFlux extends AbstractVerificateurEnteteFluxMeteo {

    /**
     *
     */
    public SpecialisteEnteteFlux() {
        this.datatypeName = Constantes.THEME_FLUX;
    }
}
