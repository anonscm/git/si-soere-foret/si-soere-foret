package org.inra.ecoinfo.foret.dataset.fluxmeteo.entete;

import org.inra.ecoinfo.foret.utils.Constantes;

/**
 *
 * @author ptcherniati
 */
@SuppressWarnings("rawtypes")
public class SpecialisteEnteteMeteo extends AbstractVerificateurEnteteFluxMeteo {

    /**
     *
     */
    public SpecialisteEnteteMeteo() {
        this.datatypeName = Constantes.THEME_METEO;
    }
}
