package org.inra.ecoinfo.foret.dataset.flux;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFluxTour;


/**
 * @author philippe
 * 
 * @param <T>
 */
public interface IValeurFluxTourDAO<T> extends IDAO<ValeurFluxTour> {

}
