/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.flux.jpa;

import org.inra.ecoinfo.foret.dataset.flux.IValeurFluxTourDAO;
import org.inra.ecoinfo.foret.dataset.flux.entity.ValeurFlux_jTour;


/**
 * @author philippe
 * 
 */
public class JPAValeurFlux_jTourDAO extends JPAValeurFluxTourDAO<ValeurFlux_jTour> implements IValeurFluxTourDAO<ValeurFlux_jTour> {

}
