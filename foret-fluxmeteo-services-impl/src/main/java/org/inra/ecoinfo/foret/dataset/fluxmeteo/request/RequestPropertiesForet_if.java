package org.inra.ecoinfo.foret.dataset.fluxmeteo.request;

/**
 *
 * @author ptcherniati
 */
public class RequestPropertiesForet_if extends RequestPropertiesFluxMeteo {

    /**
     *
     */
    protected int pas;

    /**
     *
     */
    public RequestPropertiesForet_if() {
        super();
        this.verifierSequenceDate = true;
        this.pas = 30;
    }

    /**
     *
     * @return
     */
    public int getPas() {
        return this.pas;
    }

    /**
     *
     * @param pas
     */
    public void setPas(int pas) {
        this.pas = pas;
    }
}
