/**
 * OREIForets project - see LICENCE.txt for use created: 5 mai 2009 11:50:44
 */
package org.inra.ecoinfo.foret.dataset.meteo.jpa;

import org.inra.ecoinfo.foret.dataset.meteo.IValeurMeteoTourDAO;
import org.inra.ecoinfo.foret.dataset.meteo.entity.ValeurMeteo_j;


/**
 * @author philippe
 * 
 */
public class JPAValeurMeteo_jTourDAO extends JPAValeurMeteoDAO<ValeurMeteo_j> implements IValeurMeteoTourDAO<ValeurMeteo_j> {

}
