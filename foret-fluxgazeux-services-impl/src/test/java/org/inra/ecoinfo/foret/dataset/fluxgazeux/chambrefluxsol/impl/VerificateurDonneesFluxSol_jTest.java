
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 *
 * @author tcherniatinsky
 */
public class VerificateurDonneesFluxSol_jTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }
    
    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    MockUtils m = MockUtils.getInstance();
    VerificateurDonneesFluxSol_j instance;

    /**
     *
     */
    public VerificateurDonneesFluxSol_jTest() {
    }
    
    /**
     *
     */
    @Before
    public void setUp() {
        instance = new VerificateurDonneesFluxSol_j();
        instance.setLocalizationManager(m.localizationManager);
    }
    
    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of getMessageContinuiteDate method, of class VerificateurDonneesFluxSol_j.
     */
    @Test
    public void testGetMessageContinuiteDate() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        instance.getMessageContinuiteDate(m.dateDebut, badsFormatsReport);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Pas de continuité dans les dates : la date 01/01/2012 est manquante. </p>", badsFormatsReport.getHTMLMessages());
    }
    
}
