
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;


import java.time.LocalDateTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.foret.dataset.ExpectedColumn;
import org.inra.ecoinfo.foret.dataset.ITestDuplicates;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IRequestPropertiesFluxSol;
import org.inra.ecoinfo.foret.dataset.impl.GenericTestValuesTest.SpecificRequestPropertiesTest;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.doNothing;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;
import org.mockito.Spy;

/**
 *
 * @author tcherniatinsky
 */
public class AbstractVerificateurDonneesFluxSolTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    @Spy
    AbstractVerificateurDonneesFluxSol instance;

    MockUtils m;
    @Mock
    DatasetDescriptor datasetDescriptor;
    @Mock
    IDatatypeVariableUniteForetDAO datatypeUniteVariableDAO;
    @Mock
    IRequestPropertiesFluxSol requestProperties;
    @Mock
    SpecificRequestPropertiesTest requestPropertiesIntervalValue;
    @Mock
    Map<String, DatatypeVariableUniteForet> variablesTypeDonnees;
    @Mock
    ITestDuplicates testDuplicates;
    @Mock
    DatatypeVariableUniteForet datatypeUniteVariable;
    @Mock
    Column column;
    @Mock
    Column columnVariable;
    @Mock
    ExpectedColumn expectedColumnVariable;
    @Mock
    Column columnVariableGF;
    @Mock
    ExpectedColumn expectedColumnVariableGF;
    @Mock
    Column columnNbMesure;
    @Mock
    ExpectedColumn expectedColumnNbMesure;
    @Mock
    Column columnET;
    @Mock
    ExpectedColumn expectedColumnET;
    @Mock
    Column columnQC;
    @Mock
    ExpectedColumn expectedColumnQC;
    String datatypeName = "datatype";
    List<Column> columns = new LinkedList();
    @Mock
    Map<String, ExpectedColumn> expectedColumns;
    @Mock
    Column columnDate;
    @Mock
    ITraitementDAO traitementDAO;

    @Mock
    Column columntime;

    /**
     *
     */
    public AbstractVerificateurDonneesFluxSolTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new AbstractVerificateurDonneesFluxSolImpl();
        MockitoAnnotations.initMocks(this);
        this.m = MockUtils.getInstance();
        this.instance.setDatatypeVariableUniteForetDAO(this.datatypeUniteVariableDAO);
        instance.setTraitementDAO(traitementDAO);
        doReturn(this.testDuplicates).when(this.instance).getTestDuplicates(this.requestProperties);
        when(this.datatypeUniteVariableDAO.getAllVariableTypeDonneesByDataType(this.datatypeName)).thenReturn(this.variablesTypeDonnees);
        when(this.variablesTypeDonnees.containsKey("colonne")).thenReturn(true);
        when(this.variablesTypeDonnees.get("colonne")).thenReturn(this.datatypeUniteVariable);
        this.columns.add(this.columnDate);
        this.columns.add(this.columntime);
        this.columns.add(this.columnVariable);
        this.columns.add(this.columnET);
        this.columns.add(this.columnNbMesure);
        this.columns.add(this.columnQC);
        this.columns.add(this.columnVariableGF);
        when(this.columnDate.getName()).thenReturn("date");
        when(this.columnDate.getFormatType()).thenReturn("dd/MM/yyyy");
        when(this.columnDate.getFlagType()).thenReturn(Constantes.PROPERTY_CST_DATE_TYPE);
        when(this.columntime.getName()).thenReturn("time");
        when(this.columntime.getFormatType()).thenReturn("HH:mm");
        when(this.columntime.getFlagType()).thenReturn(Constantes.PROPERTY_CST_TIME_TYPE);
        when(this.columnVariable.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        when(this.columnVariable.isVariable()).thenReturn(Boolean.TRUE);
        when(this.columnVariable.getName()).thenReturn("variable");
        when(this.columnVariableGF.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_GF);
        when(this.columnVariableGF.getName()).thenReturn("variableGF");
        when(this.columnET.getFlagType()).thenReturn(Constantes.PROPERTY_CST_ECART_TYPE);
        when(this.columnET.getName()).thenReturn("variableET");
        when(this.columnNbMesure.getFlagType()).thenReturn(Constantes.PROPERTY_CST_NBRE_MESURES);
        when(this.columnNbMesure.getName()).thenReturn("variableNbMesure");
        when(this.columnQC.getFlagType()).thenReturn(Constantes.PROPERTY_CST_QUALITY_CODE);
        when(this.columnQC.getName()).thenReturn("variableQC");
        when(this.column.getName()).thenReturn("colonne");

        when(requestProperties.getColumn(0)).thenReturn(columnDate);
        when(requestProperties.getColumn(1)).thenReturn(columntime);
        when(requestProperties.getColumn(2)).thenReturn(columnVariable);
        when(requestProperties.getColumn(3)).thenReturn(columnET);
        when(requestProperties.getColumn(4)).thenReturn(columnNbMesure);
        when(requestProperties.getColumn(5)).thenReturn(columnQC);
        when(requestProperties.getColumn(6)).thenReturn(columnVariableGF);
        when(requestProperties.getExpectedColumns()).thenReturn(expectedColumns);
        when(expectedColumns.get("variable")).thenReturn(expectedColumnVariable);
        when(expectedColumns.get("variableET")).thenReturn(expectedColumnET);
        when(expectedColumns.get("variableNbMesure")).thenReturn(expectedColumnNbMesure);
        when(expectedColumns.get("variableQC")).thenReturn(expectedColumnQC);
        when(expectedColumns.get("variableGF")).thenReturn(expectedColumnVariableGF);
        when(expectedColumnVariableGF.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_GF);
        when(expectedColumnVariableGF.getName()).thenReturn("variableGF");
        when(expectedColumnVariable.getName()).thenReturn("variable");
        when(expectedColumnVariableGF.getRefVariableName()).thenReturn("variable");
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of verifieVariableAndVariableGfCoherente method, of class
     * AbstractVerificateurDonneesFluxSol.
     */
    @Test
    public void testVerifieVariableAndVariableGfCoherente() {
        String[] values = new String[]{"25/12/2004", "23:00", "-9999", "14.2", "2", "0", "13.6"};
        long lineNumber = 4L;
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        //with bad value for variable referencing by varGF
        instance.verifieVariableAndVariableGfCoherente(requestProperties, datasetDescriptor, values, lineNumber, badsFormatsReport);
        assertFalse(badsFormatsReport.hasErrors());
         //with existing value for variable referencing by varGF
//        values = new String[]{"25/12/2004", "23:00", "12.5", "14.2", "2", "0", "13.6"};
//        instance.verifieVariableAndVariableGfCoherente(requestProperties, datasetDescriptor, values, lineNumber, badsFormatsReport);
//        assertTrue(badsFormatsReport.hasErrors());
//        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Erreur ligne 4 : La variable gapfillée variableGF a la valeur 13.6 alors que la variable variable a aussi une valeur 12.5. La valeur de la variable devrait être -9999 ou la variable gapfillée ne devrait pas avoir de valeur </p>", badsFormatsReport.getHTMLMessages());
        
    }

    /**
     * Test of verifieLine method, of class AbstractVerificateurDonneesFluxSol.
     */
    @Test
    public void testVerifieLine() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        String[] values = new String[]{"25/12/2004", "23:00", "12.5", "14.2", "2", "0", "13.6"};
        doNothing().when(instance).verifieLine(5L, m.versionFile, requestProperties, badsFormatsReport, datasetDescriptor, datatypeName, values);
        instance.verifieLine(5L, m.versionFile, requestProperties, badsFormatsReport, datasetDescriptor, datatypeName, values);
        verify(instance).verifieLine(5L, m.versionFile, requestProperties, badsFormatsReport, datasetDescriptor, datatypeName, values);
    }

    /**
     * Test of getVariablesValues method, of class
     * AbstractVerificateurDonneesFluxSol.
     */
    @Test
    public void testGetVariablesValues() {
        String[] values = new String[]{"25/12/2004", "23:00", "12.5", "14.2", "2", "0", "13.6"};
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        List<VariableValue> result = instance.getVariablesValues(values, 5L, datasetDescriptor, badsFormatsReport, requestProperties);
        testVariableValue(result, 0, "12.5", expectedColumnVariable);
        testVariableValue(result, 1, "14.2", expectedColumnET);
        testVariableValue(result, 2, "2", expectedColumnNbMesure);
        testVariableValue(result, 3, "0", expectedColumnQC);
        testVariableValue(result, 4, "13.6", expectedColumnVariableGF);
    }

    private void testVariableValue(List<VariableValue> result, int index, String value, ExpectedColumn expectedColmumn) {
        assertEquals(value, result.get(index).getValue());
        assertEquals(expectedColmumn, result.get(index).getExpectedColumn());
    }

    /**
     *
     */
    public class AbstractVerificateurDonneesFluxSolImpl extends AbstractVerificateurDonneesFluxSol {

        @Override
        public void getMessageContinuiteDate(LocalDateTime date, BadsFormatsReport badsFormatsReport) {
        }
    }

}
