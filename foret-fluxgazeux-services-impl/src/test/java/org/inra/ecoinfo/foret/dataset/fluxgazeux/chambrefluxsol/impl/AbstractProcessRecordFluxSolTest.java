
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.StringReader;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ErrorsReport;
import org.inra.ecoinfo.foret.dataset.ExpectedColumn;
import org.inra.ecoinfo.foret.dataset.IFabrique;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IMesureChambreFluxSolDAO;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.dto.ValeurComplementChaineDTO;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.dto.ValeurComplementEntierDTO;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.dto.ValeurComplementReelDTO;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationEntier;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationReel;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.fabrique.FabriqueInfoChaine;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.fabrique.FabriqueInfoComplementaire;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.fabrique.FabriqueInfoEntier;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.fabrique.FabriqueInfoReel;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import static org.mockito.Matchers.*;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author tcherniatinsky
 */
public class AbstractProcessRecordFluxSolTest {

    /**
     *
     */
    protected static final String COLONNE_6 = "colonne 6";

    /**
     *
     */
    protected static final String COLONNE_5 = "colonne 5";

    /**
     *
     */
    protected static final String COLONNE_4 = "colonne 4";

    /**
     *
     */
    protected static final String COLONNE_3 = "colonne 3";

    /**
     *
     */
    protected static final String COLONNE_2 = "colonne 2";

    /**
     *
     */
    protected static final String COLONNE_1 = "colonne 1";

    /**
     *
     */
    protected static final String[] COLUMN_NAMES = new String[]{"date", COLONNE_1, COLONNE_2, COLONNE_3, COLONNE_4, COLONNE_5, COLONNE_6};

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }
    AbstractProcessRecordFluxSolImpl instance;
    MockUtils m = MockUtils.getInstance();
    @Mock
    IMesureChambreFluxSolDAO<M> mesureChambreFluxSolDAO;
    @Mock
    Line line;
    @Mock
    DatasetDescriptorFluxSol datasetDescriptor;
    @Mock
    RequestPropertiesFluxSol requestProperties;
    @Mock
    ExpectedColumn expectedColumn1;
    @Mock
    ExpectedColumn expectedColumn2;
    @Mock
    ExpectedColumn expectedColumn3;
    @Mock
    ExpectedColumn expectedColumn4;
    @Mock
    ExpectedColumn expectedColumn5;
    @Mock
    ExpectedColumn expectedColumn6;
    @Mock
    DatatypeVariableUniteForet dvu1;
    @Mock
    DatatypeVariableUniteForet dvu2;
    @Mock
    DatatypeVariableUniteForet dvu3;
    @Mock
    DatatypeVariableUniteForet dvu4;
    @Mock
    DatatypeVariableUniteForet dvu5;
    @Mock
    DatatypeVariableUniteForet dvu6;
    @Mock
    RealNode realNode1;
    @Mock
    RealNode realNode2;
    @Mock
    RealNode realNode3;
    @Mock
    RealNode realNode4;
    @Mock
    RealNode realNode5;
    @Mock
    RealNode realNode6;
    @Mock
    VariableForet variable1;
    @Mock
    VariableForet variable2;
    @Mock
    VariableForet variable3;
    @Mock
    VariableForet variable4;
    @Mock
    VariableForet variable5;
    @Mock
    VariableForet variable6;
    Map<String, ExpectedColumn> expectedColumns = new HashMap();
    Map<String, RealNode> realnodes = new HashMap();
    Map<Variable, RealNode> realnodesByVariable = new HashMap();

    /**
     *
     */
    public AbstractProcessRecordFluxSolTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        instance = new AbstractProcessRecordFluxSolImpl();
        init(instance);
        when(datasetDescriptor.getEnTete()).thenReturn(1);
        when(requestProperties.getColumnNames()).thenReturn(COLUMN_NAMES);
        when(expectedColumn1.getName()).thenReturn(COLONNE_1);
        when(expectedColumn2.getName()).thenReturn(COLONNE_2);
        when(expectedColumn3.getName()).thenReturn(COLONNE_3);
        when(expectedColumn4.getName()).thenReturn(COLONNE_4);
        when(expectedColumn5.getName()).thenReturn(COLONNE_5);
        when(expectedColumn6.getName()).thenReturn(COLONNE_6);
        when(expectedColumn1.isVariable()).thenReturn(true);
        when(expectedColumn2.getRefVariableName()).thenReturn(COLONNE_1);
        when(expectedColumn3.getFlagType()).thenReturn(Constantes.PROPERTY_CST_GAP_FIELD_TYPE);
        when(expectedColumn4.isVariable()).thenReturn(true);
        when(expectedColumn4.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        when(expectedColumn5.getRefVariableName()).thenReturn(COLONNE_4);
        when(expectedColumn5.getFlagType()).thenReturn(Constantes.PROPERTY_CST_NBRE_MESURES);
        when(expectedColumn6.getRefVariableName()).thenReturn(COLONNE_4);
        when(expectedColumn6.getFlagType()).thenReturn(Constantes.PROPERTY_CST_QUALITY_CODE);
        expectedColumns.put(COLONNE_1, expectedColumn1);
        expectedColumns.put(COLONNE_2, expectedColumn2);
        expectedColumns.put(COLONNE_3, expectedColumn3);
        expectedColumns.put(COLONNE_4, expectedColumn4);
        expectedColumns.put(COLONNE_5, expectedColumn5);
        expectedColumns.put(COLONNE_6, expectedColumn6);
        when(realNode1.getNodeable()).thenReturn(dvu1);
        when(realNode2.getNodeable()).thenReturn(dvu2);
        when(realNode3.getNodeable()).thenReturn(dvu3);
        when(realNode4.getNodeable()).thenReturn(dvu4);
        when(realNode5.getNodeable()).thenReturn(dvu5);
        when(realNode6.getNodeable()).thenReturn(dvu6);
        when(dvu1.getVariable()).thenReturn(variable1);
        when(dvu2.getVariable()).thenReturn(variable2);
        when(dvu3.getVariable()).thenReturn(variable3);
        when(dvu4.getVariable()).thenReturn(variable4);
        when(dvu5.getVariable()).thenReturn(variable5);
        when(dvu6.getVariable()).thenReturn(variable6);
        when(variable1.getCode()).thenReturn(COLONNE_1);
        when(variable2.getCode()).thenReturn(COLONNE_2);
        when(variable3.getCode()).thenReturn(COLONNE_3);
        when(variable4.getCode()).thenReturn(COLONNE_4);
        when(variable5.getCode()).thenReturn(COLONNE_5);
        when(variable6.getCode()).thenReturn(COLONNE_6);
        when(requestProperties.getExpectedColumns()).thenReturn(expectedColumns);
        realnodesByVariable.put(variable1, realNode1);
        realnodesByVariable.put(variable2, realNode2);
        realnodesByVariable.put(variable3, realNode3);
        realnodesByVariable.put(variable4, realNode4);
        realnodesByVariable.put(variable5, realNode5);
        realnodesByVariable.put(variable6, realNode6);
        realnodes.put(COLONNE_1, realNode1);
        realnodes.put(COLONNE_2, realNode2);
        realnodes.put(COLONNE_3, realNode3);
        realnodes.put(COLONNE_4, realNode4);
        realnodes.put(COLONNE_5, realNode5);
        realnodes.put(COLONNE_6, realNode6);
    }

    private void init(AbstractProcessRecordFluxSol instance) {
        MockitoAnnotations.initMocks(this);
        instance.setDatatypeVariableUniteForetDAO(m.datatypeVariableUniteDAO);
        instance.setLocalizationManager(m.localizationManager);
        instance.setMesureChambreFluxSolDAO(mesureChambreFluxSolDAO);
        instance.setSiteForetDAO(m.siteDAO);
        instance.setTraitementDAO(m.traitementDAO);
        instance.setVariableDAO(m.variableDAO);
        instance.setVersionFileDAO(m.versionFileDAO);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of setSiteForetDAO method, of class AbstractProcessRecordFluxSol.
     */
    @Test
    public void testSetSiteForetDAO() {
        assertEquals(m.siteDAO, instance.siteForetDAO);
    }

    /**
     * Test of createLstTotalLstVariableEtInfosAssocies method, of class
     * AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateLstTotalLstVariableEtInfosAssocies() throws Exception {
        List<VariableValue> variableValues = new LinkedList();
        addVariableValues(variableValues, expectedColumn1);
        addVariableValues(variableValues, expectedColumn2);
        addVariableValues(variableValues, expectedColumn3);
        addVariableValues(variableValues, expectedColumn4);
        addVariableValues(variableValues, expectedColumn5);
        addVariableValues(variableValues, expectedColumn6);
        when(line.getLstVariablesValues()).thenReturn(variableValues);
        Collection<List<VariableValue>> result = instance.createLstTotalLstVariableEtInfosAssocies(line);
        assertTrue(3 == result.size());
        Iterator<List<VariableValue>> iterator = result.iterator();
        final List<VariableValue> list1 = iterator.next();
        assertTrue(1 == list1.size());
        final List<VariableValue> list2 = iterator.next();
        assertTrue(2 == list2.size());
        final List<VariableValue> list3 = iterator.next();
        assertTrue(3 == list3.size());
        assertTrue(variableValues.isEmpty());
    }

    /**
     *
     * @param variableValues
     * @param expectedColumn
     * @return
     */
    protected VariableValue addVariableValues(List<VariableValue> variableValues, ExpectedColumn expectedColumn) {
        VariableValue vv = mock(VariableValue.class);
        variableValues.add(vv);
        when(vv.getExpectedColumn()).thenReturn(expectedColumn);
        return vv;
    }

    /**
     * Test of createValeurChambrefluxSolInfosComplt method, of class
     * AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateValeurChambrefluxSolInfosComplt() throws Exception {
        List<VariableValue> variableValues = new LinkedList();
        addVariableValues(variableValues, expectedColumn4);
        addVariableValues(variableValues, expectedColumn5);
        addVariableValues(variableValues, expectedColumn6);
        when(variableValues.get(0).getValue()).thenReturn("13.2");
        when(variableValues.get(1).getValue()).thenReturn("15");
        when(variableValues.get(2).getValue()).thenReturn("17");
        FabriqueInfoReel fr = mock(FabriqueInfoReel.class);
        FabriqueInfoComplementaire.setFabriqueReel(fr);
        FabriqueInfoEntier fe = mock(FabriqueInfoEntier.class);
        FabriqueInfoComplementaire.setFabriqueEntier(fe);
        FabriqueInfoChaine fc = mock(FabriqueInfoChaine.class);
        FabriqueInfoComplementaire.setFabriqueChaine(fc);
        ValeurInformationReel infoCpl1 = mock(ValeurInformationReel.class);
        ValeurInformationEntier infoCpl2 = mock(ValeurInformationEntier.class);
        ValeurInformationReel infoCpl3 = mock(ValeurInformationReel.class);
        when(fr.construire(any(IFabrique.class), any(ValeurComplementReelDTO.class))).thenReturn(infoCpl1);
        when(fe.construire(any(IFabrique.class), any(ValeurComplementEntierDTO.class))).thenReturn(infoCpl2);
        when(fc.construire(any(IFabrique.class), any(ValeurComplementChaineDTO.class))).thenReturn(infoCpl3);
        when(expectedColumn4.getFlagType()).thenReturn(Constantes.PROPERTY_CST_ECART_TYPE);
        ValeurChambreFluxSol valeurChambreFluxSol = mock(ValeurChambreFluxSol.class);
        AbstractProcessRecordFluxSol instance = new AbstractProcessRecordFluxSolImpl();
        instance.createValeurChambrefluxSolInfosComplt(variableValues, valeurChambreFluxSol);
        ArgumentCaptor<IFabrique> fabriqueR = ArgumentCaptor.forClass(IFabrique.class);
        ArgumentCaptor<ValeurComplementReelDTO> dtoR = ArgumentCaptor.forClass(ValeurComplementReelDTO.class);
        ArgumentCaptor<IFabrique> fabriqueE = ArgumentCaptor.forClass(IFabrique.class);
        ArgumentCaptor<ValeurComplementEntierDTO> dtoE = ArgumentCaptor.forClass(ValeurComplementEntierDTO.class);
        ArgumentCaptor<IFabrique> fabriqueC = ArgumentCaptor.forClass(IFabrique.class);
        ArgumentCaptor<ValeurComplementChaineDTO> dtoC = ArgumentCaptor.forClass(ValeurComplementChaineDTO.class);
        verify(fr).construire(fabriqueR.capture(), dtoR.capture());
        IFabrique vr = fabriqueR.getValue();
        ValeurComplementReelDTO vcR = dtoR.getValue();
        assertEquals("colonne 4", vcR.getNom());
        assertTrue(13.2 == vcR.getValeurInfo());
        verify(valeurChambreFluxSol).ajouterInfoComplementaire(infoCpl1);
        verify(fe).construire(fabriqueE.capture(), dtoE.capture());
        IFabrique ve = fabriqueE.getValue();
        ValeurComplementEntierDTO vcE = dtoE.getValue();
        assertEquals("colonne 5", vcE.getNom());
        assertTrue(15 == vcE.getValeurInfo());
        verify(valeurChambreFluxSol).ajouterInfoComplementaire(infoCpl2);
        verify(fc).construire(fabriqueC.capture(), dtoC.capture());
        IFabrique vc = fabriqueC.getValue();
        ValeurComplementChaineDTO vcC = dtoC.getValue();
        assertEquals("colonne 6", vcC.getNom());
        assertTrue("17".equals(vcC.getValeurInfo()));
        verify(valeurChambreFluxSol).ajouterInfoComplementaire(infoCpl3);
    }

    /**
     * Test of processRecord method, of class AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        CSVParser parser = mock(CSVParser.class);
        List<Line> lines = mock(List.class, "lines");
        Map<String, RealNode> dbRealNodes = mock(Map.class);
        instance= spy(instance);
        when(m.versionFileDAO.merge(m.versionFile)).thenReturn(m.versionFile);
        doReturn(realnodes).when(instance).buildVariablesAndkipHeader(parser, (DatasetDescriptorFluxSol) datasetDescriptor, (RequestPropertiesFluxSol) requestProperties, m.datatypeRealNode);
        doReturn(lines).when(instance).createLines(eq(parser), any(BadsFormatsReport.class), eq(datasetDescriptor), eq(requestProperties));
        doNothing().when(instance).buildMesuresChambreFluxSol(eq(m.versionFile), eq(lines), any(ErrorsReport.class), eq(realnodes));
        instance.processRecord(parser, m.versionFile, requestProperties, "utf-8", datasetDescriptor);
        verify(instance).buildVariablesAndkipHeader(parser, (DatasetDescriptorFluxSol) datasetDescriptor, (RequestPropertiesFluxSol) requestProperties, m.datatypeRealNode);
        verify(instance).createLines(eq(parser), any(BadsFormatsReport.class), eq(datasetDescriptor), eq(requestProperties));
        verify(instance).buildMesuresChambreFluxSol(eq(m.versionFile), eq(lines), any(ErrorsReport.class), eq(realnodes));
    }

    /**
     * Test of setMesureChambreFluxSolDAO method, of class
     * AbstractProcessRecordFluxSol.
     */
    @Test
    public void testSetMesureChambreFluxSolDAO() {
        assertEquals(mesureChambreFluxSolDAO, instance.mesureChambreFluxSolDAO);
    }

    /**
     * Test of createLines method, of class AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateLines() throws Exception {
        StringReader in = new StringReader("ligne1\nligne2\nligne2");
        CSVParser parser = new CSVParser(in, ';');
        parser.getLine();
        assertTrue(1 == parser.getLastLineNumber());
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        instance = spy(instance);
        doReturn(line).when(instance).createLine(any(String[].class), anyLong(), eq(badsFormatsReport), eq(datasetDescriptor), eq(requestProperties));
        List<Line> result = instance.createLines(parser, badsFormatsReport, datasetDescriptor, requestProperties);
        assertTrue(2 == result.size());
        assertEquals(line, result.get(0));
        assertEquals(line, result.get(1));
    }

    /**
     * Test of createLine method, of class AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateLine() throws Exception {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        String[] values = new String[]{"date", "12.1F", "12.2F", "12.3F", "12.4F", "12.5F", "12.6F"};
        instance = spy(instance);
        final AbstractProcessRecordFluxSol.GenericValues genericValues = instance.newGenericValues();
        genericValues.genericIndex++;
        genericValues.codeTraitemement = m.TRAITEMENT;
        genericValues.date = m.dateDebutComplete.toLocalDate();
        genericValues.heure = m.dateDebutComplete.toLocalTime();
        genericValues.noChambre = "1";
        genericValues.dureeMesure = 10;
        doReturn(genericValues).when(instance).readGenericValues(values, datasetDescriptor, badsFormatsReport, 6L);
        Line result = instance.createLine(values, 6L, badsFormatsReport, datasetDescriptor, requestProperties);
        verify(instance).readGenericValues(values, datasetDescriptor, badsFormatsReport, 6L);
        assertNotNull(result);
        assertEquals(m.dateDebutComplete.toLocalDate(), result.getDate());
        assertEquals(m.dateDebutComplete.toLocalTime(), result.getTime());
        assertEquals(m.TRAITEMENT, result.getCodeTraitemement());
        assertEquals("1", result.getNoChambre());
        assertTrue(10 == result.getDureeMesure());
        assertTrue(6L == result.getLineNo());
        final List<VariableValue> lstVariablesValues = result.getLstVariablesValues();
        assertNotNull(lstVariablesValues);
        assertTrue(6 == lstVariablesValues.size());
        assertEquals(expectedColumn1, lstVariablesValues.get(0).getExpectedColumn());
        assertEquals("12.1F", lstVariablesValues.get(0).getValue());
        assertEquals(expectedColumn2, lstVariablesValues.get(1).getExpectedColumn());
        assertEquals("12.2F", lstVariablesValues.get(1).getValue());
        assertEquals(expectedColumn3, lstVariablesValues.get(2).getExpectedColumn());
        assertEquals("12.3F", lstVariablesValues.get(2).getValue());
        assertEquals(expectedColumn4, lstVariablesValues.get(3).getExpectedColumn());
        assertEquals("12.4F", lstVariablesValues.get(3).getValue());
        assertEquals(expectedColumn5, lstVariablesValues.get(4).getExpectedColumn());
        assertEquals("12.5F", lstVariablesValues.get(4).getValue());
        assertEquals(expectedColumn6, lstVariablesValues.get(5).getExpectedColumn());
        assertEquals("12.6F", lstVariablesValues.get(5).getValue());
    }

    /**
     * Test of buildVariablesAndkipHeader method, of class
     * AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildVariablesAndkipHeader() throws Exception {
        StringReader in = new StringReader("ligne1\nligne2");
        CSVParser parser = new CSVParser(in, ';');
        when(m.datatypeVariableUniteDAO.getRealNodesVariables(m.datatypeRealNode)).thenReturn(realnodesByVariable);
        Map<String, RealNode> result = instance.buildVariablesAndkipHeader(parser, datasetDescriptor, requestProperties, m.datatypeRealNode);
        assertTrue(6 == result.size());
        assertTrue(1 == parser.getLastLineNumber());
        assertEquals(realNode1, result.get(COLONNE_1));
        assertEquals(realNode2, result.get(COLONNE_2));
        assertEquals(realNode3, result.get(COLONNE_3));
        assertEquals(realNode4, result.get(COLONNE_4));
        assertEquals(realNode5, result.get(COLONNE_5));
        assertEquals(realNode6, result.get(COLONNE_6));
    }

    /**
     * Test of readGenericValues method, of class AbstractProcessRecordFluxSol.
     */
    /*@Test
    public void testReadGenericValues() {
        System.out.println("readGenericValues");
        String[] values = null;
        DatasetDescriptor datasetDescriptor = null;
        BadsFormatsReport badsFormatsReport = null;
        AbstractProcessRecordFluxSol instance = new AbstractProcessRecordFluxSolImpl();
        AbstractProcessRecordFluxSol.GenericValues expResult = null;
        AbstractProcessRecordFluxSol.GenericValues result = instance.readGenericValues(values, datasetDescriptor, badsFormatsReport);
        assertEquals(expResult, result);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    /**
     * Test of readDate method, of class AbstractProcessRecordFluxSol.
     */
    @Test
    public void testReadDate() {
        String value = m.DATE_DEBUT;
        Column column = mock(Column.class);
        when(column.getFormatType()).thenReturn(DateUtil.DD_MM_YYYY);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        //nominal
        LocalDate result = instance.readDate(value, badsFormatsReport, column, 5L,12);
        assertFalse(badsFormatsReport.hasErrors());
        assertEquals(m.dateDebutComplete.toLocalDate(), result);
        //bad format date
        badsFormatsReport = new BadsFormatsReport("erreur");
        when(column.getFormatType()).thenReturn(DateUtil.DD_MM_YYYY_HH_MM);
        when(column.getFieldName()).thenReturn("fieldname");
        result = instance.readDate(value, badsFormatsReport, column, 5L,12);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals( "<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- \"01/01/2012\" n'est pas un format de date valide à la ligne 5 colonne 12 (fieldname). La date doit-être au format \"dd/MM/yyyy HH:mm\" </p>", badsFormatsReport.getHTMLMessages());

    }

    /**
     * Test of readTime method, of class AbstractProcessRecordFluxSol.
     */
    @Test
    public void testReadTime() {
        String value = "02:23";
        Column column = mock(Column.class);
        when(column.getFormatType()).thenReturn(DateUtil.HH_MM);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        //nominal
        LocalTime result = instance.readTime(value, badsFormatsReport, column, 5L,12);
        assertFalse(badsFormatsReport.hasErrors());
        assertEquals(value, DateUtil.getUTCDateTextFromLocalDateTime(result, DateUtil.HH_MM));
        //bad format date
        badsFormatsReport = new BadsFormatsReport("erreur");
        when(column.getFormatType()).thenReturn(DateUtil.DD_MM_YYYY_HH_MM);
        when(column.getFieldName()).thenReturn("fieldname");
        result = instance.readTime(value, badsFormatsReport, column, 5L,12);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- \"02:23\" n'est pas un format d'heure valide à la ligne 5 colonne 12 (fieldname). L'heure doit-être au format \"dd/MM/yyyy HH:mm\" </p>", badsFormatsReport.getHTMLMessages());
    }

    /**
     * Test of readDureeMesure method, of class AbstractProcessRecordFluxSol.
     */
    @Test
    public void testReadDureeMesure() {
        String value = "12";
        Column column = mock(Column.class);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        //nominal
        int result = instance.readDureeMesure(value, badsFormatsReport, column, 5L,12);
        assertFalse(badsFormatsReport.hasErrors());
        assertTrue(12 == result);
        //bad format date
        badsFormatsReport = new BadsFormatsReport("erreur");
        value = "douze";
        when(column.getFieldName()).thenReturn("fieldname");
        result = instance.readDureeMesure(value, badsFormatsReport, column, 5L,12);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- \"douze\" n'est pas une durée valide à la ligne 5 colonne 12 (fieldname). La durée doit être un entier. </p>", badsFormatsReport.getHTMLMessages());
    }

    /**
     * Test of getVariableValueForVariable method, of class
     * AbstractProcessRecordFluxSol.
     */
    @Test
    public void testGetVariableValueForVariable() {
        VariableValue variableValueVariable = mock(VariableValue.class);
        VariableValue variableValueNotVariable = mock(VariableValue.class);
        when(variableValueVariable.getExpectedColumn()).thenReturn(expectedColumn1);
        when(variableValueNotVariable.getExpectedColumn()).thenReturn(expectedColumn2);
        List<VariableValue> lstVariableEtInfosAssocies = new LinkedList();
        lstVariableEtInfosAssocies.add(variableValueNotVariable);
        lstVariableEtInfosAssocies.add(variableValueNotVariable);
        lstVariableEtInfosAssocies.add(variableValueNotVariable);
        lstVariableEtInfosAssocies.add(variableValueVariable);
        lstVariableEtInfosAssocies.add(variableValueNotVariable);
        assertTrue(5 == lstVariableEtInfosAssocies.size());
        VariableValue result = instance.getVariableValueForVariable(lstVariableEtInfosAssocies);
        assertEquals(variableValueVariable, result);
        assertTrue(4 == lstVariableEtInfosAssocies.size());
        assertTrue(-1 == lstVariableEtInfosAssocies.indexOf(variableValueVariable));
    }

    /**
     * Test of buildMesuresChambreFluxSol method, of class
     * AbstractProcessRecordFluxSol.
     */
    @Test
    public void testBuildMesuresChambreFluxSol() {
        ErrorsReport errorsReport = new ErrorsReport();
        Map<String, RealNode> dbVariables = realnodes;
        List<Line> lstLines = new LinkedList();
        lstLines.add(line);
        lstLines.add(line);
        when(line.getLineNo()).thenReturn(30L, 31L);
        when(line.getCodeTraitemement()).thenReturn(m.TRAITEMENT);
        //nominal
        instance = new InstanceBuildValeur();
        try {
            instance.buildMesuresChambreFluxSol(m.versionFile, lstLines, errorsReport, dbVariables);
            verify(mesureChambreFluxSolDAO, times(2)).saveOrUpdate(any(M.class));
            verify(mesureChambreFluxSolDAO, times(1)).flush();
            assertTrue(lstLines.isEmpty());
        } catch (PersistenceException | BusinessException ex) {
            fail();
        }
    }

    /**
     * Test of buildValeurs method, of class AbstractProcessRecordFluxSol.
     */
    /*@Test
    public void testBuildValeurs() throws Exception {
        System.out.println("buildValeurs");
        AbstractProcessRecordFluxSol instance = new AbstractProcessRecordFluxSolImpl();
        instance.buildValeurs(null);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }*/
    /**
     * Test of creeValeurChambreFluxSolInfosAssoc method, of class
     * AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreeValeurChambreFluxSolInfosAssoc() throws Exception {
        List<VariableValue> lstVariableEtInfosAssocies = mock(List.class);
        Map<String, RealNode> dbRealNodes = mock(Map.class);
        VariableValue variableValue = mock(VariableValue.class);
        V valeur = mock(V.class);
        instance = spy(instance);
        doReturn(variableValue).when(instance).getVariableValueForVariable(lstVariableEtInfosAssocies);
        when(variableValue.getValue()).thenReturn("12.6");
        when(variableValue.getExpectedColumn()).thenReturn(expectedColumn1);
        when(dbRealNodes.get(COLONNE_1)).thenReturn(realNode1);
        doReturn(valeur).when(instance).getNewValeurFluxSol(realNode1, 12.6F);
        doNothing().when(instance).createValeurChambrefluxSolInfosComplt(lstVariableEtInfosAssocies, valeur);
        when(lstVariableEtInfosAssocies.size()).thenReturn(3);
        ValeurChambreFluxSol result = instance.creeValeurChambreFluxSolInfosAssoc(lstVariableEtInfosAssocies, dbRealNodes);
        assertEquals(valeur, result);
        verify(instance).getVariableValueForVariable(lstVariableEtInfosAssocies);
        verify(instance).createValeurChambrefluxSolInfosComplt(lstVariableEtInfosAssocies, valeur);
        verify(variableValue).getExpectedColumn();
        verify(variableValue).getValue();
        verify(dbRealNodes).get(COLONNE_1);
    }

    /**
     * Test of getNewMesureFluxSol method, of class
     * AbstractProcessRecordFluxSol.
     */
    @Test
    public void testGetNewMesureFluxSol() {
        M result = instance.getNewMesureFluxSol(m.versionFile, m.traitement, line);
        assertNotNull(result);
        assertEquals(m.versionFile, result.getVersion());
        assertEquals(m.traitement, result.getTraitement());
        assertEquals(line, result.line);
    }

    /**
     * Test of getNewValeurFluxSol method, of class
     * AbstractProcessRecordFluxSol.
     */
    @Test
    public void testGetNewValeurFluxSol() {
        ValeurChambreFluxSol result = instance.getNewValeurFluxSol(m.variableRealNode, 12.5F);
        assertNotNull(result);
        assertEquals(m.variableRealNode, result.getRealNode());
        assertTrue(12.5F == result.getValue());
    }

    /**
     * Test of readGenericValues method, of class AbstractProcessRecordFluxSol.
     */
    @Test
    public void testReadGenericValues() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        List<Column> columns = new LinkedList();
        Column column = mock(Column.class);
        when(datasetDescriptor.getUndefinedColumn()).thenReturn(5);
        when(column.getFlagType()).thenReturn(
                Constantes.PROPERTY_CST_DATE_TYPE,
                Constantes.PROPERTY_CST_TIME_TYPE,
                Constantes.PROPERTY_CST_DUREE_MESURE,
                Constantes.PROPERTY_CST_CODE_TRT,
                Constantes.PROPERTY_CST_NO_CHAMBRE);
        when(column.getFormatType()).thenReturn(DateUtil.DD_MM_YYYY, DateUtil.DD_MM_YYYY, DateUtil.HH_MM);
        columns.add(column);
        columns.add(column);
        columns.add(column);
        columns.add(column);
        columns.add(column);
        columns.add(column);
        when(datasetDescriptor.getColumns()).thenReturn(columns);
        String[] values = new String[]{m.DATE_DEBUT, "23:45", "12", m.TRAITEMENT, "5"};
        AbstractProcessRecordFluxSol.GenericValues expResult = null;
        AbstractProcessRecordFluxSol.GenericValues result = instance.readGenericValues(values, datasetDescriptor, badsFormatsReport, 1);
        assertEquals(m.dateDebutComplete.toLocalDate(), result.date);
        assertEquals("23:45", DateUtil.getUTCDateTextFromLocalDateTime(result.heure, DateUtil.HH_MM));
        assertTrue(12 == result.dureeMesure);
        assertEquals(m.TRAITEMENT, result.codeTraitemement);
        assertEquals("5", result.noChambre);
        assertTrue(5 == result.genericIndex);
    }

    /**
     * Test of buildValeurs method, of class AbstractProcessRecordFluxSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildValeurs() throws Exception {
        M mesure = mock(M.class);
        instance=spy(instance);
        Collection<List<VariableValue>> ttlList = new LinkedList();
        List<VariableValue> list1 = new LinkedList();
        addVariableValues(list1, expectedColumn1);
        ttlList.add(list1);
        List<VariableValue> list2 = new LinkedList();
        addVariableValues(list2, expectedColumn2);
        addVariableValues(list2, expectedColumn3);
        ttlList.add(list2);
        List<VariableValue> list3 = new LinkedList();
        addVariableValues(list3, expectedColumn4);
        addVariableValues(list3, expectedColumn4);
        ttlList.add(list3);
        doReturn(ttlList).when(instance).createLstTotalLstVariableEtInfosAssocies(line);
        V valeur1 = mock(V.class);
        V valeur2 = mock(V.class);
        V valeur3 = mock(V.class);
        doReturn(valeur1).when(instance).creeValeurChambreFluxSolInfosAssoc(eq(list1), any(Map.class));
        doReturn(valeur2).when(instance).creeValeurChambreFluxSolInfosAssoc(eq(list2), any(Map.class));
        doReturn(valeur3).when(instance).creeValeurChambreFluxSolInfosAssoc(eq(list3), any(Map.class));
        List<V> valeurs = new LinkedList();
        when(mesure.getLstValeurs()).thenReturn(valeurs);
        instance.buildValeurs(line, expectedColumns, mesure);
        verify(valeur1).setMesure(mesure);
        verify(valeur2).setMesure(mesure);
        verify(valeur3).setMesure(mesure);
        assertEquals(valeur1, valeurs.get(0));
        assertEquals(valeur3, valeurs.get(2));
        assertEquals(valeur2, valeurs.get(1));
        assertTrue(3==valeurs.size());
        assertEquals(valeur1, valeurs.get(0));
        assertEquals(valeur2, valeurs.get(1));
        assertEquals(valeur3, valeurs.get(2));
    }

    /**
     *
     */
    public class V extends ValeurChambreFluxSol<M> {

        private V(RealNode realNode, Float value) {
            super(realNode, value);
        }

    }

    /**
     *
     */
    public class M extends MesureChambreFluxSol<V> {

        private final Line line;

        private M(VersionFile dbVersionFile, Traitement traitement, Line line) {
            super();
            setVersion(dbVersionFile);
            setTraitement(traitement);
            this.line = line;
        }

    }

    /**
     *
     */
    public class AbstractProcessRecordFluxSolImpl extends AbstractProcessRecordFluxSol {

        /**
         *
         */
        public AbstractProcessRecordFluxSolImpl() {
            super();
        }

        @Override
        public M getNewMesureFluxSol(VersionFile dbVersionFile, Traitement traitement, Line line) {
            return new M(dbVersionFile, traitement, line);
        }

        @Override
        public V getNewValeurFluxSol(RealNode realNode, Float value) {
            return new V(realNode, value);
        }

        /**
         *
         * @return
         */
        public GenericValues newGenericValues() {
            return new GenericValues();
        }
    }

    /**
     *
     */
    public class InstanceBuildValeur extends AbstractProcessRecordFluxSolImpl {

        ValeurChambreFluxSol valeur = mock(ValeurChambreFluxSol.class);

        /**
         *
         */
        public InstanceBuildValeur() {
            init(this);
        }

        @Override
        protected void buildValeurs(Line line, Map dbVariables, MesureChambreFluxSol mesureChambreFluxsol_j) throws PersistenceException, BusinessException {
            mesureChambreFluxsol_j.getLstValeurs().add(valeur);
        }

    }

}
