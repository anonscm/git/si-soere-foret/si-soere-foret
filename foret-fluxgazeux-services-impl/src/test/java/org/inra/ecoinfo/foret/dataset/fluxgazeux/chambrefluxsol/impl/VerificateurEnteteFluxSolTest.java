/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.StringReader;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.infocomplstdtvar.IInfoComplementaireStdtVariableDAO;
import org.inra.ecoinfo.foret.refdata.infocomplstdtvar.InformationComplementaireStdtVariable;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.IInformationComplementaireDAO;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.InformationComplementaire;
import org.inra.ecoinfo.foret.refdata.listevaleurinfo.ListeValeurInfo;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import static org.mockito.Matchers.eq;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.times;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author tcherniatinsky
 */
public class VerificateurEnteteFluxSolTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    VerificateurEnteteFluxSol instance;
    MockUtils m = MockUtils.getInstance();
    @Mock
    RequestPropertiesFluxSol requestProperties;
    @Mock
    DatasetDescriptor datasetDescriptor;
    @Mock
    IInfoComplementaireStdtVariableDAO infoComplementaireStdtVariableDAO;
    @Mock
    IInformationComplementaireDAO informationComplementaireDAO;

    /**
     *
     */
    public VerificateurEnteteFluxSolTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new VerificateurEnteteFluxSol();
        MockitoAnnotations.initMocks(this);
        instance.setDatasetConfiguration(m.datasetConfiguration);
        instance.setDatatypeDAO(m.datatypeDAO);
        instance.setDatatypeVariableUniteForetDAO(m.datatypeVariableUniteDAO);
        instance.setLocalizationManager(m.localizationManager);
        instance.setSiteForetDAO(m.siteDAO);
        instance.setPolicyManager(m.policyManager);
        instance.setThemeDAO(m.themeDAO);
        instance.setUniteDAO(m.uniteDAO);
        instance.setUtilisateurDAO(m.utilisateurDAO);
        instance.setVariableDAO(m.variableDAO);
        instance.setInfoCpltStdtVariableDAO(infoComplementaireStdtVariableDAO);
        instance.setInfoComplementaireDAO(informationComplementaireDAO);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of setInfoComplementaireDAO method, of class
     * VerificateurEnteteFluxSol.
     */
    @Test
    public void testSetInfoComplementaireDAO() {
        assertEquals(informationComplementaireDAO, instance.infoComplementaireDAO);
    }

    /**
     * Test of setInfoCpltStdtVariableDAO method, of class
     * VerificateurEnteteFluxSol.
     */
    @Test
    public void testSetInfoCpltStdtVariableDAO() {
        assertEquals(infoComplementaireStdtVariableDAO, instance.infoCpltStdtVariableDAO);
    }

    /**
     * Test of testHeaders method, of class VerificateurEnteteFluxSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeaders() throws Exception {
        StringReader in = new StringReader("colonne1, colonne2");
        CSVParser parser = new CSVParser(in);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        instance = new Instance();
        long result = instance.testHeaders(parser, m.versionFile, requestProperties, "utf-8", badsFormatsReport, datasetDescriptor);
        assertEquals(11L, result);
    }

    /**
     * Test of getMessageFrequenceErreur method, of class
     * VerificateurEnteteFluxSol.
     */
    @Test
    public void testGetMessageFrequenceErreur() {
        String[] values = new String[]{"frequence :", "fr"};
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        long lineNumber = 2L;
        instance.getMessageFrequenceErreur(values, badsFormatsReport, lineNumber);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 2 : la fréquence \"fr\" est invalide. Les fréquences valides sont infra-journalier, journalier, mensuel  </p>", badsFormatsReport.getHTMLMessages());
    }

    /**
     * Test of testNomColonnes method, of class VerificateurEnteteFluxSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testTestNomColonnes() throws Exception {
        boolean result;
        when(informationComplementaireDAO.getByNom(Matchers.anyString())).thenReturn(Optional.empty());
        when(m.column.getRefVariableName()).thenReturn(MockUtils.VARIABLE_CODE);
        when(m.mgaRecorder.getRealNodeByNKey(Matchers.anyString())).thenReturn(Optional.empty());
        Column colonneVariable = Mockito.mock(Column.class);
        when(colonneVariable.isVariable()).thenReturn(true);
        when(colonneVariable.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        Column colonneGap = Mockito.mock(Column.class);
        when(colonneGap.getFlagType()).thenReturn(Constantes.PROPERTY_CST_GAP_FIELD_TYPE);
        when(colonneGap.isVariable()).thenReturn(true);
        Column colonneQC = Mockito.mock(Column.class);
        when(colonneQC.getFlagType()).thenReturn(Constantes.PROPERTY_CST_QUALITY_CODE);
        when(colonneQC.getRefVariableName()).thenReturn("variable");
        Column colonneNombreMesure = Mockito.mock(Column.class);
        when(colonneNombreMesure.getFlagType()).thenReturn(Constantes.PROPERTY_CST_NBRE_MESURES);
        when(colonneNombreMesure.getRefVariableName()).thenReturn("variable");
        Column colonneEcartType = Mockito.mock(Column.class);
        when(colonneEcartType.getFlagType()).thenReturn(Constantes.PROPERTY_CST_ECART_TYPE);
        when(colonneEcartType.getRefVariableName()).thenReturn("variable");
        String[] columnNames = new String[]{"variable", "variable_GF", "variable_QC", "variable_NM", "variable_EC", "autreVariable", null};
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        when(requestProperties.getLstDatasetNameColumns()).thenReturn(Arrays.asList(new String[]{"variable", "variable_GF", "variable_QC", "variable_NM", "variable_EC"}));
        when(datasetDescriptor.getColumns()).thenReturn(Arrays.asList(new Column[]{colonneVariable, colonneGap, colonneQC, colonneNombreMesure, colonneEcartType}));
        when(datasetDescriptor.getName()).thenReturn("datatype");
        List<DatatypeVariableUnite> dvus = new LinkedList();
        DatatypeVariableUniteForet dvu = Mockito.mock(DatatypeVariableUniteForet.class);
        //when(m.siteThemeDatatypeVariableDAO.getBySiteThemeDatatypeAndVariableCode(m.siteThemeDatatype, "variable")).thenReturn(m.siteThemeDatatypeVariable);
        when(dvu.getVariable()).thenReturn(m.variable);
        dvus.add(dvu);
        when(m.datatypeVariableUniteDAO.getByDatatype("datatype")).thenReturn(dvus);
        ArgumentCaptor<Map> l = ArgumentCaptor.forClass(Map.class);
        Map<String, DatatypeVariableUniteForet> datatypeVariableUnitesForet = Mockito.mock(Map.class);
        when(datatypeVariableUnitesForet.containsKey("variable")).thenReturn(true);
        when(requestProperties.getVariableTypeDonnees()).thenReturn(datatypeVariableUnitesForet);
        // colonne variable
        result = instance.testNomColonnes(columnNames, 0, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        verify(requestProperties).setVariableTypeDonnees(l.capture());
        assertEquals(l.getValue().get(m.VARIABLE_NOM), dvu);
        assertFalse(badsFormatsReport.hasErrors());
        // colonne variable
        when(datatypeVariableUnitesForet.containsKey("variable")).thenReturn(false);
        result = instance.testNomColonnes(columnNames, 0, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- La variable \"variable\" n'est pas associée au datatype \"datatype\".	 </p>", badsFormatsReport.getHTMLMessages());
        // colonne GF
        badsFormatsReport = new BadsFormatsReport("erreur");
        when(datatypeVariableUnitesForet.containsKey("variable_GF")).thenReturn(true);
        result = instance.testNomColonnes(columnNames, 1, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertFalse(badsFormatsReport.hasErrors());
        // colonne GF
        when(datatypeVariableUnitesForet.containsKey("variable_GF")).thenReturn(false);
        badsFormatsReport = new BadsFormatsReport("erreur");
        result = instance.testNomColonnes(columnNames, 1, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- La variable \"variable_GF\" n'est pas associée au datatype \"datatype\".	 </p>", badsFormatsReport.getHTMLMessages());
        // colonne QC non associée
        badsFormatsReport = new BadsFormatsReport("erreur");
        result = instance.testNomColonnes(columnNames, 2, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- L'intitulé variable_QC est incorrect. Cette information complémentaire n'est pas associée au site, thème, type de données, variable parent/site/enfant-theme-datatype-variable </p>", badsFormatsReport.getHTMLMessages());
        // colonne NM non associée
        badsFormatsReport = new BadsFormatsReport("erreur");
        result = instance.testNomColonnes(columnNames, 3, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- L'intitulé variable_NM est incorrect. Cette information complémentaire n'est pas associée au site, thème, type de données, variable parent/site/enfant-theme-datatype-variable </p>", badsFormatsReport.getHTMLMessages());
        // colonne EC non associée
        badsFormatsReport = new BadsFormatsReport("erreur");
        result = instance.testNomColonnes(columnNames, 4, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- L'intitulé variable_EC est incorrect. Cette information complémentaire n'est pas associée au site, thème, type de données, variable parent/site/enfant-theme-datatype-variable </p>", badsFormatsReport.getHTMLMessages());
        //colonne associée mais liste vide
        badsFormatsReport = new BadsFormatsReport("erreur");
        InformationComplementaire infoCPT = Mockito.mock(InformationComplementaire.class);
        when(informationComplementaireDAO.getByNom("variable_QC")).thenReturn(Optional.of(infoCPT));
        InformationComplementaireStdtVariable infoCPTVar = Mockito.mock(InformationComplementaireStdtVariable.class);
        when(infoComplementaireStdtVariableDAO.getByStdtVariableAndInfoComplt(m.variableRealNode, infoCPT)).thenReturn(Optional.of(infoCPTVar));

        when(colonneQC.getRefVariableName()).thenReturn("variablecode");
        when(dvu.getCode()).thenReturn("dvucode");
        when(m.mgaRecorder.getRealNodeByNKey("parent/site/enfant,theme,datatype,dvucode")).thenReturn(Optional.of(m.variableRealNode));
        result = instance.testNomColonnes(columnNames, 2, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- L'intitulé est incorrect. L'information complémentaire variable_QC n'est associé à aucune liste de valeur </p>", badsFormatsReport.getHTMLMessages());
        //colonne associée mais liste remplie

        when(colonneEcartType.getRefVariableName()).thenReturn("variablecode");
        badsFormatsReport = new BadsFormatsReport("erreur");
        when(informationComplementaireDAO.getByNom("variable_EC")).thenReturn(Optional.of(infoCPT));
        when(infoCPT.getListeValeurInfo()).thenReturn(new ListeValeurInfo("valeur info"));
        result = instance.testNomColonnes(columnNames, 4, badsFormatsReport, 5L, datasetDescriptor, m.versionFile, requestProperties);
        assertFalse(badsFormatsReport.hasErrors());
    }

    /**
     * Test of addExpectedColumn method, of class VerificateurEnteteFluxSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testAddExpectedColumn() throws Exception {
        String[] columnNames = new String[]{"colonne 1", "colonne 2"};
        when(requestProperties.getColumnNames()).thenReturn(columnNames);
        instance.addExpectedColumn(datasetDescriptor, requestProperties);
        verify(requestProperties).addExpectedColumn("colonne 1", datasetDescriptor);
        verify(requestProperties).addExpectedColumn("colonne 2", datasetDescriptor);
        verify(requestProperties, times(2)).addExpectedColumn(Matchers.anyString(), eq(datasetDescriptor));
    }

    class Instance extends VerificateurEnteteFluxSol {

        @Override
        protected long readUnites(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber + 1;
        }

        @Override
        protected long readNomDesColonnes(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber + 1;
        }

        @Override
        protected long readValeursMin(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestPropertiesIntervalValue requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber + 1;
        }

        @Override
        protected long readValeursMax(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestPropertiesIntervalValue requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber + 1;
        }

        @Override
        protected long readSite(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties) {
            return lineNumber + 1;
        }

        @Override
        protected long readEmptyLine(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber + 1;
        }

        @Override
        protected long readFrequence(IRequestProperties requestProperties, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber + 1;
        }

        @Override
        protected long readDatatype(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber + 1;
        }

        @Override
        protected long readBeginAndEndDates(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber + 2;
        }

        @Override
        protected long readCommentaires(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties) {
            return lineNumber + 1;
        }

        @Override
        protected void addExpectedColumn(DatasetDescriptor datasetDescriptor, IRequestPropertiesIntervalValue requestProperties) throws BusinessException {

        }

    }

}
