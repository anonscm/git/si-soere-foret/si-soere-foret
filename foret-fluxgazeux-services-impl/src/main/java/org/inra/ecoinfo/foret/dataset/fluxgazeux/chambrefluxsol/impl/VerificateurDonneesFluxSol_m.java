/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import java.time.LocalDateTime;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

/**
 * @author sophie
 *
 */
public class VerificateurDonneesFluxSol_m extends AbstractVerificateurDonneesFluxSol {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl.AbstractVerificateurDonneesFluxSol#getMessageContinuiteDate(java.util.Date, org.inra.ecoinfo.dataset.BadsFormatsReport)
     */
    /**
     *
     * @param date
     * @param badsFormatsReport
     */
    @Override
    public void getMessageContinuiteDate(LocalDateTime date, BadsFormatsReport badsFormatsReport) {
        badsFormatsReport
                .addException(new BadExpectedValueException(String.format(this.localizationManager.getMessage(AbstractVerificateurDonneesFluxSol.BUNDLE_SOURCE_PATH, "DATE_NON_CONTINUE"), DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.MM_YYYY))));
    }
}
