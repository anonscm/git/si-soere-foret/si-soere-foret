/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import org.apache.commons.lang.math.NumberUtils;
import org.inra.ecoinfo.foret.dataset.ExpectedColumn;
import org.inra.ecoinfo.foret.dataset.UtilVerifRecorder;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IRequestPropertiesFluxSol;
import org.inra.ecoinfo.foret.dataset.impl.RequestProperties;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;

/**
 * @author sophie
 *
 */
public class RequestPropertiesFluxSol extends RequestProperties implements IRequestPropertiesFluxSol {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.dataset.messages";

    private String datatype = null;
    private Map<String, ExpectedColumn> expectedColums = new TreeMap<>();
    private String[] valeurMinInFile;
    private String[] valeurMaxInFile;
    private String[] valeursMin;
    private String[] valeursMax;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IRequestPropertiesFluxSol#addExpectedColumn(java.lang.String, org.inra.ecoinfo.dataset.DatasetDescriptor)
     */
    @Override
    public ExpectedColumn addExpectedColumn(String columnName, DatasetDescriptor datasetDescriptor) {
        ExpectedColumn expectedColumn = null;

        List<String> lstDatasetDescriptorNameColumns = UtilVerifRecorder.getLstDatasetNameColumns(datasetDescriptor);

        int indexColumn = lstDatasetDescriptorNameColumns.indexOf(columnName);
        Column column = datasetDescriptor.getColumns().get(indexColumn);

        expectedColumn = new ExpectedColumn(column);
        if (this.valeurMinInFile != null && this.valeurMinInFile.length > indexColumn && NumberUtils.isNumber(this.valeurMinInFile[indexColumn])) {
            expectedColumn.setMinValue(Float.parseFloat(this.valeurMinInFile[indexColumn]));
        }
        if (this.valeurMaxInFile != null && this.valeurMaxInFile.length > indexColumn && NumberUtils.isNumber(this.valeurMaxInFile[indexColumn])) {
            expectedColumn.setMaxValue(Float.parseFloat(this.valeurMaxInFile[indexColumn]));
        }

        if (expectedColumn != null) {
            this.expectedColums.put(columnName, expectedColumn);
        }
        return expectedColumn;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS#getDatatype()
     */
    @Override
    public String getDatatype() {
        return this.datatype;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS#setDatatype(java.lang.String)
     */
    @Override
    public void setDatatype(String datatype) {
        this.datatype = datatype;
        
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IRequestPropertiesFluxSol#getExpectedColumns()
     */
    @Override
    public Map<String, ExpectedColumn> getExpectedColumns() {
        return this.expectedColums;
    }

    /**
     * @return the valeurMaxInFile
     */
    @Override
    public String[] getValeurMaxInFile() {
        return this.valeurMaxInFile;

    }

    /**
     * @param valeurMaxInFile the valeurMaxInFile to set
     */
    @Override
    public void setValeurMaxInFile(String[] valeurMaxInFile) {
        this.valeurMaxInFile = valeurMaxInFile;
    }

    /**
     * @return the valeurMinInFile
     */
    @Override
    public String[] getValeurMinInFile() {
        return this.valeurMinInFile;
    }

    /**
     * @param valeurMinInFile the valeurMinInFile to set
     */
    @Override
    public void setValeurMinInFile(String[] valeurMinInFile) {
        this.valeurMinInFile = valeurMinInFile;
    }

    @Override
    public String[] getValeursMin() {
        return this.valeursMin;
    }

    @Override
    public void setValeursMin(String[] valeursMin) {
        this.valeursMin = valeursMin;
    }

    @Override
    public String[] getValeursMax() {
        return this.valeursMax;
    }

    @Override
    public void setValeursMax(String[] valeursMax) {
        this.valeursMax = valeursMax;
    }
}
