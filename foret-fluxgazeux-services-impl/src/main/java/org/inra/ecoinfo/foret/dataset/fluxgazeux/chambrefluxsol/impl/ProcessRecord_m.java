/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_m;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_m;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * @author sophie
 *
 */
public class ProcessRecord_m extends AbstractProcessRecordFluxSol<MesureChambreFluxSol_m, ValeurChambreFluxSol_m> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecord_m.class);

    /**
     *
     * @throws IOException
     * @throws SAXException
     * @throws URISyntaxException
     */
    public ProcessRecord_m() throws IOException, SAXException, URISyntaxException {
        super();
    }

    @Override
    protected MesureChambreFluxSol_m getNewMesureFluxSol(VersionFile dbVersionFile, Traitement traitement, Line line) {
        return new MesureChambreFluxSol_m(dbVersionFile, traitement, line.getDate(), line.getLineNo(), line.getNoChambre());
        
    }

    @Override
    protected ValeurChambreFluxSol_m getNewValeurFluxSol(RealNode realNode, Float value) {
        return new ValeurChambreFluxSol_m(realNode, value);
    }
}
