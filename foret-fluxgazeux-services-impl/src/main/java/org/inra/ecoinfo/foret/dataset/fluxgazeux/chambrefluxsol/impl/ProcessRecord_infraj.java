/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import java.util.ArrayList;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_infraj;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_infraj;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * @author sophie
 *
 */
public class ProcessRecord_infraj extends AbstractProcessRecordFluxSol<MesureChambreFluxSol_infraj, ValeurChambreFluxSol_infraj> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecord_infraj.class);

    /**
     *
     * @throws IOException
     * @throws SAXException
     * @throws URISyntaxException
     */
    public ProcessRecord_infraj() throws IOException, SAXException, URISyntaxException {
        super();
    }


    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl.Recorder#createLine(java.util.Date, java.util.Date, int, java.lang.String, java.lang.String, int, java.util.List)
     */
    /**
     *
     * @param date
     * @param heure
     * @param dureeMesure
     * @param codeTraitemement
     * @param noChambre
     * @param lineCount
     * @return
     */
    @Override
    protected Line createNewLine(GenericValues genericValues, long lineCount, ArrayList<VariableValue> lstVariableValues) {
        return new Line(genericValues.date, genericValues.heure, genericValues.dureeMesure, genericValues.codeTraitemement, genericValues.noChambre, lineCount, lstVariableValues);
    }

    @Override
    protected MesureChambreFluxSol_infraj getNewMesureFluxSol(VersionFile dbVersionFile, Traitement traitement, Line line) {
        return new MesureChambreFluxSol_infraj(dbVersionFile, traitement, line.getDate(), line.getLineNo(), line.getNoChambre(), line.getTime(), line.getDureeMesure());
        
    }

    @Override
    protected ValeurChambreFluxSol_infraj getNewValeurFluxSol(RealNode realNode, Float value) {
        return new ValeurChambreFluxSol_infraj(realNode, value);
    }
}
