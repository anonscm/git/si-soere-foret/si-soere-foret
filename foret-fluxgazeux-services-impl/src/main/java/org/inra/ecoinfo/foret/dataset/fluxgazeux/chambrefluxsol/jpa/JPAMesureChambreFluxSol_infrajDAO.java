/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_infraj;


/**
 * @author sophie
 * 
 */
public class JPAMesureChambreFluxSol_infrajDAO extends JPAMesureChambreFluxSolDAO<MesureChambreFluxSol_infraj> {

}
