/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_m;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_m;

/**
 * @author sophie
 *
 */
public class JPAPublicationChambreFluxSol_mDAO extends JPAPublicationChambreFluxSolDAO<ValeurChambreFluxSol_m, MesureChambreFluxSol_m> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurChambreFluxSol_m> getValueClass() {
        return ValeurChambreFluxSol_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureChambreFluxSol_m> getMeasureClass() {
        return MesureChambreFluxSol_m.class;
    }
}
