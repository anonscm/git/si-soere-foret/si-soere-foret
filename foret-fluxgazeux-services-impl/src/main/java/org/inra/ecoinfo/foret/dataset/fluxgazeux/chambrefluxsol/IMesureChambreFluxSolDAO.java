/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol;

import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;


/**
 * @author sophie
 * @param <M0>
 * 
 */
public interface IMesureChambreFluxSolDAO<M0 extends MesureChambreFluxSol<?>> extends IDAO<M0> {

}
