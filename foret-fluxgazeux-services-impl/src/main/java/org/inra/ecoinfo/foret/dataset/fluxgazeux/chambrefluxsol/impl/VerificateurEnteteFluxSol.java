/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import com.Ostermiller.util.CSVParser;
import java.util.Optional;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;
import org.inra.ecoinfo.foret.dataset.ITestHeaders;
import org.inra.ecoinfo.foret.dataset.impl.AbstractVerificateurEntete;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.SRDV;
import org.inra.ecoinfo.foret.refdata.infocomplstdtvar.IInfoComplementaireStdtVariableDAO;
import org.inra.ecoinfo.foret.refdata.infocomplstdtvar.InformationComplementaireStdtVariable;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.IInformationComplementaireDAO;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.InformationComplementaire;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class VerificateurEnteteFluxSol extends AbstractVerificateurEntete implements ITestHeaders {

    private static final Logger LOGGER = LoggerFactory.getLogger(VerificateurEnteteFluxSol.class);

    /**
     *
     */
    protected IInformationComplementaireDAO infoComplementaireDAO;

    /**
     *
     */
    protected IInfoComplementaireStdtVariableDAO infoCpltStdtVariableDAO;

    /**
     * @param infoComplementaireDAO the infoComplementaireDAO to set
     */
    public void setInfoComplementaireDAO(IInformationComplementaireDAO infoComplementaireDAO) {
        this.infoComplementaireDAO = infoComplementaireDAO;
    }

    /**
     * @param infoCpltStdtVariableDAO the infoCpltStdtVariableDAO to set
     */
    public void setInfoCpltStdtVariableDAO(IInfoComplementaireStdtVariableDAO infoCpltStdtVariableDAO) {
        this.infoCpltStdtVariableDAO = infoCpltStdtVariableDAO;
    }

    @Override
    public long testHeaders(CSVParser parser, VersionFile versionFile, IRequestProperties requestProperties, String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor) throws BusinessException {
        long lineNumber = super.testHeaders(parser, versionFile, requestProperties, encoding, badsFormatsReport, datasetDescriptor);
        lineNumber = this.readSite(versionFile, badsFormatsReport, parser, lineNumber, requestProperties);
        lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber);
        lineNumber = this.readFrequence(requestProperties, badsFormatsReport, parser, lineNumber);
        lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readCommentaires(badsFormatsReport, parser, lineNumber, requestProperties);
        lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
        lineNumber = this.readNomDesColonnes(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readUnites(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readValeursMin(versionFile, badsFormatsReport, parser, lineNumber, (IRequestPropertiesIntervalValue) requestProperties, datasetDescriptor);
        lineNumber = this.readValeursMax(versionFile, badsFormatsReport, parser, lineNumber, (IRequestPropertiesIntervalValue) requestProperties, datasetDescriptor);
        if (!badsFormatsReport.hasErrors()) {
            this.addExpectedColumn(datasetDescriptor, (IRequestPropertiesIntervalValue) requestProperties);
        }
        return lineNumber;
    }

    /**
     * @param datasetDescriptor
     * @param columnNames
     * @param requestProperties
     * @param valeursMin
     * @throws BusinessException
     */
    protected void addExpectedColumn(DatasetDescriptor datasetDescriptor, IRequestPropertiesIntervalValue requestProperties) throws BusinessException {
        final String[] columnNames = requestProperties.getColumnNames();
        for (int i = datasetDescriptor.getUndefinedColumn(); i < columnNames.length; i++) {
            requestProperties.addExpectedColumn(columnNames[i], datasetDescriptor);
        }
    }

    /**
     * * defined abstract method of AbstractVerificateurEntete
     *
     **
     * @param values
     * @param lineNumber
     * @param badsFormatsReport
     */
    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.foret.dataset.impl.AbstractVerificateurEntete#getMessageFrequenceErreur(java.lang.String[], org.inra.ecoinfo.dataset.BadsFormatsReport, int)
     */
    @Override
    protected void getMessageFrequenceErreur(String[] values, BadsFormatsReport badsFormatsReport, long lineNumber) {
        String message = String.format(this.localizationManager.getMessage(AbstractVerificateurEntete.BUNDLE_SOURCE_PATH, "BAD_FREQUENCE"), lineNumber, values[1].toLowerCase(), Constantes.FREQUENCE_NAME[3] + ", " + Constantes.FREQUENCE_NAME[1] + ", " + Constantes.FREQUENCE_NAME[2]);
        badsFormatsReport.addException(new BadExpectedValueException(message));
    }

    /**
     *
     * @param columnNames
     * @param index
     * @param badsFormatsReport
     * @param lineNumber
     * @param datasetDescriptor
     * @param version
     * @param requestProperties
     * @return
     * @throws BusinessException
     */
    @Override
    protected boolean testNomColonnes(String[] columnNames, int index, final BadsFormatsReport badsFormatsReport, final long lineNumber, final DatasetDescriptor datasetDescriptor, final VersionFile version, IRequestProperties requestProperties) throws BusinessException {
        if (columnNames[index] == null || !requestProperties.getLstDatasetNameColumns().contains(columnNames[index])) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INTITULE_INEXISTANT"), lineNumber, index + 1, columnNames[index])));
            return false;
        }
        int indexColumn = requestProperties.getLstDatasetNameColumns().indexOf(columnNames[index]);
        Column column = datasetDescriptor.getColumns().get(indexColumn);
        this.remplirVariablesTypeDonnees(datasetDescriptor, requestProperties);
        if ((column.isVariable() || column.getFlagType().equals(Constantes.PROPERTY_CST_VARIABLE_GF)) && !requestProperties.getVariableTypeDonnees().containsKey(columnNames[index])) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_INTITULE_VAR"), columnNames[index], datasetDescriptor.getName())));
            return false;
        } else if (column.getFlagType().equals(Constantes.PROPERTY_CST_ECART_TYPE) || column.getFlagType().equals(Constantes.PROPERTY_CST_NBRE_MESURES) || column.getFlagType().equals(Constantes.PROPERTY_CST_QUALITY_CODE)) {
            final InformationComplementaire infoCmplt = this.infoComplementaireDAO.getByNom(columnNames[index]).orElse(null);
            if (infoCmplt != null && column.getFlagType().equals(Constantes.PROPERTY_CST_QUALITY_CODE) && infoCmplt.getListeValeurInfo() == null) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_QC"), columnNames[index])));
                return false;
            }
            String siteCode = Outils.chercherNodeable(version, SiteForet.class).map(s -> s.getCode()).orElseGet(String::new);
            String themeCode = Outils.chercherNodeable(version, Theme.class).map(s -> s.getCode()).orElseGet(String::new);
            String datatypeCode = Outils.chercherNodeable(version, DataType.class).map(s -> s.getCode()).orElseGet(String::new);
            String variableCode = column.getRefVariableName();
            String code = String.format("%s-%s-%s-%s", siteCode, themeCode, datatypeCode, variableCode);
            SRDV srdv = new SRDV(code, localizationManager, policyManager.getMgaServiceBuilder().getRecorder(), datatypeVariableUniteForetDAO);
            if (srdv.hasError()) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_INTITULE_INFOCOMPLT"), columnNames[index], siteCode + "-"
                            + themeCode + "-" + datatypeCode + "-" + column.getRefVariableName())));
                    return false;
            }
            Optional<InformationComplementaireStdtVariable> infoCmpltStdtVariable = this.infoCpltStdtVariableDAO.getByStdtVariableAndInfoComplt(srdv.getRealNode(), infoCmplt);
            if (!infoCmpltStdtVariable.isPresent()) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_INTITULE_INFOCOMPLT"), columnNames[index], srdv.getCode())));
                return false;
            }
        }
        if (badsFormatsReport.hasErrors()) {
            throw new BusinessException(FORETRecorder.BAD_FORMAT_EXCEPTION);
        }
        return true;
    }

}
