/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_j;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_j;

/**
 * @author sophie
 *
 */
public class JPAPublicationChambreFluxSol_jDAO extends JPAPublicationChambreFluxSolDAO<ValeurChambreFluxSol_j, MesureChambreFluxSol_j> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurChambreFluxSol_j> getValueClass() {
        return ValeurChambreFluxSol_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureChambreFluxSol_j> getMeasureClass() {
        return MesureChambreFluxSol_j.class;
    }
}
