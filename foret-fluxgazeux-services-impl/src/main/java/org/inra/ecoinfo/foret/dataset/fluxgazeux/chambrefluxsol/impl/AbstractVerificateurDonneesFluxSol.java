/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;
import java.util.Map;
import java.util.logging.Level;
import java.util.logging.Logger;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ExpectedColumn;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IRequestPropertiesFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IVerificateurDonneesFluxSol;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.dataset.impl.GenericTestValues;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.traitement.ITraitementDAO;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public abstract class AbstractVerificateurDonneesFluxSol extends GenericTestValues implements IVerificateurDonneesFluxSol {

    /**
     *
     */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.dataset.messages";

    private ITraitementDAO traitementDAO;

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    /**
     *
     * @param date
     * @param badsFormatsReport
     */
    public abstract void getMessageContinuiteDate(LocalDateTime date, BadsFormatsReport badsFormatsReport);

    /**
     * @param localizationManager the localizationManager to set
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     * <p>On test la ligne et on récupère la lise de variablesValues (liste des valeurs des colonnes de type variable, ecart-type, variable gap fillée, nbre de mesure ou quality class. (le format a déjà été vérifié)</p>
     *  <p>test des variables gap fillées : dans la liste des variables values on recherche les variables gap fillée</p>
     * <p>On recherche dans la liste la variable correspondant à cette variable gap fillée.</p>
     * <ul><li>Sa valeur est nulle → PROPERTY_CST_INVALID_BAD_MEASURE</ul></li>
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IVerificateurDonneesFluxSol#verifieVariableAndVariableGfCoherente(org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl.Line, int,
     * org.inra.ecoinfo.dataset.BadsFormatsReport)
     */
    @Override
    public void verifieVariableAndVariableGfCoherente(IRequestPropertiesFluxSol requestProperties, DatasetDescriptor datasetDescriptor, String[] values, long lineNumber, BadsFormatsReport badsFormatsReport) {
        List<VariableValue> lstVarVal = this.getVariablesValues(values, lineNumber, datasetDescriptor, badsFormatsReport, requestProperties);

        for (VariableValue varvalGf : lstVarVal) {
            String varGfName = null;
            String varRefNameGf = null;
            String valueGf = null;
            VariableValue varval = null;

            if (varvalGf.getExpectedColumn().getFlagType() != null && varvalGf.getExpectedColumn().getFlagType().equals(Constantes.PROPERTY_CST_VARIABLE_GF)) {
                valueGf = varvalGf.getValue();
                varGfName = varvalGf.getExpectedColumn().getName();
                varRefNameGf = varvalGf.getExpectedColumn().getRefVariableName();

                if (valueGf != null && !valueGf.isEmpty()) {
                    for (VariableValue lstVarVal1 : lstVarVal) {
                        varval = lstVarVal1;
                        if (varRefNameGf.equals(varval.getExpectedColumn().getName())) {
                            break;
                        }
                    }
                    String variableValue = varval.getValue();
//                    if (!variableValue.equals(Constantes.PROPERTY_CST_INVALID_BAD_MEASURE)) {
//                        badsFormatsReport.addException(new NullValueException(String.format(FORETRecorder.getForetMessage("VALEUR_VALEURGF_INCOHERENTE"), lineNumber, varGfName, valueGf,
//                                varRefNameGf, variableValue)));
//                    }
                }
            }
        }
    }

    /**
     *
     * @param traitementDAO
     */
    public void setTraitementDAO(ITraitementDAO traitementDAO) {
        this.traitementDAO = traitementDAO;
    }

    /**
     *
     * @param lineNumber
     * @param versionFile
     * @param requestProperties
     * @param badsFormatsReport
     * @param datasetDescriptor
     * @param datatypeName
     * @param values
     */
    @Override
    protected void verifieLine(long lineNumber, VersionFile versionFile, IRequestProperties requestProperties, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor, String datatypeName, String[] values) {
        super.verifieLine(lineNumber, versionFile, requestProperties, badsFormatsReport, datasetDescriptor, datatypeName, values);
        this.verifieVariableAndVariableGfCoherente((IRequestPropertiesFluxSol) requestProperties, datasetDescriptor, values, lineNumber, badsFormatsReport);
    }

    /**
     *
     * @param values
     * @param lineNumber
     * @param datasetDescriptor
     * @param badsFormatsReport
     * @param requestProperties
     * @return
     */
    protected List<VariableValue> getVariablesValues(String[] values, long lineNumber, DatasetDescriptor datasetDescriptor, BadsFormatsReport badsFormatsReport, final IRequestPropertiesFluxSol requestProperties) {
        List<VariableValue> lstVariableValues = new ArrayList<>();
        for (int i = 0; i < values.length; i++) {
            VariableValue variableValue = null;
            ExpectedColumn expectedColumn = null;
            Column column = requestProperties.getColumn(i);
            String value = values[i];
            boolean valeurCorrect = column.isVariable() && !Constantes.PROPERTY_CST_VARIABLE_GF.equals(column.getFlagType());
            valeurCorrect = valeurCorrect ||Constantes.PROPERTY_CST_ECART_TYPE.equals(column.getFlagType());
            valeurCorrect = valeurCorrect ||Constantes.PROPERTY_CST_VARIABLE_GF.equals(column.getFlagType());
            valeurCorrect = valeurCorrect ||Constantes.PROPERTY_CST_NBRE_MESURES.equals(column.getFlagType());
            valeurCorrect = valeurCorrect ||Constantes.PROPERTY_CST_QUALITY_CODE.equals(column.getFlagType());
            if (valeurCorrect) {
                expectedColumn = requestProperties.getExpectedColumns().get(column.getName());
                variableValue = new VariableValue(values[i], expectedColumn);

                lstVariableValues.add(variableValue);
            }
        }
        return lstVariableValues;
    }

    @Override
    protected void checkOtherTypeValue(String[] values, BadsFormatsReport badsFormatsReport, long lineNumber, int index, String value, Column column, Map<String, DatatypeVariableUniteForet> variablesTypesDonnees, DatasetDescriptor datasetDescriptor, IRequestProperties requestProperties) {
        super.checkOtherTypeValue(values, badsFormatsReport, lineNumber, index, value, column, variablesTypesDonnees, datasetDescriptor, requestProperties);
        if (column.getFlagType().equals(Constantes.PROPERTY_CST_CODE_TRT)) {
            String codeTraitemement = values[index];
            if (StringUtils.isNotBlank(codeTraitemement)) {
                Traitement traitement = null;
                try {
                    traitement = this.traitementDAO.getByCode(codeTraitemement)
                            .orElseThrow(()->new PersistenceException("bad treatment"));
                } catch (PersistenceException ex) {
                    Logger.getLogger(AbstractVerificateurDonneesFluxSol.class.getName()).log(Level.SEVERE, null, ex);
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage(FORETRecorder.PROPERTY_MSG_MISSING_TREATMENT), lineNumber, index + 1, codeTraitemement)));
                }
                if (traitement == null) {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage(FORETRecorder.PROPERTY_MSG_MISSING_TREATMENT), lineNumber, index + 1, codeTraitemement)));
                }
            }
        }
    }

}
