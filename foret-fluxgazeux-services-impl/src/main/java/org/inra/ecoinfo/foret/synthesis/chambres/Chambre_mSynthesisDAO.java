/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.synthesis.chambres;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_m;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_m;
import org.inra.ecoinfo.foret.synthesis.flcm.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.flcm.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class Chambre_mSynthesisDAO extends AbstractChambresSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurChambreFluxSol_m, MesureChambreFluxSol_m> {

    @Override
    Class<ValeurChambreFluxSol_m> getValueClass() {
        return ValeurChambreFluxSol_m.class;
    }

    @Override
    Class<MesureChambreFluxSol_m> getMeasureClass() {
        return MesureChambreFluxSol_m.class;
    }

}
