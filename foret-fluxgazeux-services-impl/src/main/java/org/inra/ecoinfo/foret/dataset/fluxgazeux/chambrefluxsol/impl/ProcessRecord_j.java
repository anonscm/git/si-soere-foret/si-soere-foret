/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import java.io.IOException;
import java.net.URISyntaxException;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_j;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_j;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.xml.sax.SAXException;

/**
 * @author sophie
 *
 */
public class ProcessRecord_j extends AbstractProcessRecordFluxSol<MesureChambreFluxSol_j, ValeurChambreFluxSol_j> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessRecord_j.class);

    /**
     *
     * @throws IOException
     * @throws SAXException
     * @throws URISyntaxException
     */
    public ProcessRecord_j() throws IOException, SAXException, URISyntaxException {
        super();
    }

    @Override
    protected MesureChambreFluxSol_j getNewMesureFluxSol(VersionFile dbVersionFile, Traitement traitement, Line line) {
        return new MesureChambreFluxSol_j(dbVersionFile, traitement, line.getDate(), line.getLineNo(), line.getNoChambre());
        
    }

    @Override
    protected ValeurChambreFluxSol_j getNewValeurFluxSol(RealNode realNode, Float value) {
        return new ValeurChambreFluxSol_j(realNode, value);
    }
}
