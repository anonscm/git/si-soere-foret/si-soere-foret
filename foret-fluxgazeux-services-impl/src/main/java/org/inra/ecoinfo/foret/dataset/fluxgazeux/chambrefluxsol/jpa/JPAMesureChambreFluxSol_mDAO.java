/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_m;


/**
 * @author sophie
 * 
 */
public class JPAMesureChambreFluxSol_mDAO extends JPAMesureChambreFluxSolDAO<MesureChambreFluxSol_m> {

}
