/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_j;


/**
 * @author sophie
 * 
 */
public class JPAMesureChambreFluxSol_jDAO extends JPAMesureChambreFluxSolDAO<MesureChambreFluxSol_j> {

}
