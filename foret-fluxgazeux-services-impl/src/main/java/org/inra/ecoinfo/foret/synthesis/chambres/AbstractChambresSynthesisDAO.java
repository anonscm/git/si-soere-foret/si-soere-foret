/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.synthesis.chambres;

import java.time.LocalDate;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol_;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol_;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author ptchernia
 */
public abstract class AbstractChambresSynthesisDAO<SV extends GenericSynthesisValue, SD extends GenericSynthesisDatatype, V extends ValeurChambreFluxSol<M> , M extends MesureChambreFluxSol<V> > extends AbstractSynthesis<SV, SD> {

    @Override
    public Stream<SV> getSynthesisValue() {
        CriteriaQuery<SV> query = builder.createQuery(synthesisValueClass);
        Root<V> v = query.from(getValueClass());
        Join<V, MesureChambreFluxSol> m = v.join(ValeurChambreFluxSol_.mesure);
        Join<V, RealNode> varRn = v.join(ValeurChambreFluxSol_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        Path<String> parcelleCode = siteRn.join(RealNode_.nodeable).get(Nodeable_.code);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurChambreFluxSol_.value);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = m.get(MesureChambreFluxSol_.date);
        Path<Long> treatment = m.get(MesureChambreFluxSol_.traitement).get(Traitement_.id);
        Path<String> noChambre = m.get(MesureChambreFluxSol_.noChambre);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        query.select(
                builder.construct(
                        synthesisValueClass,
                        dateMesure,
                        sitePath,
                        variableCode,
                        builder.avg(valeur),
                        treatment,
                        noChambre
                )
        )
                .where(
                        builder.gt(valeur, -9999)
                )
                .groupBy(
                        sitePath,
                        variableCode,
                        treatment,
                        noChambre,
                        dateMesure
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(variableCode),
                        builder.asc(treatment),
                        builder.asc(noChambre),
                        builder.asc(dateMesure)
                );
        return getResultAsStream(query);

    }
    abstract Class<V> getValueClass();
    abstract Class<M> getMeasureClass();

    
}
