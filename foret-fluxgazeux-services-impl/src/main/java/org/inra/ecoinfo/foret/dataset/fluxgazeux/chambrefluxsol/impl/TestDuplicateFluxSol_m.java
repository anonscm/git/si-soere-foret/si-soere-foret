/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.foret.dataset.ITestDuplicates;
import org.inra.ecoinfo.foret.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;

/**
 * The Class TestDuplicateWsol.
 * <p>
 * implementation for Tillage of {@link ITestDuplicates}
 * 
 * @author Tcherniatinsky Philippe test the existence of duplicates in flux files
 */
public class TestDuplicateFluxSol_m extends AbstractTestDuplicate {


    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String FORET_DATASET_FLUX_SOL_BUNDLE_NAME = "org/inra/ecoinfo/foret/dataset/fluxgazeux/chambrefluxsol/messages";

    /**
     *
     */
    public static final String PROPERTY_MSG_DOUBLON_LINE_M = "PROPERTY_MSG_DOUBLON_LINE_M";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The date time line.
     * 
     * @link(SortedMap<String,SortedMap<String,SortedSet<Long>>>).
     */
    SortedMap<String, SortedSet<Long>> line;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicateFluxSol_m() {
        this.line = new TreeMap<>();
    }

    /**
     * Adds the line.
     * 
     * @param values
     * @link(String[]) the values
     * @param lineNumber
     *            long the line number {@link String[]} the values
     */
    @Override
    public void addLine(String[] values, long lineNumber) {
        this.addLine(values[0], values[1], values[2], lineNumber + 1);
    }

    /**
     * Adds the line.
     *
     * @param date
     * @param codeTraitement
     * @param noChambre
     * @link(String)
     * @link(String)
     * @link(String) the numero
     * @param lineNumber
     *            long the line number
     * @link(String) the date
     * @link(String) the order of the intervention in the day
     * @link(String) the number of the tool in intervention
     */
    protected void addLine(final String date, final String codeTraitement, final String noChambre, final long lineNumber) {
        final String key = this.getKey(date, codeTraitement, noChambre);
        if (!this.line.containsKey(key)) {
            SortedSet<Long> lineNumbers = new TreeSet();
            lineNumbers.add(lineNumber);
            this.line.put(key, lineNumbers);
        } else {
            this.line.get(key).add(lineNumber);
            this.errorsReport.addErrorMessage(String.format(FORETRecorder.getForetMessageWithBundle(TestDuplicateFluxSol_m.FORET_DATASET_FLUX_SOL_BUNDLE_NAME, TestDuplicateFluxSol_m.PROPERTY_MSG_DOUBLON_LINE_M), lineNumber, date, codeTraitement, noChambre, this.line.get(key).first().intValue()));
        }

    }
}
