/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.time.format.DateTimeParseException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.Collection;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.TreeMap;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.apache.commons.lang.StringUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ErrorsReport;
import org.inra.ecoinfo.foret.dataset.ExpectedColumn;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IMesureChambreFluxSolDAO;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IRequestPropertiesFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.impl.AbstractProcessRecordForet;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.fabrique.FabriqueInfoComplementaire;
import org.inra.ecoinfo.foret.refdata.site.ISiteForetDAO;
import org.inra.ecoinfo.foret.refdata.traitement.Traitement;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sophie
 * @param <M>
 * @param <V>
 *
 */
public abstract class AbstractProcessRecordFluxSol<M extends MesureChambreFluxSol<V>, V extends ValeurChambreFluxSol<M>> extends AbstractProcessRecordForet {

    private static final Logger LOGGER = LoggerFactory.getLogger(AbstractProcessRecordFluxSol.class);

    /**
     *
     */
    protected static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.dataset.messages";

    /**
     *
     */
    protected static final List<String> GENERIC_FLAG_TYPE = Arrays.asList(new String[]{
        Constantes.PROPERTY_CST_DATE_TYPE,
        Constantes.PROPERTY_CST_TIME_TYPE,
        Constantes.PROPERTY_CST_DUREE_MESURE,
        Constantes.PROPERTY_CST_NO_CHAMBRE,
        Constantes.PROPERTY_CST_CODE_TRT});

    /**
     *
     */
    protected IMesureChambreFluxSolDAO<M> mesureChambreFluxSolDAO;

    /**
     *
     */
    protected ISiteForetDAO siteForetDAO;

    /**
     *
     */
    protected Map<String, DatatypeVariableUnite> variablesTypeDonnees;

    /**
     *
     */
    public AbstractProcessRecordFluxSol() {
        super();
    }

    /**
     *
     * @param siteForetDAO
     */
    public void setSiteForetDAO(ISiteForetDAO siteForetDAO) {
        this.siteForetDAO = siteForetDAO;
    }

    /**
     * <p>
     * regroupe les variableValues par groupes de variable ou variable gap
     * fillée</p>
     *
     * @param line
     * @return crée une liste qui contient : des sous-listes constituées d'une
     * VariableValue correspondant à une variable (ouune variable gapfillée) et
     * éventuellement d'une ou plusieurs VariableVualue correspondant aux
     * informations complémentaires de cette variable
     * @throws BusinessException
     */
    protected Collection<List<VariableValue>> createLstTotalLstVariableEtInfosAssocies(Line line) throws BusinessException {
        Map<String, List<VariableValue>> lstTotalLstVarInfosAssoc = new HashMap();
        for (Iterator<VariableValue> iterator = line.getLstVariablesValues().iterator(); iterator.hasNext();) {
            VariableValue variableValue = iterator.next();
            final ExpectedColumn expectedColumn = variableValue.getExpectedColumn();
            boolean variableOrGF = expectedColumn.isVariable() || Constantes.PROPERTY_CST_VARIABLE_GF.equals(expectedColumn.getFlagType());
            String name = variableOrGF ? expectedColumn.getName() : expectedColumn.getRefVariableName();
            if (lstTotalLstVarInfosAssoc.get(name) == null) {
                lstTotalLstVarInfosAssoc.put(name, new ArrayList());
            }
            lstTotalLstVarInfosAssoc.get(name).add(variableValue);
            iterator.remove();
        }
        return lstTotalLstVarInfosAssoc.values();
    }

    /**
     * <p>
     * parcourt la liste des variableValue d'un groupe pour ajouter à la valeur
     * les informations comlémentaires associées<p>
     * <p>
     * Utilisation de la fabrique adéquate en fonction de la nature de la valeur
     * complémentaire</p>
     *
     * @param lstVariableEtInfosAssocies
     * @param valeurChambreFluxSol
     * @throws BusinessException
     */
    protected void createValeurChambrefluxSolInfosComplt(List<VariableValue> lstVariableEtInfosAssocies, ValeurChambreFluxSol<?> valeurChambreFluxSol)
            throws BusinessException {
        for (Iterator<VariableValue> iterator = lstVariableEtInfosAssocies.iterator(); iterator.hasNext();) {
            VariableValue variableValue = iterator.next();
            if (variableValue != null) {
                ExpectedColumn expectedColumn = variableValue.getExpectedColumn();
                final String value = variableValue.getValue();
                final String flagType = expectedColumn.getFlagType();
                if (Constantes.PROPERTY_CST_ECART_TYPE.equals(flagType) && value != null && !value.isEmpty()) {
                    Double doubleValue = new Double(value);
                    FabriqueInfoComplementaire.fabriquer(expectedColumn.getName(), doubleValue, valeurChambreFluxSol);
                } else if (Constantes.PROPERTY_CST_NBRE_MESURES.equals(flagType) && value != null && !value.isEmpty()) {
                    Integer integerValue = new Integer(value);
                    FabriqueInfoComplementaire.fabriquer(expectedColumn.getName(), integerValue, valeurChambreFluxSol);
                } else if (Constantes.PROPERTY_CST_QUALITY_CODE.equals(flagType) && value != null && !value.isEmpty()) {
                    FabriqueInfoComplementaire.fabriquer(expectedColumn.getName(), value, valeurChambreFluxSol);
                } else if (Constantes.PROPERTY_CST_IS_GAP_FILLED.equals(flagType) && value != null && !value.isEmpty()) {
                    FabriqueInfoComplementaire.fabriquer(expectedColumn.getName(), "1".equals(value.trim()), valeurChambreFluxSol);
                }
            }
            iterator.remove();
        }
    }

    /**
     * <p>
     * process the record of th file </p>
     * <ul><li>skip the header and build the list of db variables</li>
     * <li>read all lines in a list of Line</li>
     * <li>use this list to build the mesures</li>
     *
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, IRequestProperties requestProperties, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        IRequestPropertiesFluxSol requestPropertiesFluxSol = (IRequestPropertiesFluxSol) requestProperties;
        ErrorsReport errorsReport = new ErrorsReport();
        try {
            BadsFormatsReport badsFormatsReport = new BadsFormatsReport(fileEncoding);
            VersionFile versionFileDB = this.versionFileDAO.merge(versionFile);
            Map<String, RealNode> dbRealNodesVariable = this.buildVariablesAndkipHeader(parser, (DatasetDescriptorFluxSol) datasetDescriptor, (RequestPropertiesFluxSol) requestPropertiesFluxSol, versionFile.getDataset().getRealNode());
            List<Line> lstLines = createLines(parser, badsFormatsReport, datasetDescriptor, requestPropertiesFluxSol);
            this.buildMesuresChambreFluxSol(versionFile, lstLines, errorsReport, dbRealNodesVariable);
            if (errorsReport.hasErrors()) {
                LOGGER.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        } catch (PersistenceException | IOException | BusinessException | BadFormatException | NumberFormatException e) {
            LOGGER.debug(e.getMessage());
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param mesureChambreFluxSolDAO
     */
    public void setMesureChambreFluxSolDAO(IMesureChambreFluxSolDAO<M> mesureChambreFluxSolDAO) {
        this.mesureChambreFluxSolDAO = mesureChambreFluxSolDAO;
    }

    /**
     * <p>
     * read each line and return a list of lines</p>
     *
     * @param requestPropertiesFluxSol
     * @param datasetDescriptor
     * @return crée une liste qui contient : une VariableValue correspondant à
     * une variable (ou une variable gapfillée) et éventuellement d'une ou
     * plusieurs VariableVualue correspondant aux informations complémentaires
     * de cette variable
     * @throws BusinessException
     * @param parser
     * @param badsFormatsReport
     * @throws BadFormatException
     * @throws java.io.IOException
     * @throws NumberFormatException
     */
    protected List<Line> createLines(CSVParser parser, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor, IRequestPropertiesFluxSol requestPropertiesFluxSol) throws BusinessException, BadFormatException, IOException {
        List<Line> lines = new LinkedList<>();
        String[] values = null;
        long linenumber = parser.getLastLineNumber();
        while ((values = parser.getLine()) != null) {
            linenumber++;
            Line line = this.createLine(values, linenumber, badsFormatsReport, datasetDescriptor, requestPropertiesFluxSol);
            lines.add(line);
        }
        if (badsFormatsReport.hasErrors()) {
            throw new BadFormatException(badsFormatsReport);
        }
        return lines;
    }

    /**
     * <p>
     * read a line of values and return a line</p>
     *
     * @param values
     * @param datasetDescriptor
     * @param requestPropertiesFluxSol
     * @param lineCount
     * @param badsFormatsReport vérifie si le format des dates/heures est
     * correct, si le type des valeurs est correct et créer la ligne
     * @return
     * @throws BusinessException
     */
    protected Line createLine(String[] values, long lineCount, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor, IRequestPropertiesFluxSol requestPropertiesFluxSol) throws BusinessException {
        ArrayList<VariableValue> lstVariableValues = new ArrayList<>();
        GenericValues genericValues = readGenericValues(values, datasetDescriptor, badsFormatsReport, lineCount);
        final String[] columnNames = requestPropertiesFluxSol.getColumnNames();
        for (int i = genericValues.genericIndex; i < values.length; i++) {
            final String columnName = columnNames[i];
            ExpectedColumn expectedColumn = requestPropertiesFluxSol.getExpectedColumns().get(columnName);
            VariableValue variableValue = new VariableValue(values[i], expectedColumn);
            lstVariableValues.add(variableValue);
        }
        if (badsFormatsReport.hasErrors()) {
            throw new BusinessException(badsFormatsReport.getHTMLMessages());
        }
        if (CollectionUtils.isEmpty(lstVariableValues) || badsFormatsReport.hasErrors()) {
            String error = String.format(FORETRecorder.getForetMessage("COLUMN_VAR_ERROR"));
            throw new BusinessException(error);
        }
        Line line = createNewLine(genericValues, lineCount, lstVariableValues);
        return line;
    }

    /**
     * <p>
     * géneric constructor for a line can be overriden<p>
     *
     * @param genericValues
     * @param lineCount
     * @param lstVariableValues
     * @return
     */
    protected Line createNewLine(GenericValues genericValues, long lineCount, ArrayList<VariableValue> lstVariableValues) {
        Line line = new Line(genericValues.date, genericValues.heure, genericValues.dureeMesure, genericValues.codeTraitemement, genericValues.noChambre, lineCount, lstVariableValues);
        return line;
    }

    /**
     * <p>
     * Set the parser line to the first line of data</p>
     * <p>
     * return the list of db variables from columns names</p>
     *
     * @param parser
     * @param datasetDescriptor
     * @param requestProperties
     * @param datatypeRealNode
     * @return
     * @throws IOException passe l'entête des fichiers d'échange
     */
    protected Map<String, RealNode> buildVariablesAndkipHeader(CSVParser parser, DatasetDescriptorFluxSol datasetDescriptor, RequestPropertiesFluxSol requestProperties, RealNode datatypeRealNode) throws IOException {
        Map<String, RealNode> dbVariables = new TreeMap();
        for (int i = 0; i < datasetDescriptor.getEnTete(); i++) {
            parser.getLine();
        }
        Map<String, RealNode> dbRealNode = datatypeVariableUniteForetDAO.getRealNodesVariables(datatypeRealNode).entrySet().stream()
                .collect(Collectors.toMap(e -> e.getKey().getCode(), e -> e.getValue()));
        for (ExpectedColumn expectedColumn : requestProperties.getExpectedColumns().values()) {
            final String variableName = expectedColumn.getName();
            dbVariables.put(variableName, dbRealNode.get(variableName));
        }
        return dbVariables;
    }

    /**
     * <p>
     * lit les colonnes génériques de la ligne et retourne un genericValue
     * contenant les informations de ces colonnes</p>
     *
     * @param values
     * @param datasetDescriptor
     * @param badsFormatsReport
     * @param lineCount
     * @return
     */
    protected GenericValues readGenericValues(String[] values, DatasetDescriptor datasetDescriptor, BadsFormatsReport badsFormatsReport, long lineCount) {
        GenericValues genericValues = new GenericValues();
        for (Iterator<Column> iterator = datasetDescriptor.getColumns().iterator(); iterator.hasNext() && genericValues.genericIndex < datasetDescriptor.getUndefinedColumn();) {
            Column column = iterator.next();
            switch (column.getFlagType()) {
                case Constantes.PROPERTY_CST_DATE_TYPE:
                    genericValues.date = readDate(values[genericValues.genericIndex], badsFormatsReport, column, lineCount, genericValues.genericIndex + 1);
                    break;
                case Constantes.PROPERTY_CST_TIME_TYPE:
                    genericValues.heure = readTime(values[genericValues.genericIndex], badsFormatsReport, column, lineCount, genericValues.genericIndex + 1);
                    break;
                case Constantes.PROPERTY_CST_DUREE_MESURE:
                    genericValues.dureeMesure = readDureeMesure(values[genericValues.genericIndex], badsFormatsReport, column, lineCount, genericValues.genericIndex + 1);
                    break;
                case Constantes.PROPERTY_CST_CODE_TRT:
                    genericValues.codeTraitemement = values[genericValues.genericIndex];
                    break;
                case Constantes.PROPERTY_CST_NO_CHAMBRE:
                    genericValues.noChambre = values[genericValues.genericIndex];
                    break;
                default:
                    return genericValues;
            }
            genericValues.genericIndex++;
        }
        return genericValues;
    }

    /**
     * <p>
     * retourne une date à partir d'une valeur</p>
     *
     * @param value
     * @param badsFormatsReport
     * @param column
     * @param lineCount
     * @param columnIndex
     * @return
     */
    protected LocalDate readDate(String value, BadsFormatsReport badsFormatsReport, Column column, long lineCount, int columnIndex) {
        try {
            LocalDate date;
            if (DateUtil.MM_YYYY.equals(column.getFormatType())) {
                date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "01/".concat(value));                
            } else {
                date = DateUtil.readLocalDateFromText(column.getFormatType(), value);
            }
            return date;
        } catch (DateTimeParseException ex) {
            String message = String.format(FORETRecorder.getForetMessage(FORETRecorder.PROPERTY_MSG_INVALID_DATE), value, lineCount, columnIndex, column.getFieldName(), column.getFormatType());
            badsFormatsReport.addException(new DateTimeException(message));
        }
        return null;
    }

    /**
     * <p>
     * retourne une heure à partir d'une valeur</p>
     *
     * @param value
     * @param badsFormatsReport
     * @param column
     * @param lineCount
     * @param columnIndex
     * @return
     */
    protected LocalTime readTime(String value, BadsFormatsReport badsFormatsReport, Column column, long lineCount, int columnIndex) {
        try {
            LocalTime time = DateUtil.readLocalTimeFromText(column.getFormatType(), value);
            return time;
        } catch (DateTimeParseException ex) {
            String message = String.format(FORETRecorder.getForetMessage(FORETRecorder.PROPERTY_MSG_INVALID_TIME), value, lineCount, columnIndex, column.getFieldName(), column.getFormatType());
            badsFormatsReport.addException(new DateTimeException(message));
        }
        return null;
    }

    /**
     * <p>
     * retourne une durée de mesure à partir d'une valeur</p>
     *
     * @param value
     * @param badsFormatsReport
     * @param column
     * @param lineCount
     * @param columnIndex
     * @return
     */
    protected int readDureeMesure(String value, BadsFormatsReport badsFormatsReport, Column column, long lineCount, int columnIndex) {
        try {
            int dureeTraitement = Integer.valueOf(value);
            return dureeTraitement;
        } catch (NumberFormatException ex) {
            String message = String.format(FORETRecorder.getForetMessage(FORETRecorder.PROPERTY_MSG_INVALID_DURATION), value, lineCount, columnIndex, column.getFieldName());
            badsFormatsReport.addException(new DateTimeException(message));
        }
        return -1;
    }

    /**
     * <p>
     * retourne la variableValue correspondant à la colonne variable ou variable
     * gap-fillée d'un groupe de variableValues</p>
     *
     * @param lstVariableEtInfosAssocies
     * @return
     */
    protected VariableValue getVariableValueForVariable(List<VariableValue> lstVariableEtInfosAssocies) {
        VariableValue variableValue = null;
        for (Iterator<VariableValue> iterator = lstVariableEtInfosAssocies.iterator(); iterator.hasNext();) {
            variableValue = iterator.next();
            final ExpectedColumn expectedColumn = variableValue.getExpectedColumn();
            boolean variableOrGF = expectedColumn.isVariable() || Constantes.PROPERTY_CST_VARIABLE_GF.equals(expectedColumn.getFlagType());
            if (variableOrGF) {
                lstVariableEtInfosAssocies.remove(variableValue);
                return variableValue;
            }
        }
        return null;

    }

    /**
     * <p>
     * construit et enregistre en base les mesures à partir de la liste des
     * lignes</p>
     *
     * @param dbVersionFile
     * @param lstLines
     * @param errorsReport
     * @param dbRealNodesVariable
     * @throws PersistenceException
     * @throws BusinessException
     */
    public void buildMesuresChambreFluxSol(VersionFile dbVersionFile, List<Line> lstLines, ErrorsReport errorsReport, Map<String, RealNode> dbRealNodesVariable) throws PersistenceException, BusinessException {
        Map<String, Traitement> traitements = new TreeMap();
        int nbTotLgn = lstLines.size();
        for (Iterator<Line> iterator = lstLines.iterator(); iterator.hasNext();) {
            Line line = iterator.next();
            try {
                Traitement traitement = traitements.get(line.getCodeTraitemement());
                if (traitement == null) {
                    traitement = this.traitementDAO.getByCode(line.getCodeTraitemement())
                            .orElseThrow(() -> new PersistenceException("bad treatment"));
                    traitements.put(line.getCodeTraitemement(), traitement);
                }
                M mesureChambreFluxsol = getNewMesureFluxSol(dbVersionFile, traitement, line);
                buildValeurs(line, dbRealNodesVariable, mesureChambreFluxsol);
                if (!CollectionUtils.isEmpty(mesureChambreFluxsol.getLstValeurs())) {
                    this.mesureChambreFluxSolDAO.saveOrUpdate(mesureChambreFluxsol);
                }
            } catch (PersistenceException e) {
                LOGGER.debug(e.getMessage());
                errorsReport.addErrorMessage(String.format(FORETRecorder.getForetMessage("ERREUR_INSERTION_MESURE"), line.getLineNo()));
            }
            if (0 == line.getLineNo() % 30) {
                mesureChambreFluxSolDAO.flush();
                dbVersionFile = reatachEntities(dbVersionFile, traitements, dbRealNodesVariable);
            }
            iterator.remove();
        }
        if (errorsReport.hasErrors()) {
            throw new PersistenceException(errorsReport.getErrorsMessages());
        }
    }

    /**
     * <p>
     * construit et ajoute à la mesure les valeurs à partir de la ligne</p>
     *
     * @param line
     * @param dbRealNodesVariable
     * @param mesureChambreFluxsol_j
     * @throws PersistenceException
     * @throws BusinessException
     */
    protected void buildValeurs(Line line, Map<String, RealNode> dbRealNodesVariable, M mesureChambreFluxsol_j) throws PersistenceException, BusinessException {
        for (Iterator<List<VariableValue>> iterator = createLstTotalLstVariableEtInfosAssocies(line).iterator(); iterator.hasNext();) {
            List<VariableValue> lstTotallstVariableEtInfosAssocie = iterator.next();
            V valeurChambreFluxSol_j = creeValeurChambreFluxSolInfosAssoc(lstTotallstVariableEtInfosAssocie, dbRealNodesVariable);
            if (valeurChambreFluxSol_j != null) {
                valeurChambreFluxSol_j.setMesure(mesureChambreFluxsol_j);
                mesureChambreFluxsol_j.getLstValeurs().add(valeurChambreFluxSol_j);
            }
            iterator.remove();
        }
    }

    /**
     * <p>
     * à partir de'un groupe de variablevalues, construit une valeur et
     * construit les informations associées et les ajoute à la valeur</p>
     *
     *
     * @param lstVariableEtInfosAssocies
     * @param dbRealNodesVariable
     * @return une nouvelle valeur à partir d'une liste de variableValue
     * @throws BusinessException
     * @throws PersistenceException créer la ValeurChambreFluxSol avec toutes
     * ses informations complémentaires associées éventuelles
     */
    protected V creeValeurChambreFluxSolInfosAssoc(List<VariableValue> lstVariableEtInfosAssocies, Map<String, RealNode> dbRealNodesVariable) throws BusinessException, PersistenceException {
        V valeurChambreFluxSol = null;
        VariableValue variableValue = getVariableValueForVariable(lstVariableEtInfosAssocies);
        final String stringValue = variableValue.getValue();
        if (StringUtils.isNotBlank(stringValue)) {
            ExpectedColumn expectedColumn = variableValue.getExpectedColumn();
            RealNode variable = dbRealNodesVariable.get(expectedColumn.getName());
            Float value = new Float(stringValue);
            valeurChambreFluxSol = getNewValeurFluxSol(variable, value);
            if (!lstVariableEtInfosAssocies.isEmpty()) {
                this.createValeurChambrefluxSolInfosComplt(lstVariableEtInfosAssocies, valeurChambreFluxSol);
            }
        }
        return valeurChambreFluxSol;
    }

    /**
     * <p>
     * fabrique d'une nouvelle mesure</p>
     *
     * @param dbVersionFile
     * @param traitement
     * @param line
     * @return
     */
    protected abstract M getNewMesureFluxSol(VersionFile dbVersionFile, Traitement traitement, Line line);

    /**
     * <p>
     * fabrique d'une nouvelle valeur</p>
     *
     * @param realNode
     * @param value
     * @return
     */
    protected abstract V getNewValeurFluxSol(RealNode realNode, Float value);

    /**
     * <p>
     * reattach entities after a flush</p>
     *
     * @param versionFile
     * @param traitements
     * @param dbRealNodesVariable
     * @return
     * @throws PersistenceException
     */
    protected VersionFile reatachEntities(VersionFile versionFile, Map<String, Traitement> traitements, Map<String, RealNode> dbRealNodesVariable) throws PersistenceException {
        versionFile = versionFileDAO.merge(versionFile);
        for (Map.Entry<String, RealNode> entry : dbRealNodesVariable.entrySet()) {
            String key = entry.getKey();
            RealNode value = entry.getValue();
            if (value == null) {
                continue;
            }
            dbRealNodesVariable.put(key, datatypeVariableUniteForetDAO.mergeRealNode(value));
        }
        for (Map.Entry<String, Traitement> entry : traitements.entrySet()) {
            String key = entry.getKey();
            Traitement value = entry.getValue();
            if (value == null) {
                continue;
            }
            traitements.put(key, traitementDAO.merge(value));
        }
        return versionFile;
    }

    /**
     * store the inforamations from generics columns</p>
     *
     */
    protected class GenericValues {

        LocalDate date;
        LocalTime heure;
        int dureeMesure;
        String codeTraitemement;
        String noChambre;
        int genericIndex;
    }

}
