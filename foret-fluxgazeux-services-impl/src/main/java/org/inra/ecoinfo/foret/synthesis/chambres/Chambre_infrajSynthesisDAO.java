/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.synthesis.chambres;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_infraj;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_infraj;
import org.inra.ecoinfo.foret.synthesis.flcij.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.flcij.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class Chambre_infrajSynthesisDAO extends AbstractChambresSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurChambreFluxSol_infraj, MesureChambreFluxSol_infraj> {

    @Override
    Class<ValeurChambreFluxSol_infraj> getValueClass() {
        return ValeurChambreFluxSol_infraj.class;
    }

    @Override
    Class<MesureChambreFluxSol_infraj> getMeasureClass() {
        return MesureChambreFluxSol_infraj.class;
    }

}
