/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.foret.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol_;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.ValeurChambreFluxSol_;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationComplementaire;
import org.inra.ecoinfo.foret.dataset.infoComplementaire.entity.ValeurInformationComplementaire_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * @param <V>
 * @param <M>
 * 
 */
public abstract class JPAPublicationChambreFluxSolDAO<V extends ValeurChambreFluxSol<M>, M extends MesureChambreFluxSol<V>> extends JPAVersionFileDAO implements ILocalPublicationDAO {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa.JPAPublicationChambreFluxSolDAO#removeVersion(java.lang.Long)
     */
    /**
     *
     * @param versionfileId
     * @throws PersistenceException
     */
    @Override
    public void removeVersion(Long versionfileId) throws PersistenceException {
        deleteValeurInfoComplementaires(versionfileId);
        deleteValues(versionfileId);
        deleteMeasures(versionfileId);
    }
    
    /**
     *
     * @return
     */
    protected abstract Class<V> getValueClass();

    /**
     *
     * @return
     */
    protected abstract Class<M> getMeasureClass();

    /**
     *
     * @param versionfileId
     */
    protected void deleteValues(Long versionfileId) {
        CriteriaDelete<V> delete = builder.createCriteriaDelete(getValueClass());
        Subquery<M> subQueryMeasure = delete.subquery(getMeasureClass());
        Root<V> v = delete.from(getValueClass());
        Root<M> m = subQueryMeasure.from(getMeasureClass());
        Join<M, VersionFile> version = m.join(MesureChambreFluxSol_.version);
        subQueryMeasure
                .select(m)
                .where(builder.equal(version.get(VersionFile_.id), versionfileId));
        delete
                .where(v.get(ValeurChambreFluxSol_.mesure).in(subQueryMeasure));
        delete(delete);
    }

    /**
     *
     * @param versionfileId
     */
    protected void deleteMeasures(Long versionfileId) {
        CriteriaDelete<M> delete = builder.createCriteriaDelete(getMeasureClass());
        Root<M> m = delete.from(getMeasureClass());
        Subquery<VersionFile> subqueryversion = delete.subquery(VersionFile.class);
        Root<VersionFile> v = subqueryversion.from(VersionFile.class);
        subqueryversion
                .select(v)
                .where(builder.equal(v.get(VersionFile_.id), versionfileId));
        delete
                .where(m.get(MesureChambreFluxSol_.version).in(subqueryversion));
        delete(delete);
    }

    /**
     *
     * @param versionfileId
     */
    protected void deleteValeurInfoComplementaires(Long versionfileId) {
        CriteriaDelete<ValeurInformationComplementaire> delete = builder.createCriteriaDelete(ValeurInformationComplementaire.class);
        Root<ValeurInformationComplementaire> info = delete.from(ValeurInformationComplementaire.class);
        Subquery<V> subqueryValue = delete.subquery(getValueClass());
        Subquery<M> subQueryMeasure = subqueryValue.subquery(getMeasureClass());
        Root<V> v = subqueryValue.from(getValueClass());
        Root<M> m = subQueryMeasure.from(getMeasureClass());
        Join<M, VersionFile> version = m.join(MesureChambreFluxSol_.version);
        subQueryMeasure
                .select(m)
                .where(builder.equal(version.get(VersionFile_.id), versionfileId));
        subqueryValue
                .select(v)
                .where(v.get(ValeurChambreFluxSol_.mesure).in(subQueryMeasure));
        delete.where(info.get(ValeurInformationComplementaire_.valeurDecoree).in(subqueryValue));
        delete(delete);
    }
}
