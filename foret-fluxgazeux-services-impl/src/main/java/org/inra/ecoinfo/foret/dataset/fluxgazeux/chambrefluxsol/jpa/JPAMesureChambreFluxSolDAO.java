/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IMesureChambreFluxSolDAO;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.entity.MesureChambreFluxSol;


/**
 * @author sophie
 * @param <M0>
 * 
 */
public class JPAMesureChambreFluxSolDAO<M0 extends MesureChambreFluxSol<?>> extends AbstractJPADAO<M0> implements IMesureChambreFluxSolDAO<M0> {

}
