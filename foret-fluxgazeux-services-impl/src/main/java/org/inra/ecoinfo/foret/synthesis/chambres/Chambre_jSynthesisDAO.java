/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.synthesis.chambres;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_j;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_j;
import org.inra.ecoinfo.foret.synthesis.flcj.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.flcj.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class Chambre_jSynthesisDAO extends AbstractChambresSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurChambreFluxSol_j, MesureChambreFluxSol_j> {

    @Override
    Class<ValeurChambreFluxSol_j> getValueClass() {
        return ValeurChambreFluxSol_j.class;
    }

    @Override
    Class<MesureChambreFluxSol_j> getMeasureClass() {
        return MesureChambreFluxSol_j.class;
    }

}
