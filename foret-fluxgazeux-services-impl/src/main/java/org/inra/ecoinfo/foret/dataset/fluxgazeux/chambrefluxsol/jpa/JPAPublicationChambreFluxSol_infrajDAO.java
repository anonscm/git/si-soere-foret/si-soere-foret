/**
 *
 */
package org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.jpa;

import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.MesureChambreFluxSol_infraj;
import org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.entity.ValeurChambreFluxSol_infraj;

/**
 * @author sophie
 *
 */
public class JPAPublicationChambreFluxSol_infrajDAO extends JPAPublicationChambreFluxSolDAO<ValeurChambreFluxSol_infraj, MesureChambreFluxSol_infraj> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurChambreFluxSol_infraj> getValueClass() {
        return ValeurChambreFluxSol_infraj.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureChambreFluxSol_infraj> getMeasureClass() {
        return MesureChambreFluxSol_infraj.class;
    }
}
