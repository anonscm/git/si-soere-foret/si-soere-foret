package org.inra.ecoinfo.foret.ui.flex.vo;

import java.util.Objects;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;

/**
 *
 * @author ptcherniati
 */
public class SiteForetVO {

    /**
     *
     */
    public static final String TYPE ="site";
    private SiteForet site;
    private Boolean   selected;

    /**
     *
     * @param siteForet
     */
    public SiteForetVO(SiteForet siteForet) {
        this.site = siteForet;
        this.selected = Boolean.FALSE;
    }

    /**
     *
     * @return
     */
    public Boolean getSelected() {
        return this.selected;
    }

    /**
     *
     * @param selected
     */
    public void setSelected(Boolean selected) {
        this.selected = selected;
    }

    /**
     *
     * @return
     */
    public SiteForet getSite() {
        return this.site;
    }

    /**
     *
     * @param site
     */
    public void setSite(SiteForet site) {
        this.site = site;
    }

    @Override
    public int hashCode() {
        int hash = 7;
        return this.getSite().getId().intValue();
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final SiteForetVO other = (SiteForetVO) obj;
        return Objects.equals(this.site.getId(), other.site.getId());
    }

    /**
     *
     * @return
     */
    public String getType(){
        return TYPE;
    }
}
