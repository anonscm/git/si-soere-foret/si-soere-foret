package org.inra.ecoinfo.foret.dataset.flux;

import org.inra.ecoinfo.foret.dataset.flux.entity.MesureFlux;


/**
 * @author philippe
 * @param <T>
 * 
 */
public interface IValeurFluxTour<T extends MesureFlux> {

    /**
     *
     * @return
     */
    MesureFlux getMesure();

    /**
     *
     * @return
     */
    Long getMesureFluxId();
}
