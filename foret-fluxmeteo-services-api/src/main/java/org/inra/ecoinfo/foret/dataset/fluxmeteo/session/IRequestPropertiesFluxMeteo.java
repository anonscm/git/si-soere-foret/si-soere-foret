package org.inra.ecoinfo.foret.dataset.fluxmeteo.session;

import org.inra.ecoinfo.foret.dataset.IRequestProperties;

/**
 *
 * @author ptcherniati
 */
public interface IRequestPropertiesFluxMeteo extends IRequestProperties {

    /**
     *
     * @return
     */
    boolean isVerifierSequenceDate();

    /**
     *
     * @param verifierSequenceDate
     */
    void setVerifierSequenceDate(boolean verifierSequenceDate);
}
