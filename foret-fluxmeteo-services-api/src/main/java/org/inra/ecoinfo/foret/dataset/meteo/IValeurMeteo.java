package org.inra.ecoinfo.foret.dataset.meteo;

import org.inra.ecoinfo.foret.dataset.meteo.entity.MesureMeteo;


/**
 * @author philippe
 * @param <T>
 * 
 */
public interface IValeurMeteo<T extends MesureMeteo> {

    /**
     *
     * @return
     */
    T getMesure();

    /**
     *
     * @return
     */
    Long getMesureMeteoId();
}
