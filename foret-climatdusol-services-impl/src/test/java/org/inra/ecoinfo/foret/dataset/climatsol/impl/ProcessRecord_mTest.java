package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.SortedMap;
import java.util.SortedSet;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ErrorsReport;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.impl.CleanerValues;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author tcherniatinsky
 */
@Ignore
public class ProcessRecord_mTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils m = MockUtils.getInstance();
    List<Column> columns = new LinkedList();
    Map<String, ExpectedColumn> columnsMap = new HashMap();
    ProcessRecord_m instance;
    ErrorsReport errorsReport = new ErrorsReport();
    @Mock
    RequestPropertiesCS requestProperties;
    @Mock
    DatasetDescriptorCS datasetDescriptor;
    @Mock
    ExpectedColumn dateColumn;
    @Mock
    ExpectedColumn timeColumn;
    @Mock
    ExpectedColumn variableColumn;
    @Mock
    ExpectedColumn variableqcColumn;
    @Mock
    IProfilClimatSolDAO profilClimatSolDAO;
    @Mock
    org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.impl.Recorder_m recorderSwc_m;
    @Mock
    org.inra.ecoinfo.foret.dataset.climatsol.temperature.impl.Recorder_m recorderTp_m;
    @Mock
    org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.impl.Recorder_m recorderG_m;
    @Mock
    org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.impl.Recorder_m recorderSMP_m;
    @Mock
    org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.impl.Recorder_m recorderGWD_m;

    /**
     *
     */
    public ProcessRecord_mTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        MockitoAnnotations.initMocks(this);
        this.instance = new ProcessRecord_m();
        initInstance(instance);
        Column colonne1 = this.m.column;
        Column colonne2 = this.m.column;
        Column colonne3 = this.m.column;
        Column colonne4 = this.m.column;
        Column colonne5 = this.m.column;
        Column colonne6 = this.m.column;
        Column colonne7 = this.m.column;
        Column colonne8 = this.m.column;
        this.columns.add(colonne1);
        this.columns.add(colonne2);
        this.columns.add(colonne3);
        this.columns.add(colonne4);
        this.columns.add(colonne5);
        this.columns.add(colonne6);
        this.columns.add(colonne7);
        this.columns.add(colonne8);
        when(dateColumn.getName()).thenReturn(Constantes.PROPERTY_CST_DATE_TYPE);
        when(dateColumn.getFormatType()).thenReturn(DateUtil.DD_MM_YYYY);
        when(timeColumn.getName()).thenReturn(Constantes.PROPERTY_CST_TIME_TYPE);
        when(timeColumn.getFormatType()).thenReturn(DateUtil.HH_MM);
        when(variableColumn.isVariable()).thenReturn(true);
        when(variableColumn.getName()).thenReturn("colonne variable");
        when(variableqcColumn.isVariable()).thenReturn(true);
        when(variableqcColumn.getFlagType()).thenReturn(Constantes.PROPERTY_CST_QUALITY_CLASS_TYPE);
        //when(m.variableDAO.getByCode("colonne variable")).thenReturn(m.variable);
        when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        when(datasetDescriptor.getEnTete()).thenReturn(2);
        columnsMap.put("variable1_2_10", variableColumn);
        columnsMap.put("variable2_1_50", variableColumn);
    }

    private void initInstance(ProcessRecord_m instance) {
        instance.setVariableDAO(this.m.variableDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setDatatypeVariableUniteForetDAO(this.m.datatypeVariableUniteDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setRecorderGWD_m(recorderGWD_m);
        instance.setRecorderSMP_m(recorderSMP_m);
        instance.setRecorderG_m(recorderG_m);
        instance.setRecorderSwc_m(recorderSwc_m);
        instance.setRecorderTp_m(recorderTp_m);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of processRecord method, of class ProcessRecord_m.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testProcessRecord() throws Exception {
        CSVParser parser = null;
        VersionFile versionFile = null;
        IRequestProperties requestProperties = null;
        String fileEncoding = "";
        DatasetDescriptor datasetDescriptor = null;
        ProcessRecord_m instance = new ProcessRecord_m();
        instance.processRecord(parser, versionFile, requestProperties, fileEncoding, datasetDescriptor);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of choiceBuildProfil method, of class ProcessRecord_m.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testChoiceBuildProfil() throws Exception {
        List<VariableForet> lstDbVariable = null;
        VersionFile versionFile = null;
        LinkedList<Line> lstLines = null;
        SiteForet zoneEtude = null;
        ErrorsReport errorsReport = null;
        ProcessRecord_m instance = new ProcessRecord_m();
        //instance.choiceBuildProfil(lstDbVariable, versionFile, lstLines, zoneEtude, errorsReport);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of setRecorderSwc_m method, of class ProcessRecord_m.
     */
    @Test
    public void testSetRecorderSwc_m() {
        assertEquals(recorderSwc_m, instance.recorderSwc_m);
    }

    /**
     * Test of setRecorderTp_m method, of class ProcessRecord_m.
     */
    @Test
    public void testSetRecorderTp_m() {
        assertEquals(recorderTp_m, instance.recorderTp_m);
    }

    /**
     * Test of setRecorderG_m method, of class ProcessRecord_m.
     */
    @Test
    public void testSetRecorderG_m() {
        assertEquals(recorderG_m, instance.recorderG_m);
    }

    /**
     * Test of setRecorderSMP_m method, of class ProcessRecord_m.
     */
    @Test
    public void testSetRecorderSMP_m() {
        assertEquals(recorderSMP_m, instance.recorderSMP_m);
    }

    /**
     * Test of setRecorderGWD_m method, of class ProcessRecord_m.
     */
    @Test
    public void testSetRecorderGWD_m() {
        assertEquals(recorderGWD_m, instance.recorderGWD_m);
    }

    /**
     * Test of buildVariableHeaderAndSkipHeader method, of class
     * ProcessRecord_m.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildVariableHeaderAndSkipHeader() throws Exception {
        StringReader in = new StringReader("ligne en tête;\n20/01/2014;12:34;35.2;40.2\n20/01/2014;12:64;;40.2");
        CSVParser parser = new CSVParser(in, ';');
        when(datasetDescriptor.getEnTete()).thenReturn(1);
        when(requestProperties.getExpectedColumns()).thenReturn(columnsMap);
        VariableForet variable2 = mock(VariableForet.class);
        /*when(m.variableDAO.getByCode("variable1")).thenReturn(m.variable);
        when(m.variableDAO.getByCode("variable2")).thenReturn(variable2);
        when(m.variableDAO.getByCode(m.VARIABLE_CODE)).thenReturn(m.variable);*/
        List<RealNode> result = instance.buildVariableHeaderAndSkipHeader(parser, requestProperties, datasetDescriptor, m.datatypeRealNode);
        assertEquals(m.variable, result.get(0));
        assertEquals(variable2, result.get(1));
        assertTrue(2 == result.size());
        assertTrue(1 == parser.getLastLineNumber());
    }

    /**
     * Test of registerLine method, of class ProcessRecord_m.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testRegisterLine() throws Exception {
        Line line = null;
        List<RealNode> lstDbVariable = null;
        VersionFile versionFile = null;
        SiteForet zoneEtude = null;
        ErrorsReport errorsReport = null;
        SortedSet<Line> ligneEnErreur = null;
        SortedMap<Long, SortedSet<String>> lstVarNameEnErreur = null;
        ProcessRecord_m instance = new ProcessRecord_m();
        instance.registerLine(line, lstDbVariable, versionFile, zoneEtude, errorsReport, ligneEnErreur, lstVarNameEnErreur);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of treatErrorLines method, of class ProcessRecord_m.
     */
    @Test
    @Ignore
    public void testTreatErrorLines() {
        SortedSet<Line> ligneEnErreur = null;
        SortedMap<Long, SortedSet<String>> lstVarNameEnErreur = null;
        ErrorsReport errorsReport = null;
        ProcessRecord_m instance = new ProcessRecord_m();
        instance.treatErrorLines(ligneEnErreur, lstVarNameEnErreur, errorsReport);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of buildSubList method, of class ProcessRecord_m.
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testBuildSubList() throws Exception {
        List<List<VariableValue>> listTotSouslstVar = null;
        List<RealNode> lstDbVariable = null;
        VersionFile versionFile = null;
        Line line = null;
        SiteForet zoneEtude = null;
        ErrorsReport errorsReport = null;
        SortedSet<Line> ligneEnErreur = null;
        SortedMap<Long, SortedSet<String>> lstVarNameEnErreur = null;
        SortedSet<String> setVarName = null;
        ProcessRecord_m instance = new ProcessRecord_m();
        instance.buildSubList(listTotSouslstVar, lstDbVariable, versionFile, line, zoneEtude, errorsReport, ligneEnErreur, lstVarNameEnErreur, setVarName);
        // TODO review the generated test code and remove the default call to fail.
        fail("The test case is a prototype.");
    }

    /**
     * Test of createLstVariableValueForlines method, of class ProcessRecord_m.
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateLstVariableValueForlines() throws Exception {
        StringReader in = new StringReader("09/2004;12.5;13.2;15\n10/2004;13.5;14.2;16");
        CSVParser parser = new CSVParser(in, ';');
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        when(requestProperties.getColumn(1)).thenReturn(variableColumn);
        when(variableColumn.getName()).thenReturn("variable_1_10");
        when(variableColumn.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        when(variableColumn.hasNombreRepetition()).thenReturn(true);
        when(variableColumn.hasEcartType()).thenReturn(true);
        when(datasetDescriptor.getUndefinedColumn()).thenReturn(1);
        columnsMap.clear();
        columnsMap.put("variable_2_10", variableColumn);
        when(requestProperties.getExpectedColumns()).thenReturn(columnsMap);
        //nominal
        List<Line> lines = null;
        try {
            lines = instance.createLstVariableValueForlines(parser, requestProperties, m.site, datasetDescriptor);
            assertTrue(2 == lines.size());
            assertFalse(badsFormatsReport.hasErrors());
            verifyLine("09/2004", 12.5F, 13.2F, 15, lines.get(0));
            verifyLine("10/2004", 13.5F, 14.2F, 16, lines.get(1));
        } catch (IOException | BadFormatException | ParseException e) {
            fail();
        }
        //with bad date
        in = new StringReader("badDate;12.5;13.2;15\n10/2004;13.5;14.2;16\n10/2004;13.5;14.2;16\n10/2004;13.5;14.2;16");
        parser = new CSVParser(in, ';');
        badsFormatsReport = new BadsFormatsReport("erreur");
        try {
            lines = instance.createLstVariableValueForlines(parser, requestProperties, m.site, datasetDescriptor);
            fail();
        } catch (IOException | BadFormatException | ParseException e) {
            assertEquals("Unparseable date: \"baddate\"", e.getMessage());
        }
        //with bad value
        in = new StringReader("09/2004;douze;13.2;15\n10/2004;13.5;quatorze;16\n10/2004;13.5;seize");
        parser = new CSVParser(in, ';');
        badsFormatsReport = new BadsFormatsReport("erreur");
        try {
            lines = instance.createLstVariableValueForlines(parser, requestProperties, m.site, datasetDescriptor);
            fail();
        } catch (IOException | BadFormatException | ParseException e) {
            assertEquals("Format invalide :- Une valeur flottante est attendue à la ligne 0, colonne 3 (variable_1_10): la valeur actuelle est \"douze\" ", e.getMessage());
        }
        //with bad et
        in = new StringReader("10/2004;13.5;quatorze;16\n10/2004;13.5;seize");
        parser = new CSVParser(in, ';');
        badsFormatsReport = new BadsFormatsReport("erreur");
        try {
            lines = instance.createLstVariableValueForlines(parser, requestProperties, m.site, datasetDescriptor);
            fail();
        } catch (IOException | BadFormatException | ParseException e) {
            assertEquals("", e.getMessage());
        }
        //with bad nbRep
        in = new StringReader("10/2004;13.5;seize");
        parser = new CSVParser(in, ';');
        badsFormatsReport = new BadsFormatsReport("erreur");
        try {
            lines = instance.createLstVariableValueForlines(parser, requestProperties, m.site, datasetDescriptor);
            fail();
        } catch (IOException | BadFormatException | ParseException e) {
            assertEquals("", e.getMessage());
        }
        /*VariableValue variableValue = lstVariablesValues.get(0);
        assertTrue(variableValue.getValue()==12.5F);
        assertTrue(variableValue.getEcartType()==13.2F);
        assertTrue(variableValue.getNombreRepetition()==15);
        //bad float value
        cleanerValues = new CleanerValues(new String[]{"douze", "13.2", "15"});
        lstVariablesValues = new LinkedList();
        instance.buildVariableValueForColumnGroup(cleanerValues, requestProperties, 0, datasetDescriptor, 5L, badsFormatsReport, lstVariablesValues);
        assertTrue(lstVariablesValues.isEmpty());
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur flottante est attendue à la ligne 5, colonne 1 (variable_1_10): la valeur actuelle est \"douze\" </p>", badsFormatsReport.getHTMLMessages());
        //bad float ET
        badsFormatsReport = new BadsFormatsReport("erreur");
        cleanerValues = new CleanerValues(new String[]{"13.2", "douze", "15"});
        lstVariablesValues = new LinkedList();
        instance.buildVariableValueForColumnGroup(cleanerValues, requestProperties, 0, datasetDescriptor, 5L, badsFormatsReport, lstVariablesValues);
        assertTrue(1==lstVariablesValues.size());
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur flottante est attendue à la ligne 5, colonne 2 (SE): la valeur actuelle est \"douze\" </p>", badsFormatsReport.getHTMLMessages());
        //bad float nb rep
        badsFormatsReport = new BadsFormatsReport("erreur");
        cleanerValues = new CleanerValues(new String[]{"13.2", "15", "douze"});
        lstVariablesValues = new LinkedList();
        instance.buildVariableValueForColumnGroup(cleanerValues, requestProperties, 0, datasetDescriptor, 5L, badsFormatsReport, lstVariablesValues);
        assertTrue(1==lstVariablesValues.size());
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur entière est attendue à la ligne 5, colonne 3 (Nb Rep): la valeur actuelle est \"douze\" </p>", badsFormatsReport.getHTMLMessages());
         */
    }

    /**
     * Test of buildVariableValueForColumnGroup method, of class
     * ProcessRecord_m.
     */
    @Test
    public void testBuildVariableValueForColumnGroup() {
        CleanerValues cleanerValues = new CleanerValues(new String[]{"12.5", "13.2", "15"});
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        List<VariableValue> lstVariablesValues = new LinkedList();
        when(requestProperties.getColumn(0)).thenReturn(variableColumn);
        when(variableColumn.getName()).thenReturn("variable_1_10");
        when(variableColumn.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        when(variableColumn.hasNombreRepetition()).thenReturn(true);
        when(variableColumn.hasEcartType()).thenReturn(true);
        //nominal
        instance.buildVariableValueForColumnGroup(cleanerValues, requestProperties, 0, datasetDescriptor, 5L, badsFormatsReport, lstVariablesValues);
        assertTrue(1 == lstVariablesValues.size());
        assertFalse(badsFormatsReport.hasErrors());
        VariableValue variableValue = lstVariablesValues.get(0);
        assertTrue(variableValue.getValue() == 12.5F);
        assertTrue(variableValue.getEcartType() == 13.2F);
        assertTrue(variableValue.getNombreRepetition() == 15);
        //bad float value
        cleanerValues = new CleanerValues(new String[]{"douze", "13.2", "15"});
        lstVariablesValues = new LinkedList();
        instance.buildVariableValueForColumnGroup(cleanerValues, requestProperties, 0, datasetDescriptor, 5L, badsFormatsReport, lstVariablesValues);
        assertTrue(lstVariablesValues.isEmpty());
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur flottante est attendue à la ligne 5, colonne 1 (variable_1_10): la valeur actuelle est \"douze\" </p>", badsFormatsReport.getHTMLMessages());
        //bad float ET
        badsFormatsReport = new BadsFormatsReport("erreur");
        cleanerValues = new CleanerValues(new String[]{"13.2", "douze", "15"});
        lstVariablesValues = new LinkedList();
        instance.buildVariableValueForColumnGroup(cleanerValues, requestProperties, 0, datasetDescriptor, 5L, badsFormatsReport, lstVariablesValues);
        assertTrue(1 == lstVariablesValues.size());
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur flottante est attendue à la ligne 5, colonne 2 (SE): la valeur actuelle est \"douze\" </p>", badsFormatsReport.getHTMLMessages());
        //bad float nb rep
        badsFormatsReport = new BadsFormatsReport("erreur");
        cleanerValues = new CleanerValues(new String[]{"13.2", "15", "douze"});
        lstVariablesValues = new LinkedList();
        instance.buildVariableValueForColumnGroup(cleanerValues, requestProperties, 0, datasetDescriptor, 5L, badsFormatsReport, lstVariablesValues);
        assertTrue(1 == lstVariablesValues.size());
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur entière est attendue à la ligne 5, colonne 3 (Nb Rep): la valeur actuelle est \"douze\" </p>", badsFormatsReport.getHTMLMessages());
    }

    /**
     * Test of readRepetitionNumber method, of class ProcessRecord_m.
     */
    @Test
    public void testReadRepetitionNumber() {
        VariableValue variableValue = mock(VariableValue.class);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        when(variableColumn.hasNombreRepetition()).thenReturn(true);
        //nominal
        instance.readRepetitionNumber(variableColumn, "12", 5L, 3, badsFormatsReport, variableValue);
        assertFalse(badsFormatsReport.hasErrors());
        verify(variableValue).setNombreRepetition(12);
        //not a float
        instance.readRepetitionNumber(variableColumn, "treize", 5L, 3, badsFormatsReport, variableValue);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur entière est attendue à la ligne 5, colonne 3 (Nb Rep): la valeur actuelle est \"treize\" </p>", badsFormatsReport.getHTMLMessages());
        //empty value
        badsFormatsReport = new BadsFormatsReport("erreur");
        instance.readRepetitionNumber(variableColumn, "", 5L, 3, badsFormatsReport, variableValue);
        assertFalse(badsFormatsReport.hasErrors());
        verify(variableValue).setNombreRepetition(0);
        //empty value
        when(variableColumn.hasNombreRepetition()).thenReturn(false);
        instance.readRepetitionNumber(variableColumn, "", 5L, 3, badsFormatsReport, variableValue);
        assertFalse(badsFormatsReport.hasErrors());
        verify(variableValue).setNombreRepetition(0);
    }

    /**
     * Test of readStandardDeviation method, of class ProcessRecord_m.
     */
    @Test
    public void testReadStandardDeviation() {
        VariableValue variableValue = mock(VariableValue.class);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        when(variableColumn.hasEcartType()).thenReturn(true);
        //nominal
        instance.readStandardDeviation(variableColumn, "12.5", 5L, 3, badsFormatsReport, variableValue);
        assertFalse(badsFormatsReport.hasErrors());
        verify(variableValue).setEcartType(12.5F);
        //not a float
        instance.readStandardDeviation(variableColumn, "treize", 5L, 3, badsFormatsReport, variableValue);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur flottante est attendue à la ligne 5, colonne 3 (SE): la valeur actuelle est \"treize\" </p>", badsFormatsReport.getHTMLMessages());
        //empty value
        badsFormatsReport = new BadsFormatsReport("erreur");
        instance.readStandardDeviation(variableColumn, "", 5L, 3, badsFormatsReport, variableValue);
        assertFalse(badsFormatsReport.hasErrors());
        verify(variableValue).setEcartType(0F);
        //empty value
        when(variableColumn.hasEcartType()).thenReturn(false);
        instance.readStandardDeviation(variableColumn, "", 5L, 3, badsFormatsReport, variableValue);
        assertFalse(badsFormatsReport.hasErrors());
        verify(variableValue).setEcartType(0F);

    }

    private void verifyLine(String date, float value, float et, int nbRep, Line line) {
        assertNotNull(line);
        assertEquals(m.site, line.getZoneEtude());
        assertEquals(date, DateUtil.getUTCDateTextFromLocalDateTime(line.getDate(), DateUtil.DD_MM_YYYY));
        final List<VariableValue> lstVariablesValues = line.getLstVariablesValues();
        assertTrue(1 == lstVariablesValues.size());
        final VariableValue variableValue = lstVariablesValues.get(0);
        assertTrue(value == variableValue.getValue());
        assertTrue(et == variableValue.getEcartType());
        assertTrue(nbRep == variableValue.getNombreRepetition());
    }

}
