
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author tcherniatinsky
 */
public class VerificateurEnteteClimatSol_mTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    VerificateurEnteteClimatSol_m instance;
    MockUtils m = MockUtils.getInstance();
    @Mock
    RequestPropertiesCS requestProperties;
    @Mock
    DatasetDescriptor datasetDescriptor;

    /**
     *
     */
    public VerificateurEnteteClimatSol_mTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new VerificateurEnteteClimatSol_m();
        instance.setDatasetConfiguration(m.datasetConfiguration);
        instance.setDatatypeDAO(m.datatypeDAO);
        instance.setDatatypeVariableUniteForetDAO(m.datatypeVariableUniteDAO);
        instance.setLocalizationManager(m.localizationManager);
        instance.setSiteForetDAO(m.siteDAO);
        instance.setThemeDAO(m.themeDAO);
        instance.setUniteDAO(m.uniteDAO);
        instance.setUtilisateurDAO(m.utilisateurDAO);
        instance.setVariableDAO(m.variableDAO);
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of testHeaders method, of class VerificateurEnteteClimatSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeaders() throws Exception {
        StringReader in = new StringReader("");
        CSVParser parser = new CSVParser(in);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        instance = new Instance();
        long result = instance.testHeaders(parser, m.versionFile, requestProperties, "utf-8", badsFormatsReport, datasetDescriptor);
        assertEquals(11L, result);
    }

    /**
     * Test of readNomDesColonnes method, of class VerificateurEnteteClimatSol.
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testReadNomDesColonnes() throws PersistenceException {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        StringReader in = new StringReader("variablecode_10;SE;Nb Rep");
        CSVParser parser = new CSVParser(in, ';');
        when(datasetDescriptor.getName()).thenReturn("datatype");
        List<DatatypeVariableUnite> dvus = new LinkedList();
        DatatypeVariableUniteForet dvu = Mockito.mock(DatatypeVariableUniteForet.class);
        when(dvu.getVariable()).thenReturn(m.variable);
        List<Column> columns = new ArrayList();
        Column column = new Column();
        column.setName("variablecode");
        columns.add(column);
        when(datasetDescriptor.getColumns()).thenReturn(columns);
        when(requestProperties.getRepetitionMax()).thenReturn(2);
        when(requestProperties.getProfondeurMax()).thenReturn(10);
        dvus.add(dvu);
        when(m.datatypeVariableUniteDAO.getByDatatype("datatype")).thenReturn(dvus);
        ArgumentCaptor<Map> l = ArgumentCaptor.forClass(Map.class);
        Map<String, DatatypeVariableUniteForet> dvusmap = new HashMap();
        dvusmap.put("variablecode", dvu);
        when(requestProperties.getVariableTypeDonnees()).thenReturn(dvusmap);
        long result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L == result);
        verify(requestProperties).addExpectedColumn(0, "variablecode_10", 10, true, true, null, null);
        assertFalse(badsFormatsReport.hasErrors());
        verify(requestProperties).setColumnNames(AdditionalMatchers.aryEq(new String[]{"variablecode_10", "SE", "Nb Rep"}), Matchers.eq(columns));
       //mauvais nom de colonne
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode;SE;Nb Rep");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 : L'intitulé de la colonne 1 est \"variablecode\", alors que \"nomDeVariable_profondeur\" est attendu. 	 </p>", badsFormatsReport.getHTMLMessages());
        //mauvais nom de colonne
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_10;bad SE;Nb Rep");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 (colonne 2) : Les colonnes sont ne pas dans l'ordre attendu (colonne1 : \"nomDeVariable_profondeur\", colonne2 : \"SE\", colonne3  : \"Nb Rep\").  </p>", badsFormatsReport.getHTMLMessages());
         //mauvais nom de colonne
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_10;SE;bad Nb Rep");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 (colonne 3) : Les colonnes sont ne pas dans l'ordre attendu (colonne1 : \"nomDeVariable_profondeur\", colonne2 : \"SE\", colonne3  : \"Nb Rep\").  </p>", badsFormatsReport.getHTMLMessages());
        //nom de variable absent
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("_10;SE;Nb Rep");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 : L'intitulé de la colonne 1 est \"_10\", alors que \"nomDeVariable_profondeur\" est attendu. 	La variable \"\" n'est pas associée au datatype \"datatype\".	 </p>", badsFormatsReport.getHTMLMessages());
        //nom de variable erronné
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("autrevariablecode_10;SE;Nb Rep");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 : L'intitulé de la colonne 1 est \"autrevariablecode_10\", alors que \"nomDeVariable_profondeur\" est attendu. 	La variable \"autrevariablecode\" n'est pas associée au datatype \"datatype\".	 </p>", badsFormatsReport.getHTMLMessages());
        //pas de profondeur
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_p;SE;Nb Rep");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 colonne 1  (\"variablecode_p\"), la profondeur doit être un entier positif. Sa valeur actuelle est p.	 </p>", badsFormatsReport.getHTMLMessages());
        //profondeur trop grande
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_11;SE;Nb Rep");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 colonne 1  (\"variablecode_11\"), la profondeur est supérieure à la profondeur maximum indiquée dans l'entête du fichier (\"10\").	 </p>", badsFormatsReport.getHTMLMessages());
            }
    class Instance extends VerificateurEnteteClimatSol_m{

        @Override
        protected long readRepetitionMax(VersionFile versionFile, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected long readProfondeurMax(VersionFile versionFile, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected void addExpectedColumn(DatasetDescriptor datasetDescriptor, IRequestPropertiesIntervalValue requestProperties) throws BusinessException {
            
        }

        @Override
        protected long readNomDesColonnes(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected long readValeursMin(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestPropertiesIntervalValue requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected long readValeursMax(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestPropertiesIntervalValue requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected void minMaxValues(DatasetDescriptor datasetDescriptor, IRequestPropertiesIntervalValue requestProperties) throws BusinessException {
            
        }

        @Override
        protected long readSite(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties) {
            return lineNumber+1;
        }

        @Override
        protected long readEmptyLine(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber+1;
        }

        @Override
        protected long readFrequence(IRequestProperties requestProperties, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber+1;
        }

        @Override
        protected long readDatatype(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber+1;
        }

        @Override
        protected long readBeginAndEndDates(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+2;
        }

        @Override
        protected long readCommentaires(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties) {
            return lineNumber+1;
        }
        
    }
}
