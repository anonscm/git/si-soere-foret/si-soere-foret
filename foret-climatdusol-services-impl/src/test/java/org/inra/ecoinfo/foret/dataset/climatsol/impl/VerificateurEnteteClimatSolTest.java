
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.StringReader;
import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.regex.Matcher;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.infocomplstdtvar.IInfoComplementaireStdtVariableDAO;
import org.inra.ecoinfo.foret.refdata.infocomplstdtvar.JPAInfoComplementaireStdtVariableDAO;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.IInformationComplementaireDAO;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.JPAInformationComplementaireDAO;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.mockito.AdditionalMatchers;
import org.mockito.ArgumentCaptor;
import org.mockito.Matchers;
import org.mockito.Mock;
import org.mockito.Mockito;
import static org.mockito.Mockito.never;
import static org.mockito.Mockito.verify;
import static org.mockito.Mockito.when;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author tcherniatinsky
 */
public class VerificateurEnteteClimatSolTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    VerificateurEnteteClimatSol instance;
    MockUtils m = MockUtils.getInstance();
    @Mock
    RequestPropertiesCS requestProperties;
    @Mock
    DatasetDescriptor datasetDescriptor;

    /**
     *
     */
    public VerificateurEnteteClimatSolTest() {
    }

    /**
     *
     */
    @Before
    public void setUp() {
        instance = new VerificateurEnteteClimatSol();
        instance.setDatasetConfiguration(m.datasetConfiguration);
        instance.setDatatypeDAO(m.datatypeDAO);
        instance.setDatatypeVariableUniteForetDAO(m.datatypeVariableUniteDAO);
        instance.setLocalizationManager(m.localizationManager);
        instance.setSiteForetDAO(m.siteDAO);
        instance.setThemeDAO(m.themeDAO);
        instance.setUniteDAO(m.uniteDAO);
        instance.setUtilisateurDAO(m.utilisateurDAO);
        instance.setVariableDAO(m.variableDAO);
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of setInfoComplementaireDAO method, of class
     * VerificateurEnteteClimatSol.
     */
    @Test
    public void testSetInfoComplementaireDAO() {
        IInformationComplementaireDAO infoComplementaireDAO = new JPAInformationComplementaireDAO();
        instance.setInfoComplementaireDAO(infoComplementaireDAO);
        assertEquals(instance.infoComplementaireDAO, infoComplementaireDAO);
    }

    /**
     * Test of setInfoCpltStdtVariableDAO method, of class
     * VerificateurEnteteClimatSol.
     */
    @Test
    public void testSetInfoCpltStdtVariableDAO() {
        IInfoComplementaireStdtVariableDAO infoCpltStdtVariableDAO = new JPAInfoComplementaireStdtVariableDAO();
        instance.setInfoCpltStdtVariableDAO(infoCpltStdtVariableDAO);
        assertEquals(instance.infoCpltStdtVariableDAO, infoCpltStdtVariableDAO);
    }

    /**
     * Test of testHeaders method, of class VerificateurEnteteClimatSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testTestHeaders() throws Exception {
        StringReader in = new StringReader("");
        CSVParser parser = new CSVParser(in);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        instance = new Instance();
        long result = instance.testHeaders(parser, m.versionFile, requestProperties, "utf-8", badsFormatsReport, datasetDescriptor);
        assertEquals(12L, result);
    }

    /**
     * Test of getMatcher method, of class VerificateurEnteteClimatSol.
     */
    @Test
    public void testGetMatcher() {
        String nomVariable = "nomVariable";
        Matcher result = instance.getMatcher(nomVariable);
        assertEquals("^(nomVariable_([0-9]+)_([0-9]+))$", result.pattern().toString());
    }

    /**
     * Test of getVariablePattern method, of class VerificateurEnteteClimatSol.
     */
    @Test
    public void testGetVariablePattern() {
        String nomVariable = "nomVariable";
        String expResult = "^(nomVariable_([0-9]+)_([0-9]+))$";
        String result = instance.getVariablePattern(nomVariable);
        assertEquals(expResult, result);
    }

    /**
     * Test of setVariableName method, of class VerificateurEnteteClimatSol.
     */
    @Test
    public void testSetVariableName() {
        String variableName = "nomVariable";
        instance.setVariableName(variableName);
        assertEquals(variableName, instance.variableName);
    }

    /**
     * Test of getDatatypeWithFrequenceCode method, of class
     * VerificateurEnteteClimatSol.
     */
    @Test
    public void testGetDatatypeWithFrequenceCode() {
        String expResult = "datatype";
        when(datasetDescriptor.getName()).thenReturn(expResult);
        String result = instance.getDatatypeWithFrequenceCode(datasetDescriptor);
        assertEquals(expResult, result);
    }

    /**
     * Test of getMessageFrequenceErreur method, of class
     * VerificateurEnteteClimatSol.
     */
    @Test
    public void testGetMessageFrequenceErreur() {
        String[] values = new String[]{"frequence :","fr"};
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        long lineNumber = 2L;
        instance.getMessageFrequenceErreur(values, badsFormatsReport, lineNumber);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 2 : la fréquence \"fr\" est invalide. Les fréquences valides sont infra-journalier, journalier, mensuel  </p>", badsFormatsReport.getHTMLMessages());
    }

    /**
     * Test of readNomDesColonnes method, of class VerificateurEnteteClimatSol.
     * @throws org.inra.ecoinfo.utils.exceptions.PersistenceException
     */
    @Test
    public void testReadNomDesColonnes() throws PersistenceException {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        StringReader in = new StringReader("variablecode_1_10;variablecode_2_10;variablecode_1_5");
        CSVParser parser = new CSVParser(in, ';');
        when(datasetDescriptor.getName()).thenReturn("datatype");
        List<DatatypeVariableUnite> dvus = new LinkedList();
        DatatypeVariableUniteForet dvu = Mockito.mock(DatatypeVariableUniteForet.class);
        when(dvu.getVariable()).thenReturn(m.variable);
        List<Column> columns = new ArrayList();
        Column column = new Column();
        columns.add(column);
        when(datasetDescriptor.getColumns()).thenReturn(columns);
        when(requestProperties.getRepetitionMax()).thenReturn(2);
        when(requestProperties.getProfondeurMax()).thenReturn(10);
        instance.variableName = m.VARIABLE_CODE;
        dvus.add(dvu);
        when(m.datatypeVariableUniteDAO.getByDatatype("datatype")).thenReturn(dvus);
        ArgumentCaptor<Map> l = ArgumentCaptor.forClass(Map.class);
        long result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L == result);
        verify(requestProperties).addExpectedColumn(0, "variablecode_1_10", 10, 1, null, null);
        verify(requestProperties).addExpectedColumn(1, "variablecode_2_10", 10, 2, null, null);
        verify(requestProperties).addExpectedColumn(2, "variablecode_1_5", 5, 1, null, null);
        verify(requestProperties).setVariableTypeDonnees(l.capture());
        assertEquals(l.getValue().get(m.VARIABLE_NOM), dvu);
        assertFalse(badsFormatsReport.hasErrors());
        verify(requestProperties).setColumnNames(AdditionalMatchers.aryEq(new String[]{"variablecode_1_10", "variablecode_2_10", "variablecode_1_5"}), Matchers.eq(columns));
        //mauvais nom de colonne
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode;variablecode_2_10;variablecode_1_5");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 : L'intitulé de la colonne 1 est \"variablecode\", alors que \"variablecode_repetition_profondeur\" est attendu. 	 </p>", badsFormatsReport.getHTMLMessages());
        //nom de variable absent
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("_1_10;variablecode_2_10;variablecode_1_5");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 : L'intitulé de la colonne 1 est \"_1_10\", alors que \"variablecode_repetition_profondeur\" est attendu. 	 </p>", badsFormatsReport.getHTMLMessages());
        //nom de variable erronné
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("autrevariablecode_1_10;variablecode_2_10;variablecode_1_5");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 : L'intitulé de la colonne 1 est \"autrevariablecode_1_10\", alors que \"variablecode_repetition_profondeur\" est attendu. 	La variable \"autrevariablecode\" n'est pas associée au datatype \"datatype\".	 </p>", badsFormatsReport.getHTMLMessages());
        //pas de profondeur
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_1_p;variablecode_2_10;variablecode_1_5");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 colonne 1  (\"variablecode_1_p\"), la profondeur doit être un entier positif. Sa valeur actuelle est p.	 </p>", badsFormatsReport.getHTMLMessages());
        //pas de repetition
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_r_10;variablecode_2_10;variablecode_1_5");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 colonne 1  (\"variablecode_r_10\"), le numéro de répétition doit être un entier positif. Sa valeur actuelle est \"r\".	 </p>", badsFormatsReport.getHTMLMessages());
        //profondeur trop grande
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_1_11;variablecode_2_10;variablecode_1_5");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 colonne 1  (\"variablecode_1_11\"), la profondeur est supérieure à la profondeur maximum indiquée dans l'entête du fichier (\"10\").	 </p>", badsFormatsReport.getHTMLMessages());
        //répétition trop grande
        badsFormatsReport = new BadsFormatsReport("erreur");
        in = new StringReader("variablecode_3_10;variablecode_2_10;variablecode_1_5");
        parser = new CSVParser(in, ';');
        result = instance.readNomDesColonnes(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4 colonne 1  (\"variablecode_3_10\"), le numéro de répétition est supérieur au numéro de répétition maximum indiqué dans l'entête du fichier (\"2\"). 	 </p>", badsFormatsReport.getHTMLMessages());
   }

    /**
     * Test of addExpectedColumn method, of class VerificateurEnteteClimatSol.
     * @throws java.lang.Exception
     */
    @Test
    public void testAddExpectedColumn() throws Exception {
        String[] columnsNames = new String[]{"colonne1","colonne 2"};
        when(datasetDescriptor.getUndefinedColumn()).thenReturn(1);
        when(requestProperties.getColumnNames()).thenReturn(columnsNames);
        instance.addExpectedColumn(datasetDescriptor, requestProperties);
        verify(requestProperties, never()).addExpectedColumn("colonne1", datasetDescriptor);
        verify(requestProperties).addExpectedColumn("colonne 2", datasetDescriptor);
    }

    /**
     * Test of readProfondeurMax method, of class VerificateurEnteteClimatSol.
     */
    @Test
    public void testReadProfondeurMax() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        StringReader in = new StringReader("profondeur maximale :;25");
        CSVParser parser = new CSVParser(in, ';');
        long result = instance.readProfondeurMax(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L==result);
        assertFalse(badsFormatsReport.hasErrors());
        verify(requestProperties).setProfondeurMax(25);
        //pas un entier
        in = new StringReader("profondeur maximale :;quatre");
        parser = new CSVParser(in, ';');
        result = instance.readProfondeurMax(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L==result);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4, la profondeur maximum doit être un entier positif. Sa valeur actuelle est quatre.	 </p>", badsFormatsReport.getHTMLMessages());
        //absent
        in = new StringReader("profondeur maximale :");
        badsFormatsReport = new BadsFormatsReport("erreur");
        parser = new CSVParser(in, ';');
        result = instance.readProfondeurMax(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L==result);
        assertFalse(badsFormatsReport.hasErrors());
        verify(requestProperties).setProfondeurMax(0);
    }

    /**
     * Test of readRepetitionMax method, of class VerificateurEnteteClimatSol.
     */
    @Test
    public void testReadRepetitionMax() {
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        StringReader in = new StringReader("répétition maximale :;25");
        CSVParser parser = new CSVParser(in, ';');
        long result = instance.readRepetitionMax(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L==result);
        assertFalse(badsFormatsReport.hasErrors());
        verify(requestProperties).setRepetitionMax(25);
        //pas un entier
        in = new StringReader("répétition maximale :;quatre");
        parser = new CSVParser(in, ';');
        result = instance.readRepetitionMax(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L==result);
        assertTrue(badsFormatsReport.hasErrors());
        assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Ligne 4, le numéro de répétition maximum doit être un entier positif. Sa valeur actuelle est quatre.	 </p>", badsFormatsReport.getHTMLMessages());
        //absent
        in = new StringReader("répétition maximale :");
        badsFormatsReport = new BadsFormatsReport("erreur");
        parser = new CSVParser(in, ';');
        result = instance.readRepetitionMax(m.versionFile, badsFormatsReport, parser, 4L, requestProperties, datasetDescriptor);
        assertTrue(5L==result);
        assertFalse(badsFormatsReport.hasErrors());
        verify(requestProperties).setRepetitionMax(0);
    }
    class Instance extends VerificateurEnteteClimatSol{

        @Override
        protected long readRepetitionMax(VersionFile versionFile, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected long readProfondeurMax(VersionFile versionFile, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected void addExpectedColumn(DatasetDescriptor datasetDescriptor, IRequestPropertiesIntervalValue requestProperties) throws BusinessException {
            
        }

        @Override
        protected long readNomDesColonnes(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected long readValeursMin(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestPropertiesIntervalValue requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected long readValeursMax(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestPropertiesIntervalValue requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+1;
        }

        @Override
        protected void minMaxValues(DatasetDescriptor datasetDescriptor, IRequestPropertiesIntervalValue requestProperties) throws BusinessException {
            
        }

        @Override
        protected long readSite(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties) {
            return lineNumber+1;
        }

        @Override
        protected long readEmptyLine(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber+1;
        }

        @Override
        protected long readFrequence(IRequestProperties requestProperties, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber+1;
        }

        @Override
        protected long readDatatype(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber) {
            return lineNumber+1;
        }

        @Override
        protected long readBeginAndEndDates(VersionFile version, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
            return lineNumber+2;
        }

        @Override
        protected long readCommentaires(BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties) {
            return lineNumber+1;
        }
        
    }
}
