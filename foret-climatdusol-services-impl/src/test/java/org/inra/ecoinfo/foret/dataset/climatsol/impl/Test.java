/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import static junit.framework.TestCase.assertFalse;
import static junit.framework.TestCase.assertTrue;


/**
 *
 * @author ptcherniati
 */
public class Test {

    /**
     *
     */
    @org.junit.Test
    public void test(){
        String PROPERTY_CST_INVALID_BAD_MESURE="-9999";
        Float badFloat = Float.parseFloat(PROPERTY_CST_INVALID_BAD_MESURE);
        Integer badInt = Integer.parseInt(PROPERTY_CST_INVALID_BAD_MESURE);
        Float value1 = -9999.000000000000000000000000000001F;
        assertFalse(badFloat==value1);
        assertTrue(badInt.byteValue()==value1.byteValue());
    }
}
