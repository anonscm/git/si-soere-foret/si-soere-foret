/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.io.StringReader;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ErrorsReport;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.dataset.test.utils.MockUtils;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.After;
import org.junit.AfterClass;
import static org.junit.Assert.*;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Ignore;
import org.junit.Test;
import org.mockito.Mock;
import static org.mockito.Mockito.*;
import org.mockito.MockitoAnnotations;

/**
 *
 * @author tcherniatinsky
 */
public class AbstractProcessRecordClimatSolTest {

    /**
     *
     */
    @BeforeClass
    public static void setUpClass() {
    }

    /**
     *
     */
    @AfterClass
    public static void tearDownClass() {
    }

    MockUtils m = MockUtils.getInstance();
    List<Column> columns = new LinkedList();
    Map<String, ExpectedColumn> columnsMap = new HashMap();
    AbstractProcessRecordClimatSol instance;
    ErrorsReport errorsReport = new ErrorsReport();
    @Mock
    RequestPropertiesCS requestProperties;
    @Mock
    DatasetDescriptorCS datasetDescriptor;
    @Mock
    ExpectedColumn dateColumn;
    @Mock
    ExpectedColumn timeColumn;
    @Mock
    ExpectedColumn variableColumn;
    @Mock
    ExpectedColumn variableqcColumn;
    @Mock
    IProfilClimatSolDAO profilClimatSolDAO;
    Map<Variable, RealNode> realNodesMap = new HashMap();

    /**
     *
     */
    public AbstractProcessRecordClimatSolTest() {
    }

    /**
     *
     * @throws PersistenceException
     */
    @Before
    public void setUp() throws PersistenceException {
        MockitoAnnotations.initMocks(this);
        this.instance = new AbstractProcessRecordClimatSolImpl();
        initInstance(instance);
        Column colonne1 = this.m.column;
        Column colonne2 = this.m.column;
        Column colonne3 = this.m.column;
        Column colonne4 = this.m.column;
        Column colonne5 = this.m.column;
        Column colonne6 = this.m.column;
        Column colonne7 = this.m.column;
        Column colonne8 = this.m.column;
        this.columns.add(colonne1);
        this.columns.add(colonne2);
        this.columns.add(colonne3);
        this.columns.add(colonne4);
        this.columns.add(colonne5);
        this.columns.add(colonne6);
        this.columns.add(colonne7);
        this.columns.add(colonne8);
        when(dateColumn.getName()).thenReturn(Constantes.PROPERTY_CST_DATE_TYPE);
        when(dateColumn.getFormatType()).thenReturn(DateUtil.DD_MM_YYYY);
        when(timeColumn.getName()).thenReturn(Constantes.PROPERTY_CST_TIME_TYPE);
        when(timeColumn.getFormatType()).thenReturn(DateUtil.HH_MM);
        when(variableColumn.isVariable()).thenReturn(true);
        when(variableColumn.getName()).thenReturn("colonne variable");
        when(variableColumn.isVariable()).thenReturn(true);
        when(variableqcColumn.isVariable()).thenReturn(true);
        when(variableqcColumn.getFlagType()).thenReturn(Constantes.PROPERTY_CST_QUALITY_CLASS_TYPE);
        when(m.datatypeVariableUniteDAO.getRealNodesVariables(m.datatypeRealNode)).thenReturn(realNodesMap);
        when(this.datasetDescriptor.getColumns()).thenReturn(this.columns);
        when(datasetDescriptor.getEnTete()).thenReturn(2);
        columnsMap.put("variable", variableColumn);
        columnsMap.put("variableqc", variableqcColumn);
        instance.setVersionFileDAO(m.versionFileDAO);
    }

    private void initInstance(AbstractProcessRecordClimatSol instance) {
        instance.setVariableDAO(this.m.variableDAO);
        instance.setLocalizationManager(MockUtils.localizationManager);
        instance.setDatatypeVariableUniteForetDAO(this.m.datatypeVariableUniteDAO);
        instance.setTraitementDAO(this.m.traitementDAO);
        instance.setProfilClimatSolDAO(profilClimatSolDAO);
    }

    /**
     *
     */
    @After
    public void tearDown() {
    }

    /**
     * Test of processRecord method, of class AbstractProcessRecordClimatSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testProcessRecord() throws Exception {
        StringReader in = new StringReader("");
        CSVParser parser = new CSVParser(in, ';');
        when(requestProperties.getSite()).thenReturn(m.site);
        // nominal
        instance = new InstanceTest(parser, 0);
        initInstance(instance);
        try {
            instance.processRecord(parser, m.versionFile, requestProperties, "utf-8", datasetDescriptor);
            assertTrue(7 == ((InstanceTest) instance).passage);
        } catch (BusinessException e) {
            fail();
        }
        // nominal with no line
        instance = new InstanceTest(parser, InstanceTest.LIGNE_VIDE);
        initInstance(instance);
        try {
            instance.processRecord(parser, m.versionFile, requestProperties, "utf-8", datasetDescriptor);
            assertTrue(7 == ((InstanceTest) instance).passage);
        } catch (BusinessException e) {
            fail();
        }
        // nominal with no line
        instance = new InstanceTest(parser, InstanceTest.ERROR_REPORT_ERROR);
        initInstance(instance);
        try {
            instance.processRecord(parser, m.versionFile, requestProperties, "utf-8", datasetDescriptor);
            fail();
        } catch (BusinessException e) {
            assertTrue(7 == ((InstanceTest) instance).passage);
            assertEquals("-une erreur quelconque\n", e.getMessage());
        }
    }

    /**
     * Test of buildProfil method, of class AbstractProcessRecordClimatSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildProfil() throws Exception {
        Line line = mock(Line.class);
        List<Line> lines = new LinkedList();
        lines.add(line);
        lines.add(line);
        //nominal
        instance = new InstanceSkipBuildProfil(true);
        instance = spy(instance);
        doReturn(Optional.of(m.datatypeRealNode)).when(instance).getDBRealNode(m.datatypeRealNode);
        instance.setVersionFileDAO(m.versionFileDAO);
        when(m.versionFileDAO.merge(m.versionFile)).thenReturn(m.versionFile);
        initInstance(instance);
        try {
            instance.buildProfil(m.datatypeRealNode, m.versionFile, lines, m.site, errorsReport);
            assertTrue(((InstanceSkipBuildProfil) instance).passed);
        } catch (BusinessException e) {
            fail();
        }
        //avec une exception
        lines.add(line);
        lines.add(line);
        instance = new InstanceSkipBuildProfil(false);
        try {
            initInstance(instance);
            instance.setVersionFileDAO(m.versionFileDAO);
            instance = spy(instance);
            doReturn(Optional.of(m.datatypeRealNode)).when(instance).getDBRealNode(m.datatypeRealNode);
            instance.buildProfil(m.datatypeRealNode, m.versionFile, lines, m.site, errorsReport);
            fail();
        } catch (BusinessException e) {
            assertTrue(((InstanceSkipBuildProfil) instance).passed);
            assertEquals("erreur", e.getMessage());
        }
        //traitement erreurs
        instance = new InstanceSkipBuildProfil(null);
        try {
            initInstance(instance);
            instance.setVersionFileDAO(m.versionFileDAO);
            instance = spy(instance);
            doReturn(Optional.of(m.datatypeRealNode)).when(instance).getDBRealNode(m.datatypeRealNode);
            when(line.getOriginalLineNumber()).thenReturn(1L);
            instance.buildProfil(m.datatypeRealNode, m.versionFile, lines, m.site, errorsReport);
            assertTrue(((InstanceSkipBuildProfil) instance).passed);
            fail();
        } catch (BusinessException e) {
            assertEquals("-Une erreur s'est produite lors de l'insertion de la ligne 1 \n", e.getMessage());
        }
        //traitement erreurs doublon
        lines.add(line);
        lines.add(line);
        instance = new InstanceSkipBuildProfil(null);
        try {
            initInstance(instance);
            instance.setVersionFileDAO(m.versionFileDAO);
            instance = spy(instance);
            doReturn(Optional.of(m.datatypeRealNode)).when(instance).getDBRealNode(m.datatypeRealNode);
            when(line.getOriginalLineNumber()).thenReturn(1L);
            when(line.getDate()).thenReturn(m.dateDebut.toLocalDate());
            final LocalTime time = DateUtil.readLocalTimeFromText(DateUtil.HH_MM, "22:30");
            when(line.getTime()).thenReturn(time);
            when(profilClimatSolDAO.getLinePublicationNameDoublon(any(ProfilClimatSol.class), eq(m.dateDebutComplete.toLocalDate()), eq(time), eq(m.site))).thenReturn(new Object[]{1L, 12L});
            instance.buildProfil(m.datatypeRealNode, m.versionFile, lines, m.site, errorsReport);
            assertTrue(((InstanceSkipBuildProfil) instance).passed);
            fail();
        } catch (BusinessException e) {
            assertEquals("-Une erreur s'est produite lors de l'insertion de la ligne 1 \n"
                    + "-Erreur ligne 12 : une mesure existe déjà dans la base à la date 01/01/2012 et l'heure 22:30\n", e.getMessage());
        }
    }

    /**
     * Test of buildVariableHeaderAndSkipHeader method, of class
     * AbstractProcessRecordClimatSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testBuildVariableHeaderAndSkipHeader() throws Exception {
        StringReader in = new StringReader("ligne en tête;\n20/01/2014;12:34;35.2;40.2\n20/01/2014;12:64;;40.2");
        CSVParser parser = new CSVParser(in, ';');
        when(datasetDescriptor.getEnTete()).thenReturn(1);
        realNodesMap.put(m.variable, m.variableRealNode);
        instance.setVariableName(m.VARIABLE_CODE);
        realNodesMap.put(m.variable, m.variableRealNode);
        when(m.variable.getCode()).thenReturn("variablecode");
        Optional<RealNode> result = instance.buildVariableHeaderAndSkipHeader(parser, datasetDescriptor, m.datatypeRealNode);
        assertEquals(Optional.of(m.variableRealNode), result);
        assertTrue(1 == parser.getLastLineNumber());
    }

    /**
     * Test of getNewProfil method, of class AbstractProcessRecordClimatSol.
     */
    @Test
    public void testGetNewProfil_4args() {
        ProfilClimatSol result = instance.getNewProfil(m.versionFile, m.dateDebutComplete.toLocalDate(), m.dateDebutComplete.toLocalTime(), 5L);
        assertNotNull(result);
        assertTrue(result instanceof P);
        assertEquals(m.versionFile, result.getVersionFile());
        assertEquals(m.dateDebutComplete.toLocalDate(), result.getDate());
        assertEquals(m.dateDebutComplete.toLocalTime(), ((P) result).time);
        assertTrue(5L == ((P) result).getNoLigne());
    }

    /**
     * Test of getNewMesure method, of class AbstractProcessRecordClimatSol.
     */
    @Test
    public void testGetNewMesure() {
        MesureClimatSol result = instance.getNewMesure();
        assertNotNull(result);
        assertTrue(result instanceof M);
    }

    /**
     * Test of getNewValeur method, of class AbstractProcessRecordClimatSol.
     */
    @Test
    public void testGetNewValeur() {
        ValeurClimatSol result = instance.getNewValeur(m.variableRealNode, 5L, 12.3F);
        assertNotNull(result);
        assertTrue(result instanceof V);
        assertEquals(m.variableRealNode, result.getRealNode());
        assertTrue(12.3F == result.getValeur());
        assertTrue(5 == ((V) result).getNbreRepetition());
    }

    /**
     * Test of getNewProfil method, of class AbstractProcessRecordClimatSol.
     */
    @Test
    public void testGetNewProfil_0args() {
        ProfilClimatSol result = instance.getNewProfil();
        assertNotNull(result);
        assertTrue(result instanceof P);
    }

    /**
     * Test of createLstVariableValueForlines method, of class
     * AbstractProcessRecordClimatSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    public void testCreateLstVariableValueForlines() throws Exception {
        StringReader in = new StringReader("ligne en tête;\n20/01/2014;12:34;35.2;40.2\n20/01/2014;13:04;;40.2");
        CSVParser parser = new CSVParser(in, ';');
        parser.getLine();
        assertTrue(1l == parser.getLastLineNumber());
        when(requestProperties.getExpectedColumns()).thenReturn(columnsMap);
        when(requestProperties.getColumnNames()).thenReturn(new String[]{"date", "time", "variable", "variableqc"});
        when(variableColumn.getFlagType()).thenReturn(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        when(variableqcColumn.getFlagType()).thenReturn(Constantes.PROPERTY_CST_QUALITY_CLASS_TYPE);
        when(datasetDescriptor.getUndefinedColumn()).thenReturn(2);
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport("erreur");
        List<Line> result;
        try {
            result = instance.createLstVariableValueForlines(requestProperties, datasetDescriptor, parser, m.site, badsFormatsReport);
            assertFalse(badsFormatsReport.hasErrors());
            assertTrue(1 == result.size());
            assertTrue(1 == result.get(0).getLstVariablesValues().size());
            Line l = result.get(0);
            VariableValue vv = result.get(0).getLstVariablesValues().get(0);
            assertEquals("20/01/2014", DateUtil.getUTCDateTextFromLocalDateTime(l.getDate(), DateUtil.DD_MM_YYYY));
            assertEquals("12:34", DateUtil.getUTCDateTextFromLocalDateTime(l.getTime(), DateUtil.HH_MM));
            assertEquals(variableColumn, vv.getColumn());
            assertTrue(35.2F == vv.getValue());
            assertTrue(2L == l.getOriginalLineNumber());
        } catch (BadFormatException e) {
            fail();
        }
        //error float
        in = new StringReader("ligne en tête;\n20/01/2014;12:34;trente-cinq;40.2\n20/01/2014;12:64;;40.2");
        parser = new CSVParser(in, ';');
        parser.getLine();
        assertTrue(1l == parser.getLastLineNumber());
        try {
            result = instance.createLstVariableValueForlines(requestProperties, datasetDescriptor, parser, m.site, badsFormatsReport);
            fail();
        } catch (BadFormatException e) {
            assertTrue(badsFormatsReport.hasErrors());
            assertEquals("<p style='color: red'>erreur :</p><p style='text-indent: 30px'>- Une valeur flottante est attendue à la ligne 2, colonne 3 (colonne variable): la valeur actuelle est \"trente-cinq\" </p>", badsFormatsReport.getHTMLMessages());
        }
    }

    /**
     * Test of addProfil method, of class AbstractProcessRecordClimatSol.
     *
     * @throws java.lang.Exception
     */
    @Test
    @Ignore
    public void testAddProfil() throws Exception {
        SortedSet<Line> ligneEnErreur = new TreeSet();
        Line line = mock(Line.class);
        List<VariableValue> variablevalues = mock(List.class);
        when(line.getLstVariablesValues()).thenReturn(variablevalues);
        instance.addProfil(m.datatypeRealNode, line, m.versionFile, ligneEnErreur, errorsReport);
    }

    /**
     *
     */
    public class P extends ProfilClimatSol<M> {

        private static final long serialVersionUID = 1L;

        /**
         *
         */
        protected LocalTime time;

        private P(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
            super(versionFileDB, date, originalLineNumber);
            this.time = time;
        }

        private P() {
            super();
        }

    }

    /**
     *
     */
    public class M extends MesureClimatSol<P, V> {
    }

    /**
     *
     */
    public class V extends ValeurClimatSol<M> {

        private V(RealNode dbRealNode, Long noRepetition, Float valeur) {
            super(dbRealNode, valeur, noRepetition);
        }

        private V() {
            throw new UnsupportedOperationException("Not supported yet."); //To change body of generated methods, choose Tools | Templates.
        }

    }

    /**
     *
     */
    public class AbstractProcessRecordClimatSolImpl extends AbstractProcessRecordClimatSol {

        @Override
        public P getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
            return new P(versionFileDB, date, time, originalLineNumber);
        }

        @Override
        public M getNewMesure() {
            return new M();
        }

        @Override
        public V getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
            return new V(dbRealNode, noRepetition, valeur);
        }

        @Override
        public P getNewProfil() {
            return new P();
        }
    }

    /**
     *
     */
    public class InstanceSkipBuildProfil extends AbstractProcessRecordClimatSolImpl {

        Boolean b;
        boolean passed = false;

        private InstanceSkipBuildProfil(Boolean b) {
            super();
            this.b = b;
        }

        /**
         *
         * @param line
         * @param versionFileDB
         * @param dbVariable
         * @param ligneEnErreur
         * @param errorsReport
         * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
         * @throws PersistenceException
         */
        @Override
        protected void addProfil(RealNode dbRealNode, Line line, VersionFile versionFileDB, SortedSet ligneEnErreur, ErrorsReport errorsReport) throws BusinessException {
            passed = true;
            if (b == null) {
                ligneEnErreur.add(line);
            } else if (!b) {
                throw new BusinessException("erreur");
            }
        }

    }

    /**
     *
     */
    public class InstanceTest extends AbstractProcessRecordClimatSol {

        /**
         *
         */
        public static final int LIGNE_VIDE = 1_024;

        /**
         *
         */
        public static final int ERROR_REPORT_ERROR = 2_048;
        int bvash = 1;
        int clvvfl = 2;
        int bp = 4;
        int passage = 0;
        BusinessException pe;
        Line ligne;
        CSVParser parser;
        int number = 0;
        List<Line> lignes = new LinkedList();

        /**
         *
         * @param parser
         * @param e
         */
        public InstanceTest(CSVParser parser, BusinessException e) {
            this.pe = e;
            this.parser = parser;
        }

        /**
         *
         * @param parser
         * @param number
         */
        public InstanceTest(CSVParser parser, int number) {
            this.number = number;
            this.parser = parser;
        }

        @Override
        public void buildProfil(RealNode dbRealNode, VersionFile versionFile, List lstLines, SiteForet zoneEtude, ErrorsReport errorsReport) throws BusinessException {
            passage += bp;
            assertTrue(lstLines.size() == (number == LIGNE_VIDE ? 0 : 1));
            if (errorsReport.hasErrors()) {
                throw new BusinessException(errorsReport.getErrorsMessages());
            } else if (pe != null) {
                throw pe;
            } else if (ERROR_REPORT_ERROR == number) {
                errorsReport.addErrorMessage("une erreur quelconque");
            }
        }

        /**
         *
         * @param requestPropertiesCS
         * @param datasetDescriptor
         * @param parser
         * @param site
         * @param badsFormatsReport
         * @return
         * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
         * @throws IOException
         * @throws ParseException
         * @throws BadFormatException
         */
        @Override
        protected List createLstVariableValueForlines(RequestPropertiesCS requestPropertiesCS, DatasetDescriptor datasetDescriptor, CSVParser parser, SiteForet site, BadsFormatsReport badsFormatsReport) throws BusinessException {
            passage += clvvfl;
            return ligneErreur(errorsReport);
        }

        @Override
        protected Optional<RealNode> buildVariableHeaderAndSkipHeader(CSVParser parser, DatasetDescriptorCS datasetDescriptor, RealNode datatypeRealNode) throws BusinessException {
            passage = passage + bvash;
            return Optional.of(m.variableRealNode);
        }

        @Override
        protected ProfilClimatSol getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
            return new P(versionFileDB, date, time, originalLineNumber);
        }

        @Override
        protected MesureClimatSol getNewMesure() {
            return new M();
        }

        @Override
        protected ValeurClimatSol getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
            return new V(dbRealNode, noRepetition, valeur);
        }

        @Override
        protected ProfilClimatSol getNewProfil() {
            return new P();
        }

        private List ligneErreur(ErrorsReport errorsReport) {
            if (number != LIGNE_VIDE) {
                lignes.add(ligne);
            }
            return lignes;
        }
    }

}
