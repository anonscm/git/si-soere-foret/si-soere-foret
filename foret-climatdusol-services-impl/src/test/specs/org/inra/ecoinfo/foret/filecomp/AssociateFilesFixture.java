package org.inra.ecoinfo.foret.filecomp;

import com.fasterxml.jackson.core.JsonProcessingException;
import java.time.format.DateTimeParseException;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.TransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.filecomp.IFileCompDAO;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.foret.ForetTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.mga.business.composite.INodeable;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tcherniatinsky
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ForetTransactionalTestFixtureExecutionListener.class})
public class AssociateFilesFixture extends org.inra.ecoinfo.security.CheckRightsFixture {

    /**
     *
     */
    public AssociateFilesFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     * @param nodePath
     * @param startDate
     * @param endDate
     * @param fileType
     * @param fileName
     * @return
     * @throws BusinessException
     * @throws DateTimeParseException
     * @throws BadExpectedValueException
     * @throws JsonProcessingException
     */
    public boolean associate(String nodePath, String startDate, String endDate, String fileType, String fileName) throws BusinessException, DateTimeParseException, BadExpectedValueException, JsonProcessingException {
            boolean addActivity = false;
            try {
            IFileCompDAO fileCompDAO = (IFileCompDAO) TransactionalTestFixtureExecutionListener.applicationContext.getBean("fileCompDAO");
            FileComp fileComp;
            fileComp = fileCompDAO.getByCode(fileName).orElseThrow(PersistenceException::new);
            addActivity = addActivity(fileComp.getGroupName(), "TREEDATASET", "associate", nodePath, startDate, endDate);
        } catch (PersistenceException e) {
            return addActivity;
        }
        return addActivity;
    }

    /**
     *
     * @return
     */
    @Override
    protected <T extends INodeable> Class<T> getTypeResourceVariable() {
        return (Class<T>) DatatypeVariableUnite.class;
    }
}
