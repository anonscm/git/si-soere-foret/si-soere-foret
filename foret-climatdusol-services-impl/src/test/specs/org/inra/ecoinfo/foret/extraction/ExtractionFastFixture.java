package org.inra.ecoinfo.foret.extraction;

import java.text.ParseException;
import java.time.LocalDateTime;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import java.util.TreeSet;
import java.util.stream.Collectors;
import java.util.stream.Stream;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.foret.ForetTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.foret.extraction.climatsol.jsf.ClimatSolParameterVO;
import org.inra.ecoinfo.foret.extraction.climatsol.ui.flex.vo.DatesFormParamCSVO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.ISiteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.IVariableForetDAO;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author ptcherniati
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class)
@TestExecutionListeners(listeners = {ForetTransactionalTestFixtureExecutionListener.class})
public class ExtractionFastFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    ISiteForetDAO siteForetDAO;
    ILocalizationManager localizationManager;
    IVariableForetDAO variableForetDAO;
    IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO;

    /**
     *
     */
    public ExtractionFastFixture() {
        super();
        MockitoAnnotations.initMocks(this);
        this.siteForetDAO = ForetTransactionalTestFixtureExecutionListener.siteForetDAO;
        this.localizationManager = ForetTransactionalTestFixtureExecutionListener.localizationManager;
        this.variableForetDAO = ForetTransactionalTestFixtureExecutionListener.variableForetDAO;
        this.datatypeVariableUniteForetDAO = ForetTransactionalTestFixtureExecutionListener.datatypeVariableUniteDAO;
    }

    /**
     *
     * @param listeSitesCodes
     * @param variablesListNames
     * @param profondeursListNames
     * @param dateDedebut
     * @param datedeFin
     * @param rythme
     * @param filecomps
     * @param commentaire
     * @param affichage
     * @return
     * @throws BusinessException
     * @throws PersistenceException
     * @throws ParseException
     * @throws BadExpectedValueException
     */
    public String extract(String listeSitesCodes, String variablesListNames, String profondeursListNames, String dateDedebut, String datedeFin, String rythme, String filecomps, String commentaire, String affichage) throws BusinessException, PersistenceException, ParseException, BadExpectedValueException {
        List<SiteForet> sites = Stream.of(listeSitesCodes.split(Constantes.CST_COMMA))
                .map(siteCode->this.siteForetDAO.getByPath(siteCode))
                .filter(s->s.isPresent())
                .map(s->(SiteForet)s.get())
                .collect(Collectors.toList());
        SortedSet<Integer> profondeurs = Stream.of( profondeursListNames.split(Constantes.CST_COMMA))
                .map(p->Integer.valueOf(p))
                .collect(Collectors.toCollection(TreeSet::new ));
        final Map<String, Object> metadatasMap = new HashMap<>();
        metadatasMap.put(SiteForet.class.getSimpleName(), sites);
        List<DatatypeVariableUniteForet> selectedVariables = datatypeVariableUniteForetDAO.getAll(DatatypeVariableUniteForet.class).stream()
                .filter(dvu->variablesListNames.contains(dvu.getVariable().getCode()))
                .distinct()
                .collect(Collectors.toList());
        IntervalDate intervalDate = null;
        if (Constantes.MENSUEL.equals(rythme)) {
            LocalDateTime startDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "01/".concat(dateDedebut));
            LocalDateTime endDate = DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, "01/".concat(datedeFin)).with(TemporalAdjusters.lastDayOfMonth());
            intervalDate = new IntervalDate(startDate, endDate,  DateUtil.MM_YYYY);
        } else {
            intervalDate = new IntervalDate(dateDedebut, datedeFin, DateUtil.DD_MM_YYYY
            );
        }
        metadatasMap.put(IExtractionManager.KEYMAP_COMMENTS, commentaire);
        ClimatSolParameterVO parameters = new ClimatSolParameterVO();
        parameters.getParameters().put(ClimatSolParameterVO.SITE_PARAMETER, sites);
        parameters.getParameters().put(ClimatSolParameterVO.VARIABLE_PARAMETER, selectedVariables);
        parameters.setCommentaire(commentaire);
        parameters.getParameters().put(ClimatSolParameterVO.SITE_PARAMETER, rythme);
        parameters.setAffichage(Integer.parseInt(affichage));
        parameters.getParameters().put(ClimatSolParameterVO.DEPHT_PARAMETER, profondeurs);
        DatesFormParamCSVO datesFormParamVO = new DatesFormParamCSVO(localizationManager);
        datesFormParamVO.setRythme(rythme);
        Map<String, String> period= new HashMap();
        period.put(datesFormParamVO.START_INDEX, dateDedebut);
        period.put(datesFormParamVO.END_INDEX, datedeFin);
        datesFormParamVO.getPeriods().clear();
        datesFormParamVO.getPeriods().add(0, period);
        datesFormParamVO.intervalsDate().add(intervalDate);
        parameters.getParameters().put(ClimatSolParameterVO.DATES_PARAMETER, datesFormParamVO);
        try {
            ForetTransactionalTestFixtureExecutionListener.extractionManager.extract(parameters, Integer.parseInt(affichage));
        } catch (final NumberFormatException | BusinessException e) {
            return "false : " + e.getMessage();
        }
        return "true";

    }
}
