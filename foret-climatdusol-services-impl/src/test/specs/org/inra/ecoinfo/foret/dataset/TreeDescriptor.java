package org.inra.ecoinfo.foret.dataset;

/**
 *
 * @author ptcherniati
 */
public class TreeDescriptor {
    String pathSite;
    String themeCode;
    String datatypeCode;

    /**
     *
     * @param pathSite
     * @param themeCode
     * @param datatypeCode
     */
    public TreeDescriptor(String pathSite, String themeCode, String datatypeCode) {
        super();
        this.pathSite = pathSite;
        this.themeCode = themeCode;
        this.datatypeCode = datatypeCode;
    }

    /**
     *
     * @return
     */
    public String getDatatypeCode() {
        return this.datatypeCode;
    }

    /**
     *
     * @return
     */
    public String getPathSite() {
        return this.pathSite;
    }

    /**
     *
     * @return
     */
    public String getThemeCode() {
        return this.themeCode;
    }
}
