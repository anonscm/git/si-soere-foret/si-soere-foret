package org.inra.ecoinfo.foret.filecomp;

import com.google.common.base.Strings;
import java.io.File;
import java.io.FileInputStream;
import java.io.IOException;
import java.io.InputStream;
import java.nio.file.Files;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.ConcordionSpringJunit4ClassRunner;
import org.inra.ecoinfo.filecomp.entity.FileComp;
import org.inra.ecoinfo.filecomp.entity.FileType;
import org.inra.ecoinfo.foret.ForetTransactionalTestFixtureExecutionListener;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.junit.runner.RunWith;
import org.mockito.MockitoAnnotations;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.TestExecutionListeners;
import org.springframework.transaction.annotation.Transactional;

/**
 *
 * @author tcherniatinsky
 */
@RunWith(ConcordionSpringJunit4ClassRunner.class)
@ContextConfiguration(locations = {"/META-INF/spring/applicationContextTest.xml"})
@Transactional(rollbackFor = Exception.class, readOnly = false, transactionManager = "transactionManager")
@TestExecutionListeners(listeners = {ForetTransactionalTestFixtureExecutionListener.class})
public class LoadFilesFixture extends org.inra.ecoinfo.extraction.AbstractExtractionFixture {

    /**
     *
     */
    public LoadFilesFixture() {
        super();
        MockitoAnnotations.initMocks(this);
    }

    /**
     *
     * @param titleFile
     * @param fileTypeName
     * @param fromValid
     * @param toValid
     * @param comment
     * @param path
     * @return
     * @throws java.io.IOException
     */
    public String loadFile(String titleFile, String fileTypeName, String fromValid, String toValid, String comment, String path) throws IOException {
        FileComp fileComp;
        LocalDate startDate, endDate;
        comment = comment == null ? "defaultComment" : comment;
        try {
            fileComp = loadFile(titleFile, fileTypeName, path);
            startDate = Strings.isNullOrEmpty(fromValid) ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, fromValid).toLocalDate();
            endDate = Strings.isNullOrEmpty(toValid) ? null : DateUtil.readLocalDateTimeFromText(DateUtil.DD_MM_YYYY, toValid).toLocalDate();
            List<String> descriptions = new LinkedList<>();
            descriptions.add(comment);
            fileComp.setDateDebutPeriode(startDate);
            fileComp.setDateFinPeriode(endDate);
            fileComp = ForetTransactionalTestFixtureExecutionListener.fileCompManager.saveFile(fileComp, descriptions);
            
        } catch (PersistenceException | BusinessException | DateTimeException e) {
            return Boolean.FALSE.toString();
        }
        return Boolean.TRUE.toString();
    }

    private FileComp loadFile(String titleFile, String typeFile, String path) throws BusinessException, IOException, PersistenceException {
        String fileName = path.substring(path.lastIndexOf('/') + 1, path.length());
        byte[] byteToCopy;
        File file = new File(path);
        try (InputStream is = new FileInputStream(file)) {
            byteToCopy = new byte[(int) file.length()];
            is.read(byteToCopy);
        } catch (IOException e) {
            throw new BusinessException("impossible de télécharger le fichier", e);
        }
        FileComp fileComp = new FileComp();
        fileComp.setData(byteToCopy);
        fileComp.setSize(byteToCopy.length);
        fileComp.setFileName(fileName);
        fileComp.setContentType(Files.probeContentType(file.toPath()));
        FileType fileType = ForetTransactionalTestFixtureExecutionListener.fileCompManager.getFileTypeFromFileTypeCode(typeFile)
                                        .orElseThrow(()-> new BusinessException("filetype non valide" + typeFile));

        fileComp.setFileType(fileType);
        return fileComp;
    }

    /**
     *
     * @return
     */
    public String checkConforme() {
        return Boolean.TRUE.toString();
    }
}
