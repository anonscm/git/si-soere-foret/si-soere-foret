package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.IOException;
import java.util.regex.Matcher;
import java.util.regex.Pattern;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;
import org.inra.ecoinfo.foret.dataset.ITestHeaders;
import org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS;
import org.inra.ecoinfo.foret.dataset.impl.AbstractVerificateurEntete;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.infocomplstdtvar.IInfoComplementaireStdtVariableDAO;
import org.inra.ecoinfo.foret.refdata.informationcomplementaire.IInformationComplementaireDAO;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author sophie
 *
 */
public class VerificateurEnteteClimatSol extends AbstractVerificateurEntete implements ITestHeaders {

    /**
     *
     */
    protected static final String INTITULE_ABSENT = "INTITULE_ABSENT";

    /**
     *
     */
    protected static final String INCORRECT_INTITULE_VAR_COLUMN_HEADER = "INCORRECT_INTITULE_VAR_COLUMN_HEADER";

    /**
     *
     */
    protected IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO;

    /**
     *
     */
    protected IInformationComplementaireDAO infoComplementaireDAO;

    /**
     *
     */
    protected IInfoComplementaireStdtVariableDAO infoCpltStdtVariableDAO;
    String variableName;

    /**
     * @param infoComplementaireDAO the infoComplementaireDAO to set
     */
    public void setInfoComplementaireDAO(IInformationComplementaireDAO infoComplementaireDAO) {
        this.infoComplementaireDAO = infoComplementaireDAO;
    }

    /**
     * @param infoCpltStdtVariableDAO the infoCpltStdtVariableDAO to set
     */
    public void setInfoCpltStdtVariableDAO(IInfoComplementaireStdtVariableDAO infoCpltStdtVariableDAO) {
        this.infoCpltStdtVariableDAO = infoCpltStdtVariableDAO;
    }

    @Override
    public long testHeaders(CSVParser parser, VersionFile versionFile, IRequestProperties requestProperties, String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor) throws BusinessException {
        long lineNumber = super.testHeaders(parser, versionFile, requestProperties, encoding, badsFormatsReport, datasetDescriptor);
        lineNumber = this.readSite(versionFile, badsFormatsReport, parser, lineNumber, requestProperties);
        lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber);
        lineNumber = this.readFrequence(requestProperties, badsFormatsReport, parser, lineNumber);
        lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readCommentaires(badsFormatsReport, parser, lineNumber, requestProperties);
        lineNumber = this.readProfondeurMax(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readRepetitionMax(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
        lineNumber = this.readNomDesColonnes(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readValeursMin(versionFile, badsFormatsReport, parser, lineNumber, (IRequestPropertiesIntervalValue) requestProperties, datasetDescriptor);
        lineNumber = this.readValeursMax(versionFile, badsFormatsReport, parser, lineNumber, (IRequestPropertiesIntervalValue) requestProperties, datasetDescriptor);
        if (!badsFormatsReport.hasErrors()) {
            this.addExpectedColumn(datasetDescriptor, (IRequestPropertiesIntervalValue) requestProperties);
            this.minMaxValues(datasetDescriptor, (IRequestPropertiesIntervalValue) requestProperties);
        }
        return lineNumber;
    }

    /**
     * @param nomVariable
     * @return a matcher pour le pattern de nom de variable
     */
    public Matcher getMatcher(String nomVariable) {
        return Pattern.compile(this.getVariablePattern(nomVariable)).matcher(nomVariable);
    }

    /**
     * @param nomVariable
     * @return format variable nameVar_profondeur_noRepetition
     */
    public String getVariablePattern(String nomVariable) {
        return String.format(Constantes.PATTERN_VARIABLE_EUROPEAN_NOM, nomVariable);
    }

    /**
     *
     * @param variableName
     */
    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    /**
     * @param datasetDescriptor
     * @return the datatype name with frequency
     */
    @Override
    protected String getDatatypeWithFrequenceCode(DatasetDescriptor datasetDescriptor) {
        return datasetDescriptor.getName();
    }

    /**
     * * defined abstract method of AbstractVerificateurEntete
     *
     **
     * @param values
     * @param lineNumber
     * @param badsFormatsReport (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.foret.dataset.impl.AbstractVerificateurEntete#getMessageFrequenceErreur(java.lang.String[],
     * org.inra.ecoinfo.dataset.BadsFormatsReport, int)
     */
    @Override
    protected void getMessageFrequenceErreur(String[] values, BadsFormatsReport badsFormatsReport, long lineNumber) {
        String message = String.format(FORETRecorder.getForetMessage("BAD_FREQUENCE"), lineNumber, values[1].toLowerCase(), Constantes.FREQUENCE_NAME[3] + ", " + Constantes.FREQUENCE_NAME[1] + ", " + Constantes.FREQUENCE_NAME[2]);
        badsFormatsReport.addException(new BadExpectedValueException(message));
    }

    /**
     * <p>
     * On remplit datatypeVariableUnite de requestProperties</p>
     * <p>
     * On remplit requestProperties.setColumnNames avec les colonnes de
     * l'en-tête et celle du dataset descriptor Pour chaque colonne : </p>
     * <p>
     * On vérifie que le nom de colonne n'est pas vide </p>
     * <ul><li>→ org,inra.ecoinfo.foret.dataset.messages /
     * INCORRECT_INTITULE_VAR_COLUMN_HEADER</li></ul>
     * <br />
     * <p>
     * Pour les colonnes < datasetDescriptor.getUndefinedColumn() le nom doit
     * correspondre à celui du descripteur. <ul><li>→
     * org,inra.ecoinfo.foret.dataset.messages /INTITULE_ABSENT</li></ul>
     * <p>
     * Pour les autres colonnes on vérifie que le pattern est correct :
     * <ul><li>pas le bon pattern ou pas de nom de variable ou nom de variable
     * incorrect→ org,inra.ecoinfo.foret.dataset.messages
     * /INCORRECT_INTITULE_VAR_COLUMN_HEADER</li>
     * <li>profondeur non entière→ org,inra.ecoinfo.foret.dataset.messages
     * /BAD_TYPE_PROFONDEUR</li>
     * <li>répétition non entière→ org,inra.ecoinfo.foret.dataset.messages
     * /BAD_TYPE_NUMERO_REPETITION</li>
     * <li>profondeur hors limite→ org,inra.ecoinfo.foret.dataset.messages
     * /PROFONDEUR_MAX</li>
     * <li>répétition hors limite→ org,inra.ecoinfo.foret.dataset.messages
     * /NUMERO_REPETITION_MAX_MAX</li></ul>
     * <p>
     * <ul><li>fin de fichier → org,inra.ecoinfo.foret.dataset.messages
     * /PROPERTY_MSG_BAD_FILE_LENGTH</li></ul></p>
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.foret.dataset.impl.IVerificateurEntete#verifierNomDesColonnes(java.lang.String[],
     * int, org.inra.ecoinfo.dataset.BadsFormatsReport,
     * org.inra.ecoinfo.dataset.DatasetDescriptor)
     *
     *
     * @param version
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @param requestProperties
     * @param datasetDescriptor
     * @return new line number
     */
    @Override
    protected long readNomDesColonnes(final VersionFile version, final BadsFormatsReport badsFormatsReport, final CSVParser parser, final long lineNumber, final IRequestProperties requestProperties, final DatasetDescriptor datasetDescriptor) {
        long finalLineNumber = lineNumber;
        try {
            this.remplirVariablesTypeDonnees(datasetDescriptor, requestProperties);
            String[] columnNames = parser.getLine();
            requestProperties.setColumnNames(columnNames, datasetDescriptor.getColumns());
            finalLineNumber++;
            Matcher matcher;
            int index = 0;
            String value;
            int noRepetitionMax = ((IRequestPropertiesCS) requestProperties).getRepetitionMax();
            int profondeurMax = ((IRequestPropertiesCS) requestProperties).getProfondeurMax();
            for (index = 0; index < columnNames.length; index++) {
                value = columnNames[index];
                if (value == null) {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_INTITULE_VAR_COLUMN_HEADER"), lineNumber, index + 1, value, this.variableName)));
                    break;
                }
                if (index < datasetDescriptor.getUndefinedColumn()) {
                    if (!datasetDescriptor.getColumns().get(index).getName().equals(value)) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INTITULE_ABSENT"), lineNumber, index + 1, value, datasetDescriptor.getColumns().get(index)
                                .getName())));
                    }
                    continue;
                }
                matcher = null;
                int noRepetition = 1;
                int profondeur = 0;
                matcher = this.getMatcher(value);
                if (!matcher.matches()) {
                    if (value.split(Constantes.CST_UNDERSCORE).length < 3) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_INTITULE_VAR_COLUMN_HEADER"), lineNumber, index + 1, value, this.variableName)));
                        continue;
                    } else if (value.split(Constantes.CST_UNDERSCORE)[0].isEmpty() || value.split(Constantes.CST_UNDERSCORE)[1].isEmpty() || value.split(Constantes.CST_UNDERSCORE)[2].isEmpty()) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_INTITULE_VAR_COLUMN_HEADER"), lineNumber, index + 1, value, this.variableName)));
                    } else if (!value.split(Constantes.CST_UNDERSCORE)[0].equals(this.variableName)) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_INTITULE_VAR_COLUMN_HEADER") + this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_INTITULE_VAR"), lineNumber, index + 1, value, this.variableName, value.split(Constantes.CST_UNDERSCORE)[0], datasetDescriptor.getName())));
                    } else {
                        try {
                            noRepetition = Math.abs(Integer.parseInt(value.split(Constantes.CST_UNDERSCORE)[1]));
                        } catch (NumberFormatException e1) {
                            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("BAD_TYPE_NUMERO_REPETITION"), lineNumber, index + 1, value, value.split(Constantes.CST_UNDERSCORE)[1])));
                            noRepetition = -1;
                        }
                        try {
                            profondeur = Math.abs(Integer.parseInt(value.split(Constantes.CST_UNDERSCORE)[2]));
                        } catch (NumberFormatException e2) {
                            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("BAD_TYPE_PROFONDEUR"), lineNumber, index + 1, value, value.split(Constantes.CST_UNDERSCORE)[2])));
                            profondeur = -1;
                        }
                    }
                }
                if (noRepetition != -1 && noRepetition > noRepetitionMax) {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("NUMERO_REPETITION_MAX"), lineNumber, index + 1, value, noRepetitionMax)));
                }
                if (profondeur != -1 && profondeur > profondeurMax) {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("PROFONDEUR_MAX"), lineNumber, index + 1, value, profondeurMax)));
                }
                if (!badsFormatsReport.hasErrors()) {
                    ((IRequestPropertiesCS) requestProperties).addExpectedColumn(index, value, profondeur, noRepetition, null, null);
                }
            }
        } catch (BusinessException e) {
            badsFormatsReport.addException(e);
            return finalLineNumber;
        } catch (IOException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("EOF"), lineNumber)));
            return finalLineNumber;
        }
        return lineNumber + 1;
    }

    /**
     * <p>
     * récupere requestProperties.getColumnNames() et pour chacun des nom de
     * colonne après datasetDescriptor.getUndefinedColumn(), appelle la fonction
     * requestProperties.addExpectedColumn(columnNames[i],
     * datasetDescriptor)</p>
     *
     * @param datasetDescriptor
     * @param requestProperties
     * @throws BusinessException
     */
    protected void addExpectedColumn(DatasetDescriptor datasetDescriptor, IRequestPropertiesIntervalValue requestProperties) throws BusinessException {
        final String[] columnNames = requestProperties.getColumnNames();
        for (int i = datasetDescriptor.getUndefinedColumn(); i < columnNames.length; i++) {
            requestProperties.addExpectedColumn(columnNames[i], datasetDescriptor);
        }
    }

    /**
     * <p>
     * Lit la profondeur maximale (0 si absente) et la stocke dans
     * requestProperties<:p>
     * <ul><li>pas un entier→ org,inra.ecoinfo.foret.dataset.messages
     * /BAD_TYPE_PROFONDEUR_MAX</li>
     * <li>fin de fichier → org,inra.ecoinfo.foret.dataset.messages
     * /PROPERTY_MSG_BAD_FILE_LENGTH</li></ul>
     *
     * @param versionFile
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @param requestProperties
     * @param datasetDescriptor
     * @return
     */
    protected long readProfondeurMax(VersionFile versionFile, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
        long finalLineNumber = lineNumber;
        String[] values = null;
        try {
            values = parser.getLine();
            finalLineNumber++;
            int profondeurMax = 0;

            if (values != null && values.length >= 2) {
                profondeurMax = Integer.parseInt(values[1]);
            }
            ((IRequestPropertiesCS) requestProperties).setProfondeurMax(profondeurMax);
            return finalLineNumber;
        } catch (IOException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("EOF"), lineNumber)));
            return finalLineNumber;
        } catch (NumberFormatException e2) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("BAD_TYPE_PROFONDEUR_MAX"), lineNumber, values[1])));
            return finalLineNumber;
        }
    }

    /**
     * <p>
     * Lit la répétition maximale (0 si absente) et la stocke dans
     * requestProperties<:p>
     * <ul><li>pas un entier→ org,inra.ecoinfo.foret.dataset.messages
     * /BAD_TYPE_PROFONDEUR_MAX</li>
     * <li>fin de fichier → org,inra.ecoinfo.foret.dataset.messages
     * /PROPERTY_MSG_BAD_FILE_LENGTH</li></ul>
     *
     * @param versionFile
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @param requestProperties
     * @param datasetDescriptor
     * @return
     */
    protected long readRepetitionMax(VersionFile versionFile, BadsFormatsReport badsFormatsReport, CSVParser parser, long lineNumber, IRequestProperties requestProperties, DatasetDescriptor datasetDescriptor) {
        long finalLineNumber = lineNumber;
        String[] values = null;
        try {
            values = parser.getLine();
            finalLineNumber++;
            int noRepetitionMax = 0;

            if (values != null && values.length >= 2) {
                noRepetitionMax = Integer.parseInt(values[1]);
            }
            ((IRequestPropertiesCS) requestProperties).setRepetitionMax(noRepetitionMax);
            return finalLineNumber;
        } catch (IOException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("EOF"), lineNumber)));
            return finalLineNumber;
        } catch (NumberFormatException e1) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("BAD_TYPE_NUMERO_REPETITION_MAX"), lineNumber, values[1])));
            return finalLineNumber;
        }
    }

}
