package org.inra.ecoinfo.foret.dataset.climatsol;

import javax.persistence.PersistenceException;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;


/**
 * @author sophie
 * @param <P1>
 * @param <M1>
 * 
 */
public interface IMesureClimatSolDAO<P1 extends ProfilClimatSol<?>, M1 extends MesureClimatSol<?, ?>> extends IDAO<M1> {

    /**
     *
     * @param mesureClass
     * @param profil
     * @param profondeur
     * @return
     * @throws PersistenceException
     */
    M1 getByNKey(M1 mesureClass, P1 profil, int profondeur) throws PersistenceException;
}
