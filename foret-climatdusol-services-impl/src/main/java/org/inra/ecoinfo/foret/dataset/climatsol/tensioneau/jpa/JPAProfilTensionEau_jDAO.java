/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_j;


/**
 * @author sophie
 * 
 */
public class JPAProfilTensionEau_jDAO extends JPAProfilClimatSolDAO<ProfilTensionEau_j> {

}
