/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_j;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_m;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_sh;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolDAO;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public class JPAClimatSolGWDDAO implements IClimatSolVariableDAO {

    private IClimatSolDAO<ValeurProfondeurNappe_j> profondeurNappe_jDAO;
    private IClimatSolDAO<ValeurProfondeurNappe_sh> profondeurNappe_shDAO;
    private IClimatSolDAO<ValeurProfondeurNappe_m> profondeurNappe_mDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#extract(java.util.List, java.util.List, java.util.SortedSet, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    
    @Override
    public List<Object[]> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user) throws PersistenceException, BusinessException {
        List<Object[]> lstValeurExtraite = new ArrayList<>();
        switch (frequence) {
            case Constantes.JOURNALIER:
                {
                    List<ValeurProfondeurNappe_j> valeurs = this.profondeurNappe_jDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                    for (ValeurProfondeurNappe_j valeur : valeurs) {
                        Object[] valeurExtraite = new Object[2];
                        valeurExtraite[0] = valeur;
                        valeurExtraite[1] = valeur.getNbreRepetition();
                        lstValeurExtraite.add(valeurExtraite);
                    }       break;
                }
            case Constantes.INFRA_JOURNALIER:
            {
                List<ValeurProfondeurNappe_sh> valeurs = this.profondeurNappe_shDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurProfondeurNappe_sh valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getMesure().getProfil().getHeure();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
            case Constantes.MENSUEL:
            {
                List<ValeurProfondeurNappe_m> valeurs = this.profondeurNappe_mDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurProfondeurNappe_m valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getEcartType();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
        }

        return lstValeurExtraite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesDepths(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Integer> lstDephts = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstDephts = this.profondeurNappe_jDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstDephts = this.profondeurNappe_shDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstDephts = this.profondeurNappe_mDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
        }

        return lstDephts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesVariables(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Variable> getAvailablesVariables(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Variable> lstVariables = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstVariables = this.profondeurNappe_jDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstVariables = this.profondeurNappe_shDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstVariables = this.profondeurNappe_mDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
        }

        return lstVariables;
    }

    /**
     * @param profondeurNappe_jDAO
     *            the profondeurNappe_jDAO to set
     */
    public void setProfondeurNappe_jDAO(IClimatSolDAO<ValeurProfondeurNappe_j> profondeurNappe_jDAO) {
        this.profondeurNappe_jDAO = profondeurNappe_jDAO;
    }

    /**
     * @param profondeurNappe_mDAO
     *            the profondeurNappe_mDAO to set
     */
    public void setProfondeurNappe_mDAO(IClimatSolDAO<ValeurProfondeurNappe_m> profondeurNappe_mDAO) {
        this.profondeurNappe_mDAO = profondeurNappe_mDAO;
    }

    /**
     * @param profondeurNappe_shDAO
     *            the profondeurNappe_shDAO to set
     */
    public void setProfondeurNappe_shDAO(IClimatSolDAO<ValeurProfondeurNappe_sh> profondeurNappe_shDAO) {
        this.profondeurNappe_shDAO = profondeurNappe_shDAO;
    }
}
