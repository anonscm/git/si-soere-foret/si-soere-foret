/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_sh;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolDAO;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public class JPAClimatSolSWCDAO implements IClimatSolVariableDAO {

    private IClimatSolDAO<ValeurHumiditeVol_j> humiditeVol_jDAO;
    private IClimatSolDAO<ValeurHumiditeVol_sh> humiditeVol_shDAO;
    private IClimatSolDAO<ValeurHumiditeVol_m> humiditeVol_mDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#extract(java.util.List, java.util.List, java.util.SortedSet, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    
    @Override
    public List<Object[]> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user) throws PersistenceException, BusinessException {
        List<Object[]> lstValeurExtraite = new ArrayList<>();

        switch (frequence) {
            case Constantes.JOURNALIER:
                {
                    List<ValeurHumiditeVol_j> valeurs = this.humiditeVol_jDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                    for (ValeurHumiditeVol_j valeur : valeurs) {
                        Object[] valeurExtraite = new Object[2];
                        valeurExtraite[0] = valeur;
                        valeurExtraite[1] = valeur.getNbreRepetition();
                        lstValeurExtraite.add(valeurExtraite);
                    }       break;
                }
            case Constantes.INFRA_JOURNALIER:
            {
                List<ValeurHumiditeVol_sh> valeurs = this.humiditeVol_shDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurHumiditeVol_sh valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getMesure().getProfil().getHeure();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
            case Constantes.MENSUEL:
            {
                List<ValeurHumiditeVol_m> valeurs = this.humiditeVol_mDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurHumiditeVol_m valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getEcartType();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
        }

        return lstValeurExtraite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesDepths(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Integer> lstDephts = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstDephts = this.humiditeVol_jDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstDephts = this.humiditeVol_shDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstDephts = this.humiditeVol_mDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
        }

        return lstDephts;
    }

    /**
     * @param frequence
     * @return
     */
    /*
     * public List<Object[]> getAvailablesSites() { List<Object[]> lstSiteForets1 = null; List<Object[]> lstSiteForets = new ArrayList<Object[]>(); lstSiteForets1 = humiditeVol_jDAO.getAvailablesSites(); lstSiteForets.addAll(lstSiteForets1);
     */

    /*
     * lstSiteForets1 = humiditeVol_shDAO.getAvailablesSites(); lstSiteForets.addAll(lstSiteForets1);
     * 
     * lstSiteForets1 = humiditeVol_mDAO.getAvailablesSites(); lstSiteForets.addAll(lstSiteForets1);
     */

    /*
     * return lstSiteForets; }
     */

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesVariables(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */
    @Override
    public List<Variable> getAvailablesVariables(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Variable> lstVariables = null;
        switch (frequence) {
            case Constantes.JOURNALIER:
                lstVariables = this.humiditeVol_jDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstVariables = this.humiditeVol_shDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstVariables = this.humiditeVol_mDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
        }

        return lstVariables;
    }

    /**
     * @param humiditeVol_jDAO
     *            the humiditeVol_jDAO to set
     */
    public void setHumiditeVol_jDAO(IClimatSolDAO<ValeurHumiditeVol_j> humiditeVol_jDAO) {
        this.humiditeVol_jDAO = humiditeVol_jDAO;
    }

    /**
     * @param humiditeVol_mDAO
     *            the humiditeVol_mDAO to set
     */
    public void setHumiditeVol_mDAO(IClimatSolDAO<ValeurHumiditeVol_m> humiditeVol_mDAO) {
        this.humiditeVol_mDAO = humiditeVol_mDAO;
    }

    /**
     * @param humiditeVol_shDAO
     *            the humiditeVol_shDAO to set
     */
    public void setHumiditeVol_shDAO(IClimatSolDAO<ValeurHumiditeVol_sh> humiditeVol_shDAO) {
        this.humiditeVol_shDAO = humiditeVol_shDAO;
    }
}
