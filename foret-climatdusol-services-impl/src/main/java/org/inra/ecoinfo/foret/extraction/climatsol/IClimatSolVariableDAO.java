/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol;

import java.util.List;
import java.util.SortedSet;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public interface IClimatSolVariableDAO {

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    List<Object[]> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user) throws PersistenceException, BusinessException;

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence);

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    List<Variable> getAvailablesVariables(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence);
}
