/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_j;


/**
 * @author sophie
 * 
 */
public class JPAProfondeurNappe_jDAO extends JPAClimatSolDAO<ValeurProfondeurNappe_j> {

    @Override
    protected Class<ValeurProfondeurNappe_j> getValeurClass() {
        return ValeurProfondeurNappe_j.class;
    }
}
