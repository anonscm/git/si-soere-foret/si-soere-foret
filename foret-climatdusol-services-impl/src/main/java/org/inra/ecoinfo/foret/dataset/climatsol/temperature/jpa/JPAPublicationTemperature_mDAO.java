package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_m;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_m;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_m;


/**
 * @author sophie
 * 
 */
public class JPAPublicationTemperature_mDAO extends JPAPublicationClimatSolDAO<ProfilTemperature_m, MesureTemperature_m, ValeurTemperature_m> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurTemperature_m> getValueClass() {
        return ValeurTemperature_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureTemperature_m> getMeasureClass() {
        return MesureTemperature_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilTemperature_m> getProfilClass() {
        return ProfilTemperature_m.class;
    }
}