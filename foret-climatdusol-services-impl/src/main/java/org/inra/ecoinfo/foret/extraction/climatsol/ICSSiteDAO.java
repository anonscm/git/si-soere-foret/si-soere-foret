/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol;

import java.util.List;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public interface ICSSiteDAO {

    /**
     *
     */
    public static final String THEME_ALIAS = "theme";

    /**
     *
     */
    public static final String SITE_FORET_ALIAS = "siteforet";

    /**
     *
     * @param user
     * @return
     */
    List<SiteForet> getAvailablesSitesForUser(IUser user);

    /**
     *
     * @param lstCodeDatatype
     * @param user
     * @return
     */
    List<VariableForet> getVariablesFromDatatypeAndUser(List<String> lstCodeDatatype, IUser user);
}
