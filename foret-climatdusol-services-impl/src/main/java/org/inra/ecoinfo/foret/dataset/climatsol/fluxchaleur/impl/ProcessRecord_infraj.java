/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_infraj extends AbstractProcessRecordClimatSol<ProfilFluxChaleur_sh, MesureFluxChaleur_sh, ValeurFluxChaleur_sh> {

    /**
     *
     */
    public ProcessRecord_infraj() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilFluxChaleur_sh getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilFluxChaleur_sh(versionFileDB, date, time, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureFluxChaleur_sh getNewMesure() {
        return new MesureFluxChaleur_sh();
    }

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurFluxChaleur_sh getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurFluxChaleur_sh(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilFluxChaleur_sh getNewProfil() {
        return new ProfilFluxChaleur_sh();
    }
}
