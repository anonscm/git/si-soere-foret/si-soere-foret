package org.inra.ecoinfo.foret.synthesis.climatsol.mpn;

import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_j;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_j;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_j;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mpnj.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mpnj.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MPN_jSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurProfondeurNappe_j, MesureProfondeurNappe_j, ProfilProfondeurNappe_j> {

    public CS_MPN_jSynthesisDAO() {
        super(ValeurProfondeurNappe_j.class);
    }
}
