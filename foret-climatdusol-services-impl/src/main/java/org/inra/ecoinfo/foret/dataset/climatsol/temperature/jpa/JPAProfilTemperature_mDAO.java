package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_m;


/**
 * @author sophie
 * 
 */
public class JPAProfilTemperature_mDAO extends JPAProfilClimatSolDAO<ProfilTemperature_m> {

}
