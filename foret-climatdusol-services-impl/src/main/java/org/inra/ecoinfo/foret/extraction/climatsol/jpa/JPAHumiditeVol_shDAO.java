package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_sh;


/**
 * @author sophie
 * 
 */
public class JPAHumiditeVol_shDAO extends JPAClimatSolDAO<ValeurHumiditeVol_sh> {

    @Override
    protected Class<ValeurHumiditeVol_sh> getValeurClass() {
        return ValeurHumiditeVol_sh.class;
    }
}
