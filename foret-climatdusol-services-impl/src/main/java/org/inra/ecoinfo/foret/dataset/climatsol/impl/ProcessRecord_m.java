package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.net.URISyntaxException;
import java.text.ParseException;
import java.time.LocalDate;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.Set;
import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ErrorsReport;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.UtilVerifRecorder;
import org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.impl.Recorder_m;
import org.inra.ecoinfo.foret.dataset.impl.AbstractProcessRecordForet;
import org.inra.ecoinfo.foret.dataset.impl.CleanerValues;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.xml.sax.SAXException;

/**
 * @author sophie
 *
 */
public class ProcessRecord_m extends AbstractProcessRecordForet {

    /**
     *
     */
    protected static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.dataset.messages";

    /**
     *
     */
    protected org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.impl.Recorder_m recorderSwc_m;

    /**
     *
     */
    protected org.inra.ecoinfo.foret.dataset.climatsol.temperature.impl.Recorder_m recorderTp_m;

    /**
     *
     */
    protected org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.impl.Recorder_m recorderG_m;

    /**
     *
     */
    protected org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.impl.Recorder_m recorderSMP_m;

    /**
     *
     */
    protected org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.impl.Recorder_m recorderGWD_m;

    /**
     *
     */
    public ProcessRecord_m() {
        super();
    }

    /**
     *
     * <p>
     * On saute l'en tête du fichier et on renvoie la liste des variables
     * correspondant au nom des colonnes </p>
     *
     * <p>
     * On construit une liste de lignes createLstVariableValueForlines(parser,
     * requestPropertiesCS, zoneEtude, datasetDescriptor)</p>
     *
     * <p>
     * On construit le profil pour chaque ligne en appelant le constructeur de
     * profil correspondant au nom de la colonne</p>
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, IRequestProperties requestProperties, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        ErrorsReport errorsReport = new ErrorsReport();
        Utils.testCastedArguments(versionFile, VersionFile.class, this.localizationManager);
        IRequestPropertiesCS requestPropertiesCS = (IRequestPropertiesCS) requestProperties;
        try {
            List<RealNode> lstNodeVariable = this.buildVariableHeaderAndSkipHeader(parser, (RequestPropertiesCS) requestProperties, datasetDescriptor, versionFile.getDataset().getRealNode());
            SiteForet zoneEtude = requestProperties.getSite();
            LinkedList<Line> lstLines = this.createLstVariableValueForlines(parser, requestPropertiesCS, zoneEtude, datasetDescriptor);
            this.choiceBuildProfil(lstNodeVariable, versionFile, lstLines, zoneEtude, errorsReport);
            if (errorsReport.hasErrors()) {
                this.LOGGER.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        } catch (IOException | BadFormatException | ParseException e) {
            this.LOGGER.debug(e.getMessage());
            throw new BusinessException(e);
        }
    }

    /**
     * <p>
     * pour chaque ligne de lstLines</p>
     * <ul><li>On enregistre la ligne</li>>
     * <li>On traite les lignes en erreur</li></ul>
     *
     * @param lstNodeVariable
     * @param versionFile
     * @param lstLines
     * @param zoneEtude
     * @param errorsReport
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     * @throws IOException
     * @throws SAXException
     * @throws URISyntaxException
     */
    public void choiceBuildProfil(List<RealNode> lstNodeVariable, VersionFile versionFile, LinkedList<Line> lstLines, SiteForet zoneEtude, ErrorsReport errorsReport) throws BusinessException {
        SortedSet<Line> ligneEnErreur = new TreeSet<>();
        SortedMap<Long, SortedSet<String>> lstVarNameEnErreur = new TreeMap<>();
        for (Line line : lstLines) {
            registerLine(line, lstNodeVariable, versionFile, zoneEtude, errorsReport, ligneEnErreur, lstVarNameEnErreur);
        }
        if (!ligneEnErreur.isEmpty()) {
            treatErrorLines(ligneEnErreur, lstVarNameEnErreur, errorsReport);
        }
        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    /**
     *
     * @param line
     * @param lstNodeVariable
     * @param versionFile
     * @param zoneEtude
     * @param errorsReport
     * @param ligneEnErreur
     * @param lstVarNameEnErreur
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    protected void registerLine(Line line, List<RealNode> lstNodeVariable, VersionFile versionFile, SiteForet zoneEtude, ErrorsReport errorsReport, SortedSet<Line> ligneEnErreur, SortedMap<Long, SortedSet<String>> lstVarNameEnErreur) throws BusinessException {
        List<VariableValue> lstVariableValues = line.getLstVariablesValues();
        if (!CollectionUtils.isEmpty(lstVariableValues)) {
            Collections.sort(lstVariableValues, new CompareVariableName());
            List<List<VariableValue>> listTotSouslstVar = new ArrayList<>();
            List<VariableValue> souslstVar = new ArrayList<>();
            for (int i = 0; i < lstVariableValues.size(); i++) {
                VariableValue variableValue = lstVariableValues.get(i);
                String variableName1 = variableValue.getColumn().getName().split(Constantes.CST_UNDERSCORE)[0];

                souslstVar.add(variableValue);

                if (i + 1 < lstVariableValues.size()) {

                    VariableValue variableValue2 = lstVariableValues.get(i + 1);
                    String variableName2 = variableValue2.getColumn().getName().split(Constantes.CST_UNDERSCORE)[0];

                    if (!variableName1.equals(variableName2)) {

                        listTotSouslstVar.add(souslstVar);

                        souslstVar = new ArrayList<>();

                    }
                } else {
                    listTotSouslstVar.add(souslstVar);
                }
            }
            SortedSet<String> setVarName = new TreeSet<>();
            buildSubList(listTotSouslstVar, lstNodeVariable, versionFile, line, zoneEtude, errorsReport, ligneEnErreur, lstVarNameEnErreur, setVarName);
        }
    }

    /**
     *
     * @param ligneEnErreur
     * @param lstVarNameEnErreur
     * @param errorsReport
     */
    protected void treatErrorLines(SortedSet<Line> ligneEnErreur, SortedMap<Long, SortedSet<String>> lstVarNameEnErreur, ErrorsReport errorsReport) {
        for (Line line : ligneEnErreur) {
            try {
                SortedSet<String> lstVarName = lstVarNameEnErreur.get(line.getOriginalLineNumber());
                Iterator<String> itr = lstVarName.iterator();
                while (itr.hasNext()) {
                    String varName = itr.next();
                    errorsReport.addErrorMessage(String.format(Constantes.VARIABLE_MSG + varName + Constantes.VARIABLE_MSG_SUITE + this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "DOUBLON_DATE"), line.getOriginalLineNumber(),
                            DateUtil.getUTCDateTextFromLocalDateTime(line.getDate(), DateUtil.DD_MM_YYYY)));
                }
            } catch (Exception e) {
                this.LOGGER.debug(e.getMessage());
                errorsReport.addErrorMessage(String.format(this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "ERREUR_INSERTION_MESURE"), line.getOriginalLineNumber()));
            }
        }
    }

    /**
     *
     * @param listTotSouslstVar
     * @param lstDbVariable
     * @param versionFile
     * @param line
     * @param zoneEtude
     * @param errorsReport
     * @param ligneEnErreur
     * @param lstVarNameEnErreur
     * @param setVarName
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    protected void buildSubList(List<List<VariableValue>> listTotSouslstVar, List<RealNode> lstNodeVariable, VersionFile versionFile, Line line, SiteForet zoneEtude, ErrorsReport errorsReport, SortedSet<Line> ligneEnErreur, SortedMap<Long, SortedSet<String>> lstVarNameEnErreur, SortedSet<String> setVarName) throws BusinessException {
        for (List<VariableValue> listTotSouslstVar1 : listTotSouslstVar) {
            String varName = listTotSouslstVar1.get(0).getColumn().getName().split(Constantes.CST_UNDERSCORE)[0];
            RealNode dbRealNode = this._searchVariableForetByName(lstNodeVariable, versionFile.getDataset().getRealNode(), varName)
                    .orElseThrow(()->new BusinessException("can't retrieve variable"));
            switch (varName) {
                case Constantes.NAME_VARIABLE_HUMIDITEVOL:
                    try {
                        this.recorderSwc_m.buildProfil(dbRealNode, versionFile, listTotSouslstVar1, varName, line, zoneEtude, errorsReport);
                    } catch (BusinessException e) {
                        ligneEnErreur.add(line);
                        lstVarNameEnErreur.put(line.getOriginalLineNumber(), setVarName);
                        lstVarNameEnErreur.get(line.getOriginalLineNumber()).add(varName);
                    }
                    break;
                case Constantes.NAME_VARIABLE_TEMPERATURE:
                    try {
                        this.recorderTp_m.buildProfil(dbRealNode, versionFile, listTotSouslstVar1, varName, line, zoneEtude, errorsReport);
                    } catch (BusinessException e) {
                        ligneEnErreur.add(line);
                        lstVarNameEnErreur.put(line.getOriginalLineNumber(), setVarName);
                        lstVarNameEnErreur.get(line.getOriginalLineNumber()).add(varName);
                    }
                    break;
                case Constantes.NAME_VARIABLE_FLUXCHALEUR:
                    try {
                        this.recorderG_m.buildProfil(dbRealNode, versionFile, listTotSouslstVar1, varName, line, zoneEtude, errorsReport);
                    } catch (BusinessException e) {
                        ligneEnErreur.add(line);
                        lstVarNameEnErreur.put(line.getOriginalLineNumber(), setVarName);
                        lstVarNameEnErreur.get(line.getOriginalLineNumber()).add(varName);
                    }
                    break;
                case Constantes.NAME_VARIABLE_TENSIONEAU:
                    try {
                        this.recorderSMP_m.buildProfil(dbRealNode, versionFile, listTotSouslstVar1, varName, line, zoneEtude, errorsReport);
                    } catch (BusinessException e) {
                        ligneEnErreur.add(line);
                        lstVarNameEnErreur.put(line.getOriginalLineNumber(), setVarName);
                        lstVarNameEnErreur.get(line.getOriginalLineNumber()).add(varName);
                    }
                    break;
                case Constantes.NAME_VARIABLE_PROFONDEURNAPPE:
                    try {
                        this.recorderGWD_m.buildProfil(dbRealNode, versionFile, listTotSouslstVar1, varName, line, zoneEtude, errorsReport);
                    } catch (BusinessException e) {
                        ligneEnErreur.add(line);
                        lstVarNameEnErreur.put(line.getOriginalLineNumber(), setVarName);
                        lstVarNameEnErreur.get(line.getOriginalLineNumber()).add(varName);
                    }
                    break;
            }
        }
    }

    /**
     *
     * @param recorder_m
     */
    public void setRecorderSwc_m(Recorder_m recorder_m) {
        this.recorderSwc_m = recorder_m;
    }

    /**
     *
     * @param recorderTp_m
     */
    public void setRecorderTp_m(org.inra.ecoinfo.foret.dataset.climatsol.temperature.impl.Recorder_m recorderTp_m) {
        this.recorderTp_m = recorderTp_m;
    }

    /**
     *
     * @param recorderG_m
     */
    public void setRecorderG_m(org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.impl.Recorder_m recorderG_m) {
        this.recorderG_m = recorderG_m;
    }

    /**
     *
     * @param recorderSMP_m
     */
    public void setRecorderSMP_m(org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.impl.Recorder_m recorderSMP_m) {
        this.recorderSMP_m = recorderSMP_m;
    }

    /**
     *
     * @param recorderGWD_m
     */
    public void setRecorderGWD_m(org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.impl.Recorder_m recorderGWD_m) {
        this.recorderGWD_m = recorderGWD_m;
    }

    /**
     * <p>
     * pour chaque ligne de valeurs </p>
     * <ul><li>On lit la date</li>
     * <li>On lit les colonnes de variables par groupes de 3</li>
     * <ul>
     * <li>On construit ke variableValue pour ce groupq si la première colonne
     * est bien une colonne varaible</li>
     * </ul></ul>
     *
     * @param parser
     * @param requestPropertiesCS
     * @param zoneEtude
     * @param datasetDescriptor
     * @return
     * @throws IOException
     * @throws BadFormatException
     * @throws ParseException
     */
    protected LinkedList<Line> createLstVariableValueForlines(CSVParser parser, IRequestPropertiesCS requestPropertiesCS, SiteForet zoneEtude, DatasetDescriptor datasetDescriptor) throws IOException, BadFormatException, ParseException {
        String[] values = null;
        long lineNumber = parser.getLastLineNumber();
        LinkedList<Line> lstLines = new LinkedList<>();
        BadsFormatsReport badsFormatsReport = new BadsFormatsReport(FORETRecorder.getForetMessage(FORETRecorder.PROPERTY_MSG_ERROR_BAD_FORMAT));
        while ((values = parser.getLine()) != null) {
            lineNumber++;
            LocalDate date = null;
            List<VariableValue> lstVariablesValues = new LinkedList<>();
            CleanerValues cleanerValues = new CleanerValues(values);
            if (!badsFormatsReport.hasErrors()) {
                final String dateString = cleanerValues.nextToken().toLowerCase().trim();
                date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, "01/".concat(dateString));
                ExpectedColumn column;
                for (int columnNumber = datasetDescriptor.getUndefinedColumn(); columnNumber < requestPropertiesCS.getExpectedColumns().size() * 3; columnNumber += 3) {
                    buildVariableValueForColumnGroup(cleanerValues, requestPropertiesCS, columnNumber + 1, datasetDescriptor, lineNumber, badsFormatsReport, lstVariablesValues);
                }
                Line line = new Line(date, null, lineNumber, zoneEtude, lstVariablesValues);
                lstLines.add(line);
            }
        }
        if (badsFormatsReport.hasErrors()) {
            throw new BadFormatException(badsFormatsReport);
        }
        return lstLines;
    }

    /**
     * <p>
     * <Pour chaque groupe</p> <ul><li>On lit la valeur (colonne 1</li>
     * <li>On lit l'écart type (colonne 2 (ou 0 si vide)</li>
     * <li>On lit le nombre de répétitions (ou 0 si vide)</li>
     * </ul>
     *
     * @param cleanerValues
     * @param requestPropertiesCS
     * @param columnNumber
     * @param datasetDescriptor
     * @param lineNumber
     * @param badsFormatsReport
     * @param lstVariablesValues
     * @throws NumberFormatException
     */
    protected void buildVariableValueForColumnGroup(CleanerValues cleanerValues, IRequestPropertiesCS requestPropertiesCS, int columnNumber, DatasetDescriptor datasetDescriptor, long lineNumber, BadsFormatsReport badsFormatsReport, List<VariableValue> lstVariablesValues) throws NumberFormatException {
        ExpectedColumn column;
        String value;
        value = cleanerValues.nextToken();
        column = (ExpectedColumn) requestPropertiesCS.getColumn(columnNumber - 1);
        if (column == null || !Constantes.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())) {
            return;
        }
        VariableValue variableValue = null;
        UtilVerifRecorder.isValueFormatFloat(lineNumber, ++columnNumber, value, column.getName(), badsFormatsReport);
        if (!badsFormatsReport.hasErrors()) {
            variableValue = new VariableValue(column, new Float(value));
            lstVariablesValues.add(variableValue);
        }
        value = cleanerValues.nextToken();
        readStandardDeviation(column, value, lineNumber, ++columnNumber, badsFormatsReport, variableValue);
        value = cleanerValues.nextToken();
        readRepetitionNumber(column, value, lineNumber, ++columnNumber, badsFormatsReport, variableValue);
    }

    /**
     * <p>
     * read repetition Number from value and set it in variableValue</p>
     * <p>
     * put 0 on error</p>
     *
     * @param column
     * @param lineNumber
     * @param columnNumber
     * @param value
     * @param badsFormatsReport
     * @param variableValue
     * @throws NumberFormatException
     */
    protected void readRepetitionNumber(ExpectedColumn column, String value, long lineNumber, int columnNumber, BadsFormatsReport badsFormatsReport, VariableValue variableValue) throws NumberFormatException {
        if (column.hasNombreRepetition()) {
            if (!Strings.isNullOrEmpty(value)) {
                UtilVerifRecorder.isValueFormatInteger(lineNumber, columnNumber, value, Constantes.NAME_COLUMN_NBREREP, badsFormatsReport);
                if (!badsFormatsReport.hasErrors()) {
                    variableValue.setNombreRepetition(Integer.parseInt(value));
                }
            } else if (!badsFormatsReport.hasErrors()) {
                variableValue.setNombreRepetition(0);
            }
        }
    }

    /**
     * <p>
     * read standard deviation from value and set it in variableValue</p>
     * <p>
     * put 0 on error</p>
     *
     * @param column
     * @param value
     * @param lineNumber
     * @param columnNumber
     * @param badsFormatsReport
     * @param variableValue
     * @throws NumberFormatException
     */
    protected void readStandardDeviation(ExpectedColumn column, String value, long lineNumber, int columnNumber, BadsFormatsReport badsFormatsReport, VariableValue variableValue) throws NumberFormatException {
        if (column.hasEcartType()) {
            if (!Strings.isNullOrEmpty(value)) {
                UtilVerifRecorder.isValueFormatFloat(lineNumber, columnNumber, value, Constantes.NAME_COLUMN_ECARTTYPE, badsFormatsReport);
                if (!badsFormatsReport.hasErrors()) {
                    variableValue.setEcartType(new Float(value));
                }
            } else if (!badsFormatsReport.hasErrors()) {
                variableValue.setEcartType(new Float(0));
            }
        }
    }

    /**
     * @param lstNodeVariable
     * @param nomVariable
     * @return
     * @throws PersistenceException
     */
    private Optional<RealNode> _searchVariableForetByName(List<RealNode> lstNodeVariable, RealNode datatypeNode, String nomVariable) {
        return lstNodeVariable.stream()
                .filter(rn->rn.getParent().equals(datatypeNode))
                .filter(rn -> ((DatatypeVariableUnite)rn.getNodeable()).getVariable().getCode().equals(nomVariable))
                .findFirst();
    }

    /**
     * <p>
     * jump header lines</p>
     * <p>
     * récupère la listes des colonnes variables et parse leur nom pour obtenir
     * le nom de la variable</p>
     * <p>
     * renvoir la liste des variables</p>
     *
     * @param parser
     * @param requestPropertiesCS
     * @param datasetDescriptor
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     * @throws IOException
     * @throws org.inra.ecoinfo.utils.exceptions.BadFormatException
     */
    protected List<RealNode> buildVariableHeaderAndSkipHeader(CSVParser parser, RequestPropertiesCS requestPropertiesCS, DatasetDescriptor datasetDescriptor, RealNode realNode) throws BusinessException {

        for (int i = 0; i < ((DatasetDescriptorCS) datasetDescriptor).getEnTete(); i++) {
            try {
                parser.getLine();
            } catch (IOException ex) {
                throw new BusinessException("Can't read line");
            }
        }
        Map<String, RealNode> realNodesVariables = this.datatypeVariableUniteForetDAO.getRealNodesVariables(realNode).entrySet()
                .stream()
                .collect(Collectors.toMap(k->k.getKey().getCode(), k->k.getValue()));
        Set<String> expectedColumns = requestPropertiesCS.getExpectedColumns().keySet();
        return expectedColumns.stream()
                .filter(expectedColumn -> realNodesVariables.containsKey(expectedColumn.split(Constantes.CST_UNDERSCORE)[0]))
                .map(expectedColumn -> realNodesVariables.get(expectedColumn.split(Constantes.CST_UNDERSCORE)[0]))
                .collect(Collectors.toList());
    }

    class CompareVariableName implements Comparator<Object> {

        @Override
        public int compare(Object o1, Object o2) {
            VariableValue v1 = (VariableValue) o1;
            VariableValue v2 = (VariableValue) o2;

            String variableName1 = v1.getColumn().getName();
            String variableName2 = v2.getColumn().getName();

            return variableName1.compareToIgnoreCase(variableName2);
        }
    }

}
