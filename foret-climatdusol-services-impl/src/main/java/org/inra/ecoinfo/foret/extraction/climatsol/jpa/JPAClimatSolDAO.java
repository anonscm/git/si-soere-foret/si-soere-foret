/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import java.time.LocalDate;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.SortedSet;
import javax.persistence.FetchType;
import javax.persistence.OneToMany;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.JoinType;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.hibernate.Session;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol_;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet_;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 * @param <V0>
 *
 */
public abstract class JPAClimatSolDAO<V0 extends ValeurClimatSol<?>> extends AbstractJPADAO<V0> implements IClimatSolDAO<V0> {

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @return
     */
    public long getSize(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence) {
        if (lstSelectedSitesForet == null || lstSelectedSitesForet.isEmpty() || lstSelectedVariables == null || lstSelectedVariables.isEmpty() || lstSelectedDepths == null || lstSelectedDepths.isEmpty() || interval == null
                || interval.getBeginDate() == null || interval.getEndDate() == null) {
            return 0l;
        }

        CriteriaQuery<Long> query = builder.createQuery(Long.class);
        Root<V0> v = query.from(getValeurClass());
        Join<V0, MesureClimatSol> m = v.join(ValeurClimatSol_.mesure);
        Join<V0, RealNode> realNodeVariable = v.join(ValeurClimatSol_.realNode);
        Join<RealNode, DatatypeVariableUniteForet> dvu = builder.treat(realNodeVariable.join(RealNode_.nodeable), DatatypeVariableUniteForet.class);
        Join<MesureClimatSol, ProfilClimatSol> p = m.join(MesureClimatSol_.profil);
        Join<RealNode, RealNode> realNodeDatatype = realNodeVariable.join(RealNode_.parent);
        Join<RealNode, RealNode> realNodeTheme = realNodeDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> realNodeSite = realNodeTheme.join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);
        List<Predicate> predicatesAnd = new LinkedList();
        predicatesAnd.add(dvu.in(lstSelectedVariables));
        predicatesAnd.add(m.get(MesureClimatSol_.profondeur).in(lstSelectedDepths));
        predicatesAnd.add(site.in(lstSelectedSitesForet));
        predicatesAnd.add(builder.between(p.<LocalDate>get(ProfilClimatSol_.date), interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()));
        query
                .select(builder.count(v))
                .where(predicatesAnd.toArray(new Predicate[0]));
        return getOptional(query).orElse(0l);
    }

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    @Override
    public List<V0> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user) {
        if (lstSelectedSitesForet == null || lstSelectedSitesForet.isEmpty() || lstSelectedVariables == null || lstSelectedVariables.isEmpty() || lstSelectedDepths == null || lstSelectedDepths.isEmpty() || interval == null
                || interval.getBeginDate() == null || interval.getEndDate() == null) {
            return new LinkedList<>();
        }

        CriteriaQuery<V0> query = builder.createQuery(getValeurClass());
        Root<V0> v = query.from(getValeurClass());
        Join<V0, MesureClimatSol> m = v.join(ValeurClimatSol_.mesure);
        Join<V0, RealNode> realNodeVariable = v.join(ValeurClimatSol_.realNode);
        Join<MesureClimatSol, ProfilClimatSol> p = m.join(MesureClimatSol_.profil);
        Join<RealNode, DatatypeVariableUniteForet> dvu = builder.treat(realNodeVariable.join(RealNode_.nodeable), DatatypeVariableUniteForet.class);
        Join<RealNode, RealNode> realNodeDatatype = realNodeVariable.join(RealNode_.parent);
        Join<RealNode, RealNode> realNodeTheme = realNodeDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> realNodeSite = realNodeTheme.join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);
        List<Predicate> predicatesAnd = new LinkedList();
        if (!user.getIsRoot()) {
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            predicatesAnd.add(builder.equal(nds.get(NodeDataSet_.realNode), realNodeVariable));
            Outils.addRestrictiveRequestOnRoles(user, query, predicatesAnd, builder, nds, p.<LocalDate>get(ProfilClimatSol_.date));
        }
        predicatesAnd.add(dvu.in(lstSelectedVariables));
        predicatesAnd.add(m.get(MesureClimatSol_.profondeur).in(lstSelectedDepths));
        predicatesAnd.add(site.in(lstSelectedSitesForet));
        predicatesAnd.add(builder.between(p.<LocalDate>get(ProfilClimatSol_.date), interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate()));
        query
                .select(v)
                .where(predicatesAnd.toArray(new Predicate[0]));
        return getResultList(query);
    }

    @Override
    public void flush() {
        ((Session) entityManager.getDelegate()).flush();
        ((Session) entityManager.getDelegate()).clear();
    }

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @return
     */
    @Override
    public List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval) {
        if (lstSelectedSiteForets == null || lstSelectedSiteForets.isEmpty() || interval == null) {
            return null;
        }
        if (interval.getBeginDate() == null || interval.getEndDate() == null) {
            return null;
        }
        CriteriaQuery<Integer> query = builder.createQuery(Integer.class);
        Root<V0> v = query.from(getValeurClass());
        Join<V0, MesureClimatSol> m = v.join(ValeurClimatSol_.mesure);
        Join<MesureClimatSol, ProfilClimatSol> p = m.join(MesureClimatSol_.profil);
        Join<ProfilClimatSol, VersionFile> version = p.join(ProfilClimatSol_.versionFile);
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Join<Dataset, RealNode> realNodeDatatype = dataset.join(Dataset_.realNode);
        Join<RealNode, RealNode> realNodeTheme = realNodeDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> realNodeSite = realNodeTheme.join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);

        query
                .select(m.get(MesureClimatSol_.profondeur))
                .where(
                        site.in(lstSelectedSiteForets),
                        builder.between(p.<LocalDate>get(ProfilClimatSol_.date), interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate())
                )
                .distinct(true);
        return getResultList(query);
    }

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @return
     */
    @SuppressWarnings("unchecked")
    @Override
    public List<Variable> getAvailablesVariables(List<SiteForet> lstSelectedSiteForets, IntervalDate interval) {
        if (lstSelectedSiteForets == null || lstSelectedSiteForets.isEmpty() || interval == null) {
            return null;
        }
        if (interval.getBeginDate() == null || interval.getEndDate() == null) {
            return null;
        }
        CriteriaQuery<Variable> query = builder.createQuery(Variable.class);
        Root<V0> v = query.from(getValeurClass());
        Join<V0, MesureClimatSol> m = v.join(ValeurClimatSol_.mesure);
        Join<V0, RealNode> realNodeVariable = v.join(ValeurClimatSol_.realNode);
        Root<VariableForet> variable = query.from(VariableForet.class);
        Join<MesureClimatSol, ProfilClimatSol> p = m.join(MesureClimatSol_.profil);
        Join<ProfilClimatSol, VersionFile> version = p.join(ProfilClimatSol_.versionFile);
        Join<VersionFile, Dataset> dataset = version.join(VersionFile_.dataset);
        Join<Dataset, RealNode> realNodeDatatype = dataset.join(Dataset_.realNode);
        Join<RealNode, RealNode> realNodeTheme = realNodeDatatype.join(RealNode_.parent);
        Join<RealNode, RealNode> realNodeSite = realNodeTheme.join(RealNode_.parent);
        Join<RealNode, Nodeable> site = realNodeSite.join(RealNode_.nodeable);

        query
                .select(builder.construct(VariableForet.class, variable.get(VariableForet_.id), variable.get(VariableForet_.code), variable.get(VariableForet_.code), variable.get(VariableForet_.definition)))
                .where(
                        builder.equal(realNodeVariable.get(RealNode_.nodeable), variable),
                        site.in(lstSelectedSiteForets),
                        builder.between(p.<LocalDate>get(ProfilClimatSol_.date), interval.getBeginDate().toLocalDate(), interval.getEndDate().toLocalDate())
                );
        return getResultList(query);
    }

    /**
     * @return String the ValeurClimatSol
     */
    abstract protected Class<V0> getValeurClass();

    private void fetch(Class entity, Root entityRoot) {
        Arrays.stream(entity.getDeclaredFields())
            .filter(field ->
                    field.isAnnotationPresent(OneToMany.class))
            .filter(field ->
                    FetchType.EAGER == field.getAnnotation(OneToMany.class).fetch())
            .forEach(field ->
                    entityRoot.fetch(field.getName(), JoinType.INNER));
    }

}
