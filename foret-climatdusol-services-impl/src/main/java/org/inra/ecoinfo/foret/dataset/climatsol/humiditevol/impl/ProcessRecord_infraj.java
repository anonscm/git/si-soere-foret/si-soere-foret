/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_infraj extends AbstractProcessRecordClimatSol<ProfilHumiditeVol_sh, MesureHumiditeVol_sh, ValeurHumiditeVol_sh> {

    /**
     *
     */
    public ProcessRecord_infraj() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilHumiditeVol_sh getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilHumiditeVol_sh(versionFileDB, date, time, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureHumiditeVol_sh getNewMesure() {
        return new MesureHumiditeVol_sh();
    }

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurHumiditeVol_sh getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurHumiditeVol_sh(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilHumiditeVol_sh getNewProfil() {
        return new ProfilHumiditeVol_sh();
    }

}
