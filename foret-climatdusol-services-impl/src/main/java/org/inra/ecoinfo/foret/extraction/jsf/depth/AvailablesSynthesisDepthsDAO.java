/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.extraction.jsf.depth;

import java.time.LocalDateTime;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.foret.extraction.jsf.date.AbstractDatesFormParam;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.synthesis.AbstractSynthesisValueCS;
import org.inra.ecoinfo.foret.synthesis.AbstractSynthesisValueCS_;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.datatypevariableunite.DatatypeVariableUnite;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue_;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptchernia
 */
public class AvailablesSynthesisDepthsDAO extends AbstractJPADAO<Nodeable> implements IAvailablesSynthesisDepthsDAO {

    Map<String, List<Class<? extends AbstractSynthesisValueCS>>> genericSynthesisValuesClasses = new HashMap<>();

    /**
     *
     */
    public AvailablesSynthesisDepthsDAO() {
    }

    /**
     *
     * @param user
     * @param datesFormParam
     * @param nodeables
     * @param dvus
     * @return
     */
    @Override
    public List<Integer> getDepths(IUser user, AbstractDatesFormParam datesFormParam, List<? extends Nodeable> nodeables, List<DatatypeVariableUniteForet> dvus) {
        if(datesFormParam.intervalsDate().isEmpty()){
            return new LinkedList<>();
        }
        IntervalDate intervalDate = datesFormParam.intervalsDate().get(0);
        String rythme = datesFormParam.getRythme();
        return genericSynthesisValuesClasses.getOrDefault(rythme, new LinkedList<>()).stream()
                .map(genericSynthesisDatatypeClasse -> buildSubquery(nodeables, genericSynthesisDatatypeClasse, intervalDate, dvus, user))
                .flatMap(s->s.stream())
                .distinct()
                .collect(Collectors.toList());

    }

    /**
     *
     * @param nodeables
     * @param genericSynthesisValueClass
     * @param intervalDate
     * @param dvus
     * @param user
     * @return
     */
    public List<Integer> buildSubquery(List<? extends Nodeable> nodeables, Class<? extends AbstractSynthesisValueCS> genericSynthesisValueClass, IntervalDate intervalDate, List<DatatypeVariableUniteForet> dvus, IUser user) {
        CriteriaQuery<Integer> query = builder.createQuery(Integer.class);
        Root<? extends AbstractSynthesisValueCS> genericSynthesisValue = query.from(genericSynthesisValueClass);
        Root<RealNode> realNode = query.from(RealNode.class);
        final Join<RealNode, Nodeable> nodeable = realNode.join(RealNode_.nodeable);
        Join<RealNode, DatatypeVariableUnite> dvu = builder.treat(nodeable, DatatypeVariableUnite.class);
        Path<String> variable = genericSynthesisValue.get(GenericSynthesisValue_.variable);
        Path<Integer> depth = genericSynthesisValue.get(AbstractSynthesisValueCS_.profondeur);
        Path<String> site = genericSynthesisValue.get(AbstractSynthesisValueCS_.site);
        List<Predicate> predicateAnd = new LinkedList();
        predicateAnd.add(dvu.in(dvus));
        predicateAnd.add(builder.equal(dvu.get(Nodeable_.code), variable));
        List<String> siteNames = nodeables.stream()
                .map(n->n.getCode()).
                collect(Collectors.toList());
        predicateAnd.add(site.in(siteNames));
        Path<LocalDateTime> date = genericSynthesisValue.<LocalDateTime>get(GenericSynthesisValue_.date);
        Optional.ofNullable(intervalDate).ifPresent(
                interval -> predicateAnd.add(builder.between(date, interval.getBeginDate(), interval.getEndDate()))
        );
        if (!user.getIsRoot()) {
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            predicateAnd.add(builder.equal(nds.get(NodeDataSet_.realNode), realNode));
            Outils.addRestrictiveRequestOnRoles(user, query, predicateAnd, builder, nds, date);
        }
        query
                .select(depth)
                .where(predicateAnd.toArray(new Predicate[0]))
                .distinct(true);
        return getResultList(query);
    }

    /**
     *
     * @return
     */
    public Map<String, List<Class<? extends AbstractSynthesisValueCS>>> getGenericSynthesisValuesClasses() {
        return genericSynthesisValuesClasses;
    }

    
}
