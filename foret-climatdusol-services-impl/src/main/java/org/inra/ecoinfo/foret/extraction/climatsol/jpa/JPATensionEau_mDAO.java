/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_m;


/**
 * @author sophie
 * 
 */
public class JPATensionEau_mDAO extends JPAClimatSolDAO<ValeurTensionEau_m> {

    @Override
    protected Class<ValeurTensionEau_m> getValeurClass() {
        return ValeurTensionEau_m.class;
    }
}
