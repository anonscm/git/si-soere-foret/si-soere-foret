/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.impl;


import java.time.LocalDate;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_m;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_m;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_m;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class Recorder_m extends org.inra.ecoinfo.foret.dataset.climatsol.impl.Recorder_m<ProfilTensionEau_m, MesureTensionEau_m, ValeurTensionEau_m> {

    /**
     * empty constructor
     */
    public Recorder_m() {

    }

    /**
     * @param profilTensionEau_mDAO
     *            the profilTensionEau_mDAO to set
     */
    public void setProfilTensionEau_mDAO(IProfilClimatSolDAO<ProfilTensionEau_m> profilTensionEau_mDAO) {
        this.profilmDAO = profilTensionEau_mDAO;
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param originalLineNumber
     * @return
     */
    @Override
    public ProfilTensionEau_m getNewProfil(VersionFile versionFileDB, LocalDate date, Long originalLineNumber) {
        return new ProfilTensionEau_m(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @param profondeur
     * @return
     */
    @Override
    public MesureTensionEau_m getNewMesure(int profondeur) {
        return new MesureTensionEau_m(profondeur);
    }

    /**
     *
     * @param dbRealNode
     * @param nbreRepetition
     * @param ecartType
     * @param valeur
     * @return
     */
    @Override
    public ValeurTensionEau_m getNewValeur(RealNode dbRealNode, Long nbreRepetition, Float ecartType, Float valeur) {
        return new ValeurTensionEau_m(dbRealNode, nbreRepetition, ecartType, valeur);
    }

}
