/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_j;


/**
 * @author sophie
 * 
 */
public class JPAProfilTemperature_jDAO extends JPAProfilClimatSolDAO<ProfilTemperature_j> {

}
