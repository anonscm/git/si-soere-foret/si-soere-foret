/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.impl;

/**
 * @author sophie
 * 
 */
public class ColumnValue {

    String columnName;
    String value;

    /**
     * Empty constructor
     */
    public ColumnValue() {

    }

    /**
     * Constructor
     * 
     * @param columnName
     * @param value
     */
    public ColumnValue(String columnName, String value) {
        this.columnName = columnName;
        this.value = value;
    }

    /**
     * @return the columnName
     */
    public String getColumnName() {
        return this.columnName;
    }

    /**
     * @param columnName
     *            the columnName to set
     */
    public void setColumnName(String columnName) {
        this.columnName = columnName;
    }

    /**
     * @return the value
     */
    public String getValue() {
        return this.value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(String value) {
        this.value = value;
    }

}
