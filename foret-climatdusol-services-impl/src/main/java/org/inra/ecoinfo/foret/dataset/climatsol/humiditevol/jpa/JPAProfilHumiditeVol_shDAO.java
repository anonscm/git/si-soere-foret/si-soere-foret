package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAProfilHumiditeVol_shDAO extends JPAProfilClimatSolDAO<ProfilHumiditeVol_sh> {

}
