/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_j;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_j;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_j;

/**
 * @author sophie
 *
 */
public class JPAPublicationProfondeurNappe_jDAO extends JPAPublicationClimatSolDAO<ProfilProfondeurNappe_j, MesureProfondeurNappe_j, ValeurProfondeurNappe_j> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurProfondeurNappe_j> getValueClass() {
        return ValeurProfondeurNappe_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureProfondeurNappe_j> getMeasureClass() {
        return MesureProfondeurNappe_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilProfondeurNappe_j> getProfilClass() {
        return ProfilProfondeurNappe_j.class;
    }

}
