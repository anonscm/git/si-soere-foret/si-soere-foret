/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_sh;


/**
 * @author sophie
 * 
 */
public class JPAPublicationTemperature_shDAO extends JPAPublicationClimatSolDAO<ProfilTemperature_sh, MesureTemperature_sh, ValeurTemperature_sh> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurTemperature_sh> getValueClass() {
        return ValeurTemperature_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureTemperature_sh> getMeasureClass() {
        return MesureTemperature_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilTemperature_sh> getProfilClass() {
        return ProfilTemperature_sh.class;
    }
}