package org.inra.ecoinfo.foret.extraction.climatsol.impl;

import java.io.File;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.time.Instant;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.HashMap;
import java.util.HashSet;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Map.Entry;
import java.util.Set;
import java.util.SortedMap;
import java.util.TreeMap;
import java.util.zip.ZipOutputStream;
import org.inra.ecoinfo.config.ICoreConfiguration;
import org.inra.ecoinfo.extraction.IOutputBuilder;
import org.inra.ecoinfo.extraction.IOutputsBuildersResolver;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.RObuildZipOutputStream;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.filecomp.config.impl.IFileCompConfiguration;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.extraction.climatsol.jsf.ClimatSolParameterVO;
import org.inra.ecoinfo.foret.extraction.entity.IProspecteurUnite;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.foret.utils.LoggerForExtraction;
import org.inra.ecoinfo.identification.entity.Utilisateur;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.AbstractIntegrator;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public abstract class CSOutputsBuildersResolver extends AbstractOutputBuilder implements IOutputsBuildersResolver {

    /**
     *
     */
    public static final String PROPERTY_MSG_NO_RESULT_EXTRACT = "PROPERTY_MSG_NO_RESULT_EXTRACT";
    private static final String PATTERN_FIELD = ";%s";
    private static final String EXTRACTION = Constantes.EXTRACTION;
    private static final String FILE_SEPARATOR = System.getProperty("file.separator");

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.extraction.impl.messages";

    /**
     *
     */
    protected static final String PROPERTY_CST_EXTRACTION = "PROPERTY_CST_EXTRACTION";

    /**
     *
     */
    protected static final String PROPERTY_CST_UNDERSCORE = "PROPERTY_CST_UNDERSCORE";

    /**
     *
     */
    protected static final String PROPERTY_CST_ZIP_SUFFIX = "PROPERTY_CST_ZIP_SUFFIX";

    /**
     *
     */
    protected static final String PROPERTY_CST_HYPHEN = "PROPERTY_CST_HYPHEN";

    /**
     *
     */
    protected static final String PROPERTY_CST_SEPARATOR_FILE = "PROPERTY_CST_SEPARATOR_FILE";

    /**
     *
     */
    protected static final String PROPERTY_CST_FILE_SUFFIX = "PROPERTY_CST_FILE_SUFFIX";

    /**
     *
     */
    protected static final String BAD_PARAMETERS = "Les paramètres en arguments sont erronés.";

    /**
     *
     */
    protected static final String EXTENSION_ZIP = Constantes.EXTENSION_ZIP;

    /**
     *
     */
    protected static final String EXTENSION_CSV = Constantes.EXTENSION_CSV;

    /**
     *
     */
    protected static final String FILENAME_REMINDER = Constantes.FILENAME_REMINDER;

    /**
     *
     */
    protected static final String DATA = Constantes.DATA;

    /**
     *
     */
    protected static final String SEPARATOR_TEXT = Constantes.SEPARATOR_TEXT;

    /**
     *
     */
    protected static final String EXTENSION_TXT = Constantes.EXTENSION_TXT;

    /**
     *
     */
    protected static final String SUFFIX_FILENAME_DEFAULT = Constantes.SUFFIX_FILENAME_DEFAULT;

    /**
     *
     */
    protected static final String CHARACTER_ENCODING_ISO_8859_1 = Constantes.CHARACTER_ENCODING_ISO_8859_1;

    /**
     *
     */
    protected static final String MAP_INDEX_0 = Constantes.MAP_INDEX_0;

    /**
     *
     */
    protected static final String INFRA_JOURNALIER = Constantes.INFRA_JOURNALIER;

    /**
     *
     */
    protected static final String MENSUEL = Constantes.MENSUEL;

    /**
     *
     */
    protected static final String JOURNALIER = Constantes.JOURNALIER;

    /**
     *
     */
    protected static final String CST_SEMICOLON = ";";

    /**
     *
     */
    protected static final String CST_UNDERSCORE = Constantes.CST_UNDERSCORE;
    private IFileCompConfiguration fileCompConfiguration;

    /**
     *
     */
    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    /**
     *
     */
    protected CSRequestReminderOutputBuilder requestReminderOutputBuilder;

    /**
     *
     */
    protected IProspecteurUnite prospecteurUnite;

    /**
     *
     * @param fileCompConfiguration
     */
    public void setFileCompConfiguration(IFileCompConfiguration fileCompConfiguration) {
        this.fileCompConfiguration = fileCompConfiguration;
    }

    /**
     *
     * @param parameters
     * @throws BusinessException
     * @throws NoExtractionResultException
     */
    public void buidOutputFile(IParameter parameters) throws BusinessException, NoExtractionResultException {
        Set<String> datatypeNames = new HashSet<>();
        datatypeNames.add(this.getPrefixFileName());
        Map<String, File> filesMap = this.buildOutputsFiles(datatypeNames, CSOutputsBuildersResolver.SUFFIX_FILENAME_DEFAULT);
        Map<String, PrintStream> outputPrintStreamMap = this.buildOutputPrintStreamMap(filesMap);
        this.buildOutputData(parameters, outputPrintStreamMap);
        /*
         * double temps = (System.currentTimeMillis() - debut)/1000; LOGGER.info("Temps pour construire le fichier csv (en secondes) : " + temps);
         */
        this.closeStreams(outputPrintStreamMap);
        ((DefaultParameter) parameters).getFilesMaps().add(filesMap);
    }

    /**
     *
     * @param parameters
     * @return
     * @throws BusinessException
     * @throws NoExtractionResultException
     */
    @Override
    public RObuildZipOutputStream buildOutput(IParameter parameters) throws BusinessException, NoExtractionResultException {
        RObuildZipOutputStream rObuildZipOutputStream = null;
        NoExtractionResultException noDataToExtract = null;
        try {
            rObuildZipOutputStream = super.buildOutput(parameters);
            try (ZipOutputStream zipOutputStream = rObuildZipOutputStream.getZipOutputStream()) {
                noDataToExtract = null;
                try {
                    this.buidOutputFile(parameters);
                } catch (NoExtractionResultException e) {
                    noDataToExtract = new NoExtractionResultException(this.getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.ERROR));
                }
                this.requestReminderOutputBuilder.buidOutputFile(parameters);
                for (Map<String, File> filesMap : ((DefaultParameter) parameters).getFilesMaps()) {
                    if (filesMap.containsKey(Constantes.CST_REQUEST_REMINDER)) {
                        LoggerForExtraction.logRequest((Utilisateur) policyManager.getCurrentUser(), filesMap.get(Constantes.CST_REQUEST_REMINDER));
                    }
                    AbstractIntegrator.embedInZip(zipOutputStream, filesMap);
                }
                zipOutputStream.flush();
            }
            if (noDataToExtract != null) {
                throw noDataToExtract;
            }
        } catch (IOException e1) {
            throw new BusinessException(e1);

        }
        return rObuildZipOutputStream;
    }

    /**
     *
     * @return
     */
    @Override
    public ILocalizationManager getLocalizationManager() {
        return this.localizationManager;
    }

    /**
     *
     * @param localizationManager
     */
    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }

    /**
     *
     * @param metadatasMap
     * @return
     */
    @Override
    public List<IOutputBuilder> resolveOutputsBuilders(Map<String, Object> metadatasMap) {
        List<IOutputBuilder> outputsBuilders = new LinkedList<>();
        outputsBuilders.add(this.requestReminderOutputBuilder);
        return outputsBuilders;
    }

    /**
     * @param configuration the configuration to set
     */
    @Override
    public void setConfiguration(ICoreConfiguration configuration) {
        this.configuration = configuration;
    }

    /**
     *
     * @param prospecteurUnite
     */
    public void setProspecteurUnite(IProspecteurUnite prospecteurUnite) {
        this.prospecteurUnite = prospecteurUnite;
    }

    /**
     * @param requestReminderOutputBuilder the requestReminderOutputBuilder to
     * set
     */
    public void setRequestReminderOutputBuilder(CSRequestReminderOutputBuilder requestReminderOutputBuilder) {
        this.requestReminderOutputBuilder = requestReminderOutputBuilder;
    }

    /**
     *
     * @param rObuildZipOutputStream
     */
    public void setrObuildZipOutputStream(RObuildZipOutputStream rObuildZipOutputStream) {
        rObuildZipOutputStream = rObuildZipOutputStream;
    }

    /**
     * @param outputHelper
     * @param climatSolParameterVO
     */
    private void _buildBodyJM(BuildOutputHelper outputHelper, ClimatSolParameterVO climatSolParameterVO) {

        SiteForet site;
        LocalDate date;
        for (Entry<SiteForet, SortedMap<LocalDate, List<ColumnValue>>> entrySiteForet : outputHelper.results.entrySet()) {
            this.buildHeader(outputHelper, climatSolParameterVO);
            site = entrySiteForet.getKey();
            /*
            * String localizedAffichageSite; localizedAffichageSite = site.getNom();
             */
            for (Entry<LocalDate, List<ColumnValue>> entryDate : entrySiteForet.getValue().entrySet()) {
                date = entryDate.getKey();
                outputHelper.printStream.println();
                outputHelper.printStream.printf("%s", localizationManager.getLocalName(site));
                outputHelper.printStream.printf(CSOutputsBuildersResolver.PATTERN_FIELD, DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY));
                this._displayValuesByColumn(outputHelper, entryDate.getValue(), climatSolParameterVO);
            }
            outputHelper.printStream.println();
        }
    }

    /**
     * @param outputHelper
     * @param climatSolParameterVO
     */
    private void _buildBodySh(BuildOutputHelper outputHelper, ClimatSolParameterVO climatSolParameterVO) {
        SiteForet site;
        LocalDate date;
        LocalTime heure;
        for (Entry<SiteForet, SortedMap<LocalDate, SortedMap<LocalTime, List<ColumnValue>>>> entrySiteForet : outputHelper.resultsSh.entrySet()) {
            this.buildHeader(outputHelper, climatSolParameterVO);
            site = entrySiteForet.getKey();
            /*
            * String localizedAffichageSite; localizedAffichageSite = site.getNom();
             */
            for (Entry<LocalDate, SortedMap<LocalTime, List<ColumnValue>>> entryDate : entrySiteForet.getValue().entrySet()) {
                date = entryDate.getKey();
                for (Entry<LocalTime, List<ColumnValue>> entryHeure : entryDate.getValue().entrySet()) {
                    heure = entryHeure.getKey();
                    outputHelper.printStream.println();
                    outputHelper.printStream.printf("%s", localizationManager.getLocalName(site));
                    outputHelper.printStream.printf(CSOutputsBuildersResolver.PATTERN_FIELD, DateUtil.getUTCDateTextFromLocalDateTime(date, DateUtil.DD_MM_YYYY));
                    outputHelper.printStream.printf(CSOutputsBuildersResolver.PATTERN_FIELD, DateUtil.getUTCDateTextFromLocalDateTime(heure, DateUtil.HH_MM));
                    this._displayValuesByColumn(outputHelper, entryHeure.getValue(), climatSolParameterVO);
                }
            }
            outputHelper.printStream.println();
        }
    }

    /**
     * @param outputHelper
     * @param entryDateOuHeure
     * @param climatSolParameterVO
     */
    private void _displayValuesByColumn(BuildOutputHelper outputHelper, List<ColumnValue> lstColumnValues, ClimatSolParameterVO climatSolParameterVO) {
        List<String> lstHeaderColumnName = outputHelper.headerColumnName;
        for (String headerColumnName : lstHeaderColumnName) {
            boolean notEmpty = false;
            for (ColumnValue columnValue : lstColumnValues) {
                if (headerColumnName.equals(columnValue.getColumnName())) {
                    if (climatSolParameterVO.getAffichage() == Constantes.EUROPEAN_FORMAT) {
                        this.displayValue(outputHelper, columnValue.getValue());
                    } else if (climatSolParameterVO.getAffichage() == Constantes.CLASSIC_FORMAT) {
                        this.displayValue(outputHelper, columnValue.getValue(), columnValue.getColumnName());
                    }
                    notEmpty = true;
                    break;
                }
            }
            if (!notEmpty) {
                this.displayEmptyValue(outputHelper);
            }
        }
    }

    /**
     *
     * @param parameters
     * @param outputPrintStreamMap
     * @throws NoExtractionResultException
     */
    protected void buildOutputData(IParameter parameters, Map<String, PrintStream> outputPrintStreamMap) throws NoExtractionResultException {
        for (String datatypeName : outputPrintStreamMap.keySet()) {
            BuildOutputHelper buildOutputHelper = new BuildOutputHelper((ClimatSolParameterVO) parameters, outputPrintStreamMap.get(datatypeName));
            this.buildBody(buildOutputHelper, (ClimatSolParameterVO) parameters);
        }
    }

    /**
     *
     * @param filesMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected Map<String, PrintStream> buildOutputPrintStreamMap(Map<String, File> filesMap) throws BusinessException {
        Map<String, PrintStream> outputPrintStreamMap = new HashMap<>();
        try {
            for (String filename : filesMap.keySet()) {
                outputPrintStreamMap.put(filename, new PrintStream(filesMap.get(filename), CSOutputsBuildersResolver.CHARACTER_ENCODING_ISO_8859_1));
            }
        } catch (FileNotFoundException | UnsupportedEncodingException e) {
            this.LOGGER.error(e.getMessage(), e);
            throw new BusinessException(e);
        }
        return outputPrintStreamMap;
    }

    /**
     *
     * @param filesNames
     * @param suffix
     * @return
     */
    @Override
    protected Map<String, File> buildOutputsFiles(Set<String> filesNames, String suffix) {
        Map<String, File> filesMaps = new HashMap<>();
        String extractionPath = this.configuration.getRepositoryURI().concat(CSOutputsBuildersResolver.EXTRACTION).concat(CSOutputsBuildersResolver.FILE_SEPARATOR);
        for (String filename : filesNames) {
            String fileRandomSuffix = Long.toString(Instant.now().getEpochSecond()).concat("-").concat(Double.toString(Math.random() * 1_000_000).substring(0, 6));
            File resultFile = new File(extractionPath.concat(filename).concat(CSOutputsBuildersResolver.SEPARATOR_TEXT).concat(suffix).concat(CSOutputsBuildersResolver.SEPARATOR_TEXT).concat(fileRandomSuffix).concat(this.getExtention()));
            filesMaps.put(String.format(String.format("%s.csv", filename)), resultFile);
        }
        return filesMaps;
    }

    /**
     *
     * @param nomVariable
     * @param frequence
     * @return
     */
    protected String chercherUnite(String nomVariable, String frequence) {
        String dataType = nomVariable.concat(CSOutputsBuildersResolver.CST_UNDERSCORE);
        if (frequence.equals(Constantes.INFRA_JOURNALIER)) {
            dataType = dataType.concat(Constantes.INFRA_JOURNALIER);
        }
        if (frequence.equals(Constantes.MENSUEL)) {
            dataType = Constantes.DATATYPE_CLIMATSOL_M;
        }
        if (frequence.equals(Constantes.JOURNALIER)) {
            dataType = dataType.concat(Constantes.JOURNALIER);
        }
        String unite = this.prospecteurUnite.getCodeUnitePourVariableDatatype(dataType, nomVariable);
        return unite;
    }

    /**
     *
     * @param outputPrintStreamMap
     */
    @Override
    protected void closeStreams(Map<String, PrintStream> outputPrintStreamMap) {
        for (PrintStream printStream : outputPrintStreamMap.values()) {
            printStream.flush();
            printStream.close();
        }
    }

    /**
     *
     * @param outputHelper
     */
    protected abstract void displayEmptyValue(BuildOutputHelper outputHelper);

    /**
     *
     * @param outputHelper
     * @param value
     */
    protected abstract void displayValue(BuildOutputHelper outputHelper, String value);

    /**
     *
     * @param outputHelper
     * @param value
     * @param columValue
     */
    protected abstract void displayValue(BuildOutputHelper outputHelper, String value, String columValue);

    /**
     *
     * @return
     */
    protected abstract String getExtention();

    /**
     *
     * @return
     */
    protected abstract String getPrefixFileName();

    /**
     * @param outputHelper
     * @param climatSolParameterVO
     */
    void buildBody(BuildOutputHelper outputHelper, ClimatSolParameterVO climatSolParameterVO) {
        String frequence = climatSolParameterVO.getRythme();
        switch (frequence) {
            case Constantes.JOURNALIER:
            case Constantes.MENSUEL:
                this._buildBodyJM(outputHelper, climatSolParameterVO);
                break;
            case Constantes.INFRA_JOURNALIER:
                this._buildBodySh(outputHelper, climatSolParameterVO);
                break;
        }
    }

    abstract void buildHeader(BuildOutputHelper outputHelper, ClimatSolParameterVO climatSolParameterVO);

    /**
     *
     */
    protected class BuildOutputHelper {

        ClimatSolParameterVO parameter;
        PrintStream printStream;
        SortedMap<SiteForet, SortedMap<LocalDate, List<ColumnValue>>> results = new TreeMap<>();
        List<String> headerColumnName = new ArrayList<>();
        SortedMap<SiteForet, SortedMap<LocalDate, SortedMap<LocalTime, List<ColumnValue>>>> resultsSh = new TreeMap<>();

        BuildOutputHelper(ClimatSolParameterVO parameter, PrintStream printStream) throws NoExtractionResultException {
            super();
            this.parameter = parameter;
            this.printStream = printStream;
            this.init();
        }

        private void init() throws NoExtractionResultException {
            Map<String, List<Object[]>> lstValeurExtraitesTot = this.parameter.getValeursExtraites();
            String rythme = this.parameter.getRythme();
            Iterator<Entry<String, List<Object[]>>> valeursExtraitesIterator = lstValeurExtraitesTot.entrySet().iterator();
            for (Iterator<Entry<String, List<Object[]>>> iterator = lstValeurExtraitesTot.entrySet().iterator(); iterator.hasNext();) {
                Entry<String, List<Object[]>> lstValeurExtraites = iterator.next();
                for (Iterator<Object[]> iterator1 = lstValeurExtraites.getValue().iterator(); iterator1.hasNext();) {
                    Object[] valeurExtraite = iterator1.next();
                    ValeurClimatSol valeurClimatSol = (ValeurClimatSol) valeurExtraite[0];
                    SiteForet site = valeurClimatSol.getMesure().getProfil().getZoneEtude();
                    LocalDate date = valeurClimatSol.getMesure().getProfil().getDate();
                    LocalTime heure = null;
                    String columnName = null;
                    switch (rythme) {
                        case Constantes.INFRA_JOURNALIER:
                            heure = (LocalTime) valeurExtraite[2];
                            resultsSh
                                    .computeIfAbsent(site, k -> new TreeMap<>())
                                    .computeIfAbsent(date, k -> new TreeMap<>())
                                    .computeIfAbsent(heure, k -> new ArrayList<>());
                            break;
                        case Constantes.JOURNALIER:
                        case Constantes.MENSUEL:
                            results
                                    .computeIfAbsent(site, k -> new TreeMap<>())
                                    .computeIfAbsent(date, k -> new ArrayList<>());
                            break;
                    }
                    switch (rythme) {
                        case Constantes.JOURNALIER:
                        case Constantes.INFRA_JOURNALIER:
                            String noRepetition = valeurExtraite[1].toString();
                            columnName = valeurClimatSol.getVariable().getCode().concat(Constantes.CST_UNDERSCORE).concat(noRepetition).concat(Constantes.CST_UNDERSCORE).concat(Integer.toString(valeurClimatSol.getMesure().getProfondeur()));
                            break;
                        case Constantes.MENSUEL:
                            columnName = valeurClimatSol.getVariable().getCode().concat(Constantes.CST_UNDERSCORE).concat(Integer.toString(valeurClimatSol.getMesure().getProfondeur()));
                            break;
                    }
                    String valueByColumn = null;
                    if (!rythme.equals(Constantes.MENSUEL)) {

                        valueByColumn = valeurClimatSol.getValeur().toString();
                    } else {

                        String nbreRepetition = valeurExtraite[1].toString();
                        String ecartType = valeurExtraite[2].toString();
                        valueByColumn = valeurClimatSol.getValeur().toString().concat(Constantes.CST_UNDERSCORE).concat(ecartType).concat(Constantes.CST_UNDERSCORE).concat(nbreRepetition);

                    }
                    ColumnValue columnValue = new ColumnValue(columnName, valueByColumn);
                    if (!rythme.equals(Constantes.INFRA_JOURNALIER)) {
                        this.results.get(site).get(date).add(columnValue);
                    } else {
                        this.resultsSh.get(site).get(date).get(heure).add(columnValue);
                    }
                    if (!this.headerColumnName.contains(columnName)) {
                        this.headerColumnName.add(columnName);
                    }
                }
            }
            if (!rythme.equals(Constantes.MENSUEL)) {
                Collections.sort(this.headerColumnName, new CompareColumnHeaderJM());
                if (this.resultsSh.isEmpty() && this.results.isEmpty()) {
                    throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH,
                            NoExtractionResultException.ERROR));
                }
            } else {
                Collections.sort(this.headerColumnName, new CompareColumnHeaderM());
                if (this.results.isEmpty()) {
                    throw new NoExtractionResultException(getLocalizationManager().getMessage(NoExtractionResultException.BUNDLE_SOURCE_PATH, NoExtractionResultException.ERROR));
                }
            }
        }
    }

    class CompareColumnHeaderJM implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {

            String variableName1 = o1.split(Constantes.CST_UNDERSCORE)[0];
            String variableName2 = o2.split(Constantes.CST_UNDERSCORE)[0];
            String numeroRepetition1 = o1.split(Constantes.CST_UNDERSCORE)[1];
            String numeroRepetition2 = o2.split(Constantes.CST_UNDERSCORE)[1];
            String profondeur1 = o1.split(Constantes.CST_UNDERSCORE)[2];
            String profondeur2 = o2.split(Constantes.CST_UNDERSCORE)[2];

            int result = variableName1.compareTo(variableName2);
            if (result == 0) {

                result = new Integer(profondeur1) - new Integer(profondeur2);
                if (result == 0) {

                    result = new Integer(numeroRepetition1) - new Integer(numeroRepetition2);
                }
            }

            return result;
        }
    }

    class CompareColumnHeaderM implements Comparator<String> {

        @Override
        public int compare(String o1, String o2) {

            String variableName1 = o1.split(Constantes.CST_UNDERSCORE)[0];
            String variableName2 = o2.split(Constantes.CST_UNDERSCORE)[0];
            String profondeur1 = o1.split(Constantes.CST_UNDERSCORE)[1];
            String profondeur2 = o2.split(Constantes.CST_UNDERSCORE)[1];

            int result = variableName1.compareTo(variableName2);
            if (result == 0) {
                result = new Integer(profondeur1) - new Integer(profondeur2);
            }

            return result;
        }
    }
}
