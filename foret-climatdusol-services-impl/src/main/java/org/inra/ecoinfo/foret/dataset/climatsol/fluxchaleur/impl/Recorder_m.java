/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.impl;


import java.time.LocalDate;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_m;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class Recorder_m extends org.inra.ecoinfo.foret.dataset.climatsol.impl.Recorder_m<ProfilFluxChaleur_m, MesureFluxChaleur_m, ValeurFluxChaleur_m> {

    private static final long serialVersionUID = 1L;

    /**
     * empty constructor
     */
    public Recorder_m() {

    }
    
    /**
     * @param profilFluxChaleur_mDAO
     *            the profilFluxChaleur_mDAO to set
     */
    public void setProfilFluxChaleur_mDAO(IProfilClimatSolDAO<ProfilFluxChaleur_m> profilFluxChaleur_mDAO) {
        this.profilmDAO = profilFluxChaleur_mDAO;
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param originalLineNumber
     * @return
     */
    @Override
    public ProfilFluxChaleur_m getNewProfil(VersionFile versionFileDB, LocalDate date, Long originalLineNumber) {
        return new ProfilFluxChaleur_m(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @param profondeur
     * @return
     */
    @Override
    public MesureFluxChaleur_m getNewMesure(int profondeur) {
        return new MesureFluxChaleur_m();
    }

    /**
     *
     * @param realNode
     * @param nbreRepetition
     * @param ecartType
     * @param valeur
     * @return
     */
    @Override
    public ValeurFluxChaleur_m getNewValeur(RealNode realNode, Long nbreRepetition, Float ecartType, Float valeur) {
        return new ValeurFluxChaleur_m(realNode, nbreRepetition, ecartType, valeur);
    }

}
