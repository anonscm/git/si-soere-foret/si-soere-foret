
package org.inra.ecoinfo.foret.synthesis.climatsol.cmsm;

import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import java.util.function.Function;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSolMensuel;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_m;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_m;
import org.inra.ecoinfo.foret.synthesis.cmsm.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.cmsm.SynthesisValue;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;

/**
 *
 * @author ptchernia
 */
public class ClimatSolSynthesis_mDAO extends AbstractSynthesis<SynthesisValue, SynthesisDatatype> {
    List<Class<? extends ValeurClimatSolMensuel>> valueClasses = new LinkedList();
    
    public ClimatSolSynthesis_mDAO(){
        super();
        valueClasses.add(ValeurFluxChaleur_m.class);
        valueClasses.add(ValeurHumiditeVol_m.class);
        valueClasses.add(ValeurProfondeurNappe_m.class);
        valueClasses.add(ValeurTemperature_m.class);
        valueClasses.add(ValeurTemperature_m.class);
    }

    @Override
    public Stream<SynthesisValue> getSynthesisValue() {
        return valueClasses.stream()
                .map(valueClass->getListMeasures(valueClass))
                .flatMap(Function.identity());

    }

    protected <E extends ValeurClimatSolMensuel> Stream<SynthesisValue> getListMeasures(Class<E> valueClass) {
        CriteriaQuery<SynthesisValue> query = builder.createQuery(synthesisValueClass);
        Root<E> v = query.from(valueClass);
        Join<E, MesureClimatSol> m = v.join(ValeurClimatSol_.mesure);
        Join<MesureClimatSol, ProfilClimatSol> p = m.join(MesureClimatSol_.profil);
        Join<E, RealNode> varRn = v.join(ValeurClimatSol_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurClimatSol_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = p.get(ProfilClimatSol_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Integer> profondeur = m.get(MesureClimatSol_.profondeur);
        Path<Long> nbRepetion = v.get(ValeurClimatSol_.nbreRepetition);
        query.select(
                builder.construct(
                        synthesisValueClass,
                        dateMesure,
                        sitePath,
                        variableCode,
                        profondeur,
                        nbRepetion,
                        builder.avg(valeur)
                )
        )
                .where(
                        builder.gt(valeur, -9999)
                )
                .groupBy(
                        sitePath,
                        variableCode,
                        dateMesure,
                        profondeur,
                        nbRepetion
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(variableCode),
                        builder.asc(dateMesure),
                        builder.asc(profondeur),
                        builder.asc(nbRepetion)
                );
        return getResultAsStream(query);
    }

    
}
