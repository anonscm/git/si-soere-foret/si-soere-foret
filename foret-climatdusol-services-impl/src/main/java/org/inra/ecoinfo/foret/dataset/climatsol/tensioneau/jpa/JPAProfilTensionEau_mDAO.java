/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_m;


/**
 * @author sophie
 * 
 */
public class JPAProfilTensionEau_mDAO extends JPAProfilClimatSolDAO<ProfilTensionEau_m> {

}
