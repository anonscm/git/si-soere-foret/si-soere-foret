package org.inra.ecoinfo.foret.synthesis.climatsol.mfc;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_sh;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mfcsh.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mfcsh.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MFC_shSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurFluxChaleur_sh, MesureFluxChaleur_sh, ProfilFluxChaleur_sh> {

    public CS_MFC_shSynthesisDAO() {
        super(ValeurFluxChaleur_sh.class);
    }
}
