package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAProfilHumiditeVol_mDAO extends JPAProfilClimatSolDAO<ProfilHumiditeVol_m> {

}
