/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_m;


/**
 * @author sophie
 * 
 */
public class JPAHumiditeVol_mDAO extends JPAClimatSolDAO<ValeurHumiditeVol_m> {

    @Override
    protected Class<ValeurHumiditeVol_m> getValeurClass() {
        return ValeurHumiditeVol_m.class;
    }
}
