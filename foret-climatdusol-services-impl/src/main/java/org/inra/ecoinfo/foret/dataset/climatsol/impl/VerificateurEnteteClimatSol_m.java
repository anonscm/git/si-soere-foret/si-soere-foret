package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.util.Iterator;
import java.util.Set;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;
import org.inra.ecoinfo.foret.dataset.UtilVerifRecorder;
import org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS;
import org.inra.ecoinfo.foret.dataset.impl.AbstractVerificateurEntete;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 * @author sophie
 *
 */
public class VerificateurEnteteClimatSol_m extends VerificateurEnteteClimatSol {

    @Override
    public long testHeaders(CSVParser parser, VersionFile versionFile, IRequestProperties requestProperties, String encoding, BadsFormatsReport badsFormatsReport, DatasetDescriptor datasetDescriptor) throws BusinessException {
        long lineNumber = 0;
        lineNumber = this.readSite(versionFile, badsFormatsReport, parser, lineNumber, requestProperties);
        lineNumber = this.readDatatype(badsFormatsReport, parser, lineNumber);
        lineNumber = this.readFrequence(requestProperties, badsFormatsReport, parser, lineNumber);
        lineNumber = this.readBeginAndEndDates(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readCommentaires(badsFormatsReport, parser, lineNumber, requestProperties);
        lineNumber = this.readProfondeurMax(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readEmptyLine(badsFormatsReport, parser, lineNumber);
        lineNumber = this.readNomDesColonnes(versionFile, badsFormatsReport, parser, lineNumber, requestProperties, datasetDescriptor);
        lineNumber = this.readValeursMin(versionFile, badsFormatsReport, parser, lineNumber, ((IRequestPropertiesIntervalValue) requestProperties), datasetDescriptor);
        lineNumber = this.readValeursMax(versionFile, badsFormatsReport, parser, lineNumber, ((IRequestPropertiesIntervalValue) requestProperties), datasetDescriptor);

        if (!badsFormatsReport.hasErrors()) {

            this.addExpectedColumn(datasetDescriptor, (IRequestPropertiesIntervalValue) requestProperties);

            this.minMaxValues(datasetDescriptor, (IRequestPropertiesIntervalValue) requestProperties);
        }

        return lineNumber;
    }

    /**
     *
     * <p>
     * On remplit datatypeVariableUnite de requestProperties</p>
     * <p>
     * OOn remplit requestProperties.setColumnNames avec les colonnes de
     * l'en-tête et celle du dataset descriptor</p>
     * <p>
     * OOn remplit requestProperties.setLstDatasetNameColumns avec les colonnes
     * du dataset descriptor</p>
     * <p>
     * Pour chaque colonne :<ul>
     * <li>On vérifie que le nom de colonne n'est pas vide →
     * org,inra.ecoinfo.foret.dataset.messages / INTITULE_ABSENT</li>
     *
     * <li>Pour les colonnes < datasetDescriptor.getUndefinedColumn() le nom
     * doit correspondre à celui du descripteur. →
     * org,inra.ecoinfo.foret.dataset.messages /INTITULE_ABSENT</li> <li>Pour
     * les autres colonnes (on les lit par groupes de 3) on vérifie que le
     * pattern est correct :<ul> <li>c'est une colonne d'ecart-type ou
     * de n
     * omb
     * re de répétition→ org,inra.ecoinfo.foret.dataset.messages
     * /INCORRECT_COLUMNS_ORDER</li>
     * <li>le pattern est incorrect→ org,inra.ecoinfo.foret.dataset.messages
     * /INCORRECT_INTITULE_VAR_COLUMN_HEADER_FREQ_M</li>
     * <li>On vérifie que le nom de variable est bien dans ceux du type de
     * données→ org,inra.ecoinfo.foret.dataset.messages
     * /INCORRECT_INTITULE_VAR_COLUMN_HEADER_FREQ_M</li>
     * <li>profondeur non entière→ org,inra.ecoinfo.foret.dataset.messages
     * /BAD_TYPE_PROFONDEUR</li>
     * <li>profondeur hors limite→ org,inra.ecoinfo.foret.dataset.messages
     * /PROFONDEUR_MAX</li></ul>
     * <li>On regarde la colonne suivante</li><ul>
     * <li>pas de colonne→ org,inra.ecoinfo.foret.dataset.messages
     * /INCORRECT_COLUMN_SE</li>
     * <li>colonne vide ou ne correspondant pas à un écart-type→
     * org,inra.ecoinfo.foret.dataset.messages
     * /INCORRECT_COLUMNS_ORDER</li></ul>
     * <li>On regarde la colonne suivante<ul>
     * <li>pas de colonne→ org,inra.ecoinfo.foret.dataset.messages
     * /INCORRECT_COLUMN_SE</li>
     * <li>colonne vide ou ne correspondant pas à un nombre de répétition→
     * org,inra.ecoinfo.foret.dataset.messages /INCORRECT_COLUMNS_ORDER</li>
     * <li>On appelle en cas de succès la fonction
     * requestProperties.addExpectedColumn(index, value, profondeur,
     * hasEcartType, hasNombreRepetition, null, null)</li></ul>
     * <li>fin de fichier → org,inra.ecoinfo.foret.dataset.messages
     * /PROPERTY_MSG_BAD_FILE_LENGTH</li></ul>
     *
     * @param version
     * @param badsFormatsReport
     * @param parser
     * @param lineNumber
     * @param requestProperties
     * @param datasetDescriptor
     * @return
     */
    @Override
    protected long readNomDesColonnes(final VersionFile version, final BadsFormatsReport badsFormatsReport, final CSVParser parser, final long lineNumber, final IRequestProperties requestProperties, final DatasetDescriptor datasetDescriptor) {
        long finalLineNumber = lineNumber;
        try {
            this.remplirVariablesTypeDonnees(datasetDescriptor, requestProperties);

            String[] columnNames = parser.getLine();
            requestProperties.setColumnNames(columnNames, datasetDescriptor.getColumns());
            finalLineNumber++;
            int index = 0;
            String nextValue = "";
            String value;
            if (CollectionUtils.isEmpty(requestProperties.getLstDatasetNameColumns())) {
                requestProperties.setLstDatasetNameColumns(UtilVerifRecorder.getLstDatasetNameColumns(datasetDescriptor));
            }
            for (index = 0; index < columnNames.length; index++) {
                value = columnNames[index];

                if (value == null) {
                    String listNames = getListVarialesNamesPossibles(requestProperties.getVariableTypeDonnees().keySet());
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INTITULE_ABSENT"), lineNumber, index + 1, value, listNames)));
                    break;
                }

                if (index < datasetDescriptor.getUndefinedColumn()) {
                    if (!datasetDescriptor.getColumns().get(index).getName().equals(value)) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INTITULE_ABSENT"), lineNumber, index + 1, value, datasetDescriptor.getColumns().get(index)
                                .getName())));
                    }
                    continue;
                }

                /**
                 *
                 */
                int profondeur = 0;

                if (value.split(Constantes.CST_UNDERSCORE).length < 2) {

                    if (value.equals(Constantes.NAME_COLUMN_ECARTTYPE) || value.equals(Constantes.NAME_COLUMN_NBREREP)) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_COLUMNS_ORDER"), lineNumber, index + 1)));
                        break;

                    } else {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_INTITULE_VAR_COLUMN_HEADER_FREQ_M"), lineNumber, index + 1, value)));
                    }
                    index += 2;
                    continue;

                }
                int initialIndex = index;
                this.variableName = value.split(Constantes.CST_UNDERSCORE)[0];

                if (!requestProperties.getVariableTypeDonnees().containsKey(this.variableName)) {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_INTITULE_VAR_COLUMN_HEADER_FREQ_M") + this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_INTITULE_VAR"), lineNumber, index + 1, value,
                            value.split(Constantes.CST_UNDERSCORE)[0], datasetDescriptor.getName())));
                } else {

                    try {
                        profondeur = Math.abs(Integer.parseInt(value.split(Constantes.CST_UNDERSCORE)[1]));
                    } catch (NumberFormatException e2) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("BAD_TYPE_PROFONDEUR"), lineNumber, index + 1, value, value.split(Constantes.CST_UNDERSCORE)[1])));
                        profondeur = -1;
                    }
                    int profondeurMax = ((IRequestPropertiesCS) requestProperties).getProfondeurMax();

                    if (profondeur != -1 && profondeur > profondeurMax) {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("PROFONDEUR_MAX"), lineNumber, index + 1, value, profondeurMax)));
                    }
                }

                boolean hasEcartType;
                boolean hasNombreRepetition;

                if (index + 1 < columnNames.length) {
                    nextValue = columnNames[index + 1];

                    if (!Strings.isNullOrEmpty(nextValue) && nextValue.equals(Constantes.NAME_COLUMN_ECARTTYPE)) {
                        index += 1;
                        hasEcartType = true;
                    } else {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_COLUMNS_ORDER"), lineNumber, index + 2)));
                        break;

                    }
                } else {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(FORETRecorder.getForetMessage("INCORRECT_COLUMN_SE"), lineNumber, index + 2)));
                    break;

                }

                if (index + 1 < columnNames.length) {
                    nextValue = columnNames[index + 1];

                    if (!Strings.isNullOrEmpty(nextValue) && nextValue.equals(Constantes.NAME_COLUMN_NBREREP)) {
                        index += 1;
                        hasNombreRepetition = true;
                    } else {
                        badsFormatsReport.addException(new BadExpectedValueException(String.format(this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_COLUMNS_ORDER"), lineNumber, index + 2)));
                        break;

                    }
                } else {
                    badsFormatsReport.addException(new BadExpectedValueException(String.format(this.localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_COLUMN_NBREREP"), lineNumber, index + 2)));
                    break;

                }

                /**
                 *
                 */
                if (!badsFormatsReport.hasErrors()) {
                    ((IRequestPropertiesCS) requestProperties).addExpectedColumn(initialIndex, value, profondeur, hasEcartType, hasNombreRepetition, null, null);
                }

            }

        } catch (BusinessException e) {
            badsFormatsReport.addException(e);
            return finalLineNumber;
        } catch (IOException e) {
            badsFormatsReport.addException(new BadExpectedValueException(String.format(this.localizationManager.getMessage(AbstractVerificateurEntete.BUNDLE_SOURCE_PATH, "EOF"), lineNumber)));
            return finalLineNumber;
        }
        return lineNumber + 1;
    }

    private String getListVarialesNamesPossibles(Set<String> keySet) {
        StringBuilder names = new StringBuilder();
        String pattern = "";
        String first = "%s";
        String middle = ", %s";
        String last = FORETRecorder.getForetMessage(FORETRecorder.PROPERTY_MSG_LAST_ELEMENT_IN_LIST);
        int i = 0;
        for (Iterator<String> iterator = keySet.iterator(); iterator.hasNext();) {
            String next = iterator.next();
            if (i == 0) {
                pattern = first;
            } else if (i == keySet.size() - 1) {
                pattern = last;
            } else {
                pattern = middle;
            }
            names.append(String.format(pattern, next));
            i++;
        }
        return names.toString();
    }

}
