/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_sh;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolDAO;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public class JPAClimatSolGDAO implements IClimatSolVariableDAO {

    private IClimatSolDAO<ValeurFluxChaleur_j> fluxChaleur_jDAO;
    private IClimatSolDAO<ValeurFluxChaleur_sh> fluxChaleur_shDAO;
    private IClimatSolDAO<ValeurFluxChaleur_m> fluxChaleur_mDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#extract(java.util.List, java.util.List, java.util.SortedSet, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    
    @Override
    public List<Object[]> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user) throws PersistenceException, BusinessException {
        List<Object[]> lstValeurExtraite = new ArrayList<>();
        switch (frequence) {
            case Constantes.JOURNALIER:
                {
                    List<ValeurFluxChaleur_j> valeurs = this.fluxChaleur_jDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                    for (ValeurFluxChaleur_j valeur : valeurs) {
                        Object[] valeurExtraite = new Object[2];
                        valeurExtraite[0] = valeur;
                        valeurExtraite[1] = valeur.getNbreRepetition();
                        lstValeurExtraite.add(valeurExtraite);
                    }       break;
                }
            case Constantes.INFRA_JOURNALIER:
            {
                List<ValeurFluxChaleur_sh> valeurs = this.fluxChaleur_shDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurFluxChaleur_sh valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getMesure().getProfil().getHeure();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
            case Constantes.MENSUEL:
            {
                List<ValeurFluxChaleur_m> valeurs = this.fluxChaleur_mDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurFluxChaleur_m valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getEcartType();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
        }

        return lstValeurExtraite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesDepths(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Integer> lstDephts = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstDephts = this.fluxChaleur_jDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstDephts = this.fluxChaleur_shDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstDephts = this.fluxChaleur_mDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
        }

        return lstDephts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesVariables(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Variable> getAvailablesVariables(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Variable> lstVariables = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstVariables = this.fluxChaleur_jDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstVariables = this.fluxChaleur_shDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstVariables = this.fluxChaleur_mDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
        }

        return lstVariables;
    }

    /**
     * @param fluxChaleur_jDAO
     *            the fluxChaleur_jDAO to set
     */
    public void setFluxChaleur_jDAO(IClimatSolDAO<ValeurFluxChaleur_j> fluxChaleur_jDAO) {
        this.fluxChaleur_jDAO = fluxChaleur_jDAO;
    }

    /**
     * @param fluxChaleur_mDAO
     *            the fluxChaleur_mDAO to set
     */
    public void setFluxChaleur_mDAO(IClimatSolDAO<ValeurFluxChaleur_m> fluxChaleur_mDAO) {
        this.fluxChaleur_mDAO = fluxChaleur_mDAO;
    }

    /**
     * @param fluxChaleur_shDAO
     *            the fluxChaleur_shDAO to set
     */
    public void setFluxChaleur_shDAO(IClimatSolDAO<ValeurFluxChaleur_sh> fluxChaleur_shDAO) {
        this.fluxChaleur_shDAO = fluxChaleur_shDAO;
    }
}
