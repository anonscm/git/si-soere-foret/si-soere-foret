/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.testDuplicates;

import java.util.SortedMap;
import java.util.SortedSet;
import java.util.TreeMap;
import java.util.TreeSet;
import org.inra.ecoinfo.foret.dataset.ITestDuplicates;
import org.inra.ecoinfo.foret.dataset.impl.AbstractTestDuplicate;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;



/**
 * The Class TestDuplicateWsol.
 * <p>
 * implementation for Tillage of {@link ITestDuplicates}
 * 
 * @author Tcherniatinsky Philippe test the existence of duplicates in flux files
 */
public class TestDuplicateCS_m extends AbstractTestDuplicate {


    /**
     * The Constant BUNDLE_SOURCE_PATH @link(String).
     */
    public static final String FORET_DATASET_CS_BUNDLE_NAME = "org.inra.ecoinfo.foret.dataset.climatsol.impl.messages";

    /**
     *
     */
    public static final String PROPERTY_MSG_DOUBLON_LINE_M = "PROPERTY_MSG_DOUBLON_LINE_M";
    /**
     * The Constant serialVersionUID @link(long).
     */
    static final long serialVersionUID = 1L;

    /**
     * The date time line.
     * 
     * @link(SortedMap<String,SortedMap<String,SortedSet<Long>>>).
     */
    SortedMap<String, SortedSet<Long>> line;

    /**
     * Instantiates a new test duplicate flux.
     */
    public TestDuplicateCS_m() {
        this.line = new TreeMap<>();
    }

    /**
     * Adds the line.
     * 
     * @param values
     * @link(String[]) the values
     * @param lineNumber
     *            long the line number {@link String[]} the values
     */
    @Override
    public void addLine(String[] values, long lineNumber) {
        this.addLine(values[0], lineNumber + 1);
    }

    /**
     * Adds the line.
     *
     * @param date
     * @link(String)
     * @link(String)
     * @link(String) the numero
     * @param lineNumber
     *            long the line number
     * @link(String) the date
     * @link(String) the order of the intervention in the day
     * @link(String) the number of the tool in intervention
     */
    protected void addLine(final String date, final long lineNumber) {
        final String key = this.getKey(date);
        if (!this.line.containsKey(key)) {
            SortedSet<Long> lineNumbers = new TreeSet();
            lineNumbers.add(lineNumber);
            this.line.put(key, lineNumbers);
        } else {
            this.errorsReport.addErrorMessage(String.format(FORETRecorder.getForetMessageWithBundle(TestDuplicateCS_m.FORET_DATASET_CS_BUNDLE_NAME, TestDuplicateCS_m.PROPERTY_MSG_DOUBLON_LINE_M), lineNumber, date, this.line.get(key).first().intValue()));
            this.line.get(key).add(lineNumber);
        }

    }
}
