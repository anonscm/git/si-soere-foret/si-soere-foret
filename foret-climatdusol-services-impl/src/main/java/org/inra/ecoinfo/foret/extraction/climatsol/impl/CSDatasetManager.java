package org.inra.ecoinfo.foret.extraction.climatsol.impl;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.LinkedList;
import java.util.List;
import java.util.Set;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.impl.DefaultDatasetManager;
import org.inra.ecoinfo.foret.extraction.climatsol.ICSDatasetManager;
import org.inra.ecoinfo.foret.extraction.climatsol.ICSSiteDAO;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.IVariableForetDAO;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import static org.inra.ecoinfo.foret.utils.Constantes.DATATYPE_CLIMATSOL_M;
import static org.inra.ecoinfo.foret.utils.Constantes.DATATYPE_FREQUENCE_FLUXCHALEUR;
import static org.inra.ecoinfo.foret.utils.Constantes.DATATYPE_FREQUENCE_HUMIDITEVOL;
import static org.inra.ecoinfo.foret.utils.Constantes.DATATYPE_FREQUENCE_PROFONDEURNAPPE;
import static org.inra.ecoinfo.foret.utils.Constantes.DATATYPE_FREQUENCE_TEMPERATURE;
import static org.inra.ecoinfo.foret.utils.Constantes.DATATYPE_FREQUENCE_TENSIONEAU;
import static org.inra.ecoinfo.foret.utils.Constantes.INFRA_JOURNALIER;
import static org.inra.ecoinfo.foret.utils.Constantes.JOURNALIER;
import static org.inra.ecoinfo.foret.utils.Constantes.MENSUEL;
import static org.inra.ecoinfo.foret.utils.Constantes.NAME_VARIABLE_FLUXCHALEUR;
import static org.inra.ecoinfo.foret.utils.Constantes.NAME_VARIABLE_HUMIDITEVOL;
import static org.inra.ecoinfo.foret.utils.Constantes.NAME_VARIABLE_PROFONDEURNAPPE;
import static org.inra.ecoinfo.foret.utils.Constantes.NAME_VARIABLE_TEMPERATURE;
import static org.inra.ecoinfo.foret.utils.Constantes.NAME_VARIABLE_TENSIONEAU;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.sitethemedatatype.ISiteThemeDatatypeDAO;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
public class CSDatasetManager extends DefaultDatasetManager implements ICSDatasetManager {

    private static final String SITE_PRIVILEGE_PATTERN = "%s/%s/*";
    private static final String VARIABLE_PRIVILEGE_PATTERN = "%s/%s/%s/%s";

    private ICSSiteDAO csSiteDAO;
    private IClimatSolVariableDAO SWCDAO;
    private IClimatSolVariableDAO tSDAO;
    private IClimatSolVariableDAO sMPDAO;
    private IClimatSolVariableDAO gDAO;
    private IClimatSolVariableDAO GWDDAO;
    private IVariableForetDAO variableDAO;

    /**
     *
     */
    protected ISiteThemeDatatypeDAO siteThemeDatatypeDAO;

    /**
     *
     */
    protected Logger LOGGER = LoggerFactory.getLogger(this.getClass().getName());

    boolean hasRight = true;

    /**
     *
     */
    public CSDatasetManager() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.ICSDatasetManager#getAvailablesDepths(java.util.List, java.util.List, org.inra.ecoinfo.utils.IntervalDate)
     */
    @Override
    public List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, List<Variable> lstSelectedVariables, IntervalDate interval, String frequence) {
        Set<Integer> lstAvailablesDephts = new TreeSet<>();
        List<Integer> lstDephts;
        for (Variable variable : lstSelectedVariables) {
            addDepthForVariableName(variable, lstSelectedSiteForets, interval, frequence, lstAvailablesDephts, NAME_VARIABLE_HUMIDITEVOL, this.SWCDAO);
            addDepthForVariableName(variable, lstSelectedSiteForets, interval, frequence, lstAvailablesDephts, NAME_VARIABLE_TEMPERATURE, this.tSDAO);
            addDepthForVariableName(variable, lstSelectedSiteForets, interval, frequence, lstAvailablesDephts, NAME_VARIABLE_TENSIONEAU, this.sMPDAO);
            addDepthForVariableName(variable, lstSelectedSiteForets, interval, frequence, lstAvailablesDephts, NAME_VARIABLE_FLUXCHALEUR, this.gDAO);
            addDepthForVariableName(variable, lstSelectedSiteForets, interval, frequence, lstAvailablesDephts, NAME_VARIABLE_PROFONDEURNAPPE, this.GWDDAO);
        }
        return new LinkedList<>(lstAvailablesDephts);
    }

    /**
     *
     * @param variable
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @param lstAvailablesDephts
     * @param variableName
     * @param dao
     */
    protected void addDepthForVariableName(Variable variable, List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence, Set<Integer> lstAvailablesDephts, String variableName, IClimatSolVariableDAO dao) {
        List<Integer> lstDephts;
        if (variable.getCode().equals(variableName)) {
            lstDephts = dao.getAvailablesDepths(lstSelectedSiteForets, interval, frequence);
            for (Integer depth : lstDephts) {
                if (!lstAvailablesDephts.contains(depth)) {
                    lstAvailablesDephts.add(depth);
                }
            }
        }
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.ICSDatasetManager#getAvailablesVariables(java.util.List, org.inra.ecoinfo.utils.IntervalDate)
     */
    @Override
    public List<VariableForet> getAvailablesVariablesForCurrentUser(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) throws PersistenceException {
        return this.getVariablesFromDatatypeForUser(frequence, policyManager.getCurrentUser());
    }

    /**
     * @return the csSiteDAO
     */
    public ICSSiteDAO getCsSiteDAO() {

        return this.csSiteDAO;
    }

    /**
     * @param csSiteDAO the csSiteDAO to set
     */
    public void setCsSiteDAO(ICSSiteDAO csSiteDAO) {
        this.csSiteDAO = csSiteDAO;
    }

    /**
     * @return the hasRight
     */
    @Override
    public boolean getHasRight() {
        /**
         * ***
         */

        return this.hasRight;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.ICSDatasetManager#getvariablesFromDatatype(java.lang.String)
     */
    @Override
    public List<VariableForet> getVariablesFromDatatypeForUser(String frequence, IUser user) {
        List<Object[]> lstVariables = new ArrayList<>();
        List<String> lstCodeDatatype = new ArrayList<>();
        switch (frequence) {
            case JOURNALIER: {
                String[] lstCode = {DATATYPE_FREQUENCE_HUMIDITEVOL[1],
                                    DATATYPE_FREQUENCE_TEMPERATURE[1],
                                    DATATYPE_FREQUENCE_FLUXCHALEUR[1],
                                    DATATYPE_FREQUENCE_TENSIONEAU[1],
                                    DATATYPE_FREQUENCE_PROFONDEURNAPPE[1]};
                lstCodeDatatype = Arrays.asList(lstCode);
                return  this.csSiteDAO.getVariablesFromDatatypeAndUser(lstCodeDatatype, user);
            }
            case INFRA_JOURNALIER: {
                String[] lstCode = {DATATYPE_FREQUENCE_HUMIDITEVOL[0],
                                    DATATYPE_FREQUENCE_TEMPERATURE[0],
                                    DATATYPE_FREQUENCE_FLUXCHALEUR[0],
                                    DATATYPE_FREQUENCE_TENSIONEAU[0],
                                    DATATYPE_FREQUENCE_PROFONDEURNAPPE[0]};
                lstCodeDatatype = Arrays.asList(lstCode);
                return this.csSiteDAO.getVariablesFromDatatypeAndUser(lstCodeDatatype, user);
            }
            case MENSUEL:
                lstCodeDatatype = Arrays.asList(DATATYPE_CLIMATSOL_M);
                return this.csSiteDAO.getVariablesFromDatatypeAndUser(lstCodeDatatype, user);
        }
        return new LinkedList();
    }

    /**
     * @param gDAO the gDAO to set
     */
    public void setgDAO(IClimatSolVariableDAO gDAO) {
        this.gDAO = gDAO;
    }

    /**
     * @param GWDDAO the GWDDAO to set
     */
    public void setGWDDAO(IClimatSolVariableDAO GWDDAO) {
        this.GWDDAO = GWDDAO;
    }

    /**
     * @param sMPDAO the sMPDAO to set
     */
    public void setsMPDAO(IClimatSolVariableDAO sMPDAO) {
        this.sMPDAO = sMPDAO;
    }

    /**
     * @param SWCDAO the SWCDAO to set
     */
    public void setSWCDAO(IClimatSolVariableDAO SWCDAO) {
        this.SWCDAO = SWCDAO;
    }

    /**
     * @param tSDAO the tSDAO to set
     */
    public void settSDAO(IClimatSolVariableDAO tSDAO) {
        this.tSDAO = tSDAO;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableForetDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param siteThemeDatatypeDAO
     */
    public void setSiteThemeDatatypeDAO(ISiteThemeDatatypeDAO siteThemeDatatypeDAO) {
        this.siteThemeDatatypeDAO = siteThemeDatatypeDAO;
    }
}
