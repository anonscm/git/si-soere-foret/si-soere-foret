/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_m;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_m;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_m;


/**
 * @author sophie
 * 
 */
public class JPAPublicationProfondeurNappe_mDAO extends JPAPublicationClimatSolDAO<ProfilProfondeurNappe_m, MesureProfondeurNappe_m, ValeurProfondeurNappe_m> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurProfondeurNappe_m> getValueClass() {
        return ValeurProfondeurNappe_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureProfondeurNappe_m> getMeasureClass() {
        return MesureProfondeurNappe_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilProfondeurNappe_m> getProfilClass() {
        return ProfilProfondeurNappe_m.class;
    }
}
