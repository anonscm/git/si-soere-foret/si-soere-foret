package org.inra.ecoinfo.foret.synthesis.climatsol.mfc;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_j;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mfcj.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mfcj.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MFC_jSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurFluxChaleur_j, MesureFluxChaleur_j, ProfilFluxChaleur_j> {

    public CS_MFC_jSynthesisDAO() {
        super(ValeurFluxChaleur_j.class);
    }


}
