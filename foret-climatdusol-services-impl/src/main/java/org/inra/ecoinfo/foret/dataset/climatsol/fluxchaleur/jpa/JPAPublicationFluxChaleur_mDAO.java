/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAPublicationFluxChaleur_mDAO extends JPAPublicationClimatSolDAO<ProfilFluxChaleur_m, MesureFluxChaleur_m, ValeurFluxChaleur_m> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurFluxChaleur_m> getValueClass() {
        return ValeurFluxChaleur_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureFluxChaleur_m> getMeasureClass() {
        return MesureFluxChaleur_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilFluxChaleur_m> getProfilClass() {
        return ProfilFluxChaleur_m.class;
    }
}