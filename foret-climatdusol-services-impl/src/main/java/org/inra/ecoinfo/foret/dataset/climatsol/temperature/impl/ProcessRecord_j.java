/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_j;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_j extends AbstractProcessRecordClimatSol<ProfilTemperature_j, MesureTemperature_j, ValeurTemperature_j> {

    /**
     *
     */
    public ProcessRecord_j() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilTemperature_j getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilTemperature_j(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureTemperature_j getNewMesure() {
        return new MesureTemperature_j();
    }

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurTemperature_j getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurTemperature_j(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilTemperature_j getNewProfil() {
        return new ProfilTemperature_j();
    }
}
