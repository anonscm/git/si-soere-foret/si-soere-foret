/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import java.util.List;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;

/**
 * @author sophie
 * 
 */
public class UtilRecorder {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.dataset.messages";

    /**
     * @param line
     * @param lineNumber
     * @param parser
     * @param undefinedColumn
     * @param badsFormatsReport
     * @param localizationManager
     */
    public static void verifieValueInMinMaxValue(Line line, int lineNumber, int undefinedColumn, ILocalizationManager localizationManager, CSVParser parser, BadsFormatsReport badsFormatsReport) {
        
        
        List<VariableValue> lstVariableValue = line.getLstVariablesValues();

        
        int columnNumber = undefinedColumn + 1;

        for (VariableValue variableValue : lstVariableValue) {
            Float valeur = variableValue.getValue();

            if (variableValue.getColumn().getMinValue() != null && !valeur.equals(new Float(Constantes.PROPERTY_CST_INVALID_BAD_MEASURE)) && valeur < variableValue.getColumn().getMinValue()) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_VALUE_MIN"), lineNumber, columnNumber, variableValue.getColumn().getName(), valeur,
                                                                                                                                                                                                                                     variableValue.getColumn().getMinValue())));
            }

            if (variableValue.getColumn().getMaxValue() != null && !valeur.equals(new Float(Constantes.PROPERTY_CST_INVALID_BAD_MEASURE)) && valeur > variableValue.getColumn().getMaxValue()) {
                badsFormatsReport.addException(new BadExpectedValueException(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, "INCORRECT_VALUE_MAX"), lineNumber, columnNumber, variableValue.getColumn().getName(), valeur,
                                                                                                                                                                                                                                     variableValue.getColumn().getMaxValue())));
            }

            columnNumber++;
        } 
    }
}
