/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAMesureFluxChaleur_mDAO extends JPAMesureClimatSolDAO<ProfilFluxChaleur_m, MesureFluxChaleur_m> {

}
