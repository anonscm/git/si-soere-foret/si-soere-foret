package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAPublicationHumiditeVol_jDAO extends JPAPublicationClimatSolDAO<ProfilHumiditeVol_j, MesureHumiditeVol_j, ValeurHumiditeVol_j> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurHumiditeVol_j> getValueClass() {
        return ValeurHumiditeVol_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureHumiditeVol_j> getMeasureClass() {
        return MesureHumiditeVol_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilHumiditeVol_j> getProfilClass() {
        return ProfilHumiditeVol_j.class;
    }
}