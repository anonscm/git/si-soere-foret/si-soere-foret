package org.inra.ecoinfo.foret.dataset.climatsol.jpa;


import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.springframework.transaction.TransactionStatus;


/**
 * @author sophie
 * 
 * @param <P0>
 */
public class JPAProfilClimatSolDAO<P0 extends ProfilClimatSol<?>> extends AbstractJPADAO<P0> implements IProfilClimatSolDAO<P0> {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO#getLinePublicationNameDoublon(org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol, java.util.Date)
     */

    /**
     *
     * @param profilClass
     * @param date
     * @param time
     * @param site
     * @return
     * @throws PersistenceException
     */
    
    @Override
    public Object[] getLinePublicationNameDoublon(P0 profilClass, LocalDate date, LocalTime time, SiteForet site){

        String profilClassName = profilClass.getClass().getName();
        profilClassName = profilClassName.substring(profilClassName.lastIndexOf(Constantes.CST_DOT) + 1);

        String queryString = "select prf.versionFile.dataset.id, prf.noLigne from " + profilClassName + " prf where zoneEtude = :site and date = :date";
        if (time != null) {
            queryString += " and heure = :time";
        }
        Query query = this.entityManager.createQuery(queryString);
        query.setParameter("date", date);
        if (time != null) {
            query.setParameter("time", time);
        }
        query.setParameter("site", site);
        try {
            Object[] doublon = (Object[]) query.getSingleResult();
            return doublon;
        } catch (Exception e) {
            return null;
        }
    }

    /**
     *
     * @param status
     */
    @Override
    public void commitTransaction(TransactionStatus status) {

        super.commitTransaction(status);

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO#getByNkey(java.lang.String, org.inra.ecoinfo.foret.refdata.site.SiteForet, java.util.Date)
     */
    /*
     * @Override public P0 getByNkey(String profilClassName, SiteForet zoneEtude, Date date) throws PersistenceException { 
     * " where zoneEtude = :zoneEtude and date = :date" ;
     * 
     * Query query = entityManager.createQuery(queryString); query.setParameter("zoneEtude", zoneEtude); query.setParameter("date", date);
     * 
     * try { return (P0) query.getSingleResult(); } catch (Exception e) { return null; } }
     */

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO#getByNkey(java.lang.String, org.inra.ecoinfo.foret.refdata.site.SiteForet, java.util.Date, java.util.Date)
     */
    /*
     * @Override public P0 getByNkey(String profilClassName, SiteForet zoneEtude, Date date, Date time) throws PersistenceException { String queryString = "from " + profilClassName + " where zoneEtude = :zoneEtude and date = :date and time = :time" ;
     * Query query = entityManager.createQuery(queryString); query.setParameter("zoneEtude", zoneEtude); query.setParameter("date", date); query.setParameter("time", time); try { return (P0) query.getSingleResult(); } catch (Exception e) { return
     * null; } }
     */

}
