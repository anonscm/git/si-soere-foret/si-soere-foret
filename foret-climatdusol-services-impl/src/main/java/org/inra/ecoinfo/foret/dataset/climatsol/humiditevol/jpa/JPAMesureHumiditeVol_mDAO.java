package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAMesureHumiditeVol_mDAO extends JPAMesureClimatSolDAO<ProfilHumiditeVol_m, MesureHumiditeVol_m> {

}
