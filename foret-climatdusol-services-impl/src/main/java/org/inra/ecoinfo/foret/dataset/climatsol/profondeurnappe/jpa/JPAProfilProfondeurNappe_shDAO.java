/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_sh;


/**
 * @author sophie
 * 
 */
public class JPAProfilProfondeurNappe_shDAO extends JPAProfilClimatSolDAO<ProfilProfondeurNappe_sh> {

}
