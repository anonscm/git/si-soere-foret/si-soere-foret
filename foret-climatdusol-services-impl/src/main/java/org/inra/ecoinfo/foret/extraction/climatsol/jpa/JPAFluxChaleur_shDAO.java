/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_sh;


/**
 * @author sophie
 * 
 */
public class JPAFluxChaleur_shDAO extends JPAClimatSolDAO<ValeurFluxChaleur_sh> {

    @Override
    protected Class<ValeurFluxChaleur_sh> getValeurClass() {
        return ValeurFluxChaleur_sh.class;
    }
}
