/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_m;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAProfilFluxChaleur_mDAO extends JPAProfilClimatSolDAO<ProfilFluxChaleur_m> {

}
