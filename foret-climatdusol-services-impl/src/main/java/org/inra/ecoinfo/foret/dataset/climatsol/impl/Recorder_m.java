/*
 * To change this license header, choose License Headers in Project Properties. To change this template file, choose Tools | Templates and open the template in the editor.
 */
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import java.time.LocalDate;
import java.util.ArrayList;
import java.util.List;
import org.inra.ecoinfo.dataset.versioning.IVersionFileDAO;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ErrorsReport;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * 
 * @author ptcherniati
 * @param <P>
 * @param <V>
 * @param <M>
 */
public abstract class Recorder_m<P extends ProfilClimatSol<M>, M extends MesureClimatSol<P, V>, V extends ValeurClimatSol<M>> {

    /**
     *
     */
    protected IVersionFileDAO versionFileDAO;

    /**
     *
     */
    protected IProfilClimatSolDAO<P> profilmDAO;

    /**
     *
     */
    protected ILocalizationManager localizationManager;

    /**
     *
     */
    public Recorder_m() {}

    /**
     * @param dbVariable
     * @param versionFile
     * @param lstVariableValues
     * @param varName
     * @param line
     * @param zoneEtude
     * @param errorsReport
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    public void buildProfil(RealNode dbRealNode, VersionFile versionFile, List<VariableValue> lstVariableValues, String varName, Line line, SiteForet zoneEtude, ErrorsReport errorsReport) throws BusinessException {
        try {
            VersionFile versionFileDB = this.versionFileDAO.getById(versionFile.getId())
                    .orElseThrow(()->new BusinessException("bad version"));
            
            P profil = this.getNewProfil(versionFileDB, line.getDate(), line.getOriginalLineNumber());
            List<M> lstMesures = new ArrayList<>();
            profil.setLstMesures(lstMesures);
            for (VariableValue lstVariableValue : lstVariableValues) {
                List<V> lstValeurs = new ArrayList<>();
                VariableValue variableValue = lstVariableValue;
                int profondeur = variableValue.getColumn().getProfondeur();
                M mesureClimatSol = this.getNewMesure(profondeur);
                lstMesures.add(mesureClimatSol);
                mesureClimatSol.setLstValeurs(lstValeurs);
                Long nbreRepetition = Long.valueOf(variableValue.getNombreRepetition());
                Float ecartType = variableValue.getEcartType();
                Float valeur = variableValue.getValue();
                V valeurClimatSol = this.getNewValeur(dbRealNode, nbreRepetition, ecartType, valeur);
                lstValeurs.add(valeurClimatSol);
                valeurClimatSol.setMesure(mesureClimatSol);
                mesureClimatSol.setProfil(profil);
            }
            this.profilmDAO.saveOrUpdate(profil);
        } catch (PersistenceException e) {
            throw new BusinessException(e);
        }
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param originalLineNumber
     * @return
     */
    abstract public P getNewProfil(VersionFile versionFileDB, LocalDate date, Long originalLineNumber);

    /**
     *
     * @param profondeur
     * @return
     */
    abstract public M getNewMesure(int profondeur);

    /**
     *
     * @param realNode
     * @param dbVariable
     * @param nbreRepetition
     * @param ecartType
     * @param valeur
     * @return
     */
    abstract public V getNewValeur(RealNode realNode, Long nbreRepetition, Float ecartType, Float valeur);

    /**
     *
     * @param versionFileDAO
     */
    public void setVersionFileDAO(IVersionFileDAO versionFileDAO) {
        this.versionFileDAO = versionFileDAO;
    }

    /**
     *
     * @param profilmDAO
     */
    public void setProfilmDAO(IProfilClimatSolDAO<P> profilmDAO) {
        this.profilmDAO = profilmDAO;
    }

    /**
     *
     * @param localizationManager
     */
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        this.localizationManager = localizationManager;
    }
}
