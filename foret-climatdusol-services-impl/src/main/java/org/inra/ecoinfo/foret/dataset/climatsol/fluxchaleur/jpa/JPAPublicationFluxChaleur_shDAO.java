/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAPublicationFluxChaleur_shDAO extends JPAPublicationClimatSolDAO<ProfilFluxChaleur_sh, MesureFluxChaleur_sh, ValeurFluxChaleur_sh> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurFluxChaleur_sh> getValueClass() {
        return ValeurFluxChaleur_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureFluxChaleur_sh> getMeasureClass() {
        return MesureFluxChaleur_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilFluxChaleur_sh> getProfilClass() {
        return ProfilFluxChaleur_sh.class;
    }
}