/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_j;


/**
 * @author sophie
 * 
 */
public class JPAFluxChaleur_jDAO extends JPAClimatSolDAO<ValeurFluxChaleur_j> {

    @Override
    protected Class<ValeurFluxChaleur_j> getValeurClass() {
        return ValeurFluxChaleur_j.class;
    }
}
