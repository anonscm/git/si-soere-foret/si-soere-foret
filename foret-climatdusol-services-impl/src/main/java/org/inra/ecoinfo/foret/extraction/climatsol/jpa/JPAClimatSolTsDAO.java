/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_m;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_sh;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolDAO;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public class JPAClimatSolTsDAO implements IClimatSolVariableDAO {

    private IClimatSolDAO<ValeurTemperature_j> temperature_jDAO;
    private IClimatSolDAO<ValeurTemperature_sh> temperature_shDAO;
    private IClimatSolDAO<ValeurTemperature_m> temperature_mDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#extract(java.util.List, java.util.List, java.util.SortedSet, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    
    @Override
    public List<Object[]> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user) throws PersistenceException, BusinessException {
        List<Object[]> lstValeurExtraite = new ArrayList<>();

        switch (frequence) {
            case Constantes.JOURNALIER:
                {
                    List<ValeurTemperature_j> valeurs = this.temperature_jDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                    for (ValeurTemperature_j valeur : valeurs) {
                        Object[] valeurExtraite = new Object[2];
                        valeurExtraite[0] = valeur;
                        valeurExtraite[1] = valeur.getNbreRepetition();
                        lstValeurExtraite.add(valeurExtraite);
                    }       break;
                }
            case Constantes.INFRA_JOURNALIER:
            {
                List<ValeurTemperature_sh> valeurs = this.temperature_shDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurTemperature_sh valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getMesure().getProfil().getHeure();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
            case Constantes.MENSUEL:
            {
                List<ValeurTemperature_m> valeurs = this.temperature_mDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurTemperature_m valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getEcartType();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
        }

        return lstValeurExtraite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesDepths(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Integer> lstDephts = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstDephts = this.temperature_jDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstDephts = this.temperature_shDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstDephts = this.temperature_mDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
        }

        return lstDephts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesVariables(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Variable> getAvailablesVariables(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Variable> lstVariables = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstVariables = this.temperature_jDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstVariables = this.temperature_shDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstVariables = this.temperature_mDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
        }

        return lstVariables;
    }

    
    /*
     * @Override public List<MesureClimatSol> extract(List<SiteForet> lstSelectedSitesForet, List<Variable> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence) throws PersistenceException,
     * BusinessException { List<MesureClimatSol> mesures = null; if (frequence.equals(Constantes.JOURNALIER)) mesures = temperature_jDAO.extract("ProfilTemperature_j", "MesureTemperature_j", lstSelectedSitesForet, lstSelectedVariables,
     * lstSelectedDepths, interval, frequence); else if (frequence.equals(Constantes.INFRA_JOURNALIER)) mesures = temperature_shDAO.extract("ProfilTemperature_sh", "MesureTemperature_sh", lstSelectedSitesForet, lstSelectedVariables,
     * lstSelectedDepths, interval, frequence); else if (frequence.equals(Constantes.MENSUEL)) mesures = temperature_mDAO.extract("ProfilTemperature_m", "MesureTemperature_m", lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval,
     * frequence);
     * 
     * return mesures; }
     * 
     * @Override public List<Object[]> getColumns(List<SiteForet> lstSelectedSitesForet, List<Variable> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence) throws PersistenceException,
     * BusinessException { List<Object[]> columns = null; if (frequence.equals(Constantes.JOURNALIER)) columns = temperature_jDAO.getColumns("ProfilTemperature_j", "MesureTemperature_j", "ValeurTemperature_j", lstSelectedSitesForet,
     * lstSelectedVariables, lstSelectedDepths, interval, frequence); else if (frequence.equals(Constantes.INFRA_JOURNALIER)) columns = temperature_shDAO.getColumns("ProfilTemperature_sh", "MesureTemperature_sh", "ValeurTemperature_sh",
     * lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence); else if (frequence.equals(Constantes.MENSUEL)) columns = temperature_mDAO.getColumns("ProfilTemperature_m", "MesureTemperature_m", "ValeurTemperature_m",
     * lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence);
     * 
     * return columns; }
     */

    /**
     * @param temperature_jDAO
     *            the temperature_jDAO to set
     */
    public void setTemperature_jDAO(IClimatSolDAO<ValeurTemperature_j> temperature_jDAO) {
        this.temperature_jDAO = temperature_jDAO;
    }

    /**
     * @param temperature_mDAO
     *            the temperature_mDAO to set
     */
    public void setTemperature_mDAO(IClimatSolDAO<ValeurTemperature_m> temperature_mDAO) {
        this.temperature_mDAO = temperature_mDAO;
    }

    /**
     * @param temperature_shDAO
     *            the temperature_shDAO to set
     */
    public void setTemperature_shDAO(IClimatSolDAO<ValeurTemperature_sh> temperature_shDAO) {
        this.temperature_shDAO = temperature_shDAO;
    }
}
