/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_m;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_m;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_m;


/**
 * @author sophie
 * 
 */
public class JPAPublicationTensionEau_mDAO extends JPAPublicationClimatSolDAO<ProfilTensionEau_m, MesureTensionEau_m, ValeurTensionEau_m> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurTensionEau_m> getValueClass() {
        return ValeurTensionEau_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureTensionEau_m> getMeasureClass() {
        return MesureTensionEau_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilTensionEau_m> getProfilClass() {
        return ProfilTensionEau_m.class;
    }
}