/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import java.util.LinkedList;
import java.util.List;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Predicate;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset;
import org.inra.ecoinfo.dataset.versioning.entity.Dataset_;
import org.inra.ecoinfo.foret.extraction.climatsol.ICSSiteDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.IDatatypeVariableUniteForetDAO;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Outils;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet;
import org.inra.ecoinfo.mga.business.composite.NodeDataSet_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.refdata.theme.Theme;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 *
 */
public class JPACSSiteDAO extends AbstractJPADAO<Object[]> implements ICSSiteDAO {

    /**
     *
     */
    protected IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.ICSSiteDAO#getAvailablesSites()
     */
    /**
     *
     * @param user
     * @return
     */
    @Override
    public List<SiteForet> getAvailablesSitesForUser(IUser user) {
        CriteriaQuery<SiteForet> query = builder.createQuery(SiteForet.class);
        Root<Dataset> dataset = query.from(Dataset.class);
        Root<RealNode> realNodeDatatype = query.from(RealNode.class);
        Join<RealNode, RealNode> realNodeTheme = realNodeDatatype.join(RealNode_.parent);
        Join<RealNode, Theme> theme = builder.treat(realNodeTheme.join(RealNode_.nodeable), Theme.class);
        Join<RealNode, RealNode> realNodeSite = realNodeTheme.join(RealNode_.parent);
        Join<RealNode, SiteForet> site = builder.treat(realNodeSite.join(RealNode_.nodeable), SiteForet.class);
        List<Predicate> predicatesAnd = new LinkedList();
        predicatesAnd.add(builder.isNotNull(dataset.get(Dataset_.publishVersion)));
        if (!user.getIsRoot()) {
            Root<NodeDataSet> nds = query.from(NodeDataSet.class);
            predicatesAnd.add(builder.equal(nds.get(NodeDataSet_.realNode), realNodeSite));
            Outils.addRestrictiveRequestOnRoles(user, query, predicatesAnd, builder, nds, null);
        }
        query
                .select(site.alias(SITE_FORET_ALIAS))
                .where(predicatesAnd.toArray(new Predicate[0]))
                .distinct(true);
        return getResultList(query);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.ICSSiteDAO#getVariablesFromDatatype(java.util.List)
     */
    /**
     *
     * @param lstCodeDatatype
     * @param user
     * @return
     */
    @Override
    public List<VariableForet> getVariablesFromDatatypeAndUser(List<String> lstCodeDatatype, IUser user) {
        return datatypeVariableUniteForetDAO.getLstVariableByLstDatatypeAndUser(lstCodeDatatype, user);
    }

    /**
     * @param datatypeVariableUniteForetDAO the datatypeVariableUniteForetDAO to
     * set
     */
    public void setDatatypeVariableUniteForetDAO(IDatatypeVariableUniteForetDAO datatypeVariableUniteForetDAO) {
        this.datatypeVariableUniteForetDAO = datatypeVariableUniteForetDAO;
    }

}
