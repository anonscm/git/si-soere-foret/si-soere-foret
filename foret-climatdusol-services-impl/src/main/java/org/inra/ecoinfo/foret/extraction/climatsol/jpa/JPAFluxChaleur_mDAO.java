/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_m;


/**
 * @author sophie
 * 
 */
public class JPAFluxChaleur_mDAO extends JPAClimatSolDAO<ValeurFluxChaleur_m> {

    @Override
    protected Class<ValeurFluxChaleur_m> getValeurClass() {
        return ValeurFluxChaleur_m.class;
    }
}
