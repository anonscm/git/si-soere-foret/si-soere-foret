package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import com.google.common.base.Strings;
import java.io.IOException;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.LocalTime;
import java.util.Collections;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Optional;
import java.util.SortedSet;
import java.util.TreeSet;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ErrorsReport;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.UtilVerifRecorder;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.dataset.impl.AbstractProcessRecordForet;
import org.inra.ecoinfo.foret.dataset.impl.CleanerValues;
import org.inra.ecoinfo.foret.dataset.impl.FORETRecorder;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.refdata.datatype.DataType;
import org.inra.ecoinfo.utils.DatasetDescriptor;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BadsFormatsReport;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 * @author sophie
 * @param <P>
 * @param <V>
 * @param <M>
 *
 */
public abstract class AbstractProcessRecordClimatSol<P extends ProfilClimatSol<M>, M extends MesureClimatSol<P, V>, V extends ValeurClimatSol<M>> extends AbstractProcessRecordForet {

    /**
     *
     */
    protected static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.dataset.messages";

    /**
     *
     */
    protected IProfilClimatSolDAO<P> profilClimatSolDAO;

    /**
     *
     */
    protected String variableName;

    /**
     *
     */
    public AbstractProcessRecordClimatSol() {
        super();
    }

    /**
     *
     * @param profilClimatSolDAO
     */
    public void setProfilClimatSolDAO(IProfilClimatSolDAO<P> profilClimatSolDAO) {
        this.profilClimatSolDAO = profilClimatSolDAO;
    }

    /**
     * <p>
     * process the record from the parser </p>
     * <p>
     * On saute l'en-tête et on retourne la variable correspondante. </p>
     * <p>
     * On lit le fichier pour obtenir une liste de ligne. </p>
     * <p>
     * Pour chaque ligne de la liste on construit le profil </p>
     *
     * @param parser
     * @param versionFile
     * @param requestProperties
     * @param fileEncoding
     * @param datasetDescriptor
     * @throws BusinessException
     */
    @Override
    public void processRecord(CSVParser parser, VersionFile versionFile, IRequestProperties requestProperties, String fileEncoding, DatasetDescriptor datasetDescriptor) throws BusinessException {
        IRequestPropertiesCS requestPropertiesCS = (IRequestPropertiesCS) requestProperties;
        Utils.testCastedArguments(versionFile, VersionFile.class, localizationManager);
        ErrorsReport errorsReport = new ErrorsReport();

        try {

            RealNode dbRealNode = this.buildVariableHeaderAndSkipHeader(parser, (DatasetDescriptorCS) datasetDescriptor, versionFile.getDataset().getRealNode())
                    .orElseThrow(PersistenceException::new);
            SiteForet zoneEtude = requestPropertiesCS.getSite();
            BadsFormatsReport badsFormatsReport = new BadsFormatsReport(fileEncoding);
            List<Line> lstLines = this.createLstVariableValueForlines((RequestPropertiesCS) requestPropertiesCS, datasetDescriptor, parser, zoneEtude, badsFormatsReport);

            this.buildProfil(dbRealNode, versionFile, lstLines, zoneEtude, errorsReport);

            if (errorsReport.hasErrors()) {
                LOGGER.debug(errorsReport.getErrorsMessages());
                throw new PersistenceException(errorsReport.getErrorsMessages());
            }

        } catch (PersistenceException e) {
            LOGGER.debug(e.getMessage());
            throw new BusinessException(e.getMessage(), e);
        } catch (BadFormatException e) {
            BadsFormatsReport badsFormatsReport = new BadsFormatsReport(BUNDLE_SOURCE_PATH);
            throw new BusinessException(badsFormatsReport.getMessages());
        }
    }

    /**
     *
     * @param variableName
     */
    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    /**
     * <p>
     * pour chaque ligne de lstLines on crée un profil</p>
     * <p>
     * <ul><li>On crée un nouveau profil this.createNewProfil(versionFileDB,
     * line) ; méthode à surcharger getNewProfil(VersionFile versionFileDB, Date
     * date, Date time, Long originalLineNumber)</li>
     * <li>on crée et on ajoute une nouvelle mesure addNewMesure(profil)méthode
     * à surcharger getNewMesure()</li>
     * <li>Pour chaque variableValue de la liste triée on ajoute une nouvelle
     * valeur addValeur méthode à surcharger addNewValeur(VariableForet
     * dbVariable, int noRepetition, Float valeur, M mesureCS)</li>
     * <li>On ajoute le profil de la liste</li>
     * <li>Si la construction est en échec on ajoute la ligne à la liste des
     * ligne en erreur</li>
     * <li>Si la liste des ligne en erreur n'est pas vide c'est qu'il y a une
     * duplication → DOUBLON_DATE_TIME</li><ul></p>
     *
     * @param dbVariable
     * @param versionFile
     * @param lstLines
     * @param zoneEtude
     * @param errorsReport
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    public void buildProfil(RealNode dbRealNode, VersionFile versionFile, List<Line> lstLines, SiteForet zoneEtude, ErrorsReport errorsReport) throws BusinessException {
        SortedSet<Line> ligneEnErreur = new TreeSet<>();
        try {
            dbRealNode = getDBRealNode(dbRealNode)
                    .orElseThrow(PersistenceException::new);
            VersionFile versionFileDB = this.versionFileDAO.merge(versionFile);
            Iterator<Line> iterator = lstLines.iterator();
            while (iterator.hasNext()) {
                Line line = iterator.next();
                this.addProfil(dbRealNode, line, versionFileDB, ligneEnErreur, errorsReport);
                iterator.remove();
                if (line.getOriginalLineNumber() % 50 == 0) {
                    profilClimatSolDAO.flush();
                    versionFileDB = this.versionFileDAO.merge(versionFileDB);
                    dbRealNode = getDBRealNode(dbRealNode)
                            .orElseThrow(PersistenceException::new);
                }
            }
        } catch (PersistenceException ex) {
            throw new BusinessException(ex);
        }
        if (!ligneEnErreur.isEmpty()) {
            registerErrorLine(ligneEnErreur, zoneEtude, errorsReport);
        }

        if (errorsReport.hasErrors()) {
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    private void registerErrorLine(SortedSet<Line> ligneEnErreur, SiteForet zoneEtude, ErrorsReport errorsReport) {
        for (Line line : ligneEnErreur) {
            P profil = this.getNewProfil();
            Object[] doublon = this.profilClimatSolDAO.getLinePublicationNameDoublon(profil, line.getDate(), line.getTime(), zoneEtude);
            if (doublon != null) {
                errorsReport.addErrorMessage(String.format(FORETRecorder.getForetMessage("DOUBLON_DATE_TIME"), doublon[1],
                        DateUtil.getUTCDateTextFromLocalDateTime(line.getDate(), DateUtil.DD_MM_YYYY),
                        DateUtil.getUTCDateTextFromLocalDateTime(line.getTime(), DateUtil.HH_MM)));
            } else {
                errorsReport.addErrorMessage(String.format(FORETRecorder.getForetMessage("ERREUR_INSERTION_MESURE"), line.getOriginalLineNumber()));
            }
        }
    }

    /**
     * <p>
     * <ul><li>Pour chaque ligne :</li><ul>
     * <li>On récupère la date et l'heure (si elle existe)</li>
     * <li>Pour chaque colonne variable, si la valeur n'est pas nulle, on
     * vérifie que c'est une valeur flottante (→ BAD_FLOAT_VALUE) et on crée une
     * nouvelle variableValue (new VariableValue(column, new Float(value))) .
     * </li>
     * <li>On crée une ligne new Line(date, time, (int) lineCount,
     * requestPropertiesCS.getSite(), lstVariablesValues)
     * </li></ul></li></ul></p>
     *
     * @param requestPropertiesCS
     * @param datasetDescriptor
     * @param parser
     * @param site
     * @param badsFormatsReport
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws IOException
     * @throws ParseException
     * @throws BadFormatException
     */
    protected List<Line> createLstVariableValueForlines(RequestPropertiesCS requestPropertiesCS, DatasetDescriptor datasetDescriptor, CSVParser parser, SiteForet site, BadsFormatsReport badsFormatsReport) throws BusinessException, BadFormatException {
        try {
            String[] values = null;
            long lineNumber = parser.getLastLineNumber();
            LinkedList<Line> lstLines = new LinkedList<>();

            while ((values = parser.getLine()) != null) {
                lineNumber++;
                LocalDate date = null;
                LocalTime time = null;
                List<VariableValue> lstVariablesValues = new LinkedList<>();
                CleanerValues cleanerValues = new CleanerValues(values);

                int columnNumber = 0;

                if (!badsFormatsReport.hasErrors()) {
                    final String dateString = cleanerValues.nextToken().toLowerCase().trim();
                    date = DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, dateString);
                    if (datasetDescriptor.getUndefinedColumn() == 2) {
                        String valueTime = cleanerValues.nextToken();
                        if (valueTime.matches("[0-9]:.*")) {
                            valueTime = String.format("0%s", valueTime);
                        }
                        final String timeString = valueTime.toLowerCase().trim();
                        time = DateUtil.readLocalTimeFromText(DateUtil.HH_MM, timeString);
                    }
                    VariableValue variableValue = null;
                    ExpectedColumn column;

                    for (int i = datasetDescriptor.getUndefinedColumn(); i < requestPropertiesCS.getExpectedColumns().size() + datasetDescriptor.getUndefinedColumn(); i++) {
                        if (buildVariableValue(cleanerValues, requestPropertiesCS, i, lineNumber, badsFormatsReport, lstVariablesValues)) {
                        }
                    }
                    Line line = new Line(date, time, lineNumber, requestPropertiesCS.getSite(), lstVariablesValues);
                    if (!lstVariablesValues.isEmpty()) {
                        lstLines.add(line);
                    }
                }
            }

            if (badsFormatsReport.hasErrors()) {
                throw new BadFormatException(badsFormatsReport);
            }

            return lstLines;
        } catch (IOException ex) {
            throw new BusinessException(ex);
        }
    }

    private boolean buildVariableValue(CleanerValues cleanerValues, RequestPropertiesCS requestPropertiesCS, int i, long lineNumber, BadsFormatsReport badsFormatsReport, List<VariableValue> lstVariablesValues) throws BusinessException, BadFormatException {
        ExpectedColumn column;
        int columnNumber;
        VariableValue variableValue;
        String value = cleanerValues.nextToken();
        column = requestPropertiesCS.getExpectedColumns().get(requestPropertiesCS.getColumnNames()[i]);
        if (column != null && !Constantes.PROPERTY_CST_VARIABLE_TYPE.equals(column.getFlagType())) {
            return true;
        }
        columnNumber = i + 1;
        if (Strings.isNullOrEmpty(value)) {
            return true;
        }
        UtilVerifRecorder.isValueFormatFloat(lineNumber, columnNumber, value, column.getName(), badsFormatsReport);
        if (badsFormatsReport.hasErrors()) {
            throw new BadFormatException(badsFormatsReport);
        }
        variableValue = new VariableValue(column, new Float(value));
        lstVariablesValues.add(variableValue);
        return false;
    }

    /**
     * <p>
     * Pour chaque ligne de la liste on construit le profil<p>
     * <ul><li>On crée un nouveau profil this.createNewProfil(versionFileDB,
     * line) ; méthode à surcharger getNewProfil(VersionFile versionFileDB, Date
     * date, Date time, Long originalLineNumber)</li>
     * <li>on crée et on ajoute une nouvelle mesure addNewMesure(profil)méthode
     * à surcharger getNewMesure()</li>
     * <li>Pour chaque variableValue de la liste triée on ajoute une nouvelle
     * valeur addValeur méthode à surcharger addNewValeur(VariableForet
     * dbVariable, int noRepetition, Float valeur, M mesureCS)</li>
     * <li>On ajoute le profil de la liste</li>
     * <li>Si la construction est en échec on ajoute la ligne à la liste des
     * ligne en erreur</li>
     * <li>Si la liste des ligne en erreur n'est pas vide c'est qu'il y a une
     * duplication → DOUBLON_DATE_TIME</li></ul>
     *
     * @param line
     * @param versionFileDB
     * @param dbVariable
     * @param ligneEnErreur
     * @param errorsReport
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     */
    protected void addProfil(RealNode dbRealNode, Line line, VersionFile versionFileDB, SortedSet<Line> ligneEnErreur, ErrorsReport errorsReport) throws BusinessException {

        try {
            List<VariableValue> lstVariableValues = line.getLstVariablesValues();
            P profil = this.createNewProfil(versionFileDB, line);
            Collections.sort(lstVariableValues, new CompareV());
            if (lstVariableValues != null && !lstVariableValues.isEmpty()) {
                M mesureCS = this.addNewMesure(profil);
                for (int i = 0; i < lstVariableValues.size(); i++) {
                    VariableValue variableValue = lstVariableValues.get(i);
                    int profondeur = variableValue.getColumn().getProfondeur();
                    this.addValeur(dbRealNode, variableValue, i, profil);
                }
            }
            this.profilClimatSolDAO.saveOrUpdate(profil);
        } catch (PersistenceException e) {
            ligneEnErreur.add(line);
            errorsReport.addErrorMessage(String.format("%s", e.getCause()));
            throw new BusinessException(errorsReport.getErrorsMessages());
        }
    }

    private void addValeur(RealNode dbRealNode, VariableValue variableValue, int i, P profil) {
        M mesureCS = ((LinkedList<M>) profil.getLstMesures()).getLast();
        int profondeur = variableValue.getColumn().getProfondeur();
        if (profondeur != mesureCS.getProfondeur()) {
            mesureCS = this.addNewMesure(profil);
            mesureCS.setProfondeur(profondeur);
        }
        Long noRepetition = Long.valueOf(variableValue.getColumn().getNumeroRepetition());
        Float valeur = variableValue.getValue();
        this.addNewValeur(dbRealNode, noRepetition, valeur, mesureCS);
    }

    private void addNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur, M mesureCS) {
        V valeurCS = this.getNewValeur(dbRealNode, noRepetition, valeur);
        mesureCS.getLstValeurs().add(valeurCS);
        valeurCS.setMesure(mesureCS);
    }

    private M addNewMesure(P profil) {
        M mesureCS = this.getNewMesure();
        profil.getLstMesures().add(mesureCS);
        mesureCS.setProfil(profil);
        List<V> lstValeurs = new LinkedList();
        mesureCS.setLstValeurs(lstValeurs);
        return mesureCS;
    }

    private P createNewProfil(VersionFile versionFileDB, Line line) {
        P profil = this.getNewProfil(versionFileDB, line.getDate(), line.getTime(), line.getOriginalLineNumber());
        List<M> lstMesures = new LinkedList();
        profil.setLstMesures(lstMesures);
        return profil;
    }

    /**
     * @param parser
     * @param datasetDescriptor
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     * @throws IOException
     * @throws org.inra.ecoinfo.utils.exceptions.BadFormatException
     */
    protected Optional<RealNode> buildVariableHeaderAndSkipHeader(CSVParser parser, DatasetDescriptorCS datasetDescriptor, RealNode datatypeRealNode) throws BusinessException {
        VariableForet dbVariable = null;
        for (int i = 0; datasetDescriptor.getEnTete() - 1 >= i; i++) {
            try {
                parser.getLine();
            } catch (IOException ex) {
                throw new BusinessException(ex);
            }
        }
        return getDBRealNode(datatypeRealNode);
    }

    /**
     *
     * @param datatypeRealNode
     * @return
     */
    protected Optional<RealNode> getDBRealNode(RealNode datatypeRealNode) {
        if (datatypeRealNode.getNodeable() instanceof DataType) {
            return datatypeVariableUniteForetDAO.getRealNodesVariables(datatypeRealNode).entrySet().stream()
                    .filter(e -> e.getKey().getCode().equals(variableName))
                    .map(e -> e.getValue())
                    .findFirst();
        }
        return Optional.ofNullable(datatypeRealNode)
                .map(rn -> datatypeVariableUniteForetDAO.mergeRealNode(rn));
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return a new profil initialized with parameters
     */
    abstract protected P getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber);

    /**
     *
     * @return a new mesure
     */
    abstract protected M getNewMesure();

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return a new valeur
     */
    abstract protected V getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur);

    /**
     *
     * @return a new profil
     */
    abstract protected P getNewProfil();

}
