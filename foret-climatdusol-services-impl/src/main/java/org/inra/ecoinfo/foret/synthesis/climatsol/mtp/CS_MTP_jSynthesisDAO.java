package org.inra.ecoinfo.foret.synthesis.climatsol.mtp;

import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_j;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mtpj.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mtpj.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MTP_jSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurTemperature_j, MesureTemperature_j, ProfilTemperature_j> {

    public CS_MTP_jSynthesisDAO() {
        super(ValeurTemperature_j.class);
    }
}
