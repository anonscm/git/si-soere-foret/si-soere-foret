package org.inra.ecoinfo.foret.dataset.climatsol.jpa;

import javax.persistence.criteria.CriteriaDelete;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Root;
import javax.persistence.criteria.Subquery;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile_;
import org.inra.ecoinfo.dataset.versioning.jpa.JPAVersionFileDAO;
import org.inra.ecoinfo.foret.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol_;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * @param <V>
 * 
 */
public abstract class JPAPublicationClimatSolDAO<P extends ProfilClimatSol<M>, M extends MesureClimatSol<P,V>, V extends ValeurClimatSol<M>>
        extends
            JPAVersionFileDAO implements ILocalPublicationDAO
{

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.ILocalPublicationDAO#removeVersion(java.lang.Long)
     */

    /**
     *
     * @param versionId
     * @throws PersistenceException
     */
    
    @Override
    public void removeVersion(Long versionId) throws PersistenceException {
        removeValues(versionId);
        removeMeasures(versionId);
        removeProfils(versionId);
    }
    
    /**
     *
     * @return
     */
    protected abstract Class<V> getValueClass();

    /**
     *
     * @return
     */
    protected abstract Class<M> getMeasureClass();

    /**
     *
     * @return
     */
    protected abstract Class<P> getProfilClass();

    private void removeValues(Long versionId) {
        CriteriaDelete<V> delete = builder.createCriteriaDelete(getValueClass());
        Root<V> v = delete.from(getValueClass());
        Subquery<M> subqueryMeasure = delete.subquery(getMeasureClass());
        Root<M> m = subqueryMeasure.from(getMeasureClass());
        Subquery<P> subqueryProfil = subqueryMeasure.subquery(getProfilClass());
        Root<P> p = subqueryProfil.from(getProfilClass());
        Join<P, VersionFile> version = p.join(ProfilClimatSol_.versionFile);
        subqueryProfil
                .select(p)
                .where(builder.equal(version.get(VersionFile_.id), versionId));
        subqueryMeasure
                .select(m)
                .where(m.get(MesureClimatSol_.profil).in(subqueryProfil));
        delete
                .where(v.get(ValeurClimatSol_.mesure).in(subqueryMeasure));
        delete(delete);
        entityManager.flush();
    }

    private void removeMeasures(Long versionId) {
        CriteriaDelete<M> delete = builder.createCriteriaDelete(getMeasureClass());
        Root<M> m = delete.from(getMeasureClass());
        Subquery<P> subqueryProfil = delete.subquery(getProfilClass());
        Root<P> p = subqueryProfil.from(getProfilClass());
        Join<P, VersionFile> version = p.join(ProfilClimatSol_.versionFile);
        subqueryProfil
                .select(p)
                .where(builder.equal(version.get(VersionFile_.id), versionId));
        delete
                .where(m.get(MesureClimatSol_.profil).in(subqueryProfil));
        delete(delete);
        entityManager.flush();
    }

    private void removeProfils(Long versionId) {        
        CriteriaDelete<P> delete = builder.createCriteriaDelete(getProfilClass());
        Root<P> p = delete.from(getProfilClass());
        Subquery<VersionFile> subqueryVersion = delete.subquery(VersionFile.class);
        Root<VersionFile> v = subqueryVersion.from(VersionFile.class);
        subqueryVersion
                .select(v)
                .where(builder.equal(v.get(VersionFile_.id), versionId));
        delete
                .where(p.get(ProfilClimatSol_.versionFile).in(subqueryVersion));
        delete(delete);
        entityManager.flush();
    }
}