package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_j;


/**
 * @author sophie
 * 
 */
public class JPAHumiditeVol_jDAO extends JPAClimatSolDAO<ValeurHumiditeVol_j> {

    @Override
    protected Class<ValeurHumiditeVol_j> getValeurClass() {
        return ValeurHumiditeVol_j.class;
    }
}
