package org.inra.ecoinfo.foret.synthesis.climatsol.mte;

import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_j;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_j;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_j;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mtej.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mtej.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MTE_jSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurTensionEau_j, MesureTensionEau_j, ProfilTensionEau_j> {

    public CS_MTE_jSynthesisDAO() {
        super(ValeurTensionEau_j.class);
    }
}
