/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_sh;


/**
 * @author sophie
 * 
 */
public class JPATensionEau_shDAO extends JPAClimatSolDAO<ValeurTensionEau_sh> {

    @Override
    protected Class<ValeurTensionEau_sh> getValeurClass() {
        return ValeurTensionEau_sh.class;
    }
}
