/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_j;


/**
 * @author sophie
 * 
 */
public class JPATensionEau_jDAO extends JPAClimatSolDAO<ValeurTensionEau_j> {

    @Override
    protected Class<ValeurTensionEau_j> getValeurClass() {
        return ValeurTensionEau_j.class;
    }
}
