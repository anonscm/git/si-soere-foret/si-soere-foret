package org.inra.ecoinfo.foret.extraction.climatsol.jsf;

import java.io.File;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.impl.DefaultParameter;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolParameter;
import org.inra.ecoinfo.foret.extraction.climatsol.ui.flex.vo.DatesFormParamCSVO;
import org.inra.ecoinfo.foret.extraction.jsf.date.AbstractDatesFormParam;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public class ClimatSolParameterVO extends DefaultParameter implements IClimatSolParameter {
    public final static String SITE_PARAMETER = Nodeable.class.getSimpleName();
    public final static String VARIABLE_PARAMETER = DatatypeVariableUniteForet.class.getSimpleName();
    public final static String DEPHT_PARAMETER = "depths";
    public final static String DISPLAY_PARAMETER = "display";
    public final static String DATES_PARAMETER = AbstractDatesFormParam.class.getSimpleName();

    private int affichage;

    /*
     * private List<MesureClimatSol> mesureClimatSol = new LinkedList<MesureClimatSol>(); private Map<ValeurClimatSol, List<String>> columns = new HashMap<ValeurClimatSol, List<String>>();
     */
    private Map<String, Map<String, List>> results;
    private final Map<String, File> filesMap = new HashMap<>();
    Map<String, List<Object[]>> valeursExtraites = new HashMap();

    /**
     *
     */
    public ClimatSolParameterVO() {
        super();
        getParameters().put(DISPLAY_PARAMETER, 1);
    }

    @Override
    public IntervalDate getIntervalDate() {
        return ((DatesFormParamCSVO) getParameters().get(DATES_PARAMETER)).intervalsDate().stream().findFirst().orElse(null);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolParameter#getAffichage()
     */
    /**
     *
     * @return
     */
    @Override
    public int getAffichage() {
        return (int) getParameters().get(DISPLAY_PARAMETER);
    }

    /* IClimatSolParameter */
    /**
     *
     * @param affichage
     */
    @Override
    public void setAffichage(int affichage) {
        this.affichage = affichage;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.extraction.IParameter#getExtractionTypeCode()
     */
    @Override
    public String getExtractionTypeCode() {
        return Constantes.CLIMATSOL_EXTRACTION_TYPE_CODE;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolParameter#getFilesMap()
     */
    /**
     *
     * @return
     */
    @Override
    public Map<String, File> getFilesMap() {
        return this.filesMap;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolParameter#getRythme()
     */
    /**
     *
     * @return
     */
    @Override
    public String getRythme() {
        return ((DatesFormParamCSVO) getParameters().get(DATES_PARAMETER)).getRythme();
    }

    /**
     * @return
     */
    @Override
    public Map<String,List<Object[]>> getValeursExtraites() {
        return this.valeursExtraites;
    }

    /**
     *
     * @param valeursExtraites
     * @param lstValeurExtraites
     */
    public void setValeursExtraites(Map<String,List<Object[]>> valeursExtraites) {
        this.valeursExtraites = valeursExtraites;
    }
    /*
     * public List<MesureClimatSol> getMesureClimatSol() { return mesureClimatSol; }
     *
     * public void setMesureClimatSol(List<MesureClimatSol> mesureClimatSol) { this.mesureClimatSol = mesureClimatSol; }
     *
     *
     * public Map<ValeurClimatSol, List<String>> getColumns() { return columns; }
     */

    /* IParameter* */
    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.extraction.IParameter#setParameters(java.util.Map)
     */
    @Override
    public void setParameters(Map<String, Object> parameters) {
        

    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.extraction.IParameter#getResults()
     */
    @Override
    public Map<String, Map<String, List>> getResults() {
        return this.results;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolParameter#getSelectedsDepths()
     */
    /**
     *
     * @return
     */
    @Override
    public SortedSet<Integer> getSelectedsDepths() {
        return ((SortedSet<Integer>) getParameters().get(DEPHT_PARAMETER));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolParameter#getSelectedSites()
     */
    /**
     *
     * @return
     */
    @Override
    public List<SiteForet> getSelectedSites() {
        return (List<SiteForet>) getParameters().get(SITE_PARAMETER);
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolParameter#getSelectedVariables()
     */
    /**
     *
     * @return
     */
    @Override
    public List<DatatypeVariableUniteForet> getSelectedVariables() {
        return (List<DatatypeVariableUniteForet>) getParameters().get(VARIABLE_PARAMETER);
    }

}
