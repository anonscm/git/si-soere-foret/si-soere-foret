/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.impl;


import java.time.LocalDate;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_m;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_m;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_m;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class Recorder_m extends org.inra.ecoinfo.foret.dataset.climatsol.impl.Recorder_m<ProfilProfondeurNappe_m, MesureProfondeurNappe_m, ValeurProfondeurNappe_m> {

    private static final long serialVersionUID = 1L;;

    /**
     * empty constructor
     */
    public Recorder_m() {

    }

    /**
     * @param profilProfondeurNappe_mDAO
     *            the profilProfondeurNappe_mDAO to set
     */
    public void setProfilProfondeurNappe_mDAO(IProfilClimatSolDAO<ProfilProfondeurNappe_m> profilProfondeurNappe_mDAO) {
        this.profilmDAO = profilProfondeurNappe_mDAO;
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param originalLineNumber
     * @return
     */
    @Override
    public ProfilProfondeurNappe_m getNewProfil(VersionFile versionFileDB, LocalDate date, Long originalLineNumber) {
        return new ProfilProfondeurNappe_m(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @param profondeur
     * @return
     */
    @Override
    public MesureProfondeurNappe_m getNewMesure(int profondeur) {
        return new MesureProfondeurNappe_m(profondeur);
    }

    /**
     *
     * @param dbRealNode
     * @param nbreRepetition
     * @param ecartType
     * @param valeur
     * @return
     */
    @Override
    public ValeurProfondeurNappe_m getNewValeur(RealNode dbRealNode, Long nbreRepetition, Float ecartType, Float valeur) {
        return new ValeurProfondeurNappe_m(dbRealNode, nbreRepetition, ecartType, valeur);
    }

}
