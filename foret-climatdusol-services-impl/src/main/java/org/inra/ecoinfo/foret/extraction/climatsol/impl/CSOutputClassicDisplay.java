package org.inra.ecoinfo.foret.extraction.climatsol.impl;

import java.io.File;
import java.util.List;
import java.util.Map;
import org.inra.ecoinfo.foret.extraction.climatsol.jsf.ClimatSolParameterVO;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class CSOutputClassicDisplay extends CSOutputsBuildersResolver {

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.extraction.climatsol.messages";
    
    private static final String PATTERN_HEADER_EMPTY_J_SH = ";;;";
    private static final String PATTERN_HEADER_EMPTY_M = ";;;;";
    private static final String UNITE_REPETION_PROFONDEUR = ";*;cm;";
    private static final String UNITE_REPETION_PROFONDEUR_MENSUEL = ";*;*;cm;";
    private static final String CST_CLIMATSOL = Constantes.CLIMATSOL;

    /**
     *
     */
    protected static final String PATTERN_COLUMNS_CLASSIC_HEADER_M = "PATTERN_COLUMNS_CLASSIC_HEADER_M";

    /**
     *
     */
    protected static final String PROPERTY_MSG_OUTPUT_HEADER_SH = "PROPERTY_MSG_OUTPUT_HEADER_SH";

    /**
     *
     */
    protected static final String PROPERTY_MSG_OUTPUT_HEADER_J_M = "PROPERTY_MSG_OUTPUT_HEADER_J_M";

    /**
     *
     */
    protected static final String PATTERN_COLUMNS_CLASSIC_HEADER_J_SH = "PATTERN_COLUMNS_CLASSIC_HEADER_J_SH";

    /**
     *
     */
    protected static final String PROPERTY_CST_INVALID_BAD_MESURE = Constantes.PROPERTY_CST_INVALID_BAD_MEASURE;

    /**
     *
     */
    public CSOutputClassicDisplay() {
        super();
    }

    private void faireLigneUnite(BuildOutputHelper outputHelper, String frequence, List<String> headerColumnName) {
        String message = CSOutputsBuildersResolver.CST_SEMICOLON;
        message = message.concat(CSOutputsBuildersResolver.CST_SEMICOLON);
        if (frequence.equals(Constantes.INFRA_JOURNALIER)) {
            message = message.concat(CSOutputsBuildersResolver.CST_SEMICOLON);
        }
        outputHelper.printStream.println();
        outputHelper.printStream.print(message);
        for (String columnName : headerColumnName) {
            String variableName = columnName.split(CSOutputsBuildersResolver.CST_UNDERSCORE)[0];
            String unite = this.chercherUnite(variableName, frequence);
            switch (frequence) {
                case Constantes.INFRA_JOURNALIER:
                case Constantes.JOURNALIER:
                    unite = unite.concat(CSOutputClassicDisplay.UNITE_REPETION_PROFONDEUR);
                    break;
                case Constantes.MENSUEL:
                    unite = unite.concat(CSOutputClassicDisplay.UNITE_REPETION_PROFONDEUR_MENSUEL);
                    break;
            }
            outputHelper.printStream.printf("%s", unite);
        }
    }

    /**
     *
     * @param headers
     * @param resultsDatasMap
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     *
     * @param requestMetadatasMap
     * @return
     * @throws BusinessException
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /**
     *
     * @param outputHelper
     */
    @Override
    protected void displayEmptyValue(BuildOutputHelper outputHelper) {
        if (!outputHelper.parameter.getRythme().equals(Constantes.MENSUEL)) {
            
            outputHelper.printStream.print(CSOutputClassicDisplay.PATTERN_HEADER_EMPTY_J_SH);
        } else {
            
            outputHelper.printStream.print(CSOutputClassicDisplay.PATTERN_HEADER_EMPTY_M);
        }
    }

    /**
     *
     * @param outputHelper
     * @param value
     */
    @Override
    protected void displayValue(BuildOutputHelper outputHelper, String value) {
        // nothing do do
    }

    /**
     *
     * @param outputHelper
     * @param value
     * @param columValue
     */
    @Override
    protected void displayValue(BuildOutputHelper outputHelper, String value, String columValue) {
        if (!outputHelper.parameter.getRythme().equals(Constantes.MENSUEL)) {
            
            outputHelper.printStream.printf(Integer.parseInt(CSOutputClassicDisplay.PROPERTY_CST_INVALID_BAD_MESURE) == new Float(value).intValue()? ";%.0f;%d;%d" : ";%.2f;%d;%d", new Float(value), new Integer(columValue.split(Constantes.CST_UNDERSCORE)[1]), new Integer(columValue.split(Constantes.CST_UNDERSCORE)[2]));
        } else {
            
            outputHelper.printStream
                    .printf(Integer.parseInt(CSOutputClassicDisplay.PROPERTY_CST_INVALID_BAD_MESURE) == new Float(value.split(Constantes.CST_UNDERSCORE)[0]).intValue()? ";%.0f;%.2f;%d;%d" : ";%.2f;%.2f;%d;%d", new Float(value.split(Constantes.CST_UNDERSCORE)[0]),
                            new Float(value.split(Constantes.CST_UNDERSCORE)[1]), new Integer(value.split(Constantes.CST_UNDERSCORE)[2]), new Integer(columValue.split(Constantes.CST_UNDERSCORE)[1]));
        }
    }

    /**
     *
     * @return
     */
    @Override
    protected String getExtention() {
        return CSOutputsBuildersResolver.EXTENSION_CSV;
    }

    /**
     *
     * @return
     */
    @Override
    protected String getPrefixFileName() {
        return CSOutputClassicDisplay.CST_CLIMATSOL;
    }

    @Override
             void buildHeader(BuildOutputHelper outputHelper, ClimatSolParameterVO climatSolParameterVO) {
                 String message = null;
                 String frequence = climatSolParameterVO.getRythme();
        switch (frequence) {
            case Constantes.JOURNALIER:
            case Constantes.MENSUEL:
                message = this.localizationManager.getMessage(CSOutputClassicDisplay.BUNDLE_SOURCE_PATH, CSOutputClassicDisplay.PROPERTY_MSG_OUTPUT_HEADER_J_M);
                break;
            case Constantes.INFRA_JOURNALIER:
                message = this.localizationManager.getMessage(CSOutputClassicDisplay.BUNDLE_SOURCE_PATH, CSOutputClassicDisplay.PROPERTY_MSG_OUTPUT_HEADER_SH);
                break;
        }
        outputHelper.printStream.print(message);
        List<String> headerColumnName = outputHelper.headerColumnName;
        for (String columnName : headerColumnName) {
            switch (frequence) {
                case Constantes.INFRA_JOURNALIER:
                case Constantes.JOURNALIER:
                    outputHelper.printStream.printf(this.localizationManager.getMessage(CSOutputClassicDisplay.BUNDLE_SOURCE_PATH, CSOutputClassicDisplay.PATTERN_COLUMNS_CLASSIC_HEADER_J_SH), columnName.split(Constantes.CST_UNDERSCORE)[0]);
                    break;
                case Constantes.MENSUEL:
                    outputHelper.printStream.printf(this.localizationManager.getMessage(CSOutputClassicDisplay.BUNDLE_SOURCE_PATH, CSOutputClassicDisplay.PATTERN_COLUMNS_CLASSIC_HEADER_M), columnName.split(Constantes.CST_UNDERSCORE)[0]);
                    break;
            }
        }
        this.faireLigneUnite(outputHelper, frequence, headerColumnName);
    }
}
