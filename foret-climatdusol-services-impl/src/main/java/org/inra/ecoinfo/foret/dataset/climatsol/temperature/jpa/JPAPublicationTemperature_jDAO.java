/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_j;


/**
 * @author sophie
 * 
 */
public class JPAPublicationTemperature_jDAO extends JPAPublicationClimatSolDAO<ProfilTemperature_j, MesureTemperature_j, ValeurTemperature_j> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurTemperature_j> getValueClass() {
        return ValeurTemperature_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureTemperature_j> getMeasureClass() {
        return MesureTemperature_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilTemperature_j> getProfilClass() {
        return ProfilTemperature_j.class;
    }
}
