package org.inra.ecoinfo.foret.dataset.climatsol;


import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.PersistenceException;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;


/**
 * @author sophie
 * @param <P0>
 * 
 */
public interface IProfilClimatSolDAO<P0 extends ProfilClimatSol<?>> extends IDAO<P0> {

    /*
     * public P0 getByNkey(String profilClassName, SiteForet zoneEtude, Date date) throws PersistenceException; public P0 getByNkey(String profilClassName, SiteForet zoneEtude, Date date, Date time) throws PersistenceException;
     */

    /**
     *
     * @param profilClass
     * @param date
     * @param time
     * @param site
     * @return
     * @throws PersistenceException
     */
    
    Object[] getLinePublicationNameDoublon(P0 profilClass, LocalDate date, LocalTime time, SiteForet site);
    

}
