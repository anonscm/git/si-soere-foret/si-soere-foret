package org.inra.ecoinfo.foret.dataset.climatsol.jpa;

import javax.persistence.PersistenceException;
import javax.persistence.Query;
import org.inra.ecoinfo.AbstractJPADAO;
import org.inra.ecoinfo.foret.dataset.climatsol.IMesureClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;


/**
 * @author sophie
 * 
 * @param <P1>
 * @param <M1>
 */
public class JPAMesureClimatSolDAO<P1 extends ProfilClimatSol<?>, M1 extends MesureClimatSol<?, ?>> extends AbstractJPADAO<M1> implements IMesureClimatSolDAO<P1, M1> {

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IMesureClimatSolDAO#getByNKey(org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol, org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol, int)
     */

    /**
     *
     * @param mesureClass
     * @param profil
     * @param profondeur
     * @return
     * @throws PersistenceException
     */
    
    @SuppressWarnings("unchecked")
    @Override
    public M1 getByNKey(M1 mesureClass, P1 profil, int profondeur) throws PersistenceException {
        
        String mesureClassName = mesureClass.getClass().getName();

        String queryString = "from " + mesureClassName + " where profil = :profil " + "and profondeur = :profondeur";

        Query query = this.entityManager.createQuery(queryString);
        query.setParameter("profil", profil);
        query.setParameter("profondeur", profondeur);

        try {
            return (M1) query.getSingleResult();
        } catch (Exception e) {
            return null;
        }
    }

}
