package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.impl;


import java.time.LocalDate;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_m;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
public class Recorder_m extends org.inra.ecoinfo.foret.dataset.climatsol.impl.Recorder_m<ProfilHumiditeVol_m, MesureHumiditeVol_m, ValeurHumiditeVol_m> {

    private static final long serialVersionUID = 1L;

    /**
     * empty constructor
     */
    public Recorder_m() {

    }

    /**
     * @param profilHumiditeVol_mDAO
     *            the profilHumiditeVol_mDAO to set
     */
    public void setProfilHumiditeVol_mDAO(IProfilClimatSolDAO<ProfilHumiditeVol_m> profilHumiditeVol_mDAO) {
        this.profilmDAO = profilHumiditeVol_mDAO;
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param originalLineNumber
     * @return
     */
    @Override
    public ProfilHumiditeVol_m getNewProfil(VersionFile versionFileDB, LocalDate date, Long originalLineNumber) {
        return new ProfilHumiditeVol_m(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @param profondeur
     * @return
     */
    @Override
    public MesureHumiditeVol_m getNewMesure(int profondeur) {
        return new MesureHumiditeVol_m(profondeur);
    }

    /**
     *
     * @param dbRealNode
     * @param nbreRepetition
     * @param ecartType
     * @param valeur
     * @return
     */
    @Override
    public ValeurHumiditeVol_m getNewValeur(RealNode dbRealNode, Long nbreRepetition, Float ecartType, Float valeur) {
        return new ValeurHumiditeVol_m(dbRealNode, nbreRepetition, valeur, valeur);
    }
}
