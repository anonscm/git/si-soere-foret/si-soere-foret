/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_m;


/**
 * @author sophie
 * 
 */
public class JPAProfondeurNappe_mDAO extends JPAClimatSolDAO<ValeurProfondeurNappe_m> {

    @Override
    protected Class<ValeurProfondeurNappe_m> getValeurClass() {
        return ValeurProfondeurNappe_m.class;
    }
}
