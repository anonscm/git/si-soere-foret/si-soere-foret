/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_sh;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_infraj extends AbstractProcessRecordClimatSol<ProfilProfondeurNappe_sh, MesureProfondeurNappe_sh, ValeurProfondeurNappe_sh> {

    /**
     *
     */
    public ProcessRecord_infraj() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilProfondeurNappe_sh getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilProfondeurNappe_sh(versionFileDB, date, time, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureProfondeurNappe_sh getNewMesure() {
        return new MesureProfondeurNappe_sh();
    }

    /**
     *
     * @param dbRealNode
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurProfondeurNappe_sh getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurProfondeurNappe_sh(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilProfondeurNappe_sh getNewProfil() {
        return new ProfilProfondeurNappe_sh();
    }
}
