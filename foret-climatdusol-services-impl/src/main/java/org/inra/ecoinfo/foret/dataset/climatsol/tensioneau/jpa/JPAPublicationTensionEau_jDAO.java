/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_j;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_j;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_j;


/**
 * @author sophie
 * 
 */
public class JPAPublicationTensionEau_jDAO extends JPAPublicationClimatSolDAO<ProfilTensionEau_j, MesureTensionEau_j, ValeurTensionEau_j> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurTensionEau_j> getValueClass() {
        return ValeurTensionEau_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureTensionEau_j> getMeasureClass() {
        return MesureTensionEau_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilTensionEau_j> getProfilClass() {
        return ProfilTensionEau_j.class;
    }
}
