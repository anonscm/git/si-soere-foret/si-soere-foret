package org.inra.ecoinfo.foret.extraction.climatsol.impl;

import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.extraction.config.impl.Extraction;
import org.inra.ecoinfo.extraction.exception.NoExtractionResultException;
import org.inra.ecoinfo.extraction.impl.AbstractExtractor;
import org.inra.ecoinfo.foret.extraction.UtilExtraction;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO;
import org.inra.ecoinfo.foret.extraction.climatsol.jsf.ClimatSolParameterVO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 * @author sophie
 *
 */
public class ClimatSolExtractor extends AbstractExtractor {

    /**
     *
     */
    public static final String CLIMATSOL = Constantes.CLIMATSOL;

    /**
     *
     */
    public static final String RYTHME_INVALID = "Le rythme \"%s\" n'est pas un rythme valide.";

    /**
     *
     */
    public static final String CST_RESULTS = "CST_RESULTS=extractionResults";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_SMP_CODE = "extractionResultSMP";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_SWC_CODE = "extractionResultSWC";

    /**
     *
     */
    public static final String CST_RESULT_EXTRACTION_TS_CODE = "extractionResultTS";
    private static final Logger LOGGER = LoggerFactory.getLogger(ClimatSolExtractor.class);
    private static final String RESOURCE_PATH_VARIABLE = "%s/%s/%s/%s";

    private IClimatSolVariableDAO SWCDAO;
    private IClimatSolVariableDAO tSDAO;
    private IClimatSolVariableDAO sMPDAO;
    private IClimatSolVariableDAO gDAO;
    private IClimatSolVariableDAO GWDDAO;

    /**
     *
     */
    /**
     * Empty constructor
     */
    public ClimatSolExtractor() {
        super();
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#extract(org.inra.ecoinfo.extraction.IParameter)
     */
    @Override
    public void extract(IParameter parameters) throws BusinessException {
        long countTotal = 0L;
        ClimatSolParameterVO climatSolParameterVO = (ClimatSolParameterVO) parameters;
        String frequence = climatSolParameterVO.getRythme();
        IntervalDate interval = climatSolParameterVO.getIntervalDate();
        if (null == interval) {
            throw new BusinessException(String.format(UtilExtraction.MSG_INTERVAL_ERROR, this._getDatePattern(frequence)));
        }
        List<SiteForet> selectedSites = climatSolParameterVO.getSelectedSites();
        List<DatatypeVariableUniteForet> selectedVariables = climatSolParameterVO.getSelectedVariables();
        SortedSet<Integer> selectedDepths = climatSolParameterVO.getSelectedsDepths();
        climatSolParameterVO.setValeursExtraites(new HashMap());
        for (DatatypeVariableUniteForet dvu : climatSolParameterVO.getSelectedVariables()) {
            try {
                switch (dvu.getVariable().getCode()) {
                    case Constantes.NAME_VARIABLE_HUMIDITEVOL: {
                        addExtractValues(Constantes.NAME_VARIABLE_HUMIDITEVOL, SWCDAO, climatSolParameterVO, selectedSites, selectedVariables, selectedDepths, interval, frequence);
                        break;
                    }
                    case Constantes.NAME_VARIABLE_TEMPERATURE: {
                        addExtractValues(Constantes.NAME_VARIABLE_TEMPERATURE, tSDAO, climatSolParameterVO, selectedSites, selectedVariables, selectedDepths, interval, frequence);
                        break;
                    }
                    case Constantes.NAME_VARIABLE_TENSIONEAU: {
                        addExtractValues(Constantes.NAME_VARIABLE_TENSIONEAU, sMPDAO, climatSolParameterVO, selectedSites, selectedVariables, selectedDepths, interval, frequence);
                        break;
                    }
                    case Constantes.NAME_VARIABLE_FLUXCHALEUR: {
                       addExtractValues(Constantes.NAME_VARIABLE_FLUXCHALEUR, gDAO, climatSolParameterVO, selectedSites, selectedVariables, selectedDepths, interval, frequence);
                        break;
                    }
                    case Constantes.NAME_VARIABLE_PROFONDEURNAPPE: {
                        addExtractValues(Constantes.NAME_VARIABLE_PROFONDEURNAPPE, GWDDAO, climatSolParameterVO, selectedSites, selectedVariables, selectedDepths, interval, frequence);
                        break;
                    }
                }
            } catch (PersistenceException e) {
                throw new BusinessException(UtilExtraction.MSG_EXTRACTION_ABORTED);
            }
        }
    }

    private List<Object[]> addExtractValues(String constanteVariable, IClimatSolVariableDAO dao, ClimatSolParameterVO climatSolParameterVO, List<SiteForet> selectedSites, List<DatatypeVariableUniteForet> selectedVariables, SortedSet<Integer> selectedDepths, IntervalDate interval, String frequence) throws BusinessException, PersistenceException {
        return climatSolParameterVO.getValeursExtraites().put(constanteVariable, dao.extract(selectedSites, selectedVariables, selectedDepths, interval, frequence, policyManager.getCurrentUser()));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.extraction.IExtractor#setExtraction(org.inra.ecoinfo.config.Extraction)
     */
    @Override
    public void setExtraction(Extraction extraction) {

    }

    /**
     * @param parameter
     * @return
     */
    public String getExtractName(IParameter parameter) {
        return ClimatSolExtractor.CLIMATSOL;
    }

    /**
     * @param gDAO the gDAO to set
     */
    public void setgDAO(IClimatSolVariableDAO gDAO) {
        this.gDAO = gDAO;
    }

    /**
     * @param GWDDAO the GWDDAO to set
     */
    public void setGWDDAO(IClimatSolVariableDAO GWDDAO) {
        this.GWDDAO = GWDDAO;
    }

    /**
     * @param sMPDAO the sMPDAO to set
     */
    public void setsMPDAO(IClimatSolVariableDAO sMPDAO) {
        this.sMPDAO = sMPDAO;
    }

    /**
     * @param SWCDAO the SWCDAO to set
     */
    public void setSWCDAO(IClimatSolVariableDAO SWCDAO) {
        this.SWCDAO = SWCDAO;
    }

    /**
     * @param tSDAO the tSDAO to set
     */
    public void settSDAO(IClimatSolVariableDAO tSDAO) {
        this.tSDAO = tSDAO;
    }

    /**
     * @param frequence
     * @return
     */
    private String _getDatePattern(String frequence) {
        return frequence.equals(Constantes.MENSUEL) ? DateUtil.MM_YYYY : DateUtil.DD_MM_YYYY;
    }

    /**
     * @param site
     * @param theme
     * @param datatype
     * @param role
     * @return
     */
    /*
     * public boolean hasRoleforSiteThemeDatatype(String site, String theme, String datatype, String role) { try { return policyManager.hasPrivilege(String.format(RESOURCE_PATH, site, theme, datatype), role,
     * ISecurityDatasetManager.PRIVILEGE_TYPE_DATASET); } catch (Exception e) { return false; } }
     */
    /**
     * AbstractExtractor
     *
     * @return
     * @throws org.inra.ecoinfo.extraction.exception.NoExtractionResultException
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException  **
     */
    /*
     * (non-Javadoc)
     *
    * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#extractDatas(java.util.Map)
     */
    @Override
    protected Map<String, List> extractDatas(Map<String, Object> requestMetadatasMap) throws NoExtractionResultException, BusinessException {

        return null;
    }

    /*
     * (non-Javadoc)
     *
    * @see org.inra.ecoinfo.extraction.impl.AbstractExtractor#prepareRequestMetadatas(java.util.Map)
     */
    @Override
    protected void prepareRequestMetadatas(Map<String, Object> requestMetadatasMap) throws BusinessException {

    }

}
