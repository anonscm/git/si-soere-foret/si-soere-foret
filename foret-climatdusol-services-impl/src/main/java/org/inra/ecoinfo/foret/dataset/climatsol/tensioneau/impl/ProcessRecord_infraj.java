/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_sh;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_infraj extends AbstractProcessRecordClimatSol<ProfilTensionEau_sh, MesureTensionEau_sh, ValeurTensionEau_sh> {

    /**
     *
     */
    public ProcessRecord_infraj() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilTensionEau_sh getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilTensionEau_sh(versionFileDB, date, time, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureTensionEau_sh getNewMesure() {
        return new MesureTensionEau_sh();
    }

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurTensionEau_sh getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurTensionEau_sh(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilTensionEau_sh getNewProfil() {
        return new ProfilTensionEau_sh();
    }
}
