package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAMesureHumiditeVol_jDAO extends JPAMesureClimatSolDAO<ProfilHumiditeVol_j, MesureHumiditeVol_j> {

}
