package org.inra.ecoinfo.foret.extraction.climatsol.impl;

import java.io.File;
import java.io.PrintStream;
import java.util.Collections;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import java.util.SortedSet;
import java.util.stream.Collectors;
import org.apache.commons.collections.CollectionUtils;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.foret.extraction.climatsol.jsf.ClimatSolParameterVO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.composite.Nodeable;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.DateUtil;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;

/**
 *
 * @author ptcherniati
 */
public class CSRequestReminderOutputBuilder extends CSOutputsBuildersResolver {

    private static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.extraction.climatsol.messages";
    private static final String PROPERTY_MSG_HEADER = "PROPERTY_MSG_HEADER";
    private static final String PROPERTY_MSG_SELECTED_DATES = "PROPERTY_MSG_SELECTED_DATES";
    private static final String PROPERTY_MSG_SELECTED_FREQ = "PROPERTY_MSG_SELECTED_FREQ";
    private static final String PROPERTY_MSG_SELECTED_INTERVAL = "PROPERTY_MSG_SELECTED_INTERVAL";
    private static final String PROPERTY_MSG_SELECTED_SITES = "PROPERTY_MSG_SELECTED_SITES";
    private static final String PROPERTY_MSG_SELECTED_VARIABLES = "PROPERTY_MSG_SELECTED_VARIABLES";
    private static final String PROPERTY_MSG_SELECTED_DEPTHS = "PROPERTY_MSG_SELECTED_DEPTHS";
    private static final String PROPERTY_MSG_SELECTED_DISPLAY = "PROPERTY_MSG_SELECTED_DISPLAY";
    private static final String PROPERTY_MSG_COMMENT = "PROPERTY_MSG_COMMENT";
    private static final String PROPERTY_MSG_EUROPEAN_FORMAT = "PROPERTY_MSG_EUROPEAN_FORMAT";
    private static final String PROPERTY_MSG_CLASSIC_FORMAT = "PROPERTY_MSG_CLASSIC_FORMAT";

    private static final String CST_REQUEST_REMINDER = Constantes.CST_REQUEST_REMINDER;
    private final static String MOTIF = Constantes.MOTIF;
    private final static String LIST_PATTERN = Constantes.LIST_PATTERN;
    private final static String SEPARATOR_PATTERN = Constantes.SEPARATOR_PATTERN;
    private static final String EXTENSION_TXT = Constantes.EXTENSION_TXT;
    private static final String TAB = Constantes.TAB;
    private static final String PARENTHESE_OUVRANTE = Constantes.CST_PARENTHESE_OUVRANTE;
    private static final String PARENTHESE_FERMANTE = Constantes.CST_PARENTHESE_FERMANTE;
    private static final String COLUMN_NOM_TABLE_VARIABLE = Constantes.COLUMN_NOM_TABLE_VARIABLE;

    private Properties propertiesVariablesNames;

    /**
     * @return the propertiesVariablesNames
     */
    public Properties getPropertiesVariablesNames() {
        return this.propertiesVariablesNames;
    }

    /**
     * @param propertiesVariablesNames the propertiesVariablesNames to set
     */
    public void setPropertiesVariablesNames(Properties propertiesVariablesNames) {
        this.propertiesVariablesNames = propertiesVariablesNames;
    }

    private void _outputPrintAffichage(PrintStream outputSteam, int affichage) {
        outputSteam.println(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DISPLAY));
        outputSteam.printf(String.format(CSRequestReminderOutputBuilder.SEPARATOR_PATTERN, affichage == Constantes.EUROPEAN_FORMAT ? this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_EUROPEAN_FORMAT) : this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_CLASSIC_FORMAT)));
    }

    private void _outputPrintCommentaire(PrintStream outputSteam, String commentaire) {
        outputSteam.println(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_COMMENT));
        outputSteam.printf(String.format(CSRequestReminderOutputBuilder.SEPARATOR_PATTERN, commentaire));
    }

    private void _outputPrintDepths(PrintStream outputSteam, SortedSet<Integer> selectedDepths) {

        outputSteam.println(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DEPTHS));
        outputSteam.print(CSRequestReminderOutputBuilder.TAB);
        int i = 1;
        for (Integer depth : selectedDepths) {
            outputSteam.printf(String.format("%d%s", depth, i == selectedDepths.size() ? "" : ", "));
            i++;
        }
    }

    private void _outputPrintFreqDates(PrintStream outputSteam, String rythme, IntervalDate intervalDate) {
        outputSteam.append(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_FREQ));
        outputSteam.println();
        outputSteam.append(String.format(CSRequestReminderOutputBuilder.LIST_PATTERN, rythme));
        outputSteam.println();
        outputSteam.println();
        outputSteam.println(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_DATES));
        outputSteam.append(String.format(
                this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, 
                        CSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_INTERVAL), 
                DateUtil.getUTCDateTextFromLocalDateTime(intervalDate.getBeginDate(), Constantes.MENSUEL.equals(rythme)?DateUtil.MM_YYYY:DateUtil.DD_MM_YYYY), 
                DateUtil.getUTCDateTextFromLocalDateTime(intervalDate.getEndDate(), Constantes.MENSUEL.equals(rythme)?DateUtil.MM_YYYY:DateUtil.DD_MM_YYYY)
        ));
    }

    private void _outputPrintSites(PrintStream outputSteam, List<SiteForet> selectedSites) {
        if (selectedSites != null && selectedSites.size() > 0) {
            outputSteam.println(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_SITES));
            for (SiteForet site : selectedSites) {
                outputSteam.append(String.format(CSRequestReminderOutputBuilder.LIST_PATTERN, localizationManager.getLocalName(site)));
                outputSteam.println();
            }
        }
    }

    private void _outputPrintVariables(PrintStream outputSteam, List<DatatypeVariableUniteForet> selectedVariables) {
        List<Variable> variables = selectedVariables.stream()
                .collect(Collectors.groupingBy(dvu->dvu.getVariable()))
                .keySet()
                .stream()
                .collect(Collectors.toList());
        Collections.sort(variables);
        propertiesVariablesNames = this.localizationManager.newProperties(Nodeable.getLocalisationEntite(VariableForet.class), Nodeable.ENTITE_COLUMN_NAME, localizationManager.getUserLocale().getLocale());
        if (!CollectionUtils.isEmpty(variables)) {
            outputSteam.println(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_SELECTED_VARIABLES));
            for (Variable variable : variables) {
                String localizedName = propertiesVariablesNames.getProperty(variable.getDefinition(),variable.getDefinition());
                outputSteam.append(String.format(CSRequestReminderOutputBuilder.LIST_PATTERN, variable.getCode())).append(CSRequestReminderOutputBuilder.PARENTHESE_OUVRANTE).append(localizedName)
                        .append(CSRequestReminderOutputBuilder.PARENTHESE_FERMANTE);
                outputSteam.println();
            }
        }

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildBody(java.lang.String, java.util.Map, java.util.Map)
     */
    @Override
    protected Map<String, File> buildBody(String headers, Map<String, List> resultsDatasMap, Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.extraction.impl.AbstractOutputBuilder#buildHeader(java.util.Map)
     */
    @Override
    protected String buildHeader(Map<String, Object> requestMetadatasMap) throws BusinessException {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.impl.CSOutputsBuildersResolver#buildOutputData(org.inra.ecoinfo.extraction.IParameter, java.util.Map)
     */
    /**
     *
     * @param parameters
     * @param outputPrintStreamMap
     */
    @Override
    protected void buildOutputData(IParameter parameters, Map<String, PrintStream> outputPrintStreamMap) {
        ClimatSolParameterVO climatSolParameterVO = (ClimatSolParameterVO) parameters;
        for (String datatypeName : outputPrintStreamMap.keySet()) {
            PrintStream outputSteam = outputPrintStreamMap.get(datatypeName);
            outputSteam.println(this.getLocalizationManager().getMessage(CSRequestReminderOutputBuilder.BUNDLE_SOURCE_PATH, CSRequestReminderOutputBuilder.PROPERTY_MSG_HEADER));
            outputSteam.println(CSRequestReminderOutputBuilder.MOTIF);
            outputSteam.println();
            outputSteam.println();
            this._outputPrintSites(outputSteam, climatSolParameterVO.getSelectedSites());
            outputSteam.println();
            this._outputPrintFreqDates(outputSteam, climatSolParameterVO.getRythme(), climatSolParameterVO.getIntervalDate());
            outputSteam.println();
            outputSteam.println();
            this._outputPrintVariables(outputSteam, climatSolParameterVO.getSelectedVariables());
            outputSteam.println();
            this._outputPrintDepths(outputSteam, climatSolParameterVO.getSelectedsDepths());
            outputSteam.println();
            outputSteam.println();
            this._outputPrintAffichage(outputSteam, climatSolParameterVO.getAffichage());
            outputSteam.println();
            outputSteam.println();
            this._outputPrintCommentaire(outputSteam, climatSolParameterVO.getCommentaire());
        }

    }

    /**
     *
     * @param outputHelper
     */
    @Override
    protected void displayEmptyValue(BuildOutputHelper outputHelper) {
        

    }

    /**
     *
     * @param outputHelper
     * @param value
     */
    @Override
    protected void displayValue(BuildOutputHelper outputHelper, String value) {
        

    }

    /**
     *
     * @param outputHelper
     * @param value
     * @param columValue
     */
    @Override
    protected void displayValue(BuildOutputHelper outputHelper, String value, String columValue) {
        

    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.impl.CSOutputsBuildersResolver#getExtention()
     */
    /**
     *
     * @return
     */
    @Override
    protected String getExtention() {
        return CSRequestReminderOutputBuilder.EXTENSION_TXT;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.impl.CSOutputsBuildersResolver#getPrefixFileName()
     */
    /**
     *
     * @return
     */
    @Override
    protected String getPrefixFileName() {
        return CSRequestReminderOutputBuilder.CST_REQUEST_REMINDER;
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.foret.extraction.climatsol.impl.CSOutputsBuildersResolver#buildHeader(org.inra.ecoinfo.foret.extraction.climatsol.impl.CSOutputsBuildersResolver.BuildOutputHelper)
     */
    @Override
    void buildHeader(BuildOutputHelper outputHelper, ClimatSolParameterVO climatSolParameterVO) {
        

    }

}
