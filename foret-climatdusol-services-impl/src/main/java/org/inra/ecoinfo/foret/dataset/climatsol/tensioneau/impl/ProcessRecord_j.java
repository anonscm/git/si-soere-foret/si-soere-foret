/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_j;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_j;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_j;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_j extends AbstractProcessRecordClimatSol<ProfilTensionEau_j, MesureTensionEau_j, ValeurTensionEau_j> {

    /**
     *
     */
    public ProcessRecord_j() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilTensionEau_j getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilTensionEau_j(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureTensionEau_j getNewMesure() {
        return new MesureTensionEau_j();
    }

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurTensionEau_j getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurTensionEau_j(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilTensionEau_j getNewProfil() {
        return new ProfilTensionEau_j();
    }
}
