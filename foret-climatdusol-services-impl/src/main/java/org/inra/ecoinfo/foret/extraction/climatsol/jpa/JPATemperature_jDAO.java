package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_j;

/**
 * @author sophie
 *
 */
public class JPATemperature_jDAO extends JPAClimatSolDAO<ValeurTemperature_j> {

    @Override
    protected Class<ValeurTemperature_j> getValeurClass() {
        return ValeurTemperature_j.class;
    }
}
