/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import java.util.ArrayList;
import java.util.List;
import java.util.SortedSet;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_j;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_m;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_sh;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolDAO;
import org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * 
 */
public class JPAClimatSolSMPDAO implements IClimatSolVariableDAO {

    private IClimatSolDAO<ValeurTensionEau_j> tensionEau_jDAO;
    private IClimatSolDAO<ValeurTensionEau_sh> tensionEau_shDAO;
    private IClimatSolDAO<ValeurTensionEau_m> tensionEau_mDAO;

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#extract(java.util.List, java.util.List, java.util.SortedSet, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     * @throws BusinessException
     */
    
    @Override
    public List<Object[]> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user) throws PersistenceException, BusinessException {
        List<Object[]> lstValeurExtraite = new ArrayList<>();
        switch (frequence) {
            case Constantes.JOURNALIER:
                {
                    List<ValeurTensionEau_j> valeurs = this.tensionEau_jDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                    for (ValeurTensionEau_j valeur : valeurs) {
                        Object[] valeurExtraite = new Object[2];
                        valeurExtraite[0] = valeur;
                        valeurExtraite[1] = valeur.getNbreRepetition();
                        lstValeurExtraite.add(valeurExtraite);
                    }       break;
                }
            case Constantes.INFRA_JOURNALIER:
            {
                List<ValeurTensionEau_sh> valeurs = this.tensionEau_shDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurTensionEau_sh valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getMesure().getProfil().getHeure();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
            case Constantes.MENSUEL:
            {
                List<ValeurTensionEau_m> valeurs = this.tensionEau_mDAO.extract(lstSelectedSitesForet, lstSelectedVariables, lstSelectedDepths, interval, frequence, user);
                for (ValeurTensionEau_m valeur : valeurs) {
                    Object[] valeurExtraite = new Object[3];
                    valeurExtraite[0] = valeur;
                    valeurExtraite[1] = valeur.getNbreRepetition();
                    valeurExtraite[2] = valeur.getEcartType();
                    lstValeurExtraite.add(valeurExtraite);
            }       break;
                }
        }

        return lstValeurExtraite;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesDepths(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Integer> lstDephts = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstDephts = this.tensionEau_jDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstDephts = this.tensionEau_shDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstDephts = this.tensionEau_mDAO.getAvailablesDepths(lstSelectedSiteForets, interval);
                break;
        }

        return lstDephts;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.extraction.climatsol.IClimatSolVariableDAO#getAvailablesVariables(java.util.List, org.inra.ecoinfo.utils.IntervalDate, java.lang.String)
     */

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     */
    
    @Override
    public List<Variable> getAvailablesVariables(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) {
        List<Variable> lstVariables = null;

        switch (frequence) {
            case Constantes.JOURNALIER:
                lstVariables = this.tensionEau_jDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.INFRA_JOURNALIER:
                lstVariables = this.tensionEau_shDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
            case Constantes.MENSUEL:
                lstVariables = this.tensionEau_mDAO.getAvailablesVariables(lstSelectedSiteForets, interval);
                break;
        }

        return lstVariables;
    }

    /**
     * @param tensionEau_jDAO
     *            the tensionEau_jDAO to set
     */
    public void setTensionEau_jDAO(IClimatSolDAO tensionEau_jDAO) {
        this.tensionEau_jDAO = tensionEau_jDAO;
    }

    /**
     * @param tensionEau_mDAO
     *            the tensionEau_mDAO to set
     */
    public void setTensionEau_mDAO(IClimatSolDAO tensionEau_mDAO) {
        this.tensionEau_mDAO = tensionEau_mDAO;
    }

    /**
     * @param tensionEau_shDAO
     *            the tensionEau_shDAO to set
     */
    public void setTensionEau_shDAO(IClimatSolDAO tensionEau_shDAO) {
        this.tensionEau_shDAO = tensionEau_shDAO;
    }
}
