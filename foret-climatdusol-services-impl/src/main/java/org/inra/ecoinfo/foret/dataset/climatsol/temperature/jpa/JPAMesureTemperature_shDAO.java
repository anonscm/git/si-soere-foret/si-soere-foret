/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_sh;


/**
 * @author sophie
 * 
 */
public class JPAMesureTemperature_shDAO extends JPAMesureClimatSolDAO<ProfilTemperature_sh, MesureTemperature_sh> {

}
