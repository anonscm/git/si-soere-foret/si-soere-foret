/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_sh;


/**
 * @author sophie
 * 
 */
public class JPAPublicationTensionEau_shDAO extends JPAPublicationClimatSolDAO<ProfilTensionEau_sh, MesureTensionEau_sh, ValeurTensionEau_sh> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurTensionEau_sh> getValueClass() {
        return ValeurTensionEau_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureTensionEau_sh> getMeasureClass() {
        return MesureTensionEau_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilTensionEau_sh> getProfilClass() {
        return ProfilTensionEau_sh.class;
    }
}