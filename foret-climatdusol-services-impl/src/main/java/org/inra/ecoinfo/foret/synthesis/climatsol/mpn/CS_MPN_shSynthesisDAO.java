package org.inra.ecoinfo.foret.synthesis.climatsol.mpn;

import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_sh;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mpnsh.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mpnsh.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MPN_shSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurProfondeurNappe_sh, MesureProfondeurNappe_sh, ProfilProfondeurNappe_sh> {

    public CS_MPN_shSynthesisDAO() {
        super(ValeurProfondeurNappe_sh.class);
    }
}
