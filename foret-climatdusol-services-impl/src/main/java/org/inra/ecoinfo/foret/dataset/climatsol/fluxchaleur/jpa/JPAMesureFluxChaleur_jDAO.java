package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAMesureFluxChaleur_jDAO extends JPAMesureClimatSolDAO<ProfilFluxChaleur_j, MesureFluxChaleur_j> {

}
