/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_j;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_j;


/**
 * @author sophie
 * 
 */
public class JPAMesureTemperature_jDAO extends JPAMesureClimatSolDAO<ProfilTemperature_j, MesureTemperature_j> {

}
