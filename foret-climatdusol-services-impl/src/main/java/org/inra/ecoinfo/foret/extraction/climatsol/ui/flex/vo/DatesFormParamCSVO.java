package org.inra.ecoinfo.foret.extraction.climatsol.ui.flex.vo;

import java.io.ByteArrayOutputStream;
import java.io.PrintStream;
import java.io.UnsupportedEncodingException;
import java.nio.charset.StandardCharsets;
import java.time.DateTimeException;
import java.time.LocalDate;
import java.time.temporal.TemporalAdjusters;
import java.util.HashMap;
import java.util.List;
import java.util.Map;
import java.util.Optional;
import java.util.stream.Collectors;
import javax.faces.component.ValueHolder;
import javax.faces.event.AjaxBehaviorEvent;
import org.inra.ecoinfo.foret.extraction.jsf.date.AbstractDatesFormParam;
import org.inra.ecoinfo.foret.extraction.jsf.date.IDateFormParameter;
import org.inra.ecoinfo.foret.extraction.jsf.date.Periode;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.utils.DateUtil;

/**
 * The Class DatesYearsContinuousFormParamVO.
 */
public class DatesFormParamCSVO extends AbstractDatesFormParam implements IDateFormParameter {

    /**
     * The Constant BUNDLE_SOURCE_PATH.
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.extraction.climatsol.jsf.extraction-climatsol-jsf";

    /**
     *
     */
    public static final String JOURNALIER_LONG = "MSG_FREQ_DATE_STEP_J_RADIOBUTTON_LABEL";

    /**
     *
     */
    public static final String MENSUEL_LONG = "MSG_FREQ_DATE_STEP_M_RADIOBUTTON_LABEL";

    /**
     *
     */
    public static final String INFRA_JOURNALIER_LONG = "MSG_FREQ_DATE_STEP_INFRAJ_RADIOBUTTON_LABEL";

    /**
     *
     */
    protected static final String BUNDLE_PATH = "org.inra.ecoinfo.foret.ui.flex.vo.messages";

    /**
     *
     * @param localizationManager
     */
    public DatesFormParamCSVO(ILocalizationManager localizationManager) {
        super(localizationManager);
        this.rythme = Constantes.INFRA_JOURNALIER;
        this.frequences.put(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, JOURNALIER_LONG)), Constantes.JOURNALIER);
        this.frequences.put(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, MENSUEL_LONG)), Constantes.MENSUEL);
        this.frequences.put(String.format(localizationManager.getMessage(BUNDLE_SOURCE_PATH, INFRA_JOURNALIER_LONG)), Constantes.INFRA_JOURNALIER);
        periods.add(new HashMap());
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#buildSummary(java.io.PrintStream
     * )
     */
    /**
     *
     * @param printStream
     * @throws DateTimeException
     */
    @Override
    public void buildSummary(PrintStream printStream) throws DateTimeException {
        String startdate = periods.get(0).get(AbstractDatesFormParam.START_INDEX);
        String endDate = periods.get(0).get(AbstractDatesFormParam.END_INDEX);
        if (Constantes.MENSUEL.equals(rythme)) {
            startdate = DateToMensualDate(startdate);
            endDate = DateToMensualDate(endDate);
        }
        printStream.print(
                String.format(
                        getLocalizationManager().getMessage(AbstractDatesFormParam.BUNDLE_SOURCE_PATH, PROPERTY_MSG_FROM_TO),
                        startdate,
                        endDate
                ));
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#customValidate()
     */
    /**
     *
     */
    @Override
    public void customValidate() {
        LOGGER.info("unused");
    }

    /*
     * (non-Javadoc)
     *
     * @see
     * org.inra.ecoinfo.acbb.extraction.jsf.IDateFormParameter#getPeriodsFromDateFormParameter()
     */
    @Override
    public List<Periode> getPeriodsFromDateFormParameter() {
        return getPeriods().stream()
                .map(period -> new Periode(period.get(AbstractDatesFormParam.START_INDEX), period.get(AbstractDatesFormParam.END_INDEX)))
                .collect(Collectors.toList());
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#getSummaryHTML()
     */
    /**
     *
     * @return @throws DateTimeException
     */
    @Override
    public String getSummaryHTML() throws DateTimeException {
        final ByteArrayOutputStream bos = new ByteArrayOutputStream();
        final PrintStream printStream;
        try {
            printStream = new PrintStream(bos, true, StandardCharsets.UTF_8.name());
            if (!getIsValid()) {
                printValidityMessages(printStream);
                return bos.toString();
            }
            intervalsDate().stream().forEach(printStream::println);
        } catch (UnsupportedEncodingException ex) {
            LOGGER.error(ex.getMessage(), ex);
        }
        return bos.toString();
    }

    /*
     * (non-Javadoc)
     *
     * @see org.inra.ecoinfo.acbb.extraction.jsf.AbstractDatesFormParam#isEmpty()
     */
    /**
     *
     * @return
     */
    @Override
    public boolean isEmpty() {
        for (final Map<String, String> periodsMap : periods) {
            if (periodsMap.entrySet().stream().anyMatch((entry) -> (periodsMap.get(entry.getKey()).isEmpty()))) {
                return true;
            }
        }
        return false;
    }

    /**
     *
     * @param event
     */
    public void changeDateStart(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        setDateStart(date);
        periods.get(0).put("start", date == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(date, getPatternDate()));
    }

    /**
     *
     * @param event
     */
    public void changeDateEnd(AjaxBehaviorEvent event) {
        LocalDate date = (LocalDate) ((ValueHolder) event.getSource()).getValue();
        date = Optional.ofNullable(date)
                .filter(d->Constantes.MENSUEL.equals(rythme))
                .map(d->d.with(TemporalAdjusters.lastDayOfMonth()))
                .orElse(date);
        setDateEnd(date);
        periods.get(0).put("end", date == null ? "" : DateUtil.getUTCDateTextFromLocalDateTime(date, getPatternDate()));
    }

    /**
     *
     * @return
     */
    @Override
    public DatesFormParamCSVO copy() {
        DatesFormParamCSVO copy = new DatesFormParamCSVO(localizationManager);
        copy.dateStart = dateStart;
        copy.dateEnd = dateEnd;
        copy.periods = periods;
        copy.rythme = rythme;
        this.frequences = frequences;
        return copy;
    }

    @Override
    public void setPeriods(List<Map<String, String>> periods) {
        this.periods = periods;
    }

    /**
     *
     * @return
     */
    public Map<String, String> getFrequences() {
        return frequences;
    }

    /**
     *
     * @return
     */
    @Override
    public String getRythme() {
        return rythme;
    }

    /**
     *
     * @param rythme
     */
    @Override
    public void setRythme(String rythme) {
        if(Constantes.JOURNALIER.equals(rythme) || Constantes.MENSUEL.equals(rythme) || Constantes.INFRA_JOURNALIER.equals(rythme)|| Constantes.SEMI_HORAIRE.equals(rythme)){
            this.rythme = rythme;
        }
    }

    private static String DateToMensualDate(String date) {
        return Optional.ofNullable(date)
                .map(sd -> DateUtil.readLocalDateFromText(DateUtil.DD_MM_YYYY, sd))
                .map(sd -> DateUtil.getUTCDateTextFromLocalDateTime(sd, DateUtil.MM_YYYY))
                .orElse(date);
    }

}
