package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAPublicationHumiditeVol_shDAO extends JPAPublicationClimatSolDAO<ProfilHumiditeVol_sh, MesureHumiditeVol_sh, ValeurHumiditeVol_sh> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurHumiditeVol_sh> getValueClass() {
        return ValeurHumiditeVol_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureHumiditeVol_sh> getMeasureClass() {
        return MesureHumiditeVol_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilHumiditeVol_sh> getProfilClass() {
        return ProfilHumiditeVol_sh.class;
    }
}