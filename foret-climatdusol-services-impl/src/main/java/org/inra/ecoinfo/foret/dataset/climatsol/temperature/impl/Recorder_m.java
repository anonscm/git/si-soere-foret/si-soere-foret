/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.impl;


import java.time.LocalDate;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.IProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_m;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_m;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_m;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class Recorder_m extends org.inra.ecoinfo.foret.dataset.climatsol.impl.Recorder_m<ProfilTemperature_m, MesureTemperature_m, ValeurTemperature_m> {

    private static final long serialVersionUID = 1L;

    /**
     * empty constructor
     */
    public Recorder_m() {

    }

    /**
     * @param profilTemperature_mDAO
     *            the profilTemperature_mDAO to set
     */
    public void setProfilTemperature_mDAO(IProfilClimatSolDAO<ProfilTemperature_m> profilTemperature_mDAO) {
        this.profilmDAO = profilTemperature_mDAO;
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param originalLineNumber
     * @return
     */
    @Override
    public ProfilTemperature_m getNewProfil(VersionFile versionFileDB, LocalDate date, Long originalLineNumber) {
        return new ProfilTemperature_m(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @param profondeur
     * @return
     */
    @Override
    public MesureTemperature_m getNewMesure(int profondeur) {
        return new MesureTemperature_m(profondeur);
    }

    /**
     *
     * @param debRealNode
     * @param nbreRepetition
     * @param ecartType
     * @param valeur
     * @return
     */
    @Override
    public ValeurTemperature_m getNewValeur(RealNode debRealNode, Long nbreRepetition, Float ecartType, Float valeur) {
        return new ValeurTemperature_m(debRealNode, nbreRepetition, ecartType, valeur);
    }

}
