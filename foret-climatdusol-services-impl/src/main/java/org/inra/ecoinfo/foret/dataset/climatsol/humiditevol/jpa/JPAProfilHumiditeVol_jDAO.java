package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAProfilHumiditeVol_jDAO extends JPAProfilClimatSolDAO<ProfilHumiditeVol_j> {

}
