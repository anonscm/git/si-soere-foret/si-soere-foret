/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol;

import java.util.List;
import java.util.SortedSet;
import org.inra.ecoinfo.IDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;


/**
 * @author sophie
 * @param <V0>
 * 
 */
public interface IClimatSolDAO<V0 extends ValeurClimatSol> extends IDAO<V0> {

    /**
     *
     * @param lstSelectedSitesForet
     * @param lstSelectedVariables
     * @param lstSelectedDepths
     * @param interval
     * @param frequence
     * @param user
     * @return
     * @throws PersistenceException
     */
    List<V0> extract(List<SiteForet> lstSelectedSitesForet, List<DatatypeVariableUniteForet> lstSelectedVariables, SortedSet<Integer> lstSelectedDepths, IntervalDate interval, String frequence, IUser user);

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @return
     */
    List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, IntervalDate interval);

    /**
     *
     * @param lstSiteForets
     * @param interval
     * @return
     */
    List<Variable> getAvailablesVariables(List<SiteForet> lstSiteForets, IntervalDate interval);

}
