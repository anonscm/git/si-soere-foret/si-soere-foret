package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_sh;


/**
 * @author sophie
 * 
 */
public class JPATemperature_shDAO extends JPAClimatSolDAO<ValeurTemperature_sh> {

    @Override
    protected Class<ValeurTemperature_sh> getValeurClass() {
        return ValeurTemperature_sh.class;
    }
}
