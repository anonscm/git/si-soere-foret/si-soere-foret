package org.inra.ecoinfo.foret.synthesis.climatsol.mhv;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_sh;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mhvsh.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mhvsh.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MHV_shSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurHumiditeVol_sh, MesureHumiditeVol_sh, ProfilHumiditeVol_sh> {

    public CS_MHV_shSynthesisDAO() {
        super(ValeurHumiditeVol_sh.class);
    }
}
