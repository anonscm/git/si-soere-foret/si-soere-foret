/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_j extends AbstractProcessRecordClimatSol<ProfilFluxChaleur_j, MesureFluxChaleur_j, ValeurFluxChaleur_j> {

    /**
     *
     */
    public ProcessRecord_j() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilFluxChaleur_j getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilFluxChaleur_j(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureFluxChaleur_j getNewMesure() {
        return new MesureFluxChaleur_j();
    }

    /**
     *
     * @param realNode
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurFluxChaleur_j getNewValeur(RealNode realNode, Long noRepetition, Float valeur) {
        return new ValeurFluxChaleur_j(realNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilFluxChaleur_j getNewProfil() {
        return new ProfilFluxChaleur_j();
    }
}
