package org.inra.ecoinfo.foret.synthesis.climatsol.mhv;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_j;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mhvj.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mhvj.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MHV_jSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurHumiditeVol_j, MesureHumiditeVol_j, ProfilHumiditeVol_j> {

    public CS_MHV_jSynthesisDAO() {
        super(ValeurHumiditeVol_j.class);
    }
}
