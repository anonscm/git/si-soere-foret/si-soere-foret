/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_m;


/**
 * @author sophie
 * 
 */
public class JPATemperature_mDAO extends JPAClimatSolDAO<ValeurTemperature_m> {

    @Override
    protected Class<ValeurTemperature_m> getValeurClass() {
        return ValeurTemperature_m.class;
    }
}
