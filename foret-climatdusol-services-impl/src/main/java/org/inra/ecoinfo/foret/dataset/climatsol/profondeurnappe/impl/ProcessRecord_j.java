/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_j;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_j;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_j;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_j extends AbstractProcessRecordClimatSol<ProfilProfondeurNappe_j, MesureProfondeurNappe_j, ValeurProfondeurNappe_j> {

    /**
     *
     */
    public ProcessRecord_j() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilProfondeurNappe_j getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilProfondeurNappe_j(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureProfondeurNappe_j getNewMesure() {
        return new MesureProfondeurNappe_j();
    }

    /**
     *
     * @param dbRealNode
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurProfondeurNappe_j getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurProfondeurNappe_j(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilProfondeurNappe_j getNewProfil() {
        return new ProfilProfondeurNappe_j();
    }
}
