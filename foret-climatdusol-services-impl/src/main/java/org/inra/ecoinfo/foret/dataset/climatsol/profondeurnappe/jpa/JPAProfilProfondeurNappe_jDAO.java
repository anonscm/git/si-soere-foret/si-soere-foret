/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_j;


/**
 * @author sophie
 * 
 */
public class JPAProfilProfondeurNappe_jDAO extends JPAProfilClimatSolDAO<ProfilProfondeurNappe_j> {

}
