/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_m;


/**
 * @author sophie
 * 
 */
public class JPAProfilProfondeurNappe_mDAO extends JPAProfilClimatSolDAO<ProfilProfondeurNappe_m> {

}
