/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.MesureFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ProfilFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity.ValeurFluxChaleur_j;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAPublicationFluxChaleur_jDAO extends JPAPublicationClimatSolDAO<ProfilFluxChaleur_j, MesureFluxChaleur_j, ValeurFluxChaleur_j> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurFluxChaleur_j> getValueClass() {
        return ValeurFluxChaleur_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureFluxChaleur_j> getMeasureClass() {
        return MesureFluxChaleur_j.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilFluxChaleur_j> getProfilClass() {
        return ProfilFluxChaleur_j.class;
    }
}