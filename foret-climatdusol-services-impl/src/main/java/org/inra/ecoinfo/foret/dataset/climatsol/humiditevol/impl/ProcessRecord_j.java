/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_j;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_j extends AbstractProcessRecordClimatSol<ProfilHumiditeVol_j, MesureHumiditeVol_j, ValeurHumiditeVol_j> {

    /**
     *
     */
    public ProcessRecord_j() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilHumiditeVol_j getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilHumiditeVol_j(versionFileDB, date, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureHumiditeVol_j getNewMesure() {
        return new MesureHumiditeVol_j();
    }

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurHumiditeVol_j getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurHumiditeVol_j(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilHumiditeVol_j getNewProfil() {
        return new ProfilHumiditeVol_j();
    }
}
