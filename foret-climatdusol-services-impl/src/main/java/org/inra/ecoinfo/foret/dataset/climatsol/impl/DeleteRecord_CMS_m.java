
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import com.Ostermiller.util.CSVParser;
import java.io.ByteArrayInputStream;
import java.io.IOException;
import java.io.InputStreamReader;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import org.inra.ecoinfo.dataset.versioning.IRepositoryManagerVersions;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.ILocalPublicationDAO;
import org.inra.ecoinfo.foret.dataset.impl.DeleteRecord;
import org.inra.ecoinfo.foret.refdata.variable.IVariableForetDAO;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.foret.utils.Constantes;
import static org.inra.ecoinfo.foret.utils.Constantes.CSY_CSV_SEPARATOR8CHAR;
import org.inra.ecoinfo.utils.Utils;
import org.inra.ecoinfo.utils.exceptions.BadFormatException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;


/**
 * 
 * @author ptcherniati
 */
public class DeleteRecord_CMS_m extends DeleteRecord {
    private static final Logger LOGGER = LoggerFactory.getLogger(DeleteRecord_CMS_m.class);

    /**
     *
     */
    protected IRepositoryManagerVersions repositoryManagerVersions;

    /**
     *
     */
    protected ILocalPublicationDAO publicationHumiditeVol_mDAO;

    /**
     *
     */
    protected ILocalPublicationDAO publicationTemperature_mDAO;

    /**
     *
     */
    protected ILocalPublicationDAO publicationFluxChaleur_mDAO;

    /**
     *
     */
    protected ILocalPublicationDAO publicationTensionEau_mDAO;

    /**
     *
     */
    protected ILocalPublicationDAO publicationProfondeurNappe_mDAO;

    /**
     *
     */
    protected DatasetDescriptorCS datasetDescriptor;
    IVariableForetDAO variableDAO;

    @Override
    public void deleteRecord(VersionFile versionFile) throws BusinessException {

        try {
            String encoding = Utils.detectStreamEncoding(versionFile.getData());
            CSVParser parser = new CSVParser(new InputStreamReader(new ByteArrayInputStream(versionFile.getData()), encoding), CSY_CSV_SEPARATOR8CHAR);

            List<VariableForet> lstVariableForet = this.buildVariableHeaderAndSkipHeader(parser);

            Iterator<VariableForet> itr = lstVariableForet.iterator();
            while (itr.hasNext()) {
                String varName = itr.next().getName();

                switch (varName) {
                    case Constantes.NAME_VARIABLE_HUMIDITEVOL:
                        this.publicationHumiditeVol_mDAO.removeVersion(versionFile.getId());
                        break;
                    case Constantes.NAME_VARIABLE_TEMPERATURE:
                        this.publicationTemperature_mDAO.removeVersion(versionFile.getId());
                        break;
                    case Constantes.NAME_VARIABLE_FLUXCHALEUR:
                        this.publicationFluxChaleur_mDAO.removeVersion(versionFile.getId());
                        break;
                    case Constantes.NAME_VARIABLE_TENSIONEAU:
                        this.publicationTensionEau_mDAO.removeVersion(versionFile.getId());
                        break;
                    case Constantes.NAME_VARIABLE_PROFONDEURNAPPE:
                        this.publicationProfondeurNappe_mDAO.removeVersion(versionFile.getId());
                        break;
                }

            }
        } catch (PersistenceException | IOException e) {
            LOGGER.debug(e.getMessage());
            throw new BusinessException(e.getMessage());
        }/* catch (BadFormatException e) {
            BadsFormatsReport badsFormatsReport = new BadsFormatsReport(BUNDLE_SOURCE_PATH);
            badsFormatsReport.addException(new BadDelimiterException(e.getMessage()));
            throw new BusinessException(badsFormatsReport.getMessages());
        }*/
    }

    /**
     *
     * @param repositoryManagerVersions
     */
    public void setRepositoryManagerVersions(IRepositoryManagerVersions repositoryManagerVersions) {
        this.repositoryManagerVersions = repositoryManagerVersions;
    }

    /**
     *
     * @param publicationHumiditeVol_mDAO
     */
    public void setPublicationHumiditeVol_mDAO(ILocalPublicationDAO publicationHumiditeVol_mDAO) {
        this.publicationHumiditeVol_mDAO = publicationHumiditeVol_mDAO;
    }

    /**
     *
     * @param publicationTemperature_mDAO
     */
    public void setPublicationTemperature_mDAO(ILocalPublicationDAO publicationTemperature_mDAO) {
        this.publicationTemperature_mDAO = publicationTemperature_mDAO;
    }

    /**
     *
     * @param publicationFluxChaleur_mDAO
     */
    public void setPublicationFluxChaleur_mDAO(ILocalPublicationDAO publicationFluxChaleur_mDAO) {
        this.publicationFluxChaleur_mDAO = publicationFluxChaleur_mDAO;
    }

    /**
     *
     * @param publicationTensionEau_mDAO
     */
    public void setPublicationTensionEau_mDAO(ILocalPublicationDAO publicationTensionEau_mDAO) {
        this.publicationTensionEau_mDAO = publicationTensionEau_mDAO;
    }

    /**
     *
     * @param publicationProfondeurNappe_mDAO
     */
    public void setPublicationProfondeurNappe_mDAO(ILocalPublicationDAO publicationProfondeurNappe_mDAO) {
        this.publicationProfondeurNappe_mDAO = publicationProfondeurNappe_mDAO;
    }

    /**
     *
     * @param datasetDescriptor
     */
    public void setDatasetDescriptor(DatasetDescriptorCS datasetDescriptor) {
        this.datasetDescriptor = datasetDescriptor;
    }

    /**
     *
     * @param variableDAO
     */
    public void setVariableDAO(IVariableForetDAO variableDAO) {
        this.variableDAO = variableDAO;
    }

    /**
     *
     * @param parser
     * @return
     * @throws org.inra.ecoinfo.utils.exceptions.BusinessException
     * @throws PersistenceException
     * @throws IOException
     * @throws BadFormatException
     */
    protected List<VariableForet> buildVariableHeaderAndSkipHeader(CSVParser parser) throws BusinessException {
        try {
            VariableForet dbVariable = null;
            List<VariableForet> lstDbVariables = new LinkedList<>();
            for (int i = 1; i < this.datasetDescriptor.getEnTete() - 2; i++) {
                parser.getLine();
            }
            String[] valeurs = parser.getLine();
            try {
                for (int i = this.datasetDescriptor.getUndefinedColumn(); i < valeurs.length; i++) {
                    dbVariable = (VariableForet) this.variableDAO.getByCode(valeurs[i].split(Constantes.CST_UNDERSCORE)[0])
                            .orElseThrow(PersistenceException::new);
                    if (i == 1) {
                        lstDbVariables.add(dbVariable);
                        i += 2;
                        continue;
                    }
                    VariableForet dbVariable2 = (VariableForet) this.variableDAO.getByCode(valeurs[i - 3].split(Constantes.CST_UNDERSCORE)[0])
                            .orElseThrow(PersistenceException::new);
                    if (!dbVariable.getCode().equals(dbVariable2.getCode())) {
                        lstDbVariables.add(dbVariable);
                    }
                    i += 2;
                }
            }catch (PersistenceException e) {
                throw new BusinessException(e);
            }
            return lstDbVariables;
        }catch (IOException ex) {
            throw  new BusinessException(ex);
        }
    }

}
