/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.AbstractProcessRecordClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_sh;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
public class ProcessRecord_infraj extends AbstractProcessRecordClimatSol<ProfilTemperature_sh, MesureTemperature_sh, ValeurTemperature_sh> {

    /**
     *
     */
    public ProcessRecord_infraj() {
        super();
    }

    /**
     *
     * @param versionFileDB
     * @param date
     * @param time
     * @param originalLineNumber
     * @return
     */
    @Override
    protected ProfilTemperature_sh getNewProfil(VersionFile versionFileDB, LocalDate date, LocalTime time, Long originalLineNumber) {
        return new ProfilTemperature_sh(versionFileDB, date, time, originalLineNumber);
    }

    /**
     *
     * @return
     */
    @Override
    protected MesureTemperature_sh getNewMesure() {
        return new MesureTemperature_sh();
    }

    /**
     *
     * @param dbRealNode
     * @param dbVariable
     * @param noRepetition
     * @param valeur
     * @return
     */
    @Override
    protected ValeurTemperature_sh getNewValeur(RealNode dbRealNode, Long noRepetition, Float valeur) {
        return new ValeurTemperature_sh(dbRealNode, noRepetition, valeur);
    }

    /**
     *
     * @return
     */
    @Override
    protected ProfilTemperature_sh getNewProfil() {
        return new ProfilTemperature_sh();
    }
}
