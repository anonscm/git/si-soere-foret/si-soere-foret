package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ValeurHumiditeVol_m;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAPublicationHumiditeVol_mDAO extends JPAPublicationClimatSolDAO<ProfilHumiditeVol_m, MesureHumiditeVol_m, ValeurHumiditeVol_m> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurHumiditeVol_m> getValueClass() {
        return ValeurHumiditeVol_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureHumiditeVol_m> getMeasureClass() {
        return MesureHumiditeVol_m.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilHumiditeVol_m> getProfilClass() {
        return ProfilHumiditeVol_m.class;
    }
}