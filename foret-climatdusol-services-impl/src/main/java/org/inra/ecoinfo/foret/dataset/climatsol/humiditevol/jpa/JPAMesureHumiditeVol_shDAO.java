package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.MesureHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity.ProfilHumiditeVol_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;


/**
 * @author sophie
 * 
 */
public class JPAMesureHumiditeVol_shDAO extends JPAMesureClimatSolDAO<ProfilHumiditeVol_sh, MesureHumiditeVol_sh> {

}
