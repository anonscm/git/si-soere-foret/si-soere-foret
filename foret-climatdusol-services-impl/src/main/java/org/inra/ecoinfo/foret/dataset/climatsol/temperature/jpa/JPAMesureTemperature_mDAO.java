package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAMesureClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_m;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_m;


/**
 * @author sophie
 * 
 */
public class JPAMesureTemperature_mDAO extends JPAMesureClimatSolDAO<ProfilTemperature_m, MesureTemperature_m> {

}
