/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAPublicationClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.MesureProfondeurNappe_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ProfilProfondeurNappe_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_sh;


/**
 * @author sophie
 * 
 */
public class JPAPublicationProfondeurNappe_shDAO extends JPAPublicationClimatSolDAO<ProfilProfondeurNappe_sh, MesureProfondeurNappe_sh, ValeurProfondeurNappe_sh> {

    /**
     *
     * @return
     */
    @Override
    protected Class<ValeurProfondeurNappe_sh> getValueClass() {
        return ValeurProfondeurNappe_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<MesureProfondeurNappe_sh> getMeasureClass() {
        return MesureProfondeurNappe_sh.class;
    }

    /**
     *
     * @return
     */
    @Override
    protected Class<ProfilProfondeurNappe_sh> getProfilClass() {
        return ProfilProfondeurNappe_sh.class;
    }
}