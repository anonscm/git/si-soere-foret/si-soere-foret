package org.inra.ecoinfo.foret.synthesis.climatsol.mtp;

import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.MesureTemperature_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ValeurTemperature_sh;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mtpsh.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mtpsh.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MTP_shSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurTemperature_sh, MesureTemperature_sh, ProfilTemperature_sh> {

    public CS_MTP_shSynthesisDAO() {
        super(ValeurTemperature_sh.class);
    }
}
