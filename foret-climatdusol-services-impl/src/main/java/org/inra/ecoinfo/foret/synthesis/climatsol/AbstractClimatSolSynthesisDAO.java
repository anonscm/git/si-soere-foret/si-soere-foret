
package org.inra.ecoinfo.foret.synthesis.climatsol;

import java.time.LocalDate;
import java.util.stream.Stream;
import javax.persistence.criteria.CriteriaQuery;
import javax.persistence.criteria.Join;
import javax.persistence.criteria.Path;
import javax.persistence.criteria.Root;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol_;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol_;
import org.inra.ecoinfo.mga.business.composite.Nodeable_;
import org.inra.ecoinfo.mga.business.composite.RealNode;
import org.inra.ecoinfo.mga.business.composite.RealNode_;
import org.inra.ecoinfo.synthesis.AbstractSynthesis;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author ptchernia
 */
public abstract class AbstractClimatSolSynthesisDAO<
        SV extends GenericSynthesisValue, 
        SD extends GenericSynthesisDatatype, 
        V extends ValeurClimatSol<M> , 
        M extends MesureClimatSol<P, V>, 
        P extends ProfilClimatSol<M> > extends AbstractSynthesis<SV, SD> {
    
    Class<V> valueClass;
    
    private AbstractClimatSolSynthesisDAO(){
        super();
    }

    public AbstractClimatSolSynthesisDAO(Class<V> valueClass) {
        super();
        this.valueClass = valueClass;
    }

    @Override
    public Stream<SV> getSynthesisValue() {
        CriteriaQuery<SV> query = builder.createQuery(synthesisValueClass);
        Root<V> v = query.from(valueClass);
        Join<V, MesureClimatSol> m = v.join(ValeurClimatSol_.mesure);
        Join<MesureClimatSol, ProfilClimatSol> p = m.join(MesureClimatSol_.profil);
        Join<V, RealNode> varRn = v.join(ValeurClimatSol_.realNode);
        Join<RealNode, RealNode> siteRn = varRn.join(RealNode_.parent).join(RealNode_.parent).join(RealNode_.parent);
        query.distinct(true);
        final Path<Float> valeur = v.get(ValeurClimatSol_.valeur);
        final Path<String> variableCode = varRn.join(RealNode_.nodeable).get(Nodeable_.code);
        final Path<LocalDate> dateMesure = p.get(ProfilClimatSol_.date);
        final Path<String> sitePath = siteRn.get(RealNode_.path);
        Path<Integer> profondeur = m.get(MesureClimatSol_.profondeur);
        Path<Long> nbRepetion = v.get(ValeurClimatSol_.nbreRepetition);
        query.select(
                builder.construct(
                        synthesisValueClass,
                        dateMesure,
                        sitePath,
                        variableCode,
                        builder.avg(valeur),
                        profondeur,
                        nbRepetion
                )
        )
                .where(
                        builder.gt(valeur, -9999)
                )
                .groupBy(
                        sitePath,
                        variableCode,
                        dateMesure,
                        profondeur,
                        nbRepetion
                )
                .orderBy(
                        builder.asc(sitePath),
                        builder.asc(variableCode),
                        builder.asc(dateMesure),
                        builder.asc(profondeur),
                        builder.asc(nbRepetion)
                );
        return getResultAsStream(query);//getResultList(query)

    }

    
}
