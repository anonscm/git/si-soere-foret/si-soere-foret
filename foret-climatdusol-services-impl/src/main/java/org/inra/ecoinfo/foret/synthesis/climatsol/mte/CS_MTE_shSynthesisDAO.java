package org.inra.ecoinfo.foret.synthesis.climatsol.mte;

import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.MesureTensionEau_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ProfilTensionEau_sh;
import org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity.ValeurTensionEau_sh;
import org.inra.ecoinfo.foret.synthesis.climatsol.AbstractClimatSolSynthesisDAO;
import org.inra.ecoinfo.foret.synthesis.mtesh.SynthesisDatatype;
import org.inra.ecoinfo.foret.synthesis.mtesh.SynthesisValue;

/**
 *
 * @author ptchernia
 */
public class CS_MTE_shSynthesisDAO extends AbstractClimatSolSynthesisDAO<SynthesisValue, SynthesisDatatype, ValeurTensionEau_sh, MesureTensionEau_sh, ProfilTensionEau_sh> {

    public CS_MTE_shSynthesisDAO() {
        super(ValeurTensionEau_sh.class);
    }
}
