/**
 *
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity.ValeurProfondeurNappe_sh;


/**
 * @author sophie
 * 
 */
public class JPAProfondeurNappe_shDAO extends JPAClimatSolDAO<ValeurProfondeurNappe_sh> {

    @Override
    protected Class<ValeurProfondeurNappe_sh> getValeurClass() {
        return ValeurProfondeurNappe_sh.class;
    }
}
