package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.Map;
import java.util.TreeMap;
import org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS;
import org.inra.ecoinfo.foret.dataset.impl.RequestProperties;
import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;


/**
 * @author sophie
 * 
 */
public class RequestPropertiesCS extends RequestProperties implements IRequestPropertiesCS {

    /**
     *
     */
    protected static final String BUNDLE_SOURCE_PATH = "org.inra.ecoinfo.foret.dataset.messages";

    /**
     *
     */
    public LinkedList<Line> lstLines;
    private String datatype;
    private Map<String, ExpectedColumn> possibleColums = new HashMap<>();
    private Map<Integer, ExpectedColumn> expectedColums = new TreeMap<>();
    private Map<String, ExpectedColumn> _expectedColums = new TreeMap<>();
    private int profondeurMax;
    private int repetionMax;
    private String[] valeurMinInFile;
    private String[] valeurMaxInFile;
    private String[] valeursMin;
    private String[] valeursMax;

    /**
     * @return the possibleColums
     */
    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS#getPossibleColums()
     */
    @Override
    public Map<String, ExpectedColumn> getPossibleColumns() {
        return this.possibleColums;
    }

    @Override
    public Column getColumn(int index) {
        return this.expectedColums.get(index);
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS#addExpectedColum(int, java.lang.String, int, int) utilisé pour fréquence -j et infra-j
     */
    @Override
    public void addExpectedColumn(int index, String name, int profondeur, int numeroRepetition, Float minValue, Float maxValue) {
        ExpectedColumn expectedColumn = new ExpectedColumn(name, profondeur, numeroRepetition, minValue, maxValue);
        this.possibleColums.put(name, expectedColumn);
        this.expectedColums.put(index, expectedColumn);
        this._expectedColums.put(name, expectedColumn);
    }

    @Override
    public ExpectedColumn addExpectedColumn(String columnName, DatasetDescriptor datasetDescriptor) {
        return null;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.fluxgazeux.chambrefluxsol.IRequestPropertiesFluxSol#getExpectedColumns()
     */
    @Override
    public Map<String, ExpectedColumn> getExpectedColumns() {
        return this._expectedColums;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.foret.dataset.climatsol.IRequestPropertiesCS#addExpectedColum(int, java.lang.String, int, int) utilisé pour fréquence -m
     */
    @Override
    public void addExpectedColumn(int index, String name, int profondeur, Boolean hasEcartType, Boolean hasNombreRepetition, Float minValue, Float maxValue) {
        ExpectedColumn expectedColumn = new ExpectedColumn(name, profondeur, hasEcartType, hasNombreRepetition, minValue, maxValue);
        this.possibleColums.put(name, expectedColumn);
        this.expectedColums.put(index, expectedColumn);
        this._expectedColums.put(name, expectedColumn);
    }

    /**
     * @return the lstLines
     */
    @Override
    public LinkedList<Line> getLstLines() {
        return this.lstLines;
    }

    /**
     * @param lstLines
     *            the lstLines to set
     */
    @Override
    public void setLstLines(LinkedList<Line> lstLines) {
        this.lstLines = lstLines;
    }

    @Override
    public void setProfondeurMax(int profondeurMax) {
        this.profondeurMax = profondeurMax;
    }

    @Override
    public int getProfondeurMax() {
        return this.profondeurMax;
    }

    @Override
    public void setRepetitionMax(int repetitionMax) {
        this.repetionMax = repetitionMax;
    }

    @Override
    public int getRepetitionMax() {
        return this.repetionMax;
    }

    /**
     *
     * @return
     */
    public Map<String, ExpectedColumn> getPossibleColums() {
        return this.possibleColums;
    }

    /**
     *
     * @param possibleColums
     */
    public void setPossibleColums(Map<String, ExpectedColumn> possibleColums) {
        this.possibleColums = possibleColums;
    }

    /**
     *
     * @return
     */
    public Map<Integer, ExpectedColumn> getExpectedColums() {
        return this.expectedColums;
    }

    /**
     *
     * @param expectedColums
     */
    public void setExpectedColums(Map<Integer, ExpectedColumn> expectedColums) {
        this.expectedColums = expectedColums;
    }

    @Override
    public String[] getValeurMinInFile() {
        return this.valeurMinInFile;
    }

    @Override
    public void setValeurMinInFile(String[] valeurMinInFile) {
        this.valeurMinInFile = valeurMinInFile;
    }

    @Override
    public String[] getValeurMaxInFile() {
        return this.valeurMaxInFile;
    }

    @Override
    public void setValeurMaxInFile(String[] valeurMaxInFile) {
        this.valeurMaxInFile = valeurMaxInFile;
    }

    @Override
    public String[] getValeursMin() {
        return this.valeursMin;
    }

    @Override
    public void setValeursMin(String[] valeursMin) {
        this.valeursMin = valeursMin;
    }

    @Override
    public String[] getValeursMax() {
        return this.valeursMax;
    }

    @Override
    public void setValeursMax(String[] valeursMax) {
        this.valeursMax = valeursMax;
    }

    /**
     *
     * @param datatype
     */
    public void setDatatype(String datatype) {
        this.datatype = datatype;
    }
}
