/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.temperature.jpa;

import org.inra.ecoinfo.foret.dataset.climatsol.jpa.JPAProfilClimatSolDAO;
import org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity.ProfilTemperature_sh;


/**
 * @author sophie
 * 
 */
public class JPAProfilTemperature_shDAO extends JPAProfilClimatSolDAO<ProfilTemperature_sh> {

}
