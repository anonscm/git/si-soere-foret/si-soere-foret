/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.extraction.climatsol.jsf;

import org.inra.ecoinfo.foret.extraction.jsf.nodeable.IAvailablesSynthesisDepositPlacesDAO;
import org.inra.ecoinfo.foret.extraction.jsf.nodeable.UITreeNode;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;

/**
 *
 * @author ptchernia
 */
public class UISite extends UITreeNode<SiteForet>{

    /**
     *
     * @param availablesSynthesisDepositPlacesDAO
     */
    public UISite(IAvailablesSynthesisDepositPlacesDAO availablesSynthesisDepositPlacesDAO) {
        super(availablesSynthesisDepositPlacesDAO);
    }
    
}
