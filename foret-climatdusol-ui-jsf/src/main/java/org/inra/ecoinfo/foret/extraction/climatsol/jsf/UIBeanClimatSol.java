package org.inra.ecoinfo.foret.extraction.climatsol.jsf;
import java.io.Serializable;
import java.text.ParseException;
import java.util.HashMap;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;
import java.util.Properties;
import javax.annotation.PostConstruct;
import javax.faces.bean.ManagedBean;
import javax.faces.bean.ManagedProperty;
import javax.faces.bean.ViewScoped;
import org.inra.ecoinfo.extraction.IExtractionManager;
import org.inra.ecoinfo.filecomp.IFileCompManager;
import org.inra.ecoinfo.foret.extraction.IForetDatasetManager;
import org.inra.ecoinfo.foret.extraction.climatsol.ICSDatasetManager;
import org.inra.ecoinfo.foret.extraction.climatsol.ui.flex.vo.DatesFormParamCSVO;
import org.inra.ecoinfo.foret.extraction.jsf.AbstractUIBeanForSteps;
import org.inra.ecoinfo.foret.extraction.jsf.Collector;
import org.inra.ecoinfo.foret.extraction.jsf.associate.UIAssociate;
import org.inra.ecoinfo.foret.extraction.jsf.date.DatesRequestParamVO;
import org.inra.ecoinfo.foret.extraction.jsf.date.UIDate;
import org.inra.ecoinfo.foret.extraction.jsf.depth.AvailablesSynthesisDepthsDAO;
import org.inra.ecoinfo.foret.extraction.jsf.depth.UIDepth;
import org.inra.ecoinfo.foret.extraction.jsf.display.UIDisplay;
import org.inra.ecoinfo.foret.extraction.jsf.nodeable.AvailablesSynthesisDepositSiteDAO;
import org.inra.ecoinfo.foret.extraction.jsf.nodeable.UISite;
import org.inra.ecoinfo.foret.extraction.jsf.variable.AvailablesSynthesisVariablesDAO;
import org.inra.ecoinfo.foret.extraction.jsf.variable.UIVariable;
import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.localization.ILocalizationManager;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisDatatype;
import org.inra.ecoinfo.utils.exceptions.BadExpectedValueException;
import org.inra.ecoinfo.utils.exceptions.BusinessException;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

/**
 *
 * @author ptcherniati
 */
@ManagedBean(name = "uiClimatSol")
@ViewScoped
public class UIBeanClimatSol extends AbstractUIBeanForSteps implements Serializable {

    private static final Logger LOGGER = LoggerFactory.getLogger(UIBeanClimatSol.class);

    /**
     *
     */
    public static final String MPS = "variableMPS";

    private static final String RULE_NAVIGATION_JSF = Constantes.RULE_NAVIGATION_JSF;

    private static final long serialVersionUID = 1L;

    private Properties propertiesVariablesNames;
    //private Map<Long, VOVariableJSF> variablesAvailables;

    /**
     *
     */
    @ManagedProperty(value = "#{cSDatasetManager}")
    protected ICSDatasetManager cSDatasetManager;
    @ManagedProperty(value = "#{availablesSynthesisDepositSiteDAO}")
    AvailablesSynthesisDepositSiteDAO availablesSynthesisDepositPlacesDAO;
    @ManagedProperty(value = "#{availablesSynthesisVariablesDAO}")
    AvailablesSynthesisVariablesDAO availablesSynthesisVariablesDAO;
    @ManagedProperty(value = "#{availablesSynthesisDepthsDAO}")
    AvailablesSynthesisDepthsDAO availablesSynthesisDepthsDAO;
    @ManagedProperty(value = "#{extractionManager}")
    IExtractionManager extractionManager;
    @ManagedProperty(value = "#{fileCompManager}")
    IFileCompManager fileCompManager;
    private DatesFormParamCSVO datesFormParamCSVO;

    /**
     *
     */
    public UIBeanClimatSol() {
        super();
    }

    @PostConstruct
    void initBean() {
        collector = new Collector(stepBuilders);
        final ClimatSolParameterVO parameters = new ClimatSolParameterVO();
        collector.setParameters(parameters);
        availablesSynthesisDepositPlacesDAO.getGenericSynthesisDatatypesClasses().addAll(getSynthesisDatatypeClasses());

        availablesSynthesisVariablesDAO.getGenericSynthesisValuesClasses().putAll(getSynthesisValueClasses());
        availablesSynthesisDepthsDAO.getGenericSynthesisValuesClasses().putAll(getSynthesisValueClasses());
        final UISite uiSite = new org.inra.ecoinfo.foret.extraction.jsf.nodeable.UISite(availablesSynthesisDepositPlacesDAO);
        stepBuilders.add(uiSite);
        uiSite.init(localizationManager, policyManager, collector);
        datesFormParamCSVO = new DatesFormParamCSVO(localizationManager);
        final DatesRequestParamVO datesRequestParamVO = new DatesRequestParamVO(datesFormParamCSVO);
        stepBuilders.add(new UIDate(datesRequestParamVO));
        stepBuilders.add(new UIVariable(availablesSynthesisVariablesDAO));
        stepBuilders.add(new UIDepth(availablesSynthesisDepthsDAO));
        stepBuilders.add(new UIDisplay(collector));
        stepBuilders.add(new UIAssociate(fileCompManager));
    }

    protected <T> Map<String, List<Class<? extends T>>> getSynthesisValueClasses() {
        Map<String, List<Class<? extends T>>> genericSynthesisValues = new HashMap();
        genericSynthesisValues.computeIfAbsent(Constantes.MENSUEL, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.cmsm.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mfcj.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.INFRA_JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mfcsh.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mhvj.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.INFRA_JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mhvsh.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mpnj.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.INFRA_JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mpnsh.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mtej.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.INFRA_JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mtesh.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mtpj.SynthesisValue.class);
        genericSynthesisValues.computeIfAbsent(Constantes.INFRA_JOURNALIER, k -> new LinkedList()).add(org.inra.ecoinfo.foret.synthesis.mtpsh.SynthesisValue.class);
        return genericSynthesisValues;
    }

    protected List<Class<? extends GenericSynthesisDatatype>> getSynthesisDatatypeClasses() {
        List<Class<? extends GenericSynthesisDatatype>> genericSynthesisDatatypes = new LinkedList();
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.cmsm.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mfcj.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mfcsh.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mhvj.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mhvsh.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mpnj.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mpnsh.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mtej.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mtesh.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mtpj.SynthesisDatatype.class);
        genericSynthesisDatatypes.add(org.inra.ecoinfo.foret.synthesis.mtpsh.SynthesisDatatype.class);
        return genericSynthesisDatatypes;
    }
    protected static final String STEP_SITE = "nodeable";
    protected static final String STEP_DATES = "dates";
    protected static final String STEP_VARIABLES = "variables";

    /**
     *
     * @return
     */
    public boolean getFormIsValid() {
        return stepBuilders.get(2).isStepValid();

    }

    /**
     * @return @throws BusinessException
     * @throws BadExpectedValueException
     * @throws ParseException***********
     */
    public String extract() throws BusinessException, BadExpectedValueException, ParseException {
        ClimatSolParameterVO parameters = (ClimatSolParameterVO) collector.getParameters();
        this.extractionManager.extract(parameters, parameters.getAffichage());

        return null;
    }

    /**
     * @param localizationManager the localizationManager to set
     */
    @Override
    public void setLocalizationManager(ILocalizationManager localizationManager) {
        super.setLocalizationManager(localizationManager);
    }

    /**
     * @return the propertiesVariablesNames
     */
    public Properties getPropertiesVariablesNames() {
        return this.propertiesVariablesNames;
    }

    /**
     * @param propertiesVariablesNames the propertiesVariablesNames to set
     */
    public void setPropertiesVariablesNames(Properties propertiesVariablesNames) {
        this.propertiesVariablesNames = propertiesVariablesNames;
    }

    /**
     *
     * @return
     */
    public String navigate() {
        return UIBeanClimatSol.RULE_NAVIGATION_JSF;
    }

    /**
     * @param cSDatasetManager the cSDatasetManager to set
     */
    public void setcSDatasetManager(ICSDatasetManager cSDatasetManager) {
        this.cSDatasetManager = cSDatasetManager;
    }

    /**
     *
     * @return
     */
//    @Override
    protected IForetDatasetManager getDatasetManager() {
        return cSDatasetManager;
    }

    public boolean isFormValid() {
        return true;
    }

    @Override
    public String getTitleForStep() {
        return super.getTitleForStep(); //To change body of generated methods, choose Tools | Templates.
    }

    public void setAvailablesSynthesisDepositPlacesDAO(AvailablesSynthesisDepositSiteDAO availablesSynthesisDepositPlacesDAO) {
        this.availablesSynthesisDepositPlacesDAO = availablesSynthesisDepositPlacesDAO;
    }

    public void setAvailablesSynthesisVariablesDAO(AvailablesSynthesisVariablesDAO availablesSynthesisVariablesDAO) {
        this.availablesSynthesisVariablesDAO = availablesSynthesisVariablesDAO;
    }

    public void setExtractionManager(IExtractionManager extractionManager) {
        this.extractionManager = extractionManager;
    }

    public void setAvailablesSynthesisDepthsDAO(AvailablesSynthesisDepthsDAO availablesSynthesisDepthsDAO) {
        this.availablesSynthesisDepthsDAO = availablesSynthesisDepthsDAO;
    }

    public void setFileCompManager(IFileCompManager fileCompManager) {
        this.fileCompManager = fileCompManager;
    }

    @Override
    public String nextStep() {
        return super.nextStep(); //To change body of generated methods, choose Tools | Templates.
    }

    @Override
    public String prevStep() {
        return super.prevStep(); //To change body of generated methods, choose Tools | Templates.
    }
    
    
}
