package org.inra.ecoinfo.foret.dataset.climatsol.impl;


import java.time.LocalDate;
import java.time.LocalTime;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;


/**
 * @author sophie
 * 
 */
public class Line implements Comparable<Line> {

    private LocalDate date;
    private LocalTime time;
    private Long originalLineNumber;

    private SiteForet zoneEtude;
    private List<VariableValue> lstVariablesValues = new LinkedList<>();

    /**
     * empty constructor
     */
    public Line() {

    }

    /**
     * @param date
     * @param time
     * @param originalLineNumber
     * @param zoneEtude
     * @param lstVariablesValues
     */
    public Line(LocalDate date, LocalTime time, Long originalLineNumber, SiteForet zoneEtude, List<VariableValue> lstVariablesValues) {
        super();
        this.date = date;
        this.time = time;
        this.originalLineNumber = originalLineNumber;
        this.zoneEtude = zoneEtude;
        this.lstVariablesValues = lstVariablesValues;
    }

    @Override
    public int hashCode() {
        int hash = 3;
        hash = 53 * hash + Objects.hashCode(this.date);
        hash = 53 * hash + Objects.hashCode(this.time);
        return hash;
    }

    @Override
    public boolean equals(Object obj) {
        if (obj == null) {
            return false;
        }
        if (getClass() != obj.getClass()) {
            return false;
        }
        final Line other = (Line) obj;
        if (!Objects.equals(this.date, other.date)) {
            return false;
        }
        return Objects.equals(this.time, other.time);
    }

    /*
     * (non-Javadoc)
     * 
     * @see java.lang.Comparable#compareTo(java.lang.Object)
     */
    @Override
    public int compareTo(Line o) {
        return (int) (this.originalLineNumber - o.originalLineNumber);
    }

    /**
     * @param line
     */
    public void copy(Line line) {
        this.date = line.getDate();
        this.time = line.getTime();
        this.originalLineNumber = line.getOriginalLineNumber();
        this.zoneEtude = line.getZoneEtude();
        this.lstVariablesValues = line.getLstVariablesValues();
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the time
     */
    public LocalTime getTime() {
        return this.time;
    }

    /**
     * @param time
     *            the time to set
     */
    public void setTime(LocalTime time) {
        this.time = time;
    }

    /**
     * @return the originalLineNumber
     */
    public Long getOriginalLineNumber() {
        return this.originalLineNumber;
    }

    /**
     * @param originalLineNumber
     *            the originalLineNumber to set
     */
    public void setOriginalLineNumber(Long originalLineNumber) {
        this.originalLineNumber = originalLineNumber;
    }

    /**
     * @return the zoneEtude
     */
    public SiteForet getZoneEtude() {
        return this.zoneEtude;
    }

    /**
     * @param zoneEtude
     *            the zoneEtude to set
     */
    public void setZoneEtude(SiteForet zoneEtude) {
        this.zoneEtude = zoneEtude;
    }

    /**
     * @return the lstVariablesValues
     */
    public List<VariableValue> getLstVariablesValues() {
        return this.lstVariablesValues;
    }

    /**
     * @param lstVariablesValues
     *            the lstVariablesValues to set
     */
    public void setLstVariablesValues(List<VariableValue> lstVariablesValues) {
        this.lstVariablesValues = lstVariablesValues;
    }
}