package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureFluxChaleur_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "fcsh_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureFluxChaleur_sh.ID_JPA))
public class MesureFluxChaleur_sh extends MesureClimatSol<ProfilFluxChaleur_sh, ValeurFluxChaleur_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MFCSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_G_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureFluxChaleur_sh() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureFluxChaleur_sh(int profondeur) {
        super(profondeur);
    }
}
