package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;


/**
 * @author sophie
 * 
 */
@Entity
@Table(name = MesureHumiditeVol_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "hvsh_pte", columnList = TableConstantes.COLUMN_ID_PROFIL),
        @Index(name = "hvsh_prof", columnList = TableConstantes.COLUMN_PROFONDEUR)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureHumiditeVol_sh.ID_JPA))
public class MesureHumiditeVol_sh extends MesureClimatSol<ProfilHumiditeVol_sh, ValeurHumiditeVol_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MHVSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_SWC_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureHumiditeVol_sh() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureHumiditeVol_sh(int profondeur) {
        super(profondeur);
    }
}
