package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureProfondeurNappe_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "pnsh_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureProfondeurNappe_sh.ID_JPA))
public class MesureProfondeurNappe_sh extends MesureClimatSol<ProfilProfondeurNappe_sh, ValeurProfondeurNappe_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MPNSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_GWD_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureProfondeurNappe_sh() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureProfondeurNappe_sh(int profondeur) {
        super(profondeur);
    }
}
