package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 * @author sophie
 *
 */
@Entity
@Table(name = ValeurHumiditeVol_sh.TABLE_NAME,
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_MESURE, TableConstantes.COLUMN_NBRE_REPETITION, VariableForet.ID_JPA}), indexes = {
            @Index(name = "mhvsh_vhvsh_index", columnList = TableConstantes.COLUMN_ID_MESURE),
            @Index(name = "vhvsh_var_idx", columnList = VariableForet.ID_JPA),
            @Index(name = "vhvsh_val_rep_idx", columnList = TableConstantes.COLUMN_VALEUR + "," + TableConstantes.COLUMN_NBRE_REPETITION)
        })
@AttributeOverrides({
        @AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ValeurHumiditeVol_sh.ID_JPA)),
        @AttributeOverride(name = TableConstantes.COLUMN_NO_REPETITION, column = @Column(name = TableConstantes.COLUMN_NBRE_REPETITION))
})
public class ValeurHumiditeVol_sh extends ValeurClimatSol<MesureHumiditeVol_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.VHVSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.VALEUR_SWC_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ValeurHumiditeVol_sh() {
        super();
    }

    /**
     * @param realNode
     * @param noRepetition
     * @param valeur
     */
    public ValeurHumiditeVol_sh(RealNode realNode, Long noRepetition, Float valeur) {
        super(realNode, valeur, noRepetition);
    }

}
