package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureFluxChaleur_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "fcm_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureFluxChaleur_m.ID_JPA))
public class MesureFluxChaleur_m extends MesureClimatSol<ProfilFluxChaleur_m, ValeurFluxChaleur_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MFCM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_G_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureFluxChaleur_m() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureFluxChaleur_m(int profondeur) {
        super(profondeur);
    }
}
