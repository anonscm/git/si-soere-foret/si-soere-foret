package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity;

import java.time.LocalDate;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;


/**
 * @author sophie
 * 
 */
@Entity
@Table(name = ProfilHumiditeVol_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ZET_ID, TableConstantes.COLUMN_DATE}),
        indexes = {
            @Index(name = "hvj_zet_idx", columnList = TableConstantes.COLUMN_ZET_ID),
            @Index(name = "hvj_ivf_idx", columnList = VersionFile.ID_JPA)
        })
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ProfilHumiditeVol_j.ID_JPA))
public class ProfilHumiditeVol_j extends ProfilClimatSol<MesureHumiditeVol_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.PHVJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.PROFIL_SWC_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ProfilHumiditeVol_j() {
        super();
    }

    /**
     * @param versionFile
     * @param date
     * @param noLigne
     */
    public ProfilHumiditeVol_j(VersionFile versionFile, LocalDate date, Long noLigne) {
        super(versionFile, date, noLigne);
    }
}
