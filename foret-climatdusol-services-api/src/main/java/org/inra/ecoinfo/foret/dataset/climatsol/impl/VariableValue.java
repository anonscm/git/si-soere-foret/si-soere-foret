package org.inra.ecoinfo.foret.dataset.climatsol.impl;

/**
 * @author sophie
 * 
 */
public class VariableValue {

    private Float          value;
    private ExpectedColumn column;
    private Float          ecartType;
    private int            nombreRepetition;

    /**
     * Constructor
     * 
     * @param column
     * @param value
     */
    public VariableValue(ExpectedColumn column, Float value) {
        super();
        this.column = column;
        this.value = value;
    }

    /**
     * Constructor
     * 
     * @param column
     * @param value
     * @param ecartType
     * @param nombreRepetition
     */
    public VariableValue(ExpectedColumn column, Float value, Float ecartType, int nombreRepetition) {
        super();
        this.column = column;
        this.value = value;
        this.ecartType = ecartType;
        this.nombreRepetition = nombreRepetition;
    }

    /**
     * @return the value
     */
    public Float getValue() {
        return this.value;
    }

    /**
     * @param value
     *            the value to set
     */
    public void setValue(Float value) {
        this.value = value;
    }

    /**
     * @return the column
     */
    public ExpectedColumn getColumn() {
        return this.column;
    }

    /**
     * @param column
     *            the column to set
     */
    public void setColumn(ExpectedColumn column) {
        this.column = column;
    }

    /**
     * @return the ecartType
     */
    public Float getEcartType() {
        return this.ecartType;
    }

    /**
     * @param ecartType
     *            the ecartType to set
     */
    public void setEcartType(Float ecartType) {
        this.ecartType = ecartType;
    }

    /**
     * @return the nombreRepetition
     */
    public int getNombreRepetition() {
        return this.nombreRepetition;
    }

    /**
     * @param nombreRepetition
     *            the nombreRepetition to set
     */
    public void setNombreRepetition(int nombreRepetition) {
        this.nombreRepetition = nombreRepetition;
    }
}
