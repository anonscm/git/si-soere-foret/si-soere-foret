/**
 * 
 */
package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import java.util.Comparator;


/**
 * @author sophie
 * 
 */
public class CompareV implements Comparator<Object> {

    

    @Override
    public int compare(Object o1, Object o2) {
        VariableValue v1 = (VariableValue) o1;
        VariableValue v2 = (VariableValue) o2;

        int profondeur1 = v1.getColumn().getProfondeur();
        int profondeur2 = v2.getColumn().getProfondeur();

        if (profondeur1 > profondeur2) {
            return 1;
        } else if (profondeur1 < profondeur2) {
            return -1;
        } else {
            return 0;
        }
    }
}
