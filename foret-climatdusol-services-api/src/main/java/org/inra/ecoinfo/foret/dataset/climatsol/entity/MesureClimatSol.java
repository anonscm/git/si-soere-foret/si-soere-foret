package org.inra.ecoinfo.foret.dataset.climatsol.entity;

import java.io.Serializable;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;


/**
 * @author sophie
 * 
 * @param <P>
 * @param <V>
 */
@MappedSuperclass
public class MesureClimatSol<P extends ProfilClimatSol<? extends MesureClimatSol<P, V>>, V extends ValeurClimatSol<? extends MesureClimatSol<P, V>>> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @Column(name = TableConstantes.COLUMN_PROFONDEUR, nullable = false)
    private int profondeur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = TableConstantes.COLUMN_ID_PROFIL)
    @LazyToOne(LazyToOneOption.PROXY)
    private P profil;

    @OneToMany(mappedBy = TableConstantes.NAME_ATTRIBUT_MESURE, cascade = {PERSIST, MERGE, REFRESH})
    private List<V> lstValeurs = new LinkedList<>();

    /**
     * empty constructor
     */
    public MesureClimatSol() {
        super();
    }

    /**
     * Constructor
     * 
     * @param profondeur
     */
    public MesureClimatSol(int profondeur) {
        super();
        this.setProfondeur(profondeur);
    }

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the lstValeurs
     */
    public List<V> getLstValeurs() {
        return this.lstValeurs;
    }

    /**
     * @param lstValeurs
     *            the lstValeurs to set
     */
    public void setLstValeurs(List<V> lstValeurs) {
        this.lstValeurs = lstValeurs;
    }

    /**
     * @return the profil
     */
    public P getProfil() {
        return this.profil;
    }

    /**
     * @param profil
     *            the profil to set
     */
    public void setProfil(P profil) {
        this.profil = profil;
    }

    /**
     * @return the profondeur
     */
    public int getProfondeur() {
        return this.profondeur;
    }

    /**
     * @param profondeur
     *            the profondeur to set
     */
    public void setProfondeur(int profondeur) {
        this.profondeur = profondeur;
    }
}
