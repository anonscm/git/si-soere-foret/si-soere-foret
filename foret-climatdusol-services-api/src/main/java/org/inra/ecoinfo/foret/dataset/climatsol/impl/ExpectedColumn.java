package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import org.inra.ecoinfo.foret.utils.Constantes;
import org.inra.ecoinfo.utils.Column;

/**
 * @author sophie
 * 
 */
public class ExpectedColumn extends org.inra.ecoinfo.foret.dataset.ExpectedColumn {

    private int     profondeur;
    private int     numeroRepetition;

    private Boolean hasNombreRepetition;
    private Boolean hasEcartType;

    /**
     * Constructeur
     * 
     * @param column
     */
    public ExpectedColumn(Column column) {
        super(column);
    }

    /**
     * @param name
     * @param profondeur
     * @param numeroRepetition
     * @param minValue
     * @param maxValue
     *            utilisé pour fréquence -j et infra-j
     */
    public ExpectedColumn(String name, int profondeur, int numeroRepetition, Float minValue,
            Float maxValue) {
        super();
        this.setMinValue(minValue);
        this.setMaxValue(maxValue);
        this.setName(name);
        this.setFlag(true);
        this.setFlagType(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        this.setInull(true);
        this.setFormatType(Constantes.FORMAT_TYPE);
        this.profondeur = profondeur;
        this.numeroRepetition = numeroRepetition;
    }

    /**
     * @param name
     * @param profondeur
     * @param hasEcartType
     * @param hasNombreRepetition
     * @param minValue
     * @param maxValue
     *            utilisé pour fréquence -m
     */
    public ExpectedColumn(String name, int profondeur, Boolean hasEcartType,
            Boolean hasNombreRepetition, Float minValue, Float maxValue) {
        super();
        this.setMinValue(minValue);
        this.setMaxValue(maxValue);
        this.setName(name);
        this.setFlag(true);
        this.setFlagType(Constantes.PROPERTY_CST_VARIABLE_TYPE);
        this.setInull(true);
        this.setFormatType(Constantes.FORMAT_TYPE);
        this.profondeur = profondeur;
        this.hasEcartType = hasEcartType;
        this.hasNombreRepetition = hasNombreRepetition;
    }

    /**
     * empty constructeur
     */
    public ExpectedColumn() {
        super();
    }

    /**
     * @return the profondeur
     */
    public int getProfondeur() {
        return this.profondeur;
    }

    /**
     * @return the numeroRepetition
     */
    public int getNumeroRepetition() {
        return this.numeroRepetition;
    }

    /**
     * @return the hasNombreRepetition
     */
    public Boolean hasNombreRepetition() {
        return this.hasNombreRepetition;
    }

    /**
     * @return the hasEcartType
     */
    public Boolean hasEcartType() {
        return this.hasEcartType;
    }

}
