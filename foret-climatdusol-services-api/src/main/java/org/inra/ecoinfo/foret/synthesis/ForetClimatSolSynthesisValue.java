package org.inra.ecoinfo.foret.synthesis;

import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author ptcherniati
 */
public class ForetClimatSolSynthesisValue extends GenericSynthesisValue {

    /**
	 * 
	 */
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected int             profondeur;

    /**
     *
     */
    protected int             repetition;

    /**
     *
     * @return
     */
    public int getProfondeur() {
        return this.profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(final int profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public int getRepetition() {
        return this.repetition;
    }

    /**
     *
     * @param repetition
     */
    public void setRepetition(final int repetition) {
        this.repetition = repetition;
    }

}
