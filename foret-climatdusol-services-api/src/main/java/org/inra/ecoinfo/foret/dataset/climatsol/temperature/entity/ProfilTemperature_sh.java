package org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProfilTemperature_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ZET_ID, TableConstantes.COLUMN_DATE, TableConstantes.COLUMN_HEURE}),
        indexes = {
            @Index(name = "ptsh_zet_idx", columnList = TableConstantes.COLUMN_ZET_ID),
            @Index(name = "ptsh_ivf_idx", columnList = VersionFile.ID_JPA)
        })
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ProfilTemperature_sh.ID_JPA))
public class ProfilTemperature_sh extends ProfilClimatSol<MesureTemperature_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.PTPSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.PROFIL_Ts_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    @Column(name = TableConstantes.COLUMN_HEURE, nullable = false)
    private LocalTime heure;

    /**
     *
     */
    public ProfilTemperature_sh() {
        super();
    }

    /**
     * @param version
     * @param date
     * @param heure
     * @param noLigne
     */
    public ProfilTemperature_sh(VersionFile version, LocalDate date, LocalTime heure, Long noLigne) {
        super(version, date, noLigne);
        this.setHeure(heure);
    }

    /**
     * @return the heure
     */
    public LocalTime getHeure() {
        return this.heure;
    }

    /**
     * @param heure
     *            the heure to set
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

}
