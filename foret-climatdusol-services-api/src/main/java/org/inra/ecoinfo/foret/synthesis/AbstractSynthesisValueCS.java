/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package org.inra.ecoinfo.foret.synthesis;

import javax.persistence.Entity;
import javax.persistence.Inheritance;
import javax.persistence.InheritanceType;
import org.inra.ecoinfo.synthesis.entity.GenericSynthesisValue;

/**
 *
 * @author ptchernia
 */
@Entity
@Inheritance(strategy = InheritanceType.TABLE_PER_CLASS)
public abstract class AbstractSynthesisValueCS extends GenericSynthesisValue implements ITypeGraphiqueSynthese, IGraphPresenceAbsence {
    
    /**
     *
     */
    protected Integer profondeur;
    /**
     *
     */
    protected Integer nbRepetition;

    /**
     *
     */
    public AbstractSynthesisValueCS() {
        super();
    }

    /**
     *
     * @param profondeur
     * @param nbRepetition
     */
    public AbstractSynthesisValueCS(Integer profondeur, Integer nbRepetition) {
        this.profondeur = profondeur;
        this.nbRepetition = nbRepetition;
    }

    /**
     *
     * @param profondeur
     * @param nbRepetition
     */
    public AbstractSynthesisValueCS(Integer profondeur, Long nbRepetition) {
        this.profondeur = profondeur;
        this.nbRepetition = nbRepetition.intValue();
    }

    /**
     *
     * @return
     */
    public Integer getProfondeur() {
        return this.profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(Integer profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @param nbRepetition
     */
    public void setRepetition(Integer nbRepetition) {
        this.nbRepetition = nbRepetition;
    }

    @Override
    public Facteur getRepetition() {
        Facteur facteur = new Facteur(this.nbRepetition, true);
        return facteur;
    }

    @Override
    public boolean isPresenceAbsence() {

        return true;
    }

    @Override
    public Facteur getOrdonnee() {
        Facteur facteur = new Facteur(this.profondeur, true);
        return facteur;
    }
    
}
