package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity;

import java.time.LocalDate;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProfilProfondeurNappe_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ZET_ID, TableConstantes.COLUMN_DATE}),
        indexes = {
            @Index(name = "pnm_zet_idx", columnList = TableConstantes.COLUMN_ZET_ID),
            @Index(name = "pnm_ivf_idx", columnList = VersionFile.ID_JPA)
        })
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ProfilProfondeurNappe_m.ID_JPA))
public class ProfilProfondeurNappe_m extends ProfilClimatSol<MesureProfondeurNappe_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.PPNM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.PROFIL_GWD_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ProfilProfondeurNappe_m() {
        super();
    }

    /**
     * @param version
     * @param date
     * @param noLigne
     */
    public ProfilProfondeurNappe_m(VersionFile version, LocalDate date, Long noLigne) {
        super(version, date, noLigne);
    }
}
