package org.inra.ecoinfo.foret.synthesis.mtpsh;


import java.time.LocalDate;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import org.inra.ecoinfo.foret.synthesis.AbstractSynthesisValueCS;

/**
 *
 * @author ptcherniati
 */
@Entity(name = "MtpshSynthesisValue")
@Table(name = "MtpshSynthesisValue",
        indexes = {
        @Index(name = "MtpshSynthesisValue_site_variable_idx", columnList = "site,variable")})
public class SynthesisValue extends AbstractSynthesisValueCS {

    private static final long serialVersionUID = 1L;

    /**
     *
     */
    protected static final boolean SEMIHORAIRE = true;

    /**
     *
     */
    public SynthesisValue() {
        super();
    }

    /**
     *
     * @param date
     * @param site
     * @param variable
     * @param value
     * @param profondeur
     * @param nbRepetition
     */
    public SynthesisValue(LocalDate date, String site, String variable, Double value, Integer profondeur, long nbRepetition) {
        super(profondeur, nbRepetition);
        this.date = date.atStartOfDay();
        this.site = site;
        this.variable = variable;
        this.valueFloat = value == null ? null : value.floatValue();
        this.valueString = value.toString();
        isMean = Boolean.FALSE;
    }

    @Override
    public boolean isSemihoraire() {
        return SEMIHORAIRE;
    }

}
