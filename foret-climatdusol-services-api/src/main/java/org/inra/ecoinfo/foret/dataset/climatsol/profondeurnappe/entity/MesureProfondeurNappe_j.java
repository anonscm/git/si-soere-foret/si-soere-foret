package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureProfondeurNappe_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "pnj_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureProfondeurNappe_j.ID_JPA))
public class MesureProfondeurNappe_j extends MesureClimatSol<ProfilProfondeurNappe_j, ValeurProfondeurNappe_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MPNJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_GWD_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureProfondeurNappe_j() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureProfondeurNappe_j(int profondeur) {
        super(profondeur);
    }
}
