package org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureTemperature_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "mtsh_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureTemperature_sh.ID_JPA))
public class MesureTemperature_sh extends MesureClimatSol<ProfilTemperature_sh, ValeurTemperature_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MTPSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_Ts_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureTemperature_sh() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureTemperature_sh(int profondeur) {
        super(profondeur);
    }
}
