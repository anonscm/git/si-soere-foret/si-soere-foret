package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity;

import java.time.LocalDate;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProfilTensionEau_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ZET_ID, TableConstantes.COLUMN_DATE}),
        indexes = {
            @Index(name = "ptej_zet_idx", columnList = TableConstantes.COLUMN_ZET_ID),
            @Index(name = "ptej_ivf_idx", columnList = VersionFile.ID_JPA)
        })
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ProfilTensionEau_j.ID_JPA))
public class ProfilTensionEau_j extends ProfilClimatSol<MesureTensionEau_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.PTEJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.PROFIL_SMP_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ProfilTensionEau_j() {
        super();
    }

    /**
     * @param version
     * @param date
     * @param noLigne
     */
    public ProfilTensionEau_j(VersionFile version, LocalDate date, Long noLigne) {
        super(version, date, noLigne);
    }
}
