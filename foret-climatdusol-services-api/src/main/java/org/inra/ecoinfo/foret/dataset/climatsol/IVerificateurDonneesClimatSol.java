/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol;

import org.inra.ecoinfo.foret.dataset.ITestValues;

/**
 * @author sophie
 * 
 */
public interface IVerificateurDonneesClimatSol extends ITestValues {

}
