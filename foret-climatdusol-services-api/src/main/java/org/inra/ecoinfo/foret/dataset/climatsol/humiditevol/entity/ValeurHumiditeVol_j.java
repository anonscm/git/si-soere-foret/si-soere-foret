package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 */
@Entity
@Table(name = ValeurHumiditeVol_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_MESURE, TableConstantes.COLUMN_NBRE_REPETITION, VariableForet.ID_JPA}), indexes = {
    @Index(name = "mhvj_vhvj_index", columnList = TableConstantes.COLUMN_ID_MESURE),
    @Index(name="vhvj_var_idx", columnList = VariableForet.ID_JPA)
})
@AttributeOverrides({
        @AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ValeurHumiditeVol_j.ID_JPA)),
        @AttributeOverride(name = TableConstantes.COLUMN_NO_REPETITION, column = @Column(name = TableConstantes.COLUMN_NBRE_REPETITION))
})
public class ValeurHumiditeVol_j extends ValeurClimatSol<MesureHumiditeVol_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.VHVJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.VALEUR_SWC_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;
    /**
     *
     */
    public ValeurHumiditeVol_j() {
        super();
    }

    /**
     * @param realNode
     * @param noRepetition
     * @param valeur
     */
    public ValeurHumiditeVol_j(RealNode realNode, Long noRepetition, Float valeur) {
        super(realNode, valeur, noRepetition);
    }

}
