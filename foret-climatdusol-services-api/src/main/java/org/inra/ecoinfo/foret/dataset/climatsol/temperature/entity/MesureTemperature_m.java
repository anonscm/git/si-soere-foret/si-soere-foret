package org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureTemperature_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "mtm_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureTemperature_m.ID_JPA))
public class MesureTemperature_m extends MesureClimatSol<ProfilTemperature_m, ValeurTemperature_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MTPM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_Ts_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureTemperature_m() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureTemperature_m(int profondeur) {
        super(profondeur);
    }
}
