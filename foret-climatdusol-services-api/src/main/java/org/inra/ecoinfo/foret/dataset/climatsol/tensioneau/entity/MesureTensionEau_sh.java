package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureTensionEau_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "mtesh_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureTensionEau_sh.ID_JPA))
public class MesureTensionEau_sh extends MesureClimatSol<ProfilTensionEau_sh, ValeurTensionEau_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MTESH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_SMP_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureTensionEau_sh() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureTensionEau_sh(int profondeur) {
        super(profondeur);
    }
    
}
