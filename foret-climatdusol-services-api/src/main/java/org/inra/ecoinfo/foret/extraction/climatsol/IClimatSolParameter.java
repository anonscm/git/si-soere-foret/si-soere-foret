package org.inra.ecoinfo.foret.extraction.climatsol;

import java.io.File;
import java.util.List;
import java.util.Map;
import java.util.SortedSet;
import org.inra.ecoinfo.extraction.IParameter;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.utils.IntervalDate;

/**
 *
 * @author ptcherniati
 */
public interface IClimatSolParameter extends IParameter {

    /**
     *
     * @return
     */
    int getAffichage();

    @Override
    String getCommentaire();

    /**
     *
     * @return
     */
    IntervalDate getIntervalDate();

    /**
     *
     * @return
     */
    Map<String, File> getFilesMap();

    /**
     *
     * @return
     */
    String getRythme();

    /**
     *
     * @return
     */
    Map<String,List<Object[]>> getValeursExtraites();

    /**
     *
     * @return
     */
    SortedSet<Integer> getSelectedsDepths();

    /**
     *
     * @return
     */
    List<SiteForet> getSelectedSites();

    /**
     *
     * @return
     */
    List<DatatypeVariableUniteForet> getSelectedVariables();

    /**
     *
     * @param affichage
     */
    void setAffichage(int affichage);

}
