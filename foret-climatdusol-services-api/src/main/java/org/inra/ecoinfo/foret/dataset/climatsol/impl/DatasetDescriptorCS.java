package org.inra.ecoinfo.foret.dataset.climatsol.impl;

import org.inra.ecoinfo.utils.Column;
import org.inra.ecoinfo.utils.DatasetDescriptor;


/**
 * @author sophie
 * 
 */
public class DatasetDescriptorCS extends DatasetDescriptor {

    private int enTete;
    private String frequence;
    private String variableName;

    /**
     * @param nom
     * @return
     */
    public Column getColumn(String nom) {
        for (Column column : this.getColumns()) {
            if (column.getName().equals(nom)) {
                return column;
            }
        }
        return null;
    }

    /**
     * @return enTete
     */
    public int getEnTete() {
        return this.enTete;
    }

    /**
     * @param enTete
     *            the enTete to set
     */
    public void setEnTete(int enTete) {
        this.enTete = enTete;
    }

    /**
     * @param enTete
     *            the enTete to set
     */
    public void setEnTete(String enTete) {
        this.enTete = Integer.parseInt(enTete);
    }

    /**
     * @return frequence
     */
    public String getFrequence() {
        return this.frequence;
    }

    /**
     * @param frequence
     *            the frequence to set
     */
    public void setFrequence(String frequence) {
        this.frequence = frequence;
    }

    /**
     * @return the variableName
     */
    public String getVariableName() {
        return this.variableName;
    }

    /**
     * @param variableName
     *            the variableName to set
     */
    public void setVariableName(String variableName) {
        this.variableName = variableName;
    }

    /**
     * @param i
     * @return index of referenced column
     */
    public int getReferencedColumn(int i) {
        if (i > this.getColumns().size()) {
            return -1;
        }
        String referencedName = this.getColumns().get(i).getRefVariableName();
        if (referencedName != null) {
            return this.getColumns().indexOf(this.getColumn(referencedName));
        }
        return -1;
    }

    /*
     * (non-Javadoc)
     * 
     * @see org.inra.ecoinfo.dataset.DatasetDescriptor#setName(java.lang.String)
     */

    /**
     *
     * @param name
     */

    @Override
    public void setName(String name) {
        super.setName(name);
    }
}
