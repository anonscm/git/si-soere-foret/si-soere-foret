package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureTensionEau_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "mtem_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureTensionEau_m.ID_JPA))
public class MesureTensionEau_m extends MesureClimatSol<ProfilTensionEau_m, ValeurTensionEau_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MTEM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_SMP_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureTensionEau_m() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureTensionEau_m(int profondeur) {
        super(profondeur);
    }
}
