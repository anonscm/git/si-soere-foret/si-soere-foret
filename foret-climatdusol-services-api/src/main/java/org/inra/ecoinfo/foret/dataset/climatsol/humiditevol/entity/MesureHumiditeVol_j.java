package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;


/**
 * @author sophie
 * 
 */
@Entity
@Table(name = MesureHumiditeVol_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "hvj_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureHumiditeVol_j.ID_JPA))
public class MesureHumiditeVol_j extends MesureClimatSol<ProfilHumiditeVol_j, ValeurHumiditeVol_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MHVJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_SWC_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureHumiditeVol_j() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureHumiditeVol_j(int profondeur) {
        super(profondeur);
    }
}
