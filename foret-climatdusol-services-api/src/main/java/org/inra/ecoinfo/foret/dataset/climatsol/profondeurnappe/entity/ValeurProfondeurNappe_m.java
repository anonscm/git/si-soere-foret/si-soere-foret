package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSolMensuel;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurProfondeurNappe_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_MESURE, VariableForet.ID_JPA}), indexes = {
    @Index(name = "mpnm_vpnm_index", columnList = TableConstantes.COLUMN_ID_MESURE),
    @Index(name="vpnm_var_idx", columnList = VariableForet.ID_JPA)
})
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ValeurProfondeurNappe_m.ID_JPA))
public class ValeurProfondeurNappe_m extends ValeurClimatSolMensuel<MesureProfondeurNappe_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.VPNM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.VALEUR_GWD_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    @Column(name = TableConstantes.COLUMN_ECART_TYPE, nullable = false)
    private Float ecartType;

    /**
     *
     */
    public ValeurProfondeurNappe_m() {
        super();
    }

    /**
     * @param realNode
     * @param nbreRepetition
     * @param ecartType
     * @param valeur
     */
    public ValeurProfondeurNappe_m(RealNode realNode, Long nbreRepetition, Float ecartType, Float valeur) {
        super(realNode, valeur,nbreRepetition);
        this.setEcartType(ecartType);
    }

    /**
     * @return the ecartType
     */
    public Float getEcartType() {
        return this.ecartType;
    }

    /**
     * @param ecartType
     *            the ecartType to set
     */
    public void setEcartType(Float ecartType) {
        this.ecartType = ecartType;
    }

}
