package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity;

import java.time.LocalDate;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ProfilFluxChaleur_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ZET_ID, TableConstantes.COLUMN_DATE}),
        indexes = {
            @Index(name = "fcm_zet_idx", columnList = TableConstantes.COLUMN_ZET_ID),
            @Index(name = "fcm_ivf_idx", columnList = VersionFile.ID_JPA)
        })
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ProfilFluxChaleur_m.ID_JPA))
public class ProfilFluxChaleur_m extends ProfilClimatSol<MesureFluxChaleur_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.PFCM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.PROFIL_G_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ProfilFluxChaleur_m() {
        super();
    }

    /**
     * @param version
     * @param date
     * @param noLigne
     */
    public ProfilFluxChaleur_m(VersionFile version, LocalDate date, Long noLigne) {
        super(version, date, noLigne);
    }
}
