package org.inra.ecoinfo.foret.dataset.climatsol.entity;

import java.io.Serializable;
import javax.persistence.MappedSuperclass;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 * @param <M>
 */
@SuppressWarnings("rawtypes")
@MappedSuperclass
public class ValeurClimatSolMensuel<M extends MesureClimatSol> extends ValeurClimatSol<M> implements Serializable {

    /**
     * empty constructor
     */
    public ValeurClimatSolMensuel() {
        super();
    }

    /**
     * Constructor
     * 
     * @param realNode
     * @param valeur
     * @param noRepetition
     */
    public ValeurClimatSolMensuel(RealNode realNode, Float valeur, Long noRepetition) {
        super(realNode, valeur, noRepetition);
    }
}
