package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity;

import java.time.LocalDate;
import java.time.LocalTime;
import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ProfilClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;


/**
 * @author sophie
 * 
 */
@Entity
@Table(name = ProfilHumiditeVol_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ZET_ID, TableConstantes.COLUMN_DATE, TableConstantes.COLUMN_HEURE}),
        indexes = {
            @Index(name = "hvsh_zet_idx", columnList = TableConstantes.COLUMN_ZET_ID),
            @Index(name = "hvsh_ivf_idx", columnList = VersionFile.ID_JPA),
            @Index(name = "hvsh_date_idx", columnList = TableConstantes.COLUMN_DATE)
        })
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ProfilHumiditeVol_sh.ID_JPA))
public class ProfilHumiditeVol_sh extends ProfilClimatSol<MesureHumiditeVol_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.PHVSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.PROFIL_SWC_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    @Column(name = TableConstantes.COLUMN_HEURE, nullable = false)
    private LocalTime heure;

    /**
     *
     */
    public ProfilHumiditeVol_sh() {
        super();
    }

    /**
     * @param versionFile
     * @param date
     * @param heure
     * @param noLigne
     */
    public ProfilHumiditeVol_sh(VersionFile versionFile, LocalDate date, LocalTime heure, Long noLigne) {
        super(versionFile, date, noLigne);
        this.setHeure(heure);
    }

    /**
     * @return the heure
     */
    public LocalTime getHeure() {
        return this.heure;
    }

    /**
     * @param heure
     *            the heure to set
     */
    public void setHeure(LocalTime heure) {
        this.heure = heure;
    }

}
