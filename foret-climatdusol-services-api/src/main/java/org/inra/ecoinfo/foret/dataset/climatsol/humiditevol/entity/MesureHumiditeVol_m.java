package org.inra.ecoinfo.foret.dataset.climatsol.humiditevol.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;


/**
 * @author sophie
 * 
 */
@Entity
@Table(name = MesureHumiditeVol_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "hvm_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureHumiditeVol_m.ID_JPA))
public class MesureHumiditeVol_m extends MesureClimatSol<ProfilHumiditeVol_m, ValeurHumiditeVol_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MHVM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_SWC_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureHumiditeVol_m() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureHumiditeVol_m(int profondeur) {
        super(profondeur);
    }
}
