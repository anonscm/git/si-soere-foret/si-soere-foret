package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurProfondeurNappe_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_MESURE, TableConstantes.COLUMN_NBRE_REPETITION, VariableForet.ID_JPA}), indexes = {
    @Index(name = "mpnsh_vpnsh_index", columnList = TableConstantes.COLUMN_ID_MESURE),
    @Index(name="vpnsh_var_idx", columnList = VariableForet.ID_JPA)
})
@AttributeOverrides({
        @AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ValeurProfondeurNappe_sh.ID_JPA)),
        @AttributeOverride(name = TableConstantes.COLUMN_NO_REPETITION, column = @Column(name = TableConstantes.COLUMN_NBRE_REPETITION))
})
public class ValeurProfondeurNappe_sh extends ValeurClimatSol<MesureProfondeurNappe_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.VPNSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.VALEUR_GWD_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ValeurProfondeurNappe_sh() {
        super();
    }

    /**
     * @param realNode
     * @param variable
     * @param noRepetition
     * @param valeur
     */
    public ValeurProfondeurNappe_sh(RealNode realNode, Long noRepetition, Float valeur) {
        super(realNode, valeur, noRepetition);
    }
}
