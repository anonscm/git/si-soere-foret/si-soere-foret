/**
 *
 */
package org.inra.ecoinfo.foret.dataset.climatsol.entity;

/**
 * @author sophie
 * 
 */
public class TableConstantes {

    /*
     * nom de l'attribut correpondant à la primary key dans les classes ProfilClimatSol,
     * MesureClimatSol, VamleurClimatSol
     */

    /**
     *
     */
    
    public static final String NAME_PK_ID               = "id";

    /* Noms des colonnes pour les tables de Profil */

    /**
     *
     */
    
    public static final String COLUMN_DATE              = "date";

    /**
     *
     */
    public static final String COLUMN_HEURE             = "heure";

    /**
     *
     */
    public static final String COLUMN_NO_LIGNE          = "no_ligne";

    /**
     *
     */
    public static final String COLUMN_ZET_ID            = "zet_id";

    /* Noms des colonnes pour les tables de Mesure */

    /**
     *
     */
    
    public static final String COLUMN_PROFONDEUR        = "profondeur";

    /**
     *
     */
    public static final String COLUMN_ID_PROFIL         = "profil_id";

    /**
     *
     */
    public static final String COLUMN_ID_VALEUR         = "valeur_id";

    /* Noms des colonnes pour les tables de Valeur */

    /**
     *
     */
    
    public static final String COLUMN_NO_REPETITION     = "no_repetition";

    /**
     *
     */
    public static final String COLUMN_VALEUR            = "valeur";

    /**
     *
     */
    public static final String COLUMN_ID_MESURE         = "mesure_id";

    /**
     *
     */
    public static final String COLUMN_NBRE_REPETITION   = "nbre_repetition";

    /**
     *
     */
    public static final String COLUMN_ECART_TYPE        = "ecart_type";

    /**
     *
     */
    public static final String NAME_ATTRIBUT_PROFIL     = "profil";

    /**
     *
     */
    public static final String NAME_ATTRIBUT_MESURE     = "mesure";

    /* Noms des tables */

    /**
     *
     */
    
    public static final String PROFIL_SWC_J_TABLE_NAME  = "profil_humidite_vol_j";

    /**
     *
     */
    public static final String MESURE_SWC_J_TABLE_NAME  = "mesure_humidite_vol_j";

    /**
     *
     */
    public static final String VALEUR_SWC_J_TABLE_NAME  = "valeur_humidite_vol_j";

    /**
     *
     */
    public static final String PROFIL_SWC_SH_TABLE_NAME = "profil_humidite_vol_sh";

    /**
     *
     */
    public static final String MESURE_SWC_SH_TABLE_NAME = "mesure_humidite_vol_sh";

    /**
     *
     */
    public static final String VALEUR_SWC_SH_TABLE_NAME = "valeur_humidite_vol_sh";

    /**
     *
     */
    public static final String PROFIL_SWC_M_TABLE_NAME  = "profil_humidite_vol_m";

    /**
     *
     */
    public static final String MESURE_SWC_M_TABLE_NAME  = "mesure_humidite_vol_m";

    /**
     *
     */
    public static final String VALEUR_SWC_M_TABLE_NAME  = "valeur_humidite_vol_m";

    /**
     *
     */
    public static final String PROFIL_Ts_J_TABLE_NAME   = "profil_temperature_j";

    /**
     *
     */
    public static final String MESURE_Ts_J_TABLE_NAME   = "mesure_temperature_j";

    /**
     *
     */
    public static final String VALEUR_Ts_J_TABLE_NAME   = "valeur_temperature_j";

    /**
     *
     */
    public static final String PROFIL_Ts_SH_TABLE_NAME  = "profil_temperature_sh";

    /**
     *
     */
    public static final String MESURE_Ts_SH_TABLE_NAME  = "mesure_temperature_sh";

    /**
     *
     */
    public static final String VALEUR_Ts_SH_TABLE_NAME  = "valeur_temperature_sh";

    /**
     *
     */
    public static final String PROFIL_Ts_M_TABLE_NAME   = "profil_temperature_m";

    /**
     *
     */
    public static final String MESURE_Ts_M_TABLE_NAME   = "mesure_temperature_m";

    /**
     *
     */
    public static final String VALEUR_Ts_M_TABLE_NAME   = "valeur_temperature_m";

    /**
     *
     */
    public static final String PROFIL_G_J_TABLE_NAME    = "profil_flux_chaleur_j";

    /**
     *
     */
    public static final String MESURE_G_J_TABLE_NAME    = "mesure_flux_chaleur_j";

    /**
     *
     */
    public static final String VALEUR_G_J_TABLE_NAME    = "valeur_flux_chaleur_j";

    /**
     *
     */
    public static final String PROFIL_G_SH_TABLE_NAME   = "profil_flux_chaleur_sh";

    /**
     *
     */
    public static final String MESURE_G_SH_TABLE_NAME   = "mesure_flux_chaleur_sh";

    /**
     *
     */
    public static final String VALEUR_G_SH_TABLE_NAME   = "valeur_flux_chaleur_sh";

    /**
     *
     */
    public static final String PROFIL_G_M_TABLE_NAME    = "profil_flux_chaleur_m";

    /**
     *
     */
    public static final String MESURE_G_M_TABLE_NAME    = "mesure_flux_chaleur_m";

    /**
     *
     */
    public static final String VALEUR_G_M_TABLE_NAME    = "valeur_flux_chaleur_m";

    /**
     *
     */
    public static final String PROFIL_SMP_J_TABLE_NAME  = "profil_tension_eau_j";

    /**
     *
     */
    public static final String MESURE_SMP_J_TABLE_NAME  = "mesure_tension_eau_j";

    /**
     *
     */
    public static final String VALEUR_SMP_J_TABLE_NAME  = "valeur_tension_eau_j";

    /**
     *
     */
    public static final String PROFIL_SMP_SH_TABLE_NAME = "profil_tension_eau_sh";

    /**
     *
     */
    public static final String MESURE_SMP_SH_TABLE_NAME = "mesure_tension_eau_sh";

    /**
     *
     */
    public static final String VALEUR_SMP_SH_TABLE_NAME = "valeur_tension_eau_sh";

    /**
     *
     */
    public static final String PROFIL_SMP_M_TABLE_NAME  = "profil_tension_eau_m";

    /**
     *
     */
    public static final String MESURE_SMP_M_TABLE_NAME  = "mesure_tension_eau_m";

    /**
     *
     */
    public static final String VALEUR_SMP_M_TABLE_NAME  = "valeur_tension_eau_m";

    /**
     *
     */
    public static final String PROFIL_GWD_J_TABLE_NAME  = "profil_profondeur_nappe_j";

    /**
     *
     */
    public static final String MESURE_GWD_J_TABLE_NAME  = "mesure_profondeur_nappe_j";

    /**
     *
     */
    public static final String VALEUR_GWD_J_TABLE_NAME  = "valeur_profondeur_nappe_j";

    /**
     *
     */
    public static final String PROFIL_GWD_SH_TABLE_NAME = "profil_profondeur_nappe_sh";

    /**
     *
     */
    public static final String MESURE_GWD_SH_TABLE_NAME = "mesure_profondeur_nappe_sh";

    /**
     *
     */
    public static final String VALEUR_GWD_SH_TABLE_NAME = "valeur_profondeur_nappe_sh";

    /**
     *
     */
    public static final String PROFIL_GWD_M_TABLE_NAME  = "profil_profondeur_nappe_m";

    /**
     *
     */
    public static final String MESURE_GWD_M_TABLE_NAME  = "mesure_profondeur_nappe_m";

    /**
     *
     */
    public static final String VALEUR_GWD_M_TABLE_NAME  = "valeur_profondeur_nappe_m";

    /**
     *
     */
    public static final String PHVJ_ID                  = "phvj_id";

    /**
     *
     */
    public static final String MHVJ_ID                  = "mhvj_id";

    /**
     *
     */
    public static final String VHVJ_ID                  = "vhvj_id";

    /**
     *
     */
    public static final String PHVSH_ID                 = "phvsh_id";

    /**
     *
     */
    public static final String MHVSH_ID                 = "mhvsh_id";

    /**
     *
     */
    public static final String VHVSH_ID                 = "vhvsh_id";

    /**
     *
     */
    public static final String PHVM_ID                  = "phvm_id";

    /**
     *
     */
    public static final String MHVM_ID                  = "mhvm_id";

    /**
     *
     */
    public static final String VHVM_ID                  = "vhvm_id";

    /**
     *
     */
    public static final String PTPJ_ID                  = "ptpj_id";

    /**
     *
     */
    public static final String MTPJ_ID                  = "mtpj_id";

    /**
     *
     */
    public static final String VTPJ_ID                  = "vtpj_id";

    /**
     *
     */
    public static final String PTPSH_ID                 = "ptpsh_id";

    /**
     *
     */
    public static final String MTPSH_ID                 = "mtpsh_id";

    /**
     *
     */
    public static final String VTPSH_ID                 = "vtpsh_id";

    /**
     *
     */
    public static final String PTPM_ID                  = "ptpm_id";

    /**
     *
     */
    public static final String MTPM_ID                  = "mtpm_id";

    /**
     *
     */
    public static final String VTPM_ID                  = "vtpm_id";

    /**
     *
     */
    public static final String PFCJ_ID                  = "pfcj_id";

    /**
     *
     */
    public static final String MFCJ_ID                  = "mfcj_id";

    /**
     *
     */
    public static final String VFCJ_ID                  = "vfcj_id";

    /**
     *
     */
    public static final String PFCSH_ID                 = "pfcsh_id";

    /**
     *
     */
    public static final String MFCSH_ID                 = "mfcsh_id";

    /**
     *
     */
    public static final String VFCSH_ID                 = "vfcsh_id";

    /**
     *
     */
    public static final String PFCM_ID                  = "pfcm_id";

    /**
     *
     */
    public static final String MFCM_ID                  = "mfcm_id";

    /**
     *
     */
    public static final String VFCM_ID                  = "vfcm_id";

    /**
     *
     */
    public static final String PTEJ_ID                  = "ptej_id";

    /**
     *
     */
    public static final String MTEJ_ID                  = "mtej_id";

    /**
     *
     */
    public static final String VTEJ_ID                  = "vtej_id";

    /**
     *
     */
    public static final String PTESH_ID                 = "ptesh_id";

    /**
     *
     */
    public static final String MTESH_ID                 = "mtesh_id";

    /**
     *
     */
    public static final String VTESH_ID                 = "vtesh_id";

    /**
     *
     */
    public static final String PTEM_ID                  = "ptem_id";

    /**
     *
     */
    public static final String MTEM_ID                  = "mtem_id";

    /**
     *
     */
    public static final String VTEM_ID                  = "vtem_id";

    /**
     *
     */
    public static final String PPNJ_ID                  = "ppnj_id";

    /**
     *
     */
    public static final String MPNJ_ID                  = "mpnj_id";

    /**
     *
     */
    public static final String VPNJ_ID                  = "vpnj_id";

    /**
     *
     */
    public static final String PPNSH_ID                 = "ppnsh_id";

    /**
     *
     */
    public static final String MPNSH_ID                 = "mpnsh_id";

    /**
     *
     */
    public static final String VPNSH_ID                 = "vpnsh_id";

    /**
     *
     */
    public static final String PPNM_ID                  = "ppnm_id";

    /**
     *
     */
    public static final String MPNM_ID                  = "mpnm_id";

    /**
     *
     */
    public static final String VPNM_ID                  = "vpnm_id";

}
