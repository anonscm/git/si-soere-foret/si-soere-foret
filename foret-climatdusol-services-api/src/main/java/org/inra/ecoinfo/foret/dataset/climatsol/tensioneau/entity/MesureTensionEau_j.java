package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureTensionEau_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "mtej_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureTensionEau_j.ID_JPA))
public class MesureTensionEau_j extends MesureClimatSol<ProfilTensionEau_j, ValeurTensionEau_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MTEJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_SMP_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureTensionEau_j() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureTensionEau_j(int profondeur) {
        super(profondeur);
    }
}
