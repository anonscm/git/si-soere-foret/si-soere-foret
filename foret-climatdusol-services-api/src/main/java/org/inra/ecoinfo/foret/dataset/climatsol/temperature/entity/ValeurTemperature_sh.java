package org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurTemperature_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_MESURE, TableConstantes.COLUMN_NBRE_REPETITION, VariableForet.ID_JPA}), indexes = {
    @Index(name = "mtsh_vtsh_index", columnList = TableConstantes.COLUMN_ID_MESURE),
    @Index(name="vtsh_var_idx", columnList = VariableForet.ID_JPA)
})
@AttributeOverrides({
        @AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ValeurTemperature_sh.ID_JPA)),
        @AttributeOverride(name = TableConstantes.COLUMN_NO_REPETITION, column = @Column(name = TableConstantes.COLUMN_NBRE_REPETITION))
})
public class ValeurTemperature_sh extends ValeurClimatSol<MesureTemperature_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.VTPSH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.VALEUR_Ts_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ValeurTemperature_sh() {
        super();
    }

    /**
     * <ProfilTemperature_sh, ValeurTemperature_sh>
     * 
     * @param realNode
     * @param noRepetition
     * @param valeur
     */
    public ValeurTemperature_sh(RealNode realNode, Long noRepetition, Float valeur) {
        super(realNode, valeur, noRepetition);
    }
}
