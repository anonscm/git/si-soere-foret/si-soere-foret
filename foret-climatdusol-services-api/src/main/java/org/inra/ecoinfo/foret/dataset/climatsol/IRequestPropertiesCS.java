package org.inra.ecoinfo.foret.dataset.climatsol;

import java.util.LinkedList;
import java.util.Map;
import org.inra.ecoinfo.foret.dataset.IRequestProperties;
import org.inra.ecoinfo.foret.dataset.IRequestPropertiesIntervalValue;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.ExpectedColumn;
import org.inra.ecoinfo.foret.dataset.climatsol.impl.Line;


/**
 * @author sophie
 * 
 */
public interface IRequestPropertiesCS extends IRequestProperties, IRequestPropertiesIntervalValue {

    /**
     *
     * @return
     */
    Map<String, ExpectedColumn> getPossibleColumns();

    /**
     *
     * @param index
     * @param name
     * @param profondeur
     * @param numeroRepetition
     * @param minValue
     * @param maxValue
     */
    void addExpectedColumn(int index, String name, int profondeur, int numeroRepetition, Float minValue, Float maxValue);

    /**
     *
     * @param index
     * @param name
     * @param profondeur
     * @param hasEcartType
     * @param hasNombreRepetition
     * @param minValue
     * @param maxValue
     */
    void addExpectedColumn(int index, String name, int profondeur, Boolean hasEcartType, Boolean hasNombreRepetition, Float minValue, Float maxValue);

    /**
     *
     * @return
     */
    LinkedList<Line> getLstLines();

    /**
     *
     * @param lstLines
     */
    void setLstLines(LinkedList<Line> lstLines);

    /**
     *
     * @param profondeurMax
     */
    void setProfondeurMax(int profondeurMax);

    /**
     *
     * @return
     */
    int getProfondeurMax();

    /**
     *
     * @param noRepetitionMax
     */
    void setRepetitionMax(int noRepetitionMax);

    /**
     *
     * @return
     */
    int getRepetitionMax();
}
