package org.inra.ecoinfo.foret.dataset.climatsol.profondeurnappe.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureProfondeurNappe_m.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "pnm_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureProfondeurNappe_m.ID_JPA))
public class MesureProfondeurNappe_m extends MesureClimatSol<ProfilProfondeurNappe_m, ValeurProfondeurNappe_m> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MPNM_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_GWD_M_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureProfondeurNappe_m() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureProfondeurNappe_m(int profondeur) {
        super(profondeur);
    }
}
