package org.inra.ecoinfo.foret.dataset.climatsol.tensioneau.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.AttributeOverrides;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.ValeurClimatSol;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = ValeurTensionEau_sh.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_MESURE, TableConstantes.COLUMN_NBRE_REPETITION, VariableForet.ID_JPA}), indexes = {
    @Index(name = "mtesh_vtesh_index", columnList = TableConstantes.COLUMN_ID_MESURE),
    @Index(name="vtesh_var_idx", columnList = VariableForet.ID_JPA)
})
@AttributeOverrides({
        @AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = ValeurTensionEau_sh.ID_JPA)),
        @AttributeOverride(name = TableConstantes.COLUMN_NO_REPETITION, column = @Column(name = TableConstantes.COLUMN_NBRE_REPETITION))
})
public class ValeurTensionEau_sh extends ValeurClimatSol<MesureTensionEau_sh> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.VTESH_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.VALEUR_SMP_SH_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public ValeurTensionEau_sh() {
        super();
    }

    /**
     * @param realNode
     * @param noRepetition
     * @param valeur
     */
    public ValeurTensionEau_sh(RealNode realNode, Long noRepetition, Float valeur) {
        super(realNode, valeur, noRepetition);
    }
}
