package org.inra.ecoinfo.foret.synthesis;

/**
 *
 * @author ptcherniati
 */
public class ClimatSolMetaSynthesisDatatype extends ForetMetaSynthesisDatatype {

    /**
     *
     */
    protected Integer profondeur;

    /**
     *
     */
    protected Integer repetition;

    /**
     *
     * @return
     */
    public Integer getProfondeur() {
        return this.profondeur;
    }

    /**
     *
     * @param profondeur
     */
    public void setProfondeur(final Integer profondeur) {
        this.profondeur = profondeur;
    }

    /**
     *
     * @return
     */
    public Integer getRepetition() {
        return this.repetition;
    }

    /**
     *
     * @param repetition
     */
    public void setRepetition(final Integer repetition) {
        this.repetition = repetition;
    }

}
