package org.inra.ecoinfo.foret.dataset.climatsol.entity;

import java.io.Serializable;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.Transient;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.foret.refdata.datatypevariableunite.DatatypeVariableUniteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.composite.RealNode;


/**
 * @author sophie
 * 
 * @param <M>
 */
@SuppressWarnings("rawtypes")
@MappedSuperclass
public class ValeurClimatSol<M extends MesureClimatSol> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = RealNode.class)
    @JoinColumn(name = RealNode.ID_JPA, referencedColumnName = RealNode.ID_JPA, nullable = false)
    private RealNode realNode;

    @Column(name = TableConstantes.COLUMN_VALEUR, nullable = false)
    private Float valeur;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false)
    @JoinColumn(name = TableConstantes.COLUMN_ID_MESURE)
    @LazyToOne(LazyToOneOption.PROXY)
    private M mesure;
    

    @Column(name = TableConstantes.COLUMN_NBRE_REPETITION, nullable = false)
    private Long nbreRepetition;

    /**
     * empty constructor
     */
    public ValeurClimatSol() {
        super();
    }

    /**
     * Constructor
     * 
     * @param realNode
     * @param valeur
     * @param noRepetition
     */
    public ValeurClimatSol(RealNode realNode, Float valeur, Long noRepetition) {
        super();
        this.realNode = realNode;
        this.valeur=valeur;
        this.nbreRepetition = noRepetition;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the mesure
     */
    public M getMesure() {
        return this.mesure;
    }

    /**
     * @param mesure
     *            the mesure to set
     */
    public void setMesure(M mesure) {
        this.mesure = mesure;
    }

    /**
     * @return the valeur
     */
    public Float getValeur() {
        return this.valeur;
    }

    /**
     * @param valeur
     *            the valeur to set
     */
    public void setValeur(Float valeur) {
        this.valeur = valeur;
    }

    /**
     * @return the realNode
     */
    public RealNode getRealNode() {
        return this.realNode;
    }

    /**
     * @param realNode
     *            the realNode to set
     */
    public void setRealNode(RealNode realNode) {
        this.realNode = realNode;
    }

    /**
     *
     * @return
     */
    @Transient
    public VariableForet getVariable() {
        return (VariableForet) ((DatatypeVariableUniteForet) getRealNode().getNodeable()).getVariable();
    }

    /**
     *
     * @return
     */
    public Long getNbreRepetition() {
        return nbreRepetition;
    }

    /**
     *
     * @param nbreRepetition
     */
    public void setNbreRepetition(Long nbreRepetition) {
        this.nbreRepetition = nbreRepetition;
    }
}
