package org.inra.ecoinfo.foret.extraction.climatsol;

import java.util.List;
import org.inra.ecoinfo.foret.extraction.IForetDatasetManager;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.refdata.variable.VariableForet;
import org.inra.ecoinfo.mga.business.IUser;
import org.inra.ecoinfo.refdata.variable.Variable;
import org.inra.ecoinfo.utils.IntervalDate;
import org.inra.ecoinfo.utils.exceptions.PersistenceException;

/**
 *
 * @author ptcherniati
 */
public interface ICSDatasetManager extends IForetDatasetManager{

    /**
     *
     * @param lstSelectedSiteForets
     * @param lstSelectedVariables
     * @param interval
     * @param frequence
     * @return
     */
    List<Integer> getAvailablesDepths(List<SiteForet> lstSelectedSiteForets, List<Variable> lstSelectedVariables, IntervalDate interval, String frequence);

    /**
     *
     * @param lstSelectedSiteForets
     * @param interval
     * @param frequence
     * @return
     * @throws PersistenceException
     */
    List<VariableForet> getAvailablesVariablesForCurrentUser(List<SiteForet> lstSelectedSiteForets, IntervalDate interval, String frequence) throws PersistenceException;

    /**
     *
     * @return
     */
    boolean getHasRight();

    /**
     *
     * @param frequence
     * @param user
     * @return
     */
    List<VariableForet> getVariablesFromDatatypeForUser(String frequence, IUser user);
}
