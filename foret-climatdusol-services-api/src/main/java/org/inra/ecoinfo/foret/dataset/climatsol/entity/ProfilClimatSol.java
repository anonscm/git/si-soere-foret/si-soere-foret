package org.inra.ecoinfo.foret.dataset.climatsol.entity;

import java.io.Serializable;
import java.time.LocalDate;
import java.util.LinkedList;
import java.util.List;
import static javax.persistence.CascadeType.MERGE;
import static javax.persistence.CascadeType.PERSIST;
import static javax.persistence.CascadeType.REFRESH;
import javax.persistence.Column;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.ManyToOne;
import javax.persistence.MappedSuperclass;
import javax.persistence.OneToMany;
import org.hibernate.annotations.LazyToOne;
import org.hibernate.annotations.LazyToOneOption;
import org.inra.ecoinfo.dataset.versioning.entity.VersionFile;
import org.inra.ecoinfo.foret.refdata.site.SiteForet;
import org.inra.ecoinfo.foret.utils.Outils;


/**
 * @author sophie
 * 
 * @param <M0>
 */
@MappedSuperclass
public class ProfilClimatSol<M0 extends MesureClimatSol<?, ?>> implements Serializable {

    private static final long serialVersionUID = 1L;

    @Id
    @GeneratedValue(strategy = GenerationType.SEQUENCE)
    private Long id;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = VersionFile.class)
    @JoinColumn(name = VersionFile.ID_JPA, referencedColumnName = VersionFile.ID_JPA, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private VersionFile versionFile;

    @ManyToOne(cascade = {PERSIST, MERGE, REFRESH}, optional = false, targetEntity = SiteForet.class)
    @JoinColumn(name = TableConstantes.COLUMN_ZET_ID, referencedColumnName = SiteForet.RECURENT_NAME_ID, nullable = false)
    @LazyToOne(LazyToOneOption.PROXY)
    private SiteForet zoneEtude;

    @Column(name = TableConstantes.COLUMN_DATE, nullable = false)
    private LocalDate date;

    @Column(nullable = false, name = TableConstantes.COLUMN_NO_LIGNE, length = 20)
    private Long noLigne;

    @OneToMany(mappedBy = TableConstantes.NAME_ATTRIBUT_PROFIL, cascade = {PERSIST, MERGE, REFRESH})
    private List<M0> lstMesures = new LinkedList<>();

    /**
     * empty constructor
     */
    public ProfilClimatSol() {
        super();
    }

    /**
     * Constructor
     * 
     * @param versionFile
     * @param date
     * @param noLigne
     */
    public ProfilClimatSol(VersionFile versionFile, LocalDate date, Long noLigne) {
        super();
        this.setVersionFile(versionFile);
        this.setZoneEtude(Outils.chercherNodeable(versionFile, SiteForet.class).orElse(null));
        this.setDate(date);
        this.setNoLigne(noLigne);
    }

    /**
     * @return the date
     */
    public LocalDate getDate() {
        return this.date;
    }

    /**
     * @param date
     *            the date to set
     */
    public void setDate(LocalDate date) {
        this.date = date;
    }

    /**
     * @return the id
     */
    public Long getId() {
        return this.id;
    }

    /**
     * @param id
     *            the id to set
     */
    public void setId(Long id) {
        this.id = id;
    }

    /**
     * @return the lstMesures
     */
    public List<M0> getLstMesures() {
        return this.lstMesures;
    }

    /**
     * @param lstMesures
     *            the lstMesures to set
     */
    public void setLstMesures(List<M0> lstMesures) {
        this.lstMesures = lstMesures;
    }

    /**
     * @return the noLigne
     */
    public Long getNoLigne() {
        return this.noLigne;
    }

    /**
     * @param noLigne
     *            the noLigne to set
     */
    public void setNoLigne(Long noLigne) {
        this.noLigne = noLigne;
    }

    /**
     * @return the versionFile
     */
    public VersionFile getVersionFile() {
        return this.versionFile;
    }

    /**
     * @param versionFile
     *            the versionFile to set
     */
    public void setVersionFile(VersionFile versionFile) {
        this.versionFile = versionFile;
    }

    /**
     * @return the zoneEtude
     */
    public SiteForet getZoneEtude() {
        return this.zoneEtude;
    }

    /**
     * @param zoneEtude
     *            the zoneEtude to set
     */
    public void setZoneEtude(SiteForet zoneEtude) {
        this.zoneEtude = zoneEtude;
    }
}
