package org.inra.ecoinfo.foret.dataset.climatsol.temperature.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureTemperature_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "mtj_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureTemperature_j.ID_JPA))
public class MesureTemperature_j extends MesureClimatSol<ProfilTemperature_j, ValeurTemperature_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MTPJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_Ts_J_TABLE_NAME;
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureTemperature_j() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureTemperature_j(int profondeur) {
        super(profondeur);
    }
}
