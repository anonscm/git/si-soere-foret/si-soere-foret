package org.inra.ecoinfo.foret.dataset.climatsol.fluxchaleur.entity;

import javax.persistence.AttributeOverride;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Index;
import javax.persistence.Table;
import javax.persistence.UniqueConstraint;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.MesureClimatSol;
import org.inra.ecoinfo.foret.dataset.climatsol.entity.TableConstantes;

/**
 *
 * @author ptcherniati
 */
@Entity
@Table(name = MesureFluxChaleur_j.TABLE_NAME, 
        uniqueConstraints = @UniqueConstraint(columnNames = {TableConstantes.COLUMN_ID_PROFIL, TableConstantes.COLUMN_PROFONDEUR}),
        indexes = {
        @Index(name = "fcj_pte", columnList = TableConstantes.COLUMN_ID_PROFIL)}
)
@AttributeOverride(name = TableConstantes.NAME_PK_ID, column = @Column(name = MesureFluxChaleur_j.ID_JPA))
public class MesureFluxChaleur_j extends MesureClimatSol<ProfilFluxChaleur_j, ValeurFluxChaleur_j> {

    /**
     *
     */
    public static final String ID_JPA = TableConstantes.MFCJ_ID;

    /**
     *
     */
    public static final String TABLE_NAME = TableConstantes.MESURE_G_J_TABLE_NAME; 
    private static final long serialVersionUID = 1L;

    /**
     *
     */
    public MesureFluxChaleur_j() {
        super();
    }

    /**
     *
     * @param profondeur
     */
    public MesureFluxChaleur_j(int profondeur) {
        super(profondeur);
    }
}
